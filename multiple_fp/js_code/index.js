// server functions
var express = require('express')
var app = express();
var server = require('http').createServer(app);
var io = require('socket.io')(server);
var path = require('path')
var formidable = require('formidable');
var fs = require('fs');
var PythonShell = require('python-shell');
var ncp = require('ncp').ncp;
var shell = require('shelljs');

server.listen(8080);

var parent_dir = __dirname + '/..'
// console.log(parent_dir);
// console.log(__dirname);
app.use( express.static( parent_dir+"/files/html_input/" ) );

const filesArray = new Array();
var htmlInputPath = parent_dir+'/files/html_input/';
//-----main input path to the whole program is defined in 'inputImagesPath'
var inputImagesPath = parent_dir+'/files/uploaded_input/serious_test/';
// var inputImagesPath = parent_dir+'/files/uploaded_input/another_test/';
var preprocessPath = parent_dir+'/files/uploaded_input/pre_processed/';

create_folders();
//---create folders and pre-process
function create_folders(){
    var pyshell = new PythonShell('../python/0_create_folders/Model/MainLogic.py');
    var MainProgramPath = String(parent_dir)+'/python/0_create_folders/';
    var python_results_path = parent_dir+'/files/outputs/';
    console.log(python_results_path);

    pyshell.send(MainProgramPath);
    pyshell.send(inputImagesPath);
    pyshell.send(python_results_path);
    pyshell.send(preprocessPath);
    pyshell.send(htmlInputPath);
        
    pyshell.on('message', function (message) {
        console.log(message);
    });

    pyshell.end(function (err) {
        if (err) {
            throw err;
        }
        else{
            console.log('Folders Created');
            //get list of input files from input files folder
            //the filesArray population will take some time. But by the time user enters'/' hopefully it'll be done

            fs.readdir(inputImagesPath, function(err, items) {
                for (var i=0; i<items.length; i++) {
                    // console.log(String(items[i]).slice(0,-4));
                    filesArray.push(String(items[i]));
                    console.log(String(items[i]));
                }
            });
        }
        
    });
}
     



var count_index = 0;


// when user types only port number: 
app.get('/', function (req, res) {
    return res.sendFile(path.join(__dirname+'/image_display.html'));
    res.end();
});


var currentResultsPath;
var imageName;
//connect to socket
io.on('connection', function (socket) {
    socket.on('start',function (){
        console.log('-------------- '+filesArray[count_index]);
        socket.emit('GetText', { 'ImageName' : filesArray[count_index]});
        imageName = String(filesArray[count_index]);
        imageName_only = imageName.slice(0,-4);
        currentResultsPath = parent_dir+'/files/outputs/'+imageName_only+'/';
        // copy input image for text detect folder
        oldImagePath = preprocessPath+imageName_only+'_visible.svg';
        newImagePath = currentResultsPath+'1_text/1_images/input/'+imageName_only+'.svg';
        fs.createReadStream(oldImagePath).pipe(fs.createWriteStream(newImagePath));
    });


    // if 'GetRectData' is triggered
    socket.on('DetectText', function (data) {
        var PathLengthsData = data['PathLengths'];
        // get bb rect data passed as 'Rects' in data object
        var TextTagDataArray = data['TextTags'];
        writeBBsAttribsToFile(data['AllElements'],currentResultsPath+'8_JS_results/');
        
        // P1- if there are text tags
        if (TextTagDataArray.length > 0){
            textTagsFlag = true;
            console.log('TExt Tags Exist');
            // 1. extract data about rects and put in to an array
            var elementList = extractRectData(TextTagDataArray);
            // 2. write to file
            var countWords_detected = writeTextToFile(elementList,currentResultsPath,'8_JS_results/',imageName_only);
            console.log(countWords_detected);
            // console.log('length',countWords_detected);
            if (countWords_detected > 0) {
                // 3. call python program to erase selected elements
                // 3.1 extract id data
                var idArray = new Array();
                for (var i = 0; i< elementList.length; ++i) {
                    var id = elementList[i]['ID'];
                    idArray.push(id);
                }
                // 3.2 call python program to erase detected elements
                // Later make pyton read id array from elementList written to text file
                patternFindEraseElements('T',socket);
            }
            else {
                console.log('Empty Text Tags');
                // 1------Detecting Text with python ----------
                findElementsPython('T',socket);
            }
        }
        
        // P2- if there are no text tags
        else{
            console.log('No Text Tags');
            // 1------Detecting Text with python ----------
            findElementsPython('T',socket);
        }

        //write path length to file
        if (PathLengthsData.length > 0){
            writePathToFile(PathLengthsData, currentResultsPath,imageName_only)
        }
    });

    // socket.on('FindStairsPatterns', function (data) {
    //     // console.log(data);
    //     // 4------Find Stairs patterns and erase with python ----------
    //     patternFindEraseElements('S',socket);
    // });

    // socket.on('FindobjectPatterns', function (data) {
    //     // console.log(data);
    //     // 14------Find Objects patterns and erase with python ----------
    //     patternFindEraseElements('O',socket);
    // });

});


function extractRectData(rectDataArray){
    var elementList = new Array();
    //get each element in rectDataArray
    for (var i = 0; i< rectDataArray.length; ++i) {
        // elements in rectDataArray are seperated by ','
        var elementDataList = rectDataArray[i].split(',');
        var id = elementDataList[0];
        var textName = elementDataList[1];
        // bb elements position data = 'top:60'
        var top = elementDataList[2].split(':')
        var left = elementDataList[3].split(':')
        var right = elementDataList[4].split(':')
        var bottom = elementDataList[5].split(':')
        // add bb data as an object
        var bbDataObjects = {
            'ID':id,
            'Name':textName,
            'Left_T':[parseInt(left[1]),parseInt(top[1])],
            'Right_B': [parseInt(right[1]),parseInt(bottom[1])],
        }
        //add bbbox data to array
        elementList.push(bbDataObjects);    
    }
    return elementList
}



function writeTextToFile(elementList,currentResultsPath,JS_path,imageName_only){
    var countWords_detected = 0
    for (var i = 0; i< elementList.length; ++i) {
        var name = elementList[i]['Name'].toLowerCase();
        if (name.replace(/\s/g, '').length) {
            countWords_detected ++;
        }
    
    } 

    if  (countWords_detected > 0){
        // create svg element text file and python usable text file
        var svg_data_path = currentResultsPath+JS_path+'TextTagData.txt';
        var svg_data_file = fs.createWriteStream(svg_data_path);
        var python_foler_path = currentResultsPath+'1_text/3_detection_files/6_text_details.txt';
        var python_text_file = fs.createWriteStream(python_foler_path);
        var results_folder_path = currentResultsPath+'../results_collected/'+imageName_only+'_6_text_details.txt';
        var results_text_file = fs.createWriteStream(results_folder_path);

        // var countWords_detected = 0
        for (var i = 0; i< elementList.length; ++i) {
            var name = elementList[i]['Name'].toLowerCase();
            // console.log(name,name.replace(/\s/g, '').length);
            if (name.replace(/\s/g, '').length > 0) {
                // console.log(name);
                // console.log('entered with ',name,countWords_detected);
                var leftT_list = elementList[i]['Left_T'];
                var rightB_list = elementList[i]['Right_B'];
                // console.log(leftT_list);
                var cx = Math.round((parseInt((leftT_list[0] + rightB_list[0]) / 2))/0.288);
                var cy = Math.round((parseInt((leftT_list[1] + rightB_list[1]) / 2))/0.288);
                var bb_x1 = Math.round(parseInt(leftT_list[0])/0.288);
                    bb_y1 = Math.round(parseInt(leftT_list[1])/0.288);
                    bb_x2 = Math.round(parseInt(rightB_list[0])/0.288);
                    bb_y2 = Math.round(parseInt(rightB_list[1])/0.288);
                var name_string = String(name)+' : ';
                var centrepointdata = '['+String(cx)+','+String(cy)+'] :';
                var bb_data = '['+String(bb_x1)+','+String(bb_y1)+','+String(bb_x2)+','+String(bb_y2)+'] :';
                // 'DM'and confidence is '100' since it's a direct match (no need of dict checking even)
                // 'None,0' since edit distance will also be 0 if matched with a dict
                // var other_values = 'confidence,dict,original_word,difference : [100,DM,None,0]';
                var other_values = '100'
                python_text_file.write(name_string+centrepointdata+bb_data+other_values+'\n');
                results_text_file.write(name_string+centrepointdata+bb_data+other_values+'\n');
                
                var id = elementList[i]['ID'];
                svg_data_file.write(String(id)+' : '+name+'\n');

                // countWords_detected ++;
            } 
        } 
        python_text_file.end(); 
        svg_data_file.end();
        results_text_file.end();
    }
    
    return countWords_detected

}

function writeBBsAttribsToFile(elementData,imagepath){
    var file = fs.createWriteStream(imagepath+'AllElementData.txt');
    console.log('No of Ele is: '+elementData.length) 
    for (var i = 0; i< elementData.length; ++i) {
        tagName = elementData[i][0];
        attribData = elementData[i][1];
        bbData = elementData[i][2];
        // console.log(tagName+', bbData: '+bbData);
        listforFile = new Array();
        attribStr = '';
        for (var j = 0; j< attribData.length; ++j) {
            attribStr += attribData[j]+' :: ';
        }
        file.write(tagName +' ::: '+ attribStr +' ::: BBData='+ bbData+'\n');
    }
    file.end();  
}

function writePathToFile(PathLengthsData,currentResultsPath,imageName_only){
    filePath1 = currentResultsPath + '8_JS_results/';
    filePath2 = currentResultsPath +'../results_collected/';
    var file1 = fs.createWriteStream(filePath1+imageName_only+'_PathLengthData.txt');
    var file2 = fs.createWriteStream(filePath2+imageName_only+'_PathLengthData.txt');
    for (var i = 0; i< PathLengthsData.length; ++i) {
        tagID = PathLengthsData[i][0];
        length = PathLengthsData[i][1];
        file1.write(tagID + ' ::: ' + length + '\n');
        file2.write(tagID + ' ::: ' + length + '\n');
    }
    file1.end();  
    file2.end();
}


function patternFindEraseElements(elementType,socket){
    // set paths
    if (elementType == 'T'){
        var inputFolder = '1_text';
            outputFolder= '2_roc'
            OutputImageName = '';
    }
   
    // else if (elementType == 'O'){
    //     // in finding objects the input out put folder might change later
    //     var inputFolder = '6_rooms';
    //         outputFolder= '7_objects'
    //         OutputImageName = '';
    // }

    // if (elementType == 'Win' |  elementType == 'T'){
    //     // non-test program
    //     programFolder = '2_detect_patterns'
    // }
    // else{
    //     // test program
    //     programFolder = 'Others/9_pattern_erase/1_findTextpatterns'
    // }

    //working program
    programFolder = '2_detect_patterns'
    // test program
    // programFolder = 'Others/9_pattern_erase/1_findTextpatterns'
    

    // program path
    var pyshell = new PythonShell('../python/'+programFolder+'/Model/MainLogic.py');
    //path to current directory passed over to python progam
    var MainProgramPath = String(parent_dir)+'/python/'+programFolder+'/';
    // path to folder with all input files
    var SystemInputFolderPath = currentResultsPath;
    //path to get uploaded image
    var SystemInputImagePath = currentResultsPath+inputFolder+'/';
    //path to store the SVG created by python program
    var OutputImagePath = currentResultsPath+outputFolder+'/';

    // console.log('1**** '+MainProgramPath);
    // console.log('2**** '+SystemInputFolderPath);
    // console.log('3**** '+SystemInputImagePath);
    // console.log('4**** '+OutputImagePath);
    // console.log('5**** '+elementType);
    // console.log('6**** '+preprocessPath);
    // console.log('7**** '+String(filesArray[count_index]).slice(0,-4));
    
    //pass paths to python program
    pyshell.send(MainProgramPath);
    pyshell.send(SystemInputFolderPath);
    pyshell.send(SystemInputImagePath);
    pyshell.send(OutputImagePath)
    pyshell.send(elementType);
    pyshell.send(preprocessPath);
    pyshell.send(String(filesArray[count_index]).slice(0,-4))
        
    // //executing python program
    pyshell.on('message', function (message) {
        // received a message sent from the Python script (a simple "print" statement)
        console.log(message);
    });

    // end the input stream and allow the process to exit
    pyshell.end(function (err) {
        if (err) {
            throw err;
            console.log('****error with Pattern find for '+elementType+ ' in '+ imageName);
            
            // continue to next image
            count_index += 1;
            if (count_index<filesArray.length){
                // console.log(filesArray);
                console.log('----'+count_index,filesArray[count_index]);
                socket.emit('GetText', { 'ImageName' : filesArray[count_index]});
                imageName = String(filesArray[count_index]);
                imageName_only = imageName.slice(0,-4);
                currentResultsPath = parent_dir+'/files/outputs/'+imageName_only+'/';
                // copy input image for text detect folder
                oldImagePath = preprocessPath+imageName_only+'_visible.svg';
                newImagePath = currentResultsPath+'1_text/1_images/input/'+imageName_only+'.svg';
                fs.createReadStream(oldImagePath).pipe(fs.createWriteStream(newImagePath));
            }
            else{
                console.log('End of Images');
            }
        }
        else{
            console.log('Executed Pattern Python for '+elementType);
            if (elementType == 'T'){
                // console.log('Moving to stair detect');
                findElementsPython('A',socket);
            }
        
            //     count_index += 1;
            //     if (count_index<filesArray.length){
            //         // console.log(filesArray);
            //         // console.log(count_index,filesArray[count_index]);
            //         socket.emit('GetText', { 'ImageName' : filesArray[count_index]});
            //         imageName = String(filesArray[count_index]);
            //         imageName_only = imageName.slice(0,-4);
            //         currentResultsPath = parent_dir+'/files/outputs/'+imageName_only+'/';
            //         oldImagePath = preprocessPath+imageName_only+'_visible.svg';
            //         newImagePath = currentResultsPath+'1_text/1_images/input/'+imageName_only+'.svg';
            //         fs.createReadStream(oldImagePath).pipe(fs.createWriteStream(newImagePath));
            //     }
            //     else{
            //         console.log('End of Images');
            //     }
            // }
            else{
                console.log ('An error in Find Pattern program finding');
            }
        }
        
    });
    
}


function findElementsPython(elementType,socket){
    // set paths
    if (elementType == 'T'){
        var programFolderName = '1_detect_text';
            inputFolder = '1_text';
            outputFolder= '1_text'
            OutputImageName = '';
    }
    else if (elementType == 'A'){
        var programFolderName = '2_detect_roc';
        inputFolder = '2_roc';
        outputFolder= '2_roc'
        OutputImageName = '';
    }
    // else if (elementType == 'O'){
    //     var programFolderName = '7_detect_objects';
    //         inputFolder = '7_objects';
    //         outputFolder= '7_objects'
    //         OutputImageName = '';
    // }

    //testing
    // var programFolderName = 'test_no_function';

    //execute element detection
    // console.log('----------------------------Executing '+programFolderName+' for '+inputFolder);
    // program path
    var pyshell = new PythonShell('../python/1_detect_elements/'+programFolderName+'/Model/MainLogic.py');
    //path to current directory passed over to python progam
    var MainProgramPath = String(parent_dir)+'/python/1_detect_elements/'+programFolderName+'/';
    //path to store image uploaded
    var SystemInputImagePath = currentResultsPath + inputFolder+'/'
    //path to store the SVG created by python program
    var OutputImagePath = currentResultsPath + outputFolder+'/';
    
    //pass paths to python program
    pyshell.send(MainProgramPath);
    pyshell.send(SystemInputImagePath);
    pyshell.send(OutputImagePath);
        
    // executing python program
    pyshell.on('message', function (message) {
        // received a message sent from the Python script (a simple "print" statement)
        console.log(message);
    });

    // end the input stream and allow the process to exit
    pyshell.end(function (err) {
        if (err) {
            throw err;
            console.log('****error with Element find for '+elementType+ ' in '+ imageName);
            
            // continue to next image
            count_index += 1;
            if (count_index<filesArray.length){
                // console.log(filesArray);
                console.log('----'+count_index,filesArray[count_index]);
                socket.emit('GetText', { 'ImageName' : filesArray[count_index]});
                imageName = String(filesArray[count_index]);
                currentResultsPath = parent_dir+'/files/outputs/'+imageName.slice(0,-4)+'/';
                oldImagePath = inputImagesPath+imageName;
                newImagePath = currentResultsPath+'1_text/1_images/input/'+imageName;
                fs.createReadStream(oldImagePath).pipe(fs.createWriteStream(newImagePath));
            }
            else{
                console.log('End of Images');
            }
        }
        else{
            console.log('Executed Raster Python for '+elementType);
            // console.log('---'+imageName);
            // elementImagename = imageName.slice(0,-4) +'_'+ inputFolder.slice(2)+'.svg';
            // console.log('Display image name is '+elementImagename)

            if (elementType == 'T'){
                patternFindEraseElements('T',socket);
            }
            else if (elementType == 'A'){
                // continue to next image
                console.log('Program Execution Completed for '+String(filesArray[count_index]));
                count_index += 1;
                if (count_index<filesArray.length){
                    // console.log(filesArray);
                    console.log('-------------- '+filesArray[count_index]);
                    socket.emit('GetText', { 'ImageName' : filesArray[count_index]});
                    imageName = String(filesArray[count_index]);
                    imageName_only = imageName.slice(0,-4);
                    currentResultsPath = parent_dir+'/files/outputs/'+imageName_only+'/';
                    oldImagePath = preprocessPath+imageName_only+'_visible.svg';
                    newImagePath = currentResultsPath+'1_text/1_images/input/'+imageName_only+'.svg';
                    fs.createReadStream(oldImagePath).pipe(fs.createWriteStream(newImagePath));
                }
                else{
                    console.log('End of Images');
                }
            } 
            // else if (elementType == 'O'){
            //     socket.emit('detectedObjects', elementImagename);
            // } 
            else{
                console.log ('An error in detect element program finding');
            }
        }
        
    });
}