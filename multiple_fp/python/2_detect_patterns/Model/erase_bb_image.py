import os,shutil,cv2
import xml.etree.ElementTree as ET
from lxml import etree
from natsort import natsorted


#---class files
from math_functions import math_functions
from text_detect import text_detect

class bb_image:
    def __init__(self,input_im_path,image_output_path,bb_image_path,current_directory):
        self.input_im_path = input_im_path
        self.image_output_path = image_output_path
        self.bb_image_path = bb_image_path
        self.math_functions_obj = math_functions()
        self.text_detect_obj = text_detect(current_directory)

    def extract_bb_elements(self,bb_element_data,ocr_data):
        # for row in bb_element_data:
        #     print row
        # ocr_data =[
        #     ['1.png', [['pdr', [293, 89], [271, 83, 315, 95], 91, 'match'],
        #                ['ldy', [225, 239], [210, 228, 240, 250], 34, 'match'],
        #                ['greatroom', [410, 237], [328, 230, 493, 245], 82, 'match'],
        #                ['pan', [76, 342], [60, 337, 91, 346], 89, 'match'],
        #                ['dining', [168, 401], [122, 393, 214, 409], 86, 'match'],
        #                ['office', [351, 514], [307, 506, 395, 521], 80, 'match'],
        #                ['O', [146, 139], [141, 135, 150, 144], 90, 'none'],
        #                ['micro', [86, 244], [61, 239, 113, 248], 91, 'none'],
        #                ['rec', [167, 265], [151, 260, 182, 270], 89, 'none'],
        #                ['ref', [79, 290], [65, 283, 94, 296], 78, 'none'],
        #                ['opseich', [360, 397], [313, 386, 408, 409], 57, 'none'],
        #                ['opt', [95, 420], [79, 415, 111, 425], 88, 'none'],
        #                ['cabinets', [95, 433], [59, 429, 131, 438], 88, 'none'],
        #                ['dn', [465, 434], [455, 429, 476, 439], 94, 'none'],
        #                ['up', [522, 448], [513, 443, 531, 453], 89, 'none'],
        #                ['californiaterrace', [151, 569], [97, 553, 206, 586], 76, 'none'],
        #                ['casdsiieet', [400, 583], [368, 572, 433, 595], 41, 'none'],
        #                ['secondfloor', [191, 694], [51, 682, 331, 706], 72, 'none'],
        #                ['O', [670, 165], [666, 160, 675, 169], 86, 'none'],
        #                ["='", [284, 436], [271, 434, 297, 439], 80, 'none'],
        #                ['lla', [297, 522], [256, 518, 338, 527], 78, 'none']]
        #      ]
        #
        # ]
        # bb_element_data = [
        #     ['1.png', [792, 612], [['pdr', ['n_id_996 ', 'n_id_997 ', 'n_id_998 ', 'n_id_999 ']],
        #                            ['ldy', ['n_id_1061 ', 'n_id_1062 ', 'n_id_1063 ']], ['greatroom',
        #                                                                                  ['n_id_959 ', 'n_id_960 ',
        #                                                                                   'n_id_961 ', 'n_id_962 ',
        #                                                                                   'n_id_963 ', 'n_id_964 ',
        #                                                                                   'n_id_965 ', 'n_id_966 ',
        #                                                                                   'n_id_967 ', 'n_id_968 ']],
        #                            ['pan', ['n_id_1010 ', 'n_id_1011 ', 'n_id_1012 ', 'n_id_1013 ']], ['dining',
        #                                                                                                ['n_id_952 ',
        #                                                                                                 'n_id_953 ',
        #                                                                                                 'n_id_954 ',
        #                                                                                                 'n_id_955 ',
        #                                                                                                 'n_id_956 ',
        #                                                                                                 'n_id_957 ',
        #                                                                                                 'n_id_958 ']],
        #                            ['office',
        #                             ['n_id_969 ', 'n_id_970 ', 'n_id_971 ', 'n_id_972 ', 'n_id_973 ', 'n_id_974 ',
        #                              'n_id_975 ']], ['O', ['n_id_589 ']], ['micro',
        #                                                                    ['n_id_1002 ', 'n_id_1003 ', 'n_id_1004 ',
        #                                                                     'n_id_1005 ', 'n_id_1006 ', 'n_id_1007 ']],
        #                            ['rec', ['n_id_1065 ', 'n_id_1066 ', 'n_id_1067 ', 'n_id_1068 ']],
        #                            ['ref', ['n_id_1016 ', 'n_id_1017 ', 'n_id_1018 ', 'n_id_1019 ']], ['opseich',
        #                                                                                                ['n_id_1036 ',
        #                                                                                                 'n_id_1037 ',
        #                                                                                                 'n_id_1038 ',
        #                                                                                                 'n_id_1039 ',
        #                                                                                                 'n_id_1041 ',
        #                                                                                                 'n_id_1042 ',
        #                                                                                                 'n_id_1043 ',
        #                                                                                                 'n_id_1044 ',
        #                                                                                                 'n_id_1045 ',
        #                                                                                                 'n_id_1046 ',
        #                                                                                                 'n_id_1047 ',
        #                                                                                                 'n_id_1048 ',
        #                                                                                                 'n_id_1049 ',
        #                                                                                                 'n_id_1050 ',
        #                                                                                                 'n_id_1051 ',
        #                                                                                                 'n_id_1052 ']],
        #                            ['opt', ['n_id_1022 ', 'n_id_1023 ', 'n_id_1024 ', 'n_id_1025 ']], ['cabinets',
        #                                                                                                ['n_id_1027 ',
        #                                                                                                 'n_id_1028 ',
        #                                                                                                 'n_id_1029 ',
        #                                                                                                 'n_id_1030 ',
        #                                                                                                 'n_id_1031 ',
        #                                                                                                 'n_id_1032 ',
        #                                                                                                 'n_id_1033 ',
        #                                                                                                 'n_id_1034 ',
        #                                                                                                 'n_id_1035 ']],
        #                            ['dn', ['n_id_1058 ', 'n_id_1059 ', 'n_id_1060 ']],
        #                            ['up', ['n_id_1054 ', 'n_id_1055 ', 'n_id_1056 ']], ['californiaterrace',
        #                                                                                 ['n_id_977 ', 'n_id_978 ',
        #                                                                                  'n_id_979 ', 'n_id_980 ',
        #                                                                                  'n_id_981 ', 'n_id_982 ',
        #                                                                                  'n_id_983 ', 'n_id_984 ',
        #                                                                                  'n_id_985 ', 'n_id_987 ',
        #                                                                                  'n_id_988 ', 'n_id_989 ',
        #                                                                                  'n_id_990 ', 'n_id_991 ',
        #                                                                                  'n_id_992 ', 'n_id_993 ',
        #                                                                                  'n_id_994 ']], ['casdsiieet',
        #                                                                                                  ['n_id_1071 ',
        #                                                                                                   'n_id_1072 ',
        #                                                                                                   'n_id_1073 ',
        #                                                                                                   'n_id_1074 ',
        #                                                                                                   'n_id_1076 ',
        #                                                                                                   'n_id_1077 ',
        #                                                                                                   'n_id_1078 ',
        #                                                                                                   'n_id_1079 ',
        #                                                                                                   'n_id_1080 ',
        #                                                                                                   'n_id_1081 ',
        #                                                                                                   'n_id_1082 ',
        #                                                                                                   'n_id_1083 ']],
        #                            ['secondfloor',
        #                             ['n_id_1085 ', 'n_id_1086 ', 'n_id_1087 ', 'n_id_1088 ', 'n_id_1089 ', 'n_id_1090 ',
        #                              'n_id_1091 ', 'n_id_1092 ', 'n_id_1093 ', 'n_id_1094 ', 'n_id_1095 ',
        #                              'n_id_1096 ']], ['O', []], ["='", []], ['lla', []]]]
        # ]



        #---take each image data row
        for i_row_num, image_row in enumerate(bb_element_data):
            image_name = image_row[0][:-4]

            #--create output destination
            parent_folder_path = self.bb_image_path + image_name+'/'
            os.mkdir(parent_folder_path)
            destination_folder_path = parent_folder_path + 'svg_images/'
            os.mkdir(destination_folder_path)

            #-- get input svg image
            svg_file_path = self.input_im_path+image_name+'.svg'
            im_height,im_width = image_row[1]

            #--read input svg file
            tree = etree.parse(svg_file_path)

            #-- get each word
            for b_row_num, bb_data_row in enumerate(image_row[2]):
                # -- get element data from rows
                # text = bb_data_row[0]
                element_id_list = bb_data_row[1]

                #--create output image
                svg_id = image_name+'_'+str(b_row_num)
                svg_file = open(destination_folder_path +svg_id+'.svg', 'w')

                #-- add data to output image
                svg_file.write(
                    '<svg id="'+svg_id+'" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="' + str(
                        im_width) + '" height="' + str(im_height) + '" style="background-color: #FFFFFF;">' + '\n')
                l2_space = '  '

                #---find elements for each id
                for each_id in element_id_list:
                    element_list = tree.xpath('//*[@id="' + each_id.rstrip() + '"]')
                    #--- when a <g><polyline/><polyline/></g> exist,
                    #--- I find as all g,polyline1, polyline2 are inside ocrbb since
                    #--- polyline1 and polyline2 are inside it
                    #--- so if we had a <g> len(element_list) will be > 1
                    #--- as a temp fix I am ignoring such <g> and taking only <polylines>
                    element = element_list[0]
                    if len(element)==0:
                        #--- add all atttributes to attributes_list string
                        attributes_list = ''
                        for a,attrib_name in enumerate(element.attrib):
                            if a ==0:
                                attributes_list = attrib_name+'="'+element.attrib[attrib_name]+'"'
                            else:
                                attributes_list = attributes_list + ' ' + attrib_name + '="' + element.attrib[
                                    attrib_name] +'"'
                        svg_file.write(l2_space+'<'+element.tag.split('}')[1]+' '+str(attributes_list)+'/>'+ '\n')

                svg_file.write('</svg>')
                svg_file.close()



    def perform_ocr(self):
        for image_folder in natsorted(os.listdir(self.bb_image_path)):
            svg_folder_path = self.bb_image_path + image_folder+'/svg_images/'
            png_folder_path = self.bb_image_path + image_folder + '/png_images/'
            os.mkdir(png_folder_path)
            temp_folder_path = self.bb_image_path + image_folder + '/temp/'
            os.mkdir(temp_folder_path)
            #---convert all bb_images to png
            for bb_svg_image in natsorted(os.listdir(svg_folder_path)):
                os.system('convert -density 250 ' + svg_folder_path + str(
                    bb_svg_image) + ' -set density 250 ' + png_folder_path + bb_svg_image[:-4] + '.png')

            #---take each png image and perform ocr
            for bb_image_name in natsorted(os.listdir(png_folder_path)):
                current_image_path = png_folder_path+bb_image_name
                bb_image = cv2.imread(current_image_path,cv2.IMREAD_GRAYSCALE)
                text_details_file_path = self.bb_image_path + str(image_folder) + '_text_details.txt'
                self.text_detect_obj.identify_text('eng',str(bb_image_name[:-4]),current_image_path,
                                                   text_details_file_path,bb_image,temp_folder_path)



