import os,shutil,cv2
from natsort import natsorted
from lxml import etree

#---program files
from find_bb_elements import bb_elements_class


class find_elements_class:
    def find_svg_elements(self,input_path, fp_feature,debug_folder_path):
        if fp_feature == 'T':
            text_tags_flag = self.find_text_tags_exist(input_path)
            if text_tags_flag:
                text_tag_file_path = input_path + '8_JS_results/TextTagData.txt'
                bb_elements = self.find_text_tag_elements(text_tag_file_path)
                return bb_elements, text_tags_flag
            else:
                bb_elements = self.find_svg_using_bb_data(fp_feature,input_path,debug_folder_path)
        else:
            bb_elements = self.find_svg_using_bb_data(fp_feature,input_path,debug_folder_path)

        text_tags_flag = False
        return bb_elements, text_tags_flag

    def find_text_tag_elements(self,text_tag_file_path):
        text_details_file = open(text_tag_file_path, 'r')
        bb_elements = []
        for text_line in text_details_file:
            text_data = text_line.strip().split(':')
            if len(text_data)>1:
                text_id = text_data[0].strip()
                text = text_data[1].strip()
                bb_elements.append([text,[text_id]])
        return bb_elements

    def find_text_tags_exist(self,input_path):
        js_file_path = input_path + '8_JS_results/'
        file_list = os.listdir(js_file_path)
        if len(file_list) ==3:
            text_tags_flag = True
        else:
            text_tags_flag = False
        return text_tags_flag

    def find_svg_using_bb_data(self,fp_feature,input_path,debug_folder_path):
        bb_elements_obj = bb_elements_class()
        # -- since we are using 250 density  its always 0.288 for both x and y
        image_dimensions = [0.288, 0.288]

        #--- create list to find text file name and results folder name
        fp_feature_list = ['T', 'S', 'D', 'Wl', 'Win', 'R', 'O']
        txt_file_name_list = ['6_text', '3_stairs', '5_doors', '7_walls', '4_windows',
                              '1_rooms', '2_objects']
        results_folder_list = ['1_text','2_stairs', '3_doors', '4_walls', '5_windows',
                               '6_rooms', '7_objects']

        fp_feature_index = fp_feature_list.index(fp_feature)
        txt_file_name = txt_file_name_list[fp_feature_index]
        results_folder_name = results_folder_list[fp_feature_index]

        raster_detection_text_file = txt_file_name+'_details.txt'
        # change later to uncomment this when 4_svg_detection folder in text contains a text fiel with text-bb details only
        # raster_detection_text_path = input_path + results_folder_name + '/4_svg_process_files/' + raster_detection_text_file

        #--- special case for reading text detection files
        if fp_feature == 'T':
            raster_detection_text_path = input_path + results_folder_name + '/3_detection_files/' + raster_detection_text_file
            raster_detection_list = bb_elements_obj.get_OCR_data(raster_detection_text_path, image_dimensions)
            # --- read data written to file from js
            svg_element_text_path = input_path + '8_JS_results/AllElementData.txt'
            svg_element_list = bb_elements_obj.process_js_data(svg_element_text_path)
            # --- from svg_element_list find elements inside raster detected BBxes
            bb_elements = bb_elements_obj.find_matchingBB_text(svg_element_list,
                                                          raster_detection_list, debug_folder_path)

        else:
            raster_detection_text_path = input_path + results_folder_name + '/4_svg_process_files/' + '4_svg_window_details.txt'
            raster_detection_list = bb_elements_obj.get_raster_data(raster_detection_text_path, image_dimensions,txt_file_name)

            # --- read data written to file from js
            svg_element_text_path = input_path + '8_JS_results/AllElementData.txt'
            svg_element_list = bb_elements_obj.process_js_data(svg_element_text_path)
            # --- from svg_element_list find elements inside raster detected BBxes
            bb_elements = bb_elements_obj.find_matchingBB(svg_element_list,
                                                          raster_detection_list,debug_folder_path)
            # bb_elements = []

        return bb_elements





