import sys,os,shutil,cv2
from find_main import find_elements_class
# from fPat_TP import text_pattern
# from fPat_SP import stairs_pattern
# from fPat_DP import door_pattern
# from fPat_WP import wall_pattern
from fPat_WinP import window_pattern_class
# from fPat_RP import room_pattern
# from fPat_OP import object_pattern
from erase_main import erase_elements_class
from debug_prog import debug_class

class MainFormLogic():

    def __init__(self):
        # current_directory = '/home/anu/Documents/Bit_Bucket/SVG_Detection/multiple_fp/js_code/../python/2_detect_patterns/'
        # general_folder_path = '/home/anu/Documents/Bit_Bucket/SVG_Detection/multiple_fp/js_code/../files/outputs/41/'
        #
        # # input_folder_path = '/home/anu/Documents/Bit_Bucket/SVG_Detection/multiple_fp/2orMoreimageUploadFiles/../files/outputs/12/5_windows/'
        # # output_folder_path = '/home/anu/Documents/Bit_Bucket/SVG_Detection/multiple_fp/2orMoreimageUploadFiles/../files/outputs/12/6_rooms/'
        # # fp_feature = 'Win'
        #
        # #-----T testing paths
        # input_folder_path = '/home/anu/Documents/Bit_Bucket/SVG_Detection/multiple_fp/js_code/../files/outputs/41/1_text/'
        # output_folder_path = '/home/anu/Documents/Bit_Bucket/SVG_Detection/multiple_fp/js_code/../files/outputs/41/2_roc/'
        # fp_feature = 'T'
        # preprocess_path = '/home/anu/Documents/Bit_Bucket/SVG_Detection/multiple_fp/js_code/../files/uploaded_input/pre_processed/'
        # current_image_name = '41'


        # #---1. get paths from JS
        all_paths = self.getJSargs()
        current_directory, general_folder_path, input_folder_path, output_folder_path, fp_feature, preprocess_path,current_image_name = all_paths
        cleaned_image_path = preprocess_path + current_image_name + '_cleaned.svg'
        visible_image_path = preprocess_path + current_image_name + '_visible.svg'


        debug_folder_path = ''
        #---debug method
        debug_folder_path = input_folder_path+'debug_folder/'
        if os.path.exists(debug_folder_path):
            shutil.rmtree(debug_folder_path)
        os.mkdir(debug_folder_path)
        im_height, im_width = self.convert_svg_png(input_folder_path+'1_images/input/',debug_folder_path)
        debug_obj = debug_class()

        # if fp_feature=='T' or fp_feature=='Win':
        if fp_feature == 'T':
            # --- 2. call find_elements prog
            find_elements_obj = find_elements_class()
            bb_elements,text_tags_flag = find_elements_obj.find_svg_elements(general_folder_path,fp_feature,debug_folder_path)

            ### debug_obj.extract_bb_elements(bb_elements,input_folder_path,debug_folder_path)


            # # #--- 3. call find patterns programs
            # if fp_feature == 'Win':
            #     for win_row in [row[1] for row in bb_elements]:
            #         print win_row
            #     window_patterns_obj = window_pattern_class()
            #     # window_patterns_obj.find_window_pattern_main(bb_elements,
            #     #                     input_folder_path,im_height, im_width)
            #     # window_patterns_obj.search_window_pattern_01(bb_elements,input_folder_path,im_height, im_width)
            #     # bb_elements = window_patterns_obj.search_window_pattern_02(bb_elements, input_folder_path)
            #     bb_elements = window_patterns_obj.search_window_pattern_03(bb_elements, input_folder_path)

            # elif fp_feature=='T':
            #     bb_elements = bb_elements

        else:
            bb_elements = []

           
        #--- 4. call svg element erase and write to text file prog
        erase_elements_obj = erase_elements_class()
        input_im_path = input_folder_path + '1_images/input/'
        output_im_path = output_folder_path + '1_images/input/'


        erase_elements_obj.erase_identified_elements(bb_elements,cleaned_image_path,
                                                     visible_image_path,input_im_path,
                                                     output_im_path,preprocess_path,
                                                     fp_feature,current_image_name)

        #---5. Copy other files
        self.copy_files(input_folder_path,output_folder_path)

        collected_results_path = general_folder_path + '../results_collected/final_lines/'
        source_path = output_folder_path+'/1_images/input/'
        for each_file in os.listdir(source_path):
            if os.path.isfile(source_path + str(each_file)):
                shutil.copy(source_path + str(each_file),collected_results_path)

        if os.path.exists(debug_folder_path):
            shutil.rmtree(debug_folder_path)



    def getJSargs(self):
        js_message = sys.stdin.readlines()
        current_directory,general_folder_path,input_folder_path = '','', ''
        output_folder_path, fp_feature, preprocess_path, current_image_name= '','','',''
        for row_num, each in enumerate(js_message):
            if row_num == 0:
                current_directory = each.rstrip()
            elif row_num == 1:
                general_folder_path = each.rstrip()
            elif row_num == 2:
                input_folder_path = each.rstrip()
            elif row_num == 3:
                output_folder_path = each.rstrip()
            elif row_num == 4:
                fp_feature = each.rstrip()
            elif row_num == 5:
                preprocess_path = each.rstrip()
            else:
                current_image_name = each.rstrip()
        all_paths = [current_directory,general_folder_path,input_folder_path,
                     output_folder_path,fp_feature,preprocess_path,current_image_name]

        return all_paths


    def getlocalargs(self):
        current_directory, general_folder_path, input_folder_path = '', '', ''
        output_folder_path, fp_feature, preprocess_path, current_image_name = '', '', '', ''

        current_directory = self.go_up_python_path(os.getcwd(),1)+'/'
        print current_directory

        #--- read each image
        #--- get each image name
        #--- find output folders realted to each image
        #--- assign them as input_folder_path, output_folder_path

        return current_directory, general_folder_path, input_folder_path, output_folder_path, fp_feature, preprocess_path,current_image_name




    def copy_files(self,input_folder_path,output_folder_path):
        folders = ['/1_images/input/', '/2_state_files/', '/3_detection_files/']

        for each_folder in folders:
            source_path = os.path.join(input_folder_path+each_folder)
            destination_path = os.path.join(output_folder_path+each_folder)
            source_files = os.listdir(source_path)
            for source_file in source_files:
                # ---special case for '1_images/' folder
                # ---since new svg with erased detected elements is already saved
                if each_folder == '/1_images/input/':
                    detection_type = ((output_folder_path.split('/'))[-2])[2:]
                    parent_dir = self.go_up_python_path(output_folder_path,3)
                    destination_path = parent_dir + '/html_input/'+source_file[0:-4]+'_'+detection_type+'.svg'

                    source_path = output_folder_path +each_folder+ str(source_file)
                    shutil.copy(source_path,destination_path)

                else:
                    if os.path.isfile(source_path + str(source_file)):
                        shutil.copy(source_path + str(source_file),
                                    destination_path)
        # if fp_feature!='Win':
            # --- clear content in '8_JS_results/'
        parent_dir = self.go_up_python_path(output_folder_path, 1)
        source_path = os.path.join(parent_dir + '/8_JS_results/')
        for each_file in os.listdir(source_path):
            os.remove(source_path + str(each_file))
            # print 'deleted ', source_path + str(each_file)


    def go_up_python_path(self,path, levels):
        final_path = path
        while levels >0:
            final_path = os.path.abspath(os.path.join(final_path, '..'))
            levels = levels - 1
        return final_path

    def convert_svg_png(self,input_im_path,debug_path):
        im_height, im_width = 0,0
        for image_name in os.listdir(input_im_path):
            # only_image_name = image_name[:-4]
            os.system('convert ' + input_im_path + str(
                image_name) + ' ' + debug_path + 'test_input.png')
            png_image = cv2.imread(debug_path + 'test_input.png', cv2.IMREAD_GRAYSCALE)
            im_height, im_width = png_image.shape
            break
        return im_height, im_width

if __name__ == '__main__':
    hwl1 = MainFormLogic()