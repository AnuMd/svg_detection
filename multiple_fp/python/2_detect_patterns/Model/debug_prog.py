import os,shutil,cv2
from lxml import etree

class debug_class:
    def extract_bb_elements(self,bb_element_data,input_folder_path,debug_folder_path):
        input_image_folder = input_folder_path + '/1_images/input/'
        svg_file_path = ''
        for image_name in os.listdir(input_image_folder):
            svg_file_path = input_image_folder + image_name

        png_image = cv2.imread(debug_folder_path+'test_input.png',cv2.IMREAD_GRAYSCALE)
        im_height, im_width = png_image.shape

        # --create output destination
        destination_folder_path = debug_folder_path+'windows/'
        if os.path.exists(destination_folder_path):
            shutil.rmtree(destination_folder_path)
        os.mkdir(destination_folder_path)


        #--read input svg file
        tree = etree.parse(svg_file_path)

        #-- get each word
        for b_row_num, bb_data_row in enumerate(bb_element_data):
            # -- get element data from rows
            # text = bb_data_row[0]
            element_id_list = bb_data_row[1]

            #--create output image
            svg_id = str(b_row_num)
            svg_file = open(destination_folder_path +svg_id+'.svg', 'w')

            #-- add data to output image
            svg_file.write(
                '<svg id="'+svg_id+'" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="' + str(
                    im_width) + '" height="' + str(im_height) + '" style="background-color: #FFFFFF;">' + '\n')
            l2_space = '  '

            #---find elements for each id
            for each_id in element_id_list:
                element_list = tree.xpath('//*[@id="' + each_id.rstrip() + '"]')
                #--- when a <g><polyline/><polyline/></g> exist,
                #--- I find as all g,polyline1, polyline2 are inside ocrbb since
                #--- polyline1 and polyline2 are inside it
                #--- so if we had a <g> len(element_list) will be > 1
                #--- as a temp fix I am ignoring such <g> and taking only <polylines>
                element = element_list[0]
                if len(element)==0:
                    #--- add all atttributes to attributes_list string
                    attributes_list = ''
                    for a,attrib_name in enumerate(element.attrib):
                        if a ==0:
                            attributes_list = attrib_name+'="'+element.attrib[attrib_name]+'"'
                        else:
                            attributes_list = attributes_list + ' ' + attrib_name + '="' + element.attrib[
                                attrib_name] +'"'
                    svg_file.write(l2_space+'<'+element.tag.split('}')[1]+' '+str(attributes_list)+'/>'+ '\n')

            svg_file.write('</svg>')
            svg_file.close()

