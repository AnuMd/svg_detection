
import os,cv2,re,math
from natsort import natsorted

from math_functions import math_functions

class bb_elements_class:
    def __init__(self):
        self.math_functions_obj = math_functions()

    # def find_BB_elements(self,raster_detection_text_path,
    #                      svg_element_text_path):
    #     bb_elements = []
    #
    #     # --- 1. get ocr data (From text_details.txt and get converted value)
    #     #-- since we are using 250 density  its always 0.288 for both x and y
    #     #-- use below method if we change density some day
    #     # image_dimensions = self.get_image_conversion_values(image_name,input_im_path,convert_path)
    #     image_dimensions = [0.288,0.288]
    #     text_details_list = self.get_OCR_data(raster_detection_text_path,image_dimensions)
    #
    #     #--if text is not as text tags:
    #     if len(text_details_list) != 0:
    #         # --- read data written to file from js
    #         svg_element_list = self.process_js_data(svg_element_text_path)
    #         # --- from svg_element_list find elements inside ocr BBxes
    #         bb_elements = self.find_matchingBB(svg_element_list,text_details_list)
    #     else:
    #         print ' has Text Tags'
    #
    #     return bb_elements


    def process_js_data(self,svg_element_text_path):
        js_data_list = []
        #-- read text file
        js_data = open(svg_element_text_path)
        for element_data in js_data:
            #--get tagname ::: attribute data ::: BBdata
            element_data_list = element_data.split(':::')
            tagName = element_data_list[0].strip()
            position = element_data_list[1].strip()
            bbdata = element_data_list[2].strip()
            #---BB procesiing
            bb_data_list = bbdata.split(',')
            #--keep only numbers and '.'s
            bb_data_list[0] = bb_data_list[0][7:]
            num_bb_data_list =  [float(x) for x in bb_data_list]

            #--- attributes processing
            attributes = position.split('::')

            #--- get centre point of bbxes
            x1,y1,x2,y2 = num_bb_data_list
            center_x, center_y = (x1 + x2) / 2, (y1 + y2) / 2

            #-- add al to data list
            js_data_list.append([tagName,attributes,num_bb_data_list,[center_x, center_y]])

        js_data_list.reverse()

        return js_data_list

    def go_up_python_path(self,path, levels):
        final_path = path
        while levels >0:
            final_path = os.path.abspath(os.path.join(final_path, '..'))
            levels = levels - 1
        return final_path

    def get_OCR_data(self,output_path,image_dimensions):
        x_ratio, y_ratio = image_dimensions
        ocr_text_file_path = output_path
        if os.path.exists(ocr_text_file_path):
            # ---text details extraction from text file
            text_details_file = open(ocr_text_file_path, 'r')
            text_details_list = []
            for text_line in text_details_file:
                text_line_to_analyze = []
                detail_seperate = text_line.split(':')
                # ---since we find all text, it can have words like ':U' or 'KK:' or 'K:K',
                # ---need to add ':' to the detected text in that instance
                if len(detail_seperate) == 3:
                    text_line_to_analyze = detail_seperate
                elif len(detail_seperate) == 4:
                    word = []
                    if len(detail_seperate[0]) == 0:
                        word.append(':' + detail_seperate[1])
                    elif len(detail_seperate[1]) == 0:
                        word.append(detail_seperate[0] + ':')
                    else:
                        word.append(detail_seperate[0] + ':' + detail_seperate[1])

                    text_line_to_analyze = [word[0], detail_seperate[2], detail_seperate[3].strip()]

                if len(text_line_to_analyze) > 0:
                    # --seperate and convert BB-box to int
                    bb_cordinates_string_2 = text_line_to_analyze[1].split(',')
                    bb_cordinates_string_2[0] = bb_cordinates_string_2[0][2:]
                    bb_cordinates_string_2[-1] = bb_cordinates_string_2[-1][:-2]
                    int_bb_cordinates = [int(float(x)) for x in bb_cordinates_string_2]
                    # int_bb_cordinates2 = [int(x) for x in int_bb_cordinates]
                    bb_cordintes_chunks = [int_bb_cordinates[x:x + 4] for x in xrange(0, len(int_bb_cordinates), 4)]
                    bb_cordinate_to_append = 0
                    for bb_cord in bb_cordintes_chunks:
                        bb_cordinate_to_append = bb_cord
                    # confidence_string = re.findall(r"[\w']+", text_line_to_analyze[3])
                    # confidence = int(confidence_string[0])
                    #
                    # match_data = re.findall(r"[\w']+", text_line_to_analyze[-1])

                    text_details_list.append([text_line_to_analyze[0].strip(), bb_cordinate_to_append])
            text_details_file.close()

            # -- change cords
            for r,row in enumerate(text_details_list):
                # centre_x,centre_y = row[1]
                bb_x1, bb_y1, bb_x2, bb_y2 = row[1]
                # centre_x, centre_y = int(centre_x*x_ratio),int(centre_y*y_ratio)
                bb_x1, bb_y1, bb_x2, bb_y2 = int(bb_x1*x_ratio), int(bb_y1*y_ratio),int(bb_x2*x_ratio), int(bb_y2*y_ratio)
                # text_details_list[r][1] = [centre_x,centre_y]
                text_details_list[r][1] = [bb_x1, bb_y1, bb_x2, bb_y2]
        else:
            text_details_list=[]

        return text_details_list

    def get_raster_data(self,raster_detection_text_path, image_dimensions,txt_file_name):
        x_ratio, y_ratio = image_dimensions
        fp_feature_name = txt_file_name[2:]
        raster_details_file = open(raster_detection_text_path, 'r')
        raster_details_list = []
        for raster_line in raster_details_file:
            # remove '[' and ',' from text row
            raster_number_string = re.findall(r"[\w']+", raster_line.strip())
            # convert str numbers to int
            raster_number_only = [int(x) for x in raster_number_string]
            # combine two numbers closeby and create [x,y],[x1,y1] type row
            raster_bb_4p_list = [raster_number_only[x:x + 2] for x in xrange(0, len(raster_number_only), 2)]

            #--- convert raster_bb_list based on image dimensions to fit to original svg cords
            #---- select only 1st and 3rd points of bb_4-p list to match required structure
            bb_x1, bb_y1 = raster_bb_4p_list[0]
            bb_x2, bb_y2 = raster_bb_4p_list[2]
            bb_x1, bb_y1, bb_x2, bb_y2 = int(bb_x1 * x_ratio), int(bb_y1 * y_ratio), int(bb_x2 * x_ratio), int(bb_y2 * y_ratio)
            raster_bb_list = [bb_x1, bb_y1, bb_x2, bb_y2]
            raster_details_list.append([fp_feature_name,raster_bb_list])

        return raster_details_list


    def find_matchingBB_text(self,svg_element_list,raster_details_list,debug_folder_path):
        bb_elements = []
        # test_image_path = debug_folder_path +  'test_input.png'
        # test_image = cv2.imread(test_image_path, cv2.IMREAD_COLOR)

        for raster_row, raster_data_row in enumerate(raster_details_list):
            # test_image_copy = test_image.copy()
            #--get raster BB
            raster_x1,raster_y1,raster_x2,raster_y2 = raster_data_row[1]
            text = raster_data_row[0]
            element_id_list = []
            # cv2.rectangle(test_image_copy, (int(raster_x1), int(raster_y1)),
            #                                   (int(raster_x2), int(raster_y2)), (0, 255, 0), 2)

            #--get each svg element BB
            for r, js_row in enumerate(svg_element_list):
                attributes = js_row[1]
                bb_data = js_row[2]
                bb_x1, bb_y1, bb_x2, bb_y2 = bb_data
                bb_cx1, bb_cy1 = js_row[3]

                # for ele in attributes:
                #     if 'id=n_id_256' in ele:
                #         print ele


                #---Check 01. if svg_bb centre point is inside ocr_bb
                inside_flag = self.math_functions_obj.check_if_point_inside_rect(bb_cx1,bb_cy1,raster_x1,raster_y1,raster_x2,raster_y2)
                if inside_flag:
                    # test_image_copy = test_image.copy()
                    # cv2.rectangle(test_image_copy, (int(raster_x1), int(raster_y1)),
                    #               (int(raster_x2), int(raster_y2)), (0, 255, 0), 2)
                    # cv2.rectangle(test_image_copy, (int(bb_x1), int(bb_y1)),
                    #               (int(bb_x2), int(bb_y2)), (0, 0, 255), 1)
                    # cv2.imwrite(debug_folder_path + str(raster_row) + '_' + str(r) + '.png',
                    #             test_image_copy)

                    intersect_x1, intersect_y1, intersect_x2, intersect_y2 = self.math_functions_obj.find_intersection_of_two_rectangles(raster_x1,raster_y1,raster_x2,raster_y2,bb_x1, bb_y1, bb_x2, bb_y2)
                    intersect_width, intersect_height = self.math_functions_obj.get_rect_dimensions(intersect_x1,intersect_y1,intersect_x2,intersect_y2)
                    bb_width, bb_height = self.math_functions_obj.get_rect_dimensions(bb_x1, bb_y1, bb_x2, bb_y2 )

                    svg_bb_area = bb_width * bb_height
                    intersect_area = intersect_width * intersect_height
                    svg_bb_outside_ocr_area = svg_bb_area - intersect_area

                    # ---Check 02. To remove long thin lines' BBxes
                    if svg_bb_outside_ocr_area/svg_bb_area < 0.5:
                        ocr_width, ocr_height = self.math_functions_obj.get_rect_dimensions(raster_x1,raster_y1,raster_x2,raster_y2)
                        ocr_area = ocr_width * ocr_height

                        # ---Check 03. to remove small glyphs' bbxes inside ocr area
                        if svg_bb_area/ocr_area > 0.005:
                            element_id = self.get_attribute_id(attributes)
                            element_id_list.append(element_id)
                            # print raster_row, r

            # cv2.putText(image_color, text,
            #             (raster_x1-5,raster_y1-5), cv2.FONT_HERSHEY_PLAIN, 1,
            #             (0, 0, 255), 2)
            bb_elements.append([text,element_id_list])
            # cv2.imwrite(debug_folder_path + str(raster_row)+'.png', test_image_copy)
        return bb_elements

    def get_attribute_id(self,attributes):
        id = ''
        for single_attrib in attributes:
            if 'id' in single_attrib:
                id = (single_attrib.split('=')[1]).strip()
                break
        return id

    def find_matchingBB(self,svg_element_list,raster_details_list,debug_folder_path):
        bb_elements = []
        # test_image_path = debug_folder_path +  'test_input.png'
        # test_image = cv2.imread(test_image_path, cv2.IMREAD_COLOR)

        for raster_row, raster_data_row in enumerate(raster_details_list):
            # print '----', raster_data_row
            # test_image_copy = test_image.copy()
            # #--get raster BB
            # raster_x1, raster_y1, raster_x2, raster_y2 = raster_data_row[1]
            # #--enlarge raster BB
            # large_raster_x1, large_raster_y1, large_raster_x2, large_raster_y2 = self.increase_raster_bb(raster_data_row[1])
            raster_x1, raster_y1, raster_x2, raster_y2 = self.increase_raster_bb(
                raster_data_row[1])

            text = raster_data_row[0]
            element_id_list = []
            # cv2.rectangle(test_image_copy, (int(raster_x1), int(raster_y1)),
            #                                   (int(raster_x2), int(raster_y2)), (0, 255, 0), 2)

            #--get each svg element BB
            for r, js_row in enumerate(svg_element_list):
                attributes = js_row[1]
                bb_data = js_row[2]
                bb_x1, bb_y1, bb_x2, bb_y2 = bb_data
                bb_cx1, bb_cy1 = js_row[3]


                #---Check 01. if svg_bb centre point is inside enlargened raster_bb
                #-- raster bb is enlarged coz window etc bb might not have all svg elements in it
                #-- not as simple as a text raster bb & text svgbbxes
                inside_flag = self.math_functions_obj.check_if_point_inside_rect(bb_cx1,bb_cy1,raster_x1, raster_y1, raster_x2, raster_y2)
                if inside_flag:
                    # print js_row
                    # # ---Check 02. Check distance from real raster_bb centre to svg_bb_centre
                    # # ---Done to remove small wall segments that might be already taken due to
                    # # ---enlargned windows
                    # rs_center_x, rs_center_y = (raster_x1 + raster_x2) / 2, (raster_y1 + raster_y2) / 2
                    # distance_in_elements = math.hypot(rs_center_x-bb_cx1, rs_center_y-bb_cy1)
                    # # print 'hiii', raster_row,r
                    # # print distance_in_elements




                    intersect_x1, intersect_y1, intersect_x2, intersect_y2 = self.math_functions_obj.find_intersection_of_two_rectangles(raster_x1,raster_y1,raster_x2,raster_y2,bb_x1, bb_y1, bb_x2, bb_y2)
                    intersect_width, intersect_height = self.math_functions_obj.get_rect_dimensions(intersect_x1,intersect_y1,intersect_x2,intersect_y2)
                    bb_width, bb_height = self.math_functions_obj.get_rect_dimensions(bb_x1, bb_y1, bb_x2, bb_y2 )

                    if bb_width > 0 and  bb_height >0:
                        svg_bb_area = bb_width * bb_height
                        intersect_area = intersect_width * intersect_height
                        svg_bb_outside_ocr_area = svg_bb_area - intersect_area

                        # ---Check 02. To remove long thin lines' BBxes
                        if svg_bb_outside_ocr_area/svg_bb_area < 0.5:
                            ocr_width, ocr_height = self.math_functions_obj.get_rect_dimensions(raster_x1,raster_y1,raster_x2,raster_y2)
                            ocr_area = ocr_width * ocr_height

                            # ---Check 03. to remove small glyphs' bbxes inside ocr area
                            if svg_bb_area/ocr_area > 0.005:
                                element_id = self.get_attribute_id(attributes)
                                element_id_list.append(element_id)

                                # test_image_copy = test_image.copy()
                                # cv2.rectangle(test_image_copy, (int(raster_x1), int(raster_y1)),
                                #               (int(raster_x2), int(raster_y2)), (0, 255, 0), 2)
                                # cv2.rectangle(test_image_copy, (int(bb_x1), int(bb_y1)),
                                #               (int(bb_x2), int(bb_y2)), (0, 0, 255), 1)
                                # cv2.imwrite(debug_folder_path + str(raster_row) + '_' + str(r) + '.png',
                                #             test_image_copy)



            # cv2.putText(image_color, text,
            #             (raster_x1-5,raster_y1-5), cv2.FONT_HERSHEY_PLAIN, 1,
            #             (0, 0, 255), 2)
            bb_elements.append([text,element_id_list])
            # cv2.imwrite(debug_folder_path + str(raster_row)+'.png', test_image_copy)
        return bb_elements

    def increase_raster_bb(self, bbox):
        raster_x1, raster_y1, raster_x2, raster_y2 = bbox

        min_x = min(raster_x1,raster_x2)
        min_y = min(raster_y1,raster_y2)

        max_x = max(raster_x1,raster_x2)
        max_y = max(raster_y1,raster_y2)

        # print min(abs(raster_y1-raster_y2),abs(raster_x1-raster_x2))
        thresh = (min(abs(raster_y1-raster_y2),abs(raster_x1-raster_x2)))/5
        # print thresh

        n_raster_x1, n_raster_y1 = min_x- thresh, min_y-thresh
        n_raster_x2, n_raster_y2 = max_x + thresh, max_y + thresh

        return n_raster_x1, n_raster_y1,n_raster_x2, n_raster_y2