import math

class math_functions:
    def __init__(self):
        a = 2

    def get_rect_dimensions(self,x1,y1,x2,y2):
        width = abs(x1-x2)
        height = abs(y1-y2)
        return width, height

    def check_if_point_inside_rect(self,p_x1,p_y1,x1,y1,x2,y2):
        if p_x1 < max(x1,x2) and p_x1 > min(x1, x2):
            if p_y1 < max(y1, y2) and p_y1 > min(y1, y2):
                return True
            else:
                return False
        else:
            return False

    def find_intersection_of_two_rectangles(self,x1,y1,x2,y2,x3,y3,x4,y4):
        #---find min_x of rectangle 1 and rectangle 2
        r1_min_x = min(x1, x2)
        r2_min_x = min(x3, x4)
        # ---find max_x of rectangle 1 and rectangle 2
        r1_max_x = max(x1, x2)
        r2_max_x = max(x3, x4)

        intersect_x1 = max(r1_min_x, r2_min_x)
        intersect_x2 = min(r1_max_x, r2_max_x)

        # ---find min_y of rectangle 1 and rectangle 2
        r1_min_y = min(y1, y2)
        r2_min_y = min(y3, y4)
        # ---find max_y of rectangle 1 and rectangle 2
        r1_max_y = max(y1, y2)
        r2_max_y = max(y3, y4)

        intersect_y1 = max(r1_min_y, r2_min_y)
        intersect_y2 = min(r1_max_y, r2_max_y)

        return intersect_x1,intersect_y1,intersect_x2,intersect_y2

    def find_centre_of_line(self,line):
        p1,p2 = line
        x1,y1 = p1
        x2,y2 = p2
        center_x, center_y= (x2+x1)/2,(y1+y2)/2
        # center_x, center_y= x1+((x2-x1)/2),y1+((y1-y2)/2)
        return center_x, center_y

    def calculate_angle_vectors(self,vx1,vy1,vx2,vy2):
        # Use dotproduct to find angle between vectors
        # This always returns an angle between 0, pi
        numer = (vx1 * vx2 + vy1 * vy2)
        denom = math.sqrt((vx1 ** 2 + vy1 ** 2) * (vx2 ** 2 + vy2 ** 2))
        angle = math.degrees(math.acos(numer / denom))
        return angle