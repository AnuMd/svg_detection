import os,cv2
from natsort import natsorted
from lxml import etree


#---program files


class erase_elements_class:

    def erase_identified_elements(self,bb_element_data, cleaned_image_path,
                                  visible_image_path, input_im_path,output_im_path,
                                  preprocess_path, fp_feature,current_image_name):
        # --create output destination
        output_python_path = ''
        for file in os.listdir(input_im_path):
            output_python_path = output_im_path + str(file)

        #-- if 'T' or 'Wl' since input was visible.svg, we need to
        #-- 1. erase in visible.svg
        #-- 2. find matching elements in cleaned.svg
        #-- 3. erase cleaned.svg elements too
        if fp_feature=='T' or fp_feature == 'Wl':
            #-- find matcing elements from svg that has lines<-broken paths
            new_bb_data = self.find_matching_new_elements(preprocess_path,
                                        bb_element_data,current_image_name)
            #-- erase in image that has paths as paths
            visible_tree = self.erase_elements_from_svg(input_image_path=visible_image_path,
                                         output_image_path=visible_image_path,
                                         bb_element_data=bb_element_data)
            bb_element_data = new_bb_data
            #--- write visible image as input to next stage if fp_feature is 'T'
            if fp_feature == 'T':
                visible_tree.write(output_python_path)

        #-- for all fp_features erase in clean.svg
        #-- erase in image that has paths/rects/polygons/polylines as lines
        cleaned_tree = self.erase_elements_from_svg(input_image_path=cleaned_image_path,
                                     output_image_path=cleaned_image_path,
                                     bb_element_data=bb_element_data)

        #--- write cleaned image as input to next stage for Wl,St,D,Win,R,O
        if fp_feature != 'T':
            cleaned_tree.write(output_python_path)



    def erase_elements_from_svg(self,input_image_path,output_image_path,
                                bb_element_data):
        # --- open input svg image
        tree = etree.parse(input_image_path)
        for bb_data_row in bb_element_data:
            # -- get element data from rows
            element_id_list = bb_data_row[1]
            for element_id in element_id_list:
                # -- find in svg elements_subelements that have an id == element_id
                element = tree.xpath("//*[@id='" + element_id + "']")
                # -- check if there is a match
                if len(element) != 0:
                    for x in element:
                        # -- get parent of selected element and delete child
                        x.getparent().remove(x)
        # --remove if erased.svg exists
        if os.path.exists(output_image_path):
            os.remove(output_image_path)
        # --write erased.svg
        tree.write(output_image_path)
        #--- write to python next fp feature folder
        # tree.write(python_output_folder)

        return tree


    def find_matching_new_elements(self,preprocess_path,bb_element_data,current_image_name):
        cleaned_svg_file = open(preprocess_path+current_image_name+'_svg_text_data.txt')
        cleaned_svg_ids = []
        for row in cleaned_svg_file:
            if len(row) > 1:
                extracted_data = []
                #---remove start and end space(including '/n')
                #--- original element data :::: new elements id list <- extract
                original_element_data, new_element_ids = (row.strip()).split('::::')
                for org_attribute in original_element_data.split(':::'):
                    attribute_info = org_attribute.split('::')
                    if len(attribute_info) > 1:
                        attribute = attribute_info[0]
                        attribute_value = attribute_info[1]
                        if attribute == 'id':
                            extracted_data.append(attribute_value)
                            break
                new_element_list = []
                for new_id in new_element_ids.split(','):
                    new_element_list.append(new_id.strip())

                extracted_data.append(new_element_list)
                cleaned_svg_ids.append(extracted_data)

        new_bb_data = []
        for bb_row in bb_element_data:
            bb_element_ids = bb_row[1]
            if len(bb_element_ids) > 0:
                for bb_id in bb_element_ids:
                    for id_row in cleaned_svg_ids:
                        original_id = (id_row[0]).strip()
                        new_ids = id_row[1]
                        if (bb_id).strip()==original_id:
                            new_bb_data.append([bb_row[1],new_ids])

        return new_bb_data













