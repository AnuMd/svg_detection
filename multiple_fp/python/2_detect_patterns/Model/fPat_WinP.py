import os, math
from lxml import etree

#-- program files
from math_functions import math_functions

class window_pattern_class:

    def __init__(self):
        self.math_functions_obj = math_functions()

    def find_window_pattern_main(self,bb_elements,input_folder_path,im_height, im_width):
        svg_id_list = [row[1] for row in bb_elements]

        svg_id_list = [
            ['n_id_292','n_id_293'],
            ['n_id_294','n_id_295'],
            ['n_id_296','n_id_297'],
            ['n_id_298','n_id_299','n_id_300'],
            ['n_id_301','n_id_302','n_id_303'],
            ['n_id_304','n_id_305']
        ]




        #--get input svg path
        input_svg_file_path = ''
        #-- folder will contain only 1 image
        for file in os.listdir(input_folder_path+'1_images/input/'):
            file_name = str(file)
            input_svg_file_path = input_folder_path+'1_images/input/' + file_name

        # --- open input svg image
        tree = etree.parse(input_svg_file_path)


        # self.break_path_to_lines(tree,svg_id_list)


        # ------------------------ CASE 01 - Rect + Rect ---------------------------------------
        #-- find elements related to each window elements
        for element_id_list in svg_id_list:
            for element_id in element_id_list:
                # -- find in svg elements_subelements that have an id == element_id
                element = tree.xpath("//*[@id='" + element_id + "']")
                # -- check if there is a match
                if len(element) != 0:
                    for svg_element in element:
                        #-- limits the number of splits to 1
                        tag_name = svg_element.tag.split('}',1)[1]
                        main_id = svg_element.attrib['id']

                        #---01. if is inside <g>
                        if tag_name == 'g':
                            no_children = len(svg_element)

                            #-- 02. if has 2 children
                            if no_children == 2:

                                #---03. if both elements are 'rect'
                                condition_invalid = False
                                for child in svg_element:
                                    child_tag = child.tag.split('}', 1)[1]
                                    if child_tag != 'rect':
                                        condition_invalid = True
                                        break
                                    else:
                                        rect_width, rect_height = float(child.attrib['width']), float(child.attrib['height'])

                                        # -- 04. if max dimension is of reasonable window size
                                        # -- get max of width or height / by their respective dimension
                                        max_dimension = max(rect_width / im_width, rect_height / im_height)
                                        # -- thresholds found from testing
                                        if not (max_dimension > 0.02 and max_dimension < 0.10):
                                            condition_invalid = True
                                            break
                                if condition_invalid:
                                    continue

                                else:
                                    child_1, child_2 = svg_element[0], svg_element[1]

                                    # --- 05. if big_rect_area/small_rect_area ratio between threshold
                                    # --calculate areas
                                    child_1_area = round((float(child_1.attrib['width']) * float(child_1.attrib['height']))
                                                      , 5)
                                    child_2_area = round(
                                        (float(child_2.attrib['width']) * float(child_2.attrib['height']))
                                        , 5)
                                    small_rect_area = min(child_1_area, child_2_area)
                                    big_rect_area = max(child_1_area, child_2_area)
                                    g_area_ratio = small_rect_area / big_rect_area
                                    if g_area_ratio > 0.05 and g_area_ratio < 0.3:


                                        # --- 06/07. if small rect, big rect calculations-----
                                        if small_rect_area == child_1_area:
                                            min_rect_child = child_1
                                            max_rect_child = child_2
                                        else:
                                            min_rect_child = child_2
                                            max_rect_child = child_1
                                        min_x, min_y = float(min_rect_child.attrib['x']),float(min_rect_child.attrib['y'])
                                        min_height, min_width = float(min_rect_child.attrib['height']), float(min_rect_child.attrib[
                                            'width'])
                                        min_x1, min_y1 = min_x + min_width, min_y + min_height
                                        diagonal_line = [[min_x, min_y],[min_x1, min_y1]]
                                        min_centre_x, min_centre_y = self.math_functions_obj.find_centre_of_line(diagonal_line)

                                        max_x, max_y = float(max_rect_child.attrib['x']),float(max_rect_child.attrib['y'])
                                        max_height, max_width = float(max_rect_child.attrib['height']), float(max_rect_child.attrib['width'])
                                        max_x1, max_y1 = max_x + max_width, max_y + max_height

                                        #--- 06. if small rect is inside big rect-----
                                        inside_flag = self.math_functions_obj.check_if_point_inside_rect(min_centre_x, min_centre_y,
                                                                     max_x, max_y,max_x1, max_y1)
                                        if inside_flag:

                                            #--- 07. if small rect/big rect area <> threshold
                                            intersect_x1, intersect_y1, intersect_x2, intersect_y2 = self.math_functions_obj.find_intersection_of_two_rectangles(
                                                min_x1, min_y1, min_x1, min_y1, max_x, max_y, max_x1, max_y1)
                                            intersect_width, intersect_height = self.math_functions_obj.get_rect_dimensions(
                                                intersect_x1, intersect_y1, intersect_x2, intersect_y2)

                                            min_rect_area = min_width * min_height
                                            intersect_area = intersect_width * intersect_height
                                            min_rect_outside_max_rect_area = min_rect_area - intersect_area
                                            min_rect_outside_ratio = min_rect_outside_max_rect_area/min_rect_area

                                            if min_rect_outside_ratio > 0.8 and min_rect_outside_ratio < 1.2:
                                                # print main_id, child_1.attrib['id'], child_2.attrib['id']
                                                a = 2

        # ------------------------ CASE 02 - Rect + Line ---------------------------------------
        # -- find elements related to each window elements
        a = []
        svg_data = []
        for element_id_list in svg_id_list:
            group = []
            for element_id in element_id_list:
                # -- find in svg elements_subelements that have an id == element_id
                element = tree.xpath("//*[@id='" + element_id + "']")
                # -- check if there is a match
                if len(element) != 0:
                    for svg_element in element:
                        # -- limits the number of splits to 1
                        tag_name = svg_element.tag.split('}', 1)[1]
                        main_id = svg_element.attrib['id']

                        if tag_name == 'rect':
                            rect_width = float(svg_element.attrib['width'])
                            rect_height = float(svg_element.attrib['height'])
                            rect_x = float(svg_element.attrib['x'])
                            rect_y = float(svg_element.attrib['y'])
                            svg_data.append([tag_name,main_id,rect_width,rect_height,
                                             rect_x,rect_y])
                        elif tag_name == 'line':
                            x1 = float(svg_element.attrib['x1'])
                            y1 = float(svg_element.attrib['y1'])
                            x2 = float(svg_element.attrib['x2'])
                            y2 = float(svg_element.attrib['y2'])
                            length = round(math.hypot(x1 - x2, y1 - y2),2)
                            # --calculate line centre point
                            l_cx, l_cy = self.math_functions_obj.find_centre_of_line([[x1, y1], [x2, y2]])
                            # -- calculate length
                            length = math.hypot(x1 - x2, y1 - y2)
                            # --calculate angle
                            line_angle = self.math_functions_obj.calculate_angle_vectors(x1, y1, x2, y2)

                            group.append([tag_name, main_id, x1, y1, x2, y2,
                                                      l_cx, l_cy, line_angle, length])
            svg_data.append(group)

            new_svg_data = svg_data
            #---- only relatable for pattern finding
            #--- since there are  rect 2, line 2 in this example need to keep rect 1, line 1
            # rect_count, line_count = 0,0
            # new_svg_data = []
            # for svg_dat in svg_data:
            #     if svg_dat[0] == 'rect':
            #         rect_count = rect_count + 1
            #         if rect_count < 2:
            #             new_svg_data.append(svg_dat)
            #     elif svg_dat[0] == 'line':
            #         line_count = line_count + 1
            #         if line_count < 2:
            #             new_svg_data.append(svg_dat)

            for svg_row in new_svg_data:
                test = []
                for svg_element in svg_row:
                    if svg_element[0] == 'rect':
                        width = svg_element[2]
                        height = svg_element[3]
                        area = height * width
                        # print area

                        print max(width,height)
                        a.append(max(width,height))

                        # rect_ratio = max(width,height)/min(width,height)
                        # print svg_element[1], rect_ratio

                        # print width,im_width,round(width/im_width,3)
                        # print height, im_height, round(height/im_height,3)
                        # if round(width/im_width,3) > 0.001 and round(width/im_width,3) < 0.2:
                        #     print width, im_width, round(width / im_width, 3)
                        # if round(height/im_height,3) > 0.001 and round(height/im_height,3) < 0.05:
                        #     print height, im_height, round(height/im_height, 3)

                        # if area_ratio > 0.0001 and area_ratio < 0.001:
                        #     print area, area_ratio
                        # print '-----'

                    if svg_element[0] == 'line':
                        length = svg_element[-1]
                        line_angle = svg_element[-2]
                        l1_cx, l1_cy = svg_element[6], svg_element[7]
                        # test.append([l1_cx, l1_cy ])
                        test.append(line_angle)
                        # print line_angle
                        # print length
                print test
                print abs(test[0]-test[1])
                # p1_x,p1_y = test[0]
                # p2_x, p2_y = test[1]
                # print math.hypot(p1_x-p2_x,p1_y-p2_y)
                print '-----'

        # print '----'
        # ab = a[:-1]
        # print ab
        # print sum(a)/ len(a)




    def break_path_to_lines(self,tree,svg_id_list):
        a = 2

        for svg_id_row in svg_id_list:
            if len(svg_id_row) > 0:
                for svg_id in svg_id_row:
                    # -- find in svg elements_subelements that have an id == element_id
                    element = tree.xpath("//*[@id='" + svg_id + "']")
                    # -- check if there is a match
                    if len(element) != 0:
                        for x in element:
                            print x


    def search_window_pattern_01(self,bb_elements,input_folder_path,im_height, im_width):
        svg_id_list = [row[1] for row in bb_elements]

        svg_id_list = [
            ['g1753', 'rect1743', 'rect1745'],
            ['g1769', 'rect1764', 'rect1766'],
            # ['g1787', 'rect1772', 'rect1774'],
            # ['g1781', 'rect1776', 'rect1778'],
            ['g1761', 'rect1756', 'rect1758']
        ]

        # --get input svg path
        input_svg_file_path = ''
        # -- folder will contain only 1 image
        for file in os.listdir(input_folder_path + '1_images/input/'):
            file_name = str(file)
            input_svg_file_path = input_folder_path + '1_images/input/' + file_name

        # --- open input svg image
        tree = etree.parse(input_svg_file_path)
        root = tree.getroot()




        #------------------------ CASE 01---------------------------------------
        #-- 01. find <g> tags
        expr = "//*[local-name() = $name]"
        #--- find all <g> from all levels in xml
        for group in root.xpath(expr, name="g"):
            main_id = group.attrib['id']
            no_children = len(group)

            # -- 02. if has 2 children
            if no_children == 2:

                # ---03. if both elements are 'rect'
                condition_invalid = False
                for child in group:
                    child_tag = child.tag.split('}', 1)[1]
                    if child_tag != 'rect':
                        condition_invalid = True
                        break
                    else:
                        rect_width, rect_height = float(child.attrib['width']), float(child.attrib['height'])

                        # -- 04. if max dimension is of reasonable window size
                        # -- get max of width or height / by their respective dimension
                        max_dimension = max(rect_width / im_width, rect_height / im_height)
                        # -- thresholds found from testing
                        if not (max_dimension > 0.02 and max_dimension < 0.12):
                            condition_invalid = True
                            break
                if condition_invalid:
                    continue

                else:
                    child_1, child_2 = group[0], group[1]

                    # --- 05. if big_rect_area/small_rect_area ratio between threshold
                    # --calculate areas
                    child_1_area = round((float(child_1.attrib['width']) * float(child_1.attrib['height']))
                                         , 5)
                    child_2_area = round(
                        (float(child_2.attrib['width']) * float(child_2.attrib['height']))
                        , 5)
                    small_rect_area = min(child_1_area, child_2_area)
                    big_rect_area = max(child_1_area, child_2_area)
                    g_area_ratio = small_rect_area / big_rect_area
                    if g_area_ratio > 0.05 and g_area_ratio < 0.3:

                        # --- 06/07. if small rect, big rect calculations-----
                        if small_rect_area == child_1_area:
                            min_rect_child = child_1
                            max_rect_child = child_2
                        else:
                            min_rect_child = child_2
                            max_rect_child = child_1
                        min_x, min_y = float(min_rect_child.attrib['x']), float(min_rect_child.attrib['y'])
                        min_height, min_width = float(min_rect_child.attrib['height']), float(min_rect_child.attrib[
                                                                                                  'width'])
                        min_x1, min_y1 = min_x + min_width, min_y + min_height
                        diagonal_line = [[min_x, min_y], [min_x1, min_y1]]
                        min_centre_x, min_centre_y = self.math_functions_obj.find_centre_of_line(diagonal_line)

                        max_x, max_y = float(max_rect_child.attrib['x']), float(max_rect_child.attrib['y'])
                        max_height, max_width = float(max_rect_child.attrib['height']), float(
                            max_rect_child.attrib['width'])
                        max_x1, max_y1 = max_x + max_width, max_y + max_height

                        # --- 06. if small rect is inside big rect-----
                        inside_flag = self.math_functions_obj.check_if_point_inside_rect(min_centre_x, min_centre_y,
                                                                                         max_x, max_y, max_x1, max_y1)
                        if inside_flag:

                            # --- 07. if small rect/big rect area <> threshold
                            intersect_x1, intersect_y1, intersect_x2, intersect_y2 = self.math_functions_obj.find_intersection_of_two_rectangles(
                                min_x1, min_y1, min_x1, min_y1, max_x, max_y, max_x1, max_y1)
                            intersect_width, intersect_height = self.math_functions_obj.get_rect_dimensions(
                                intersect_x1, intersect_y1, intersect_x2, intersect_y2)

                            min_rect_area = min_width * min_height
                            intersect_area = intersect_width * intersect_height
                            min_rect_outside_max_rect_area = min_rect_area - intersect_area
                            min_rect_outside_ratio = min_rect_outside_max_rect_area / min_rect_area

                            if min_rect_outside_ratio > 0.8 and min_rect_outside_ratio < 1.2:
                                group_detected = False
                                for id_row in svg_id_list:
                                    if main_id in id_row:
                                        group_detected = True
                                        break
                                if group_detected== False:
                                    print main_id, child_1.attrib['id'], child_2.attrib['id']

    def search_window_pattern_02(self, bb_elements, input_folder_path):
        # --get input svg path
        input_svg_file_path = ''
        # -- folder will contain only 1 image
        for file in os.listdir(input_folder_path + '1_images/input/'):
            file_name = str(file)
            input_svg_file_path = input_folder_path + '1_images/input/' + file_name

        # --- open input svg image
        tree = etree.parse(input_svg_file_path)

        #--- find rects and lines in svg that fulfill some conditions
        svg_data_rect, svg_data_line = [],[]
        for svg_element in tree.iter():
            # -- limits the number of splits to 1
            tag_name = svg_element.tag.split('}', 1)[1]
            main_id = svg_element.attrib['id']

            #---- find rects
            if tag_name == 'rect':
                rect_width = float(svg_element.attrib['width'])
                rect_height = float(svg_element.attrib['height'])
                rect_ratio = max(rect_width, rect_height) / min(rect_width, rect_height)
                rect_area = rect_height * rect_width

                #---- C 01. check if rect widht/rect_height is window-ish (206->122)
                #---- and if rect_area is windowish(122->104)
                if rect_ratio > 4 and rect_ratio < 10:
                    if rect_area > 70 and rect_area < 155:
                        rect_x = float(svg_element.attrib['x'])
                        rect_y = float(svg_element.attrib['y'])
                        rect_x1 = rect_x + rect_width
                        rect_y1 = rect_y + rect_height
                        rect_cx, rect_cy = self.math_functions_obj.find_centre_of_line([[rect_x,rect_y],
                                                                     [rect_x1,rect_y1]])
                        svg_data_rect.append([tag_name,main_id,rect_width,rect_height,
                                         rect_x,rect_y,rect_cx, rect_cy])


            #---- find lines
            elif tag_name == 'line':
                x1 = float(svg_element.attrib['x1'])
                y1 = float(svg_element.attrib['y1'])
                x2 = float(svg_element.attrib['x2'])
                y2 = float(svg_element.attrib['y2'])
                lx,ly = self.math_functions_obj.find_centre_of_line([[x1,y1],[x2,y2]])
                length = math.hypot(x1 - x2, y1 - y2)

                #--- C 02. if line length == rect_max_width_height, which was found in testing
                if length > 17 and length < 40:
                    svg_data_line.append([tag_name, main_id, x1, y1, x2, y2, lx,ly,length])

        #---- take out any similar rects (some svgs have 2 rects per window 1 -white fill, 1- black border
        #---- it's same for <line> too. So trying to remove duplicates)
        svg_data_rect_org, svg_data_rect_repeat = self.remove_similar_rects(svg_data_rect)
        svg_data_line_org, svg_data_line_repeat = self.remove_similar_lines(svg_data_line)


        bb_new_elements = []
        #---- find lines with length == to each rect max side
        for rect_row,svg_rect in enumerate(svg_data_rect_org):
            rect_width = svg_rect[2]
            rect_height = svg_rect[3]
            max_dimension = max(rect_width,rect_height)

            for line_row, svg_line in enumerate(svg_data_line_org):
                line_length = svg_line[-1]
                if abs(line_length - max_dimension) < 2:
                    rect_cx, rect_cy = svg_rect[-2],svg_rect[-1]
                    l_cx, l_cy = svg_line[6],svg_line[7]


                    #--- C 03. find lines in vicinity of rects by
                    #--- finding lines closest to rect centre point
                    distance = math.hypot(rect_cx-l_cx,rect_cy-l_cy)
                    if distance < 0.5:


                        #---- C 04. find lines in similar angle to max(width,height) line
                        x1,y1 = svg_rect[4],svg_rect[5]
                        if max_dimension == rect_width:
                            x2 = x1 + rect_width
                            y2 = y1
                        else:
                            x2 = x1
                            y2 = y1 + rect_height
                        #-- get line cords
                        l_x1,l_y1,l_x2,l_y2 = svg_line[2],svg_line[3],svg_line[4],svg_line[5]
                        #--calculate angle
                        rect_angle = self.math_functions_obj.calculate_angle_vectors(x1, y1, x2, y2)
                        line_angle = self.math_functions_obj.calculate_angle_vectors(l_x1,l_y1,l_x2,l_y2)
                        #--- check angle similarity
                        if abs(rect_angle - line_angle) < 4:
                            id_list = [svg_rect[1], svg_line[1]]

                            #-- get repeated elements also as detections
                            id_list = self.find_repeat_elements(svg_data_rect_repeat,rect_row,id_list)
                            id_list = self.find_repeat_elements(svg_data_line_repeat, line_row, id_list)

                            #--- add all detection to identification list
                            bb_new_elements.append(['window',id_list])

        return bb_new_elements


    def remove_similar_rects(self,svg_data_rect):
        svg_data_rect_org, svg_data_rect_repeat = [], []
        for rect_1, svg_rect in enumerate(svg_data_rect):
            rect_1_id = svg_rect[1]
            rect_width = svg_rect[2]
            rect_height = svg_rect[3]
            rect_x = svg_rect[4]
            rect_y = svg_rect[5]
            if rect_1 == 0:
                svg_data_rect_org.append(svg_rect)
            else:
                found_match = False
                for rect_2, svg_rect_2 in enumerate(svg_data_rect_org):
                    rect_width_2 = svg_rect_2[2]
                    rect_height_2 = svg_rect_2[3]
                    rect_x_2 = svg_rect_2[4]
                    rect_y_2 = svg_rect_2[5]
                    if rect_width == rect_width_2 and rect_height == rect_height_2 and rect_x == rect_x_2 and rect_y == rect_y_2:
                        svg_data_rect_repeat.append([rect_2, svg_rect])
                        found_match = True
                        break
                if found_match == False:
                    svg_data_rect_org.append(svg_rect)

        return svg_data_rect_org, svg_data_rect_repeat

    def remove_similar_lines(self,svg_data_line):
        svg_data_line_org, svg_data_line_repeat = [], []
        for r_1, svg_line in enumerate(svg_data_line):
            x1 = svg_line[2]
            y1 = svg_line[3]
            x2 = svg_line[4]
            y2 = svg_line[5]
            if r_1 == 0:
                svg_data_line_org.append(svg_line)
            else:
                found_match = False
                for r_2, svg_line_2 in enumerate(svg_data_line_org):
                    l2_x1 = svg_line_2[2]
                    l2_y1 = svg_line_2[3]
                    l2_x2 = svg_line_2[4]
                    l2_y2 = svg_line_2[5]
                    if x1 == l2_x1 and y1 == l2_y1 and x2 == l2_x2 and y2 == l2_y2:
                        svg_data_line_repeat.append([r_2, svg_line])
                        found_match = True
                        break
                if found_match == False:
                    svg_data_line_org.append(svg_line)

        return svg_data_line_org, svg_data_line_repeat

    def find_repeat_elements(self,svg_data_repeat,row_id,id_list):
        repeat_indexes = [row[0] for row in svg_data_repeat]
        repeat_ids = -1
        if row_id in repeat_indexes:
            repeat_index = repeat_indexes.index(row_id)
            repeat_ids = svg_data_repeat[repeat_index][1][1]
        if repeat_ids != -1:
            id_list.append(repeat_ids)
        return id_list


    def search_window_pattern_03(self, bb_elements, input_folder_path):
        # --get input svg path
        input_svg_file_path = ''
        # -- folder will contain only 1 image
        for file in os.listdir(input_folder_path + '1_images/input/'):
            file_name = str(file)
            input_svg_file_path = input_folder_path + '1_images/input/' + file_name

        # --- open input svg image
        tree = etree.parse(input_svg_file_path)

        #--- find rects and lines in svg that fulfill some conditions
        svg_data_rect, svg_data_line = [],[]
        for svg_element in tree.iter():
            # -- limits the number of splits to 1
            tag_name = svg_element.tag.split('}', 1)[1]
            main_id = svg_element.attrib['id']

            #---- find lines
            if tag_name == 'line':
                x1 = float(svg_element.attrib['x1'])
                y1 = float(svg_element.attrib['y1'])
                x2 = float(svg_element.attrib['x2'])
                y2 = float(svg_element.attrib['y2'])
                #--calculate line centre point
                l_cx,l_cy = self.math_functions_obj.find_centre_of_line([[x1,y1],[x2,y2]])
                #-- calculate length
                length = math.hypot(x1 - x2, y1 - y2)
                # --calculate angle
                line_angle = self.math_functions_obj.calculate_angle_vectors(x1, y1, x2, y2)

                #--- C 02. if line length == rect_max_width_height, which was found in testing
                if length > 20 and length < 65:
                    svg_data_line.append([tag_name, main_id, x1, y1, x2, y2,
                                          l_cx, l_cy,line_angle,length])

        bb_new_elements = []
        #---- find lines with approx length == to each other

        for line_row, svg_line in enumerate(svg_data_line):
            line_length = svg_line[-1]
            line_angle = svg_line[-2]
            l1_cx, l1_cy = svg_line[6],svg_line[7]
            print svg_line[1]

            for line_row_2, svg_line_2 in enumerate(svg_data_line):
                if line_row_2 > line_row:
                    line_length_2 = svg_line_2[-1]

                    #-- C 03. check if length is approx similar
                    if abs(line_length - line_length_2) < 5:
                        line_angle_2 = svg_line_2[-2]

                        #-- C 04. check if angle is approx similar
                        if abs(line_angle-line_angle_2) < 1.2:


                            #-- C 05. check if line 1 and line 2 is close to each other
                            l2_cx, l2_cy = svg_line_2[6], svg_line_2[7]
                            distance = math.hypot(l1_cx-l2_cx,l1_cy-l2_cy)
                            if distance < 5:
                                print svg_line_2[1]


            print '-------'

        return bb_new_elements
