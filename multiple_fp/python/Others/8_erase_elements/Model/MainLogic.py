__author__ = 'anu'

#--- libraries
import sys,os,traceback
from natsort import natsorted
# import xml.etree.ElementTree as ET
from lxml import etree

class MainFormLogic:

    def __init__(self):
        # current_directory, input_path, output_path, idArray = self.get_path_from_local_machine()
        current_directory, input_path, output_path, output_image_name,idArray = self.get_path_from_js()
        #--- start executing element erase
        self.execution_function(input_path,output_path,output_image_name,idArray)


    def get_path_from_local_machine(self):
        #--/home/ub/Documents/node_js/socketio/python/floor_plan_code
        current_directory = os.path.abspath(os.path.join(os.getcwd(), '..'))
        #--/home/ub/Documents/node_js/socketio/python/floor_plan_code/input_fps/anu/
        input_path = current_directory+ '/input_fps/anu/'
        # input_path = current_directory + '/input_fps/current_working_fps/'
        #--/home/ub/Documents/node_js/socketio/python/floor_plan_code/SVG_results/final_floorplan.svg
        output_path = current_directory+'/Component_Data/erased.svg'
        idArray = 't1,t2,t3'
        # idArray = '10,11,12,13,14,15,16,17'
        return current_directory, input_path, output_path,idArray

    def get_path_from_js(self):
        js_message = sys.stdin.readlines()
        input_path, current_directory,output_path,output_image_name,idArray = '','','','',[]
        for row_num,each in enumerate(js_message):
            if row_num==0:
                current_directory = each.rstrip()
            elif row_num==1:
                input_path = each.rstrip()+'1_images/input/'
            elif row_num==2:
                output_path = each.rstrip()+'1_images/output/'
            elif row_num==3:
                output_image_name = each.rstrip()
            else:
                idArray = each.rstrip()
        return current_directory, input_path, output_path,output_image_name,idArray

    def execution_function(self,input_path,output_path,output_image_name,idArray):
        #---check path exists and get list of files inside given path
        original_fp_path = ''
        if not(os.path.exists(input_path)):
            print 'Directory Not Found.'
            sys.exit()
        else:
            original_fp_path_unsorted = os.listdir(input_path)
            original_fp_path = natsorted(original_fp_path_unsorted)
        #--- take each file in sorted path file list
        for file_name in original_fp_path:
            try:
                print 'Image is : ',file_name
                #--- erase the elements
                self.erase_elements(input_path,output_path,output_image_name,file_name,idArray)


            except(KeyboardInterrupt, SystemExit):
                print "User interrupted!"
                try:
                    sys.exit(0)
                except SystemExit:
                    os._exit(0)

            except:
                print "---------------------Error occured with-------------------------", str(file_name)
                traceback.print_exc()

    def erase_elements(self,input_path,output_path,output_image_name,file_name,idArray):

        #--- since js gives a single string like 't1,t2,t3' we need to split it
        id_list =  idArray.split(',')
        # --- open original svg image
        tree = etree.parse(input_path+ file_name)
        for js_id in id_list:
            #-- find in svg elements_subelements that have an id ==jsid
            element = tree.xpath("//*[@id='"+js_id+"']")
            #-- check if there is no match
            if len(element) != 0:
                for x in element:
                    #-- get parent of selected element and delete child
                    print x.getparent().remove(x)
        # --remove if erased.svg exists
        if os.path.exists(output_path+output_image_name):
            os.remove(output_path+output_image_name)
        #--write erased.svg
        tree.write(output_path+output_image_name)

        # #--- open original svg image
        # file = open(input_path+ file_name, "r")
        # tree = ET.parse(file)
        # root = tree.getroot()
        # #-- get a copy and iterate in it, and use original root to delete elements
        # #-- this is done to ensure that iteration happens correctly withour missing elements
        # root_copy = list(root)
        # for js_id in id_list:
        #     r = tree.xpath()
        #     print r[0].tag
        # # for child in root_copy:
        # #     #--- check if current tag is a 'svg,style,g' sort of non element tag
        # #     tag = child.tag.split("}")[1]
        # #     if tag != 'svg' and tag != 'style' and tag != 'g':
        # #         for js_id in id_list:
        # #             #-- for each element in list passed from js find similar svg elements in image
        # #             if js_id == child.attrib["id"]:
        # #                 root.remove(child)
        # #                 break
        # #--- find namespace of original svg
        # namespace = root.tag.split('}')[0][1:]
        # #--- use same namespace in new svg : otherwise python adds a 'ns0' namesapce
        # ET.register_namespace("", namespace)
        # tree.write(output_path+ 'removed.svg','utf-8')

if __name__ == '__main__':
    hwl1 = MainFormLogic()
