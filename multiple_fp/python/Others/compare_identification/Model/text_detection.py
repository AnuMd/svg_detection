import os,re
from natsort import natsorted

from math_functions import math_functions

class text_detection:
    def __init__(self,current_directory):
        self.text_detection_path = current_directory + '/text_detection/'
        self.math_functions_obj = math_functions()

    def compare_text_detection(self):
        file_list = self.get_matching_file_paths()
        self.find_matching_words(file_list)


    def get_matching_file_paths(self):
        file_list = []
        # ---read png texts
        png_path = self.text_detection_path + 'png/'
        svg_path = self.text_detection_path + 'svg/'
        for raster_text in natsorted(os.listdir(png_path)):
            # ---read svg texts
            if os.path.isfile(svg_path+raster_text):
                matching_svg_text_index = natsorted(os.listdir(svg_path)).index(raster_text)
                svg_text = natsorted(os.listdir(svg_path))[matching_svg_text_index]
                file_list.append([png_path+raster_text,svg_path+svg_text])
        return file_list

    def find_matching_words(self,file_list):
        for file_row in file_list:
            png_file_path = file_row[0]
            svg_file_path = file_row[1]
            png_text_list = self.read_text_data(png_file_path)
            svg_text_list = self.read_text_data(svg_file_path)

            list1,list2 = [],[]
            for png_row in png_text_list:
                png_text = png_row[0]
                png_cx, png_cy = png_row[1]

                for svg_row in svg_text_list:
                    svg_text = svg_row[0]
                    svg_cx, svg_cy = svg_row[1]

                    similar = self.math_functions_obj.measure_closeness_points(png_cx, png_cy,svg_cx, svg_cy,30)
                    if similar:
                        list1.append([png_text,png_row[1],png_row[3],png_row[4]])
                        list2.append([svg_text ,svg_row[1],svg_row[3],svg_row[4]])
            for row in list1:
                print row
            print '-----------'
            for row in list2:
                print row

    def read_text_data(self,path):
        # ---text details extraction from text file
        text_details_file = open(path, 'r')
        text_details_list = []
        for text_line in text_details_file:
            text_line_to_analyze = []
            detail_seperate = text_line.split(':')
            #---since we find all text, it can have words like ':U' or 'KK:' or 'K:K',
            #---need to add ':' to the detected text in that instance
            if len(detail_seperate) == 5:
                text_line_to_analyze = detail_seperate
            elif len(detail_seperate) == 6:
                word =[]
                if len(detail_seperate[0]) == 0:
                    word.append(':' + detail_seperate[1])
                elif len(detail_seperate[1]) == 0:
                    word.append(detail_seperate[0] + ':')
                else:
                    word.append(detail_seperate[0]+':'+detail_seperate[1])

                text_line_to_analyze = [word[0],detail_seperate[2],detail_seperate[3],
                                        detail_seperate[4],detail_seperate[5]]


            if len(text_line_to_analyze)>0:
                # --seperate and convert text cordinate to int
                cordinates_string_2 = re.findall(r"[\w']+", text_line_to_analyze[1])
                int_cordinates = [int(x) for x in cordinates_string_2]
                cordintes_chunks = [int_cordinates[x:x + 2] for x in xrange(0, len(int_cordinates), 2)]
                cordinate_to_append = 0
                for cord in cordintes_chunks:
                    cordinate_to_append = cord

                # --seperate and convert BB-box to int
                bb_cordinates_string_2 = re.findall(r"[\w']+", text_line_to_analyze[2])
                int_bb_cordinates = [int(x) for x in bb_cordinates_string_2]
                bb_cordintes_chunks = [int_bb_cordinates[x:x + 4] for x in xrange(0, len(int_bb_cordinates), 4)]
                bb_cordinate_to_append = 0
                for bb_cord in bb_cordintes_chunks:
                    bb_cordinate_to_append = bb_cord

                confidence_string = re.findall(r"[\w']+", text_line_to_analyze[3])
                confidence = int(confidence_string[0])

                match_data  = re.findall(r"[\w']+", text_line_to_analyze[-1])

                text_details_list.append([text_line_to_analyze[0].strip(),
                                          cordinate_to_append, bb_cordinate_to_append,
                                          confidence,str(match_data[0])])
        text_details_file.close()

        return text_details_list
