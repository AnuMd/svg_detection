import sys

class MainFormLogic:
    def __init__(self):
        current_directory, input_path, output_path = self.get_path_from_js()

    def get_path_from_js(self):
        js_message = sys.stdin.readlines()
        input_path, current_directory,output_path,image_name = '','','',''
        for row_num,each in enumerate(js_message):
            if row_num==0:
                current_directory = each.rstrip()
            elif row_num==1:
                input_path = each.rstrip()+'1_images/input/'
            elif row_num==2:
                output_path = each.rstrip()+'/3_detection_files/'
            # else:
            #     image_name = each.rstrip()+'.svg'
        return current_directory, input_path, output_path

if __name__ == '__main__':
    hwl1 = MainFormLogic()