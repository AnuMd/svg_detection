__author__ = 'anu'

from accessible_fp import accessible_fp_generator

class svg_generator:

    def __init__(self,current_directory,path_output,file_name,avg_door_width,
                 path_gravvitas,path_gravvitas_svg,orginal_img_height, orginal_img_width):
        self.current_directory = current_directory
        self.path_output = path_output
        self.file_name = file_name
        self.avg_door_width = avg_door_width
        self.path_gravvitas = path_gravvitas
        self.path_gravvitas_svg = path_gravvitas_svg
        self.orginal_img_height, self.orginal_img_width = orginal_img_height, orginal_img_width
        # ------pass current_directory and language to 'accessible_fp_generator'
        self.acceessible_obj = accessible_fp_generator(self.current_directory, 'F')

    def generate_all_svg(self):
        all_fp_comps, new_img_height, new_img_width = self.svg_complete_fp_functions()
        self.svg_room_functions(all_fp_comps,new_img_height, new_img_width)




    def svg_complete_fp_functions(self):
        # --- 1. get outer contour as a contour and as a list of lines
        outer_contour, outer_contour_lines = self.acceessible_obj.get_fp_outer_contour(
            self.current_directory, self.path_output, self.file_name, self.avg_door_width)
        # --- 2. crop floorplan to fit only floorplan contour and remove outside space
        # ----- and modify all fp elements to the cropped floorplan
        all_fp_comps, fp_heig_wid, outer_cont_data = self.acceessible_obj.modify_cordinates(
            self.current_directory, self.path_gravvitas, self.file_name, outer_contour,
            outer_contour_lines, self.orginal_img_height, self.orginal_img_width)
        new_img_height, new_img_width = fp_heig_wid
        modified_outer_contour, modified_outer_contour_lines = outer_cont_data
        # --- 3. generate fp text - 'location of entrance door, outside room order, internal rooms list'
        fp_text = self.acceessible_obj.get_room_order(
            self.current_directory, self.file_name, self.path_gravvitas,
            modified_outer_contour, modified_outer_contour_lines,
            new_img_height, new_img_width, all_fp_comps)
        # --- 4. generate building shape
        self.acceessible_obj.get_building_shape(self.current_directory, self.path_gravvitas,
                                                self.file_name, modified_outer_contour,
                                                new_img_height, new_img_width)
        # --- 5. extract walls based on all other fp components
        all_fp_comps = self.acceessible_obj.extract_walls(self.current_directory, self.path_gravvitas,
                                                          modified_outer_contour, new_img_height, new_img_width,
                                                          self.file_name, all_fp_comps)
        # --- 6.draw the whole fp svg
        self.acceessible_obj.draw_svg(self.file_name, self.path_gravvitas_svg, new_img_height,
                                      new_img_width, all_fp_comps, fp_text)

        return all_fp_comps,new_img_height, new_img_width



    def svg_room_functions(self,all_fp_comps,new_img_height, new_img_width):
        # --- 7. categorize fp data by rooms
        room_data = self.acceessible_obj.assign_fp_data_to_rooms(all_fp_comps,
                                                                 self.path_gravvitas,
                                                                 new_img_height, new_img_width)
        # --- 8. crop room image to fit only room and map cordinates to fit in to cropped image
        modified_cordinates = self.acceessible_obj.modify_room_cordinates(room_data)
        # --- 9. generate room svg
        self.acceessible_obj.generate_room_svg(self.file_name, modified_cordinates,
                                               self.path_gravvitas_svg)