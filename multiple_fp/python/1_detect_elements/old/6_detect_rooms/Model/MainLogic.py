__author__ = 'stefano'


import shutil,os,cv2,time, sys, subprocess,getopt,traceback,copy,pyttsx
import numpy as np
from PIL import Image
from natsort import natsorted


from image import Image_class
from graphCC import GraphCC
import imageOperations as imop
from connectedcomponent import CC
from room_functions import room_functions_class
from read_gravvitas_text_files import read_gravvitas_text_files_class




class MainFormLogic():
    #-------initialization method
    def __init__(self):
        self.thresholds = [0,0,0,0,0]
        self.count = 0
        self.name = ''
        self.path_floorplan = ''
        self.converted_floorplan_path = ''
        self.path_output = ''
        self.path_output_d = ''
        self.path_results = ''
        self.path_final_lines = ''
        self.path_gravvitas = ''
        self.path_gravvitas_svg = ''
        self.object_path = ''
        self.object_output_path = ''
        self.current_directory =''
        self.path_component_data = ''
        #--make 'debug_mode' True to see intermediate results-------
        debug_mode = False
        self.read_gravvitas_text_obj = read_gravvitas_text_files_class()


        #----get path
        self.current_directory, self.input_path, self.output_path = self.get_path()


        #--create folders
        self.create_output_folders(self.current_directory,debug_mode)


        #--get list of input floorplans
        list_images_floorplan = self.pre_process_images(self.current_directory)

        #--define weight_lists
        criteria_weight_list = [['line_length', 0.6], ['Gradient', 0.75], ['TxtDistance', 1.0],
                                ['EELengthRatio', 0.4], ['ConvexityDefect', 0.5],['IplWeight',0.5]]

        weight_list = [row[1] for row in criteria_weight_list]

        # --copy back from js text files
        text_filePath = self.output_path + '/3_detection_files/'
        text_files = os.listdir(text_filePath)
        for text_file in text_files:
            if os.path.isfile(text_filePath + str(text_file)):
                shutil.copy(text_filePath + str(text_file), self.path_component_data)

        ##---take each floorplan name from list of images
        for file_name in list_images_floorplan:
            try:
                # # start_image_process_time = time.time()
                print 'Image is : ',file_name
                img = cv2.imread(self.path_floorplan + file_name, cv2.IMREAD_GRAYSCALE)
                orginal_img_height, orginal_img_width = img.shape
                self.floorplan_component_ID(file_name,img,weight_list,self.path_floorplan,orginal_img_height, orginal_img_width,debug_mode)




            except(KeyboardInterrupt,SystemExit):
                print "User interrupted!"
                try:
                    sys.exit(0)
                except SystemExit:
                    os._exit(0)

            except:
                print "---------------------Error occured with-------------------------", str(file_name)
                traceback.print_exc()

        # ---copy text files
        svg_files = os.listdir(self.path_gravvitas)
        for svg_file in svg_files:
            if os.path.isfile(self.path_gravvitas + str(svg_file)):
                if '_9_openPlan_details.txt' in str(svg_file):
                    shutil.copy(self.path_gravvitas + str(svg_file),
                                self.output_path + '3_detection_files/9_openPlan_details.txt')
                elif '_1_room_details.txt' in str(svg_file):
                    shutil.copy(self.path_gravvitas + str(svg_file),
                                self.output_path + '3_detection_files/1_room_details.txt')
                elif '_10_door_width.txt' in str(svg_file):
                    shutil.copy(self.path_gravvitas + str(svg_file),
                                self.output_path + '3_detection_files/10_door_width.txt')

    def create_output_folders(self,current_directory,debug_mode):
        # #create folders to store output - OUTPUT/OUTPUT_DEBUG/RESULTS_TEST/EDGES folders
        # self.path_output=current_directory+'/OUTPUT/'
        # if not(os.path.exists(self.path_output)):
        #         os.mkdir(self.path_output)
        self.path_output_d=current_directory+'/OUTPUT_DEBUG/'
        if not(os.path.exists(self.path_output_d)):
                os.mkdir(self.path_output_d)
        self.path_component_data = current_directory + '/Component_Data/'
        if not (os.path.exists(self.path_component_data)):
            os.mkdir(self.path_component_data)
        #--create place to write text for GraVVITTAS
        self.path_gravvitas=current_directory+'/GraVVITAS/'
        if not(os.path.exists(self.path_gravvitas)):
            os.mkdir(self.path_gravvitas)


        #--incremental folder creation--------------------------------------
        #OUTPUT folder incremental values for folder name
        list_dir_output = os.listdir(self.path_output_d)
        if len(list_dir_output)>0:
            t = [int(x) for x in list_dir_output]
            t.sort()
            name = str(t[len(t)-1]+1)
            self.path_output_d = self.path_output_d+name+'/'
            os.mkdir(self.path_output_d)
        else:
            self.path_output_d = self.path_output_d+str(1)+'/'
            name="1"
            os.mkdir(self.path_output_d)

        # #OUTPUT_DEBUG folder incremental values for folder name
        # if not(os.path.exists(self.path_output_d+ name +"/")):
        #     os.mkdir(self.path_output_d+ name +"/")
        #     self.path_output_d = self.path_output_d+ name +"/"
        # else:
        #     shutil.rmtree(self.path_output_d+ name +"/")
        #     os.mkdir(self.path_output_d+ name +"/")
        #     self.path_output_d = self.path_output_d+ name +"/"

        #GRAVVITAS folder incremental values for folder name
        if not(os.path.exists(self.path_gravvitas+ name +"/")):
            os.mkdir(self.path_gravvitas+ name +"/")
            self.path_gravvitas = self.path_gravvitas+ name +"/"
        else:
            shutil.rmtree(self.path_gravvitas+ name +"/")
            os.mkdir(self.path_gravvitas+ name +"/")
            self.path_gravvitas = self.path_gravvitas+ name +"/"
        self.path_gravvitas=self.path_gravvitas+'/TEXT/'
        if not(os.path.exists(self.path_gravvitas)):
            os.mkdir(self.path_gravvitas)



        #create subfolders inside main OUTPUT and OUTPUT_DEBUG folders
        self.create_path_preprocessing()
        self.create_path_state_files()



    def pre_process_images(self,current_directory):
        input_path_floorplan = self.input_path

        #--check if input folder exists, carry out rest of the program if it does
        if not(os.path.exists(input_path_floorplan)):
            print 'Directory Not Found.'
            sys.exit()
        else:
            # self.path_floorplan = input_path_floorplan
            # print 'input_path_floorplan',input_path_floorplan
            original_fp_path_unsorted = os.listdir(input_path_floorplan)
            original_fp_path = natsorted(original_fp_path_unsorted)
            # for each_image in original_fp_path:


            self.converted_floorplan_path = self.current_directory+'/input_fps/converted_fps/'
            if (os.path.exists(self.converted_floorplan_path)):
                shutil.rmtree(self.converted_floorplan_path)
            os.makedirs(self.converted_floorplan_path)


            for floor in original_fp_path:
                # print 'str(floor)',str(floor)
                if str(floor)=='.DS_Store':
                    os.remove(input_path_floorplan+str(floor))
                else:
                    if floor[-4:]=='.png':
                        shutil.copy(input_path_floorplan+str(floor),self.converted_floorplan_path+str(floor))
                    elif floor[-4:]=='.svg' or floor[-4:]=='.SVG':
                        im_name = str(floor)
                        only_im_name = im_name[:-4]
                        # os.system('convert -density 40 '+input_path_floorplan+str(floor)+' -set density 40 '+self.converted_floorplan_path+only_im_name+'_40.png')
                        # os.system('convert -density 50 '+input_path_floorplan+str(floor)+' -set density 50 '+self.converted_floorplan_path+only_im_name+'_50.png')
                        # os.system('convert -density 60 '+input_path_floorplan+str(floor)+' -set density 60 '+self.converted_floorplan_path+only_im_name+'_60.png')
                        # os.system('convert -density 70 '+input_path_floorplan+str(floor)+' -set density 70 '+self.converted_floorplan_path+only_im_name+'_70.png')
                        # os.system('convert -density 100 '+input_path_floorplan+str(floor)+' -set density 100 '+self.converted_floorplan_path+only_im_name+'_100.png')
                        # os.system('convert -density 150 '+input_path_floorplan+str(floor)+' -set density 150 '+self.converted_floorplan_path+only_im_name+'_150.png')
                        # os.system('convert -density 200 '+input_path_floorplan+str(floor)+' -set density 200 '+self.converted_floorplan_path+only_im_name+'_200.png')
                        os.system('convert -density 250 '+input_path_floorplan+str(floor)+' -set density 250 '+self.converted_floorplan_path+only_im_name+'_250.png')

                    else:
                        im_conv = Image.open(input_path_floorplan+str(floor))
                        im_name = str(floor)
                        only_im_name = im_name[:-4]
                        im_conv.save(self.converted_floorplan_path+only_im_name+'.png')

            self.path_floorplan = self.converted_floorplan_path
            list_images_floorplan_unsorted = os.listdir(self.converted_floorplan_path)
            list_images_floorplan = natsorted(list_images_floorplan_unsorted)

            self.create_path_open_plan(list_images_floorplan)
            return list_images_floorplan

    def floorplan_component_ID(self,file_name,img,weight_list,
                               orginal_img_path,orginal_img_height,
                               orginal_img_width,debug_mode):
        # ---extract only image name without extensions
        only_image_name = file_name[0:-4]
        path = self.path_output_d + only_image_name + '/'
        if not (os.path.exists(path)):
            os.mkdir(path)

        # image_color = cv2.imread(orginal_img_path + file_name, cv2.IMREAD_COLOR)
        #
        # ---------Otsu's thresholding to clean image and remove smoothing effects
        ret2, otsu = cv2.threshold(img, 0, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)
        # cv2.imwrite(self.path_output_d + 'preprocessing/' + only_image_name + '_otsu.png', otsu)

        #---get image dimensions
        dimensions_image = cv2.imread(orginal_img_path + file_name,cv2.IMREAD_GRAYSCALE)
        image_height, image_width =  dimensions_image.shape



        print 're-create detection image'
        room_obj = room_functions_class()
        recreate_image_path = self.path_component_data + only_image_name + '_new_walls_windows_door.png'
        room_obj.recreate_detections(self.path_component_data,self.current_directory,
                                     only_image_name,image_height, image_width,recreate_image_path)

        print 'extract text detection data'
        all_detected_text = self.read_gravvitas_text_obj.read_text_data(self.path_component_data, only_image_name)
        # --find only room names
        text_cordinates = self.read_gravvitas_text_obj.find_dictionary_matched_words(all_detected_text)

        print 'Find door width'
        avg_door_width = room_obj.find_door_width(self.current_directory, recreate_image_path, only_image_name)
        self.write_door_width_to_file(self.path_gravvitas+only_image_name,avg_door_width)

        print 'Finding rooms/partitioning OPs'
        room_obj.all_room_functions(self.current_directory, self.path_output_d, recreate_image_path,
                                    self.path_gravvitas, self.path_final_lines, avg_door_width,
                                    weight_list, only_image_name, otsu, text_cordinates,
                                    debug_mode)


    def write_door_width_to_file(self,path,avg_door_width):
        door_width_file = open(path + '_10_door_width.txt', 'w')
        door_width_file.write(str(avg_door_width))
        door_width_file.close()

    def get_path(self):
        # current_directory, input_path, output_path = self.get_path_from_local_machine()
        current_directory, input_path, output_path = self.get_path_from_js()
        # print 'current_directory',current_directory
        # print 'input_path',input_path
        # print 'output_path',output_path
        return current_directory, input_path, output_path

    def get_path_from_local_machine(self):
        #--/home/ub/Documents/node_js/socketio/python/floor_plan_code
        current_directory = os.path.abspath(os.path.join(os.getcwd(), '..'))
        #--/home/ub/Documents/node_js/socketio/python/floor_plan_code/input_fps/anu/
        input_path = current_directory+ '/input_fps/anu/'
        # input_path = current_directory + '/input_fps/current_working_fps/'
        #--/home/ub/Documents/node_js/socketio/python/floor_plan_code/SVG_results/final_floorplan.svg
        output_path = current_directory+'/Component_Data/'
        return current_directory, input_path, output_path

    def get_path_from_js(self):
        js_message = sys.stdin.readlines()
        input_path, current_directory,output_path = '','',''
        for row_num,each in enumerate(js_message):
            if row_num==0:
                current_directory = each.rstrip()
            elif row_num==1:
                input_path = each.rstrip()+ '1_images/input/'
            else:
                output_path = each.rstrip()
        return current_directory, input_path, output_path












    def create_path_state_files(self):
        state_path = self.path_component_data + 'state/'
        if not (os.path.exists(state_path)):
            os.mkdir(state_path)


    def create_path_preprocessing(self):
        path_preprocessing=self.path_output_d+'preprocessing/'
        if not(os.path.exists(path_preprocessing)):
            os.mkdir(path_preprocessing)


    def create_path_open_plan(self,list_images_floorplan):
        path_open_plan=self.path_output_d+'Rooms/'
        if not(os.path.exists(path_open_plan)):
            os.mkdir(path_open_plan)
        for each_image in list_images_floorplan:
            image_name = each_image[0:-4]
            image_folder_path = path_open_plan+image_name+'/'
            if not(os.path.exists(image_folder_path)):
                os.mkdir(image_folder_path)
                os.mkdir(image_folder_path+'All_Rooms/')
                os.mkdir(image_folder_path+'OpenPlans/')







if __name__ == '__main__':
    start = time.time()
    hwl1 = MainFormLogic()
    end = time.time()
    print end - start
