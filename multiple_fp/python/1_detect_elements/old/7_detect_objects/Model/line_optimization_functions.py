__author__ = 'anu'

import cv2, os,math
import numpy as np
from operator import itemgetter

from open_plan_line import line
from iterative_functions import iterative_functions_class

class line_optimization_function_class:
    def __init__(self):
        self.line_obj = line()
        self.iterative_function_obj = iterative_functions_class()

    def find_approx_straight_line(self,current_line,line_gradient,image_contour_lines):
        new_line = []
        x1, y1 = current_line[0]
        x2, y2 = current_line[1]

        current_x, current_y = x1, y1
        other_x, other_y = x2,y2

        # for cord_num, cord in current_line:
        #     if cord_num==0:
        #         current_x, current_y = x1,y1
        #         other_x, other_y = x2,y2
        #     else:
        #         current_x, current_y = x2, y2
        #         other_x, other_y = x1, y1

        #--find gradient angle
        if line_gradient==0 or (line_gradient < 0.2 and line_gradient > -0.2):
            gradient = 'H'
        else:
            gradient = 'V'

        contour_m, contour_c= 0,0
        sel_line_max_x, sel_line_min_x, sel_line_max_y, sel_line_min_y = 0,0,0,0
        for contour_line in image_contour_lines:
            x3, y3 = contour_line[0]
            x4, y4 = contour_line[1]
            c_line_max_x = max(x3, x4)
            c_line_min_x = min(x3, x4)
            c_line_max_y = max(y3, y4)
            c_line_min_y = min(y3, y4)
            distance_from_text, intersect_x, intersect_y = self.iterative_function_obj.calculate_distance_from_point_to_line([other_x,other_y],contour_line)
            if distance_from_text<2 and (intersect_x >= c_line_min_x and intersect_x <= c_line_max_x and intersect_y >= c_line_min_y and intersect_y <= c_line_max_y):
                contour_m, contour_c = self.iterative_function_obj.find_mc_of_line(x3, y3,x4,y4)
                sel_line_max_x, sel_line_min_x, sel_line_max_y, sel_line_min_y = c_line_max_x,c_line_min_x,c_line_max_y,c_line_min_y

                if gradient=='H':
                    y0 = current_y
                    if contour_m=='a':
                        x0 = sel_line_max_x
                    else:
                        x0 = self.iterative_function_obj.find_other_cord_of_point_on_line('y',y0,contour_m, contour_c)
                else:
                    x0 = current_x
                    y0 = self.iterative_function_obj.find_other_cord_of_point_on_line('x',x0,contour_m, contour_c)

                if (x0 >= sel_line_min_x and x0 <= sel_line_max_x and y0 >= sel_line_min_y and y0 <= sel_line_max_y):
                    new_line = [[current_x,current_y],[int(x0),int(y0)]]
                    break
        return new_line

    def find_lines_not_intersecting_with_contour(self,improved_voronoi_lines_temp,boundaries,open_plan_text_cordinate,image_contour_lines,text_bounding_boxes):
        contour_non_intersect_improved_voronoi_lines = []
        # ----get radius for circle area for text cordinate
        for r, row1 in enumerate(improved_voronoi_lines_temp):
            row_list = []
            text1 = row1[0]
            contour_non_intersect_improved_voronoi_lines.append([])
            contour_non_intersect_improved_voronoi_lines[r].append(row1[0])
            row = row1[1]
            for b, boundary in enumerate(row):
                boundary_list = []
                b_cord_row = []
                for b_row in boundaries:
                    if b_row[0] == row1[0]:
                        b_cord_row = b_row[1]
                boundary_cordinates = b_cord_row[b]
                text_cordinates_to_check = []
                for t_row_num, text_row in enumerate(open_plan_text_cordinate):
                    if t_row_num == text1:
                        text_cordinates_to_check.append(text_row)
                    else:
                        for text2 in boundary_cordinates:
                            if t_row_num == text2:
                                text_cordinates_to_check.append(text_row)
                text_IDs = [text1] + boundary_cordinates
                # for tt in text_IDs:
                #     print text_words[tt]
                for b_num, each_line_set in enumerate(boundary):
                    over_intersect_flag = False
                    for l, each_line in enumerate(each_line_set):
                        if len(each_line)>0:
                            x1, y1 = each_line[0]
                            x2, y2 = each_line[1]
                            i_line_max_x = max(x1, x2)
                            i_line_min_x = min(x1, x2)
                            i_line_max_y = max(y1, y2)
                            i_line_min_y = min(y1, y2)
                            intersects_flag = False
                            for contour_lines in image_contour_lines:
                                x3, y3 = contour_lines[0]
                                x4, y4 = contour_lines[1]
                                c_line_max_x = max(x3, x4)
                                c_line_min_x = min(x3, x4)
                                c_line_max_y = max(y3, y4)
                                c_line_min_y = min(y3, y4)
                                line_obj = line()
                                intersect_x, intersect_y = line_obj.find_contour_intersections(x1, y1, x2, y2, x3, y3, x4,
                                                                                               y4)
                                if intersect_x != 0 and intersect_y != 0 and (
                                                intersect_x > i_line_min_x and intersect_x < i_line_max_x and intersect_y > i_line_min_y and intersect_y < i_line_max_y) and (
                                                intersect_x >= c_line_min_x and intersect_x <= c_line_max_x and intersect_y >= c_line_min_y and intersect_y <= c_line_max_y):
                                    intersects_flag = True
                                    break
                            if intersects_flag:
                                over_intersect_flag = True
                                break
                            else:
                                # -----check if improved_line intersects with the text boundary box
                                # --- START change on 5/03/2017
                                box_lines = []
                                for bb_row_num, current_bb in enumerate(text_bounding_boxes):
                                    validation = False
                                    for tt_num in text_IDs:
                                        if bb_row_num == tt_num:
                                            validation = True
                                            break
                                    if validation:
                                        tx0, ty0, tx1, ty1 = current_bb
                                        box_lines.append([
                                            [[tx0, ty0], [tx1, ty0]],
                                            [[tx1, ty0], [tx1, ty1]],
                                            [[tx1, ty1], [tx0, ty1]],
                                            [[tx0, ty1], [tx0, ty0]]
                                        ])
                                line_obj = line()
                                for bb_line1 in box_lines:
                                    for bb_line in bb_line1:
                                        bb_line_x1, bb_line_y1 = bb_line[0]
                                        bb_line_x2, bb_line_y2 = bb_line[1]
                                        bb_line_max_x = max(bb_line_x1, bb_line_x2)
                                        bb_line_min_x = min(bb_line_x1, bb_line_x2)
                                        bb_line_max_y = max(bb_line_y1, bb_line_y2)
                                        bb_line_min_y = min(bb_line_y1, bb_line_y2)
                                        intersect_x1, intersect_y1 = line_obj.find_contour_intersections(x1, y1, x2, y2,
                                                                                                         bb_line_x1,
                                                                                                         bb_line_y1,
                                                                                                         bb_line_x2,
                                                                                                         bb_line_y2)
                                        if (
                                                        intersect_x1 >= i_line_min_x and intersect_x1 <= i_line_max_x and intersect_y1 >= i_line_min_y and intersect_y1 <= i_line_max_y) and (
                                                        intersect_x1 >= bb_line_min_x and intersect_x1 <= bb_line_max_x and intersect_y1 >= bb_line_min_y and intersect_y1 <= bb_line_max_y):
                                            over_intersect_flag = True
                                            break
                                    if over_intersect_flag:
                                        break
                                        # --- END change on 5/03/2017
                        else:
                            del each_line_set[l]
                    if over_intersect_flag == False:
                        boundary_list.append(each_line_set)
                row_list.append(boundary_list)
            contour_non_intersect_improved_voronoi_lines[r].append(row_list)  # *---------
        return contour_non_intersect_improved_voronoi_lines


    def find_lines_seperating_text(self,contour_non_intersect_improved_voronoi_lines,boundaries,open_plan_text_cordinate,floor_plan_copy,avg_door_width,output_path):
        improved_voronoi_lines = []
        for r, row1 in enumerate(contour_non_intersect_improved_voronoi_lines):
            row_list = []
            text1 = row1[0]
            improved_voronoi_lines.append([])
            improved_voronoi_lines[r].append(row1[0])
            row = row1[1]
            for b, boundary in enumerate(row):
                boundary_list = []
                b_cord_row = []
                for b_row in boundaries:
                    if b_row[0] == row1[0]:
                        b_cord_row = b_row[1]
                boundary_cordinates = b_cord_row[b]
                text_cordinates_to_check = []
                for t_row_num, text_row in enumerate(open_plan_text_cordinate):
                    if t_row_num == text1:
                        text_cordinates_to_check.append(text_row)
                    else:
                        for text2 in boundary_cordinates:
                            if t_row_num == text2:
                                text_cordinates_to_check.append(text_row)
                for b_num, each_line_set in enumerate(boundary):
                    over_intersect_flag = False
                    outer_cont = floor_plan_copy.copy()
                    for l, each_line in enumerate(each_line_set):
                        cv2.line(outer_cont, tuple(each_line[0]), tuple(each_line[1]), [0, 0, 0], 2)
                    cv2.imwrite(output_path + 'test.png', outer_cont)
                    selected_contour_image = cv2.imread(output_path + 'test.png', cv2.IMREAD_GRAYSCALE)
                    ret, thresh = cv2.threshold(selected_contour_image, 0, 255, 1)
                    contours, hierachy = cv2.findContours(thresh, 1, 2)
                    if len(contours) > 2:
                        for num, cnt in enumerate(contours):
                            if num != len(contours) - 1 and cv2.contourArea(cnt) > avg_door_width * 10:
                                text_cordinate_count = 0
                                for each_text in text_cordinates_to_check:
                                    if cv2.pointPolygonTest(cnt, tuple(each_text), False) == 1:
                                        text_cordinate_count += 1
                                if text_cordinate_count == 0 or text_cordinate_count == len(text_cordinates_to_check):
                                    over_intersect_flag = True
                                    break
                    else:
                        over_intersect_flag = True
                    os.remove(output_path + 'test.png')
                    if over_intersect_flag == False:
                        boundary_list.append(each_line_set)
                row_list.append(boundary_list)
            improved_voronoi_lines[r].append(row_list)
        return improved_voronoi_lines


    def partition_cupboard(self, image_contour_lines, text_bounding_boxes, text_words, text_words_row_num,
                              output_path, max_line_floor_plan,avg_door_width):
        max_line = 0
        #-- find & store PL textword, cord, BB
        cupboard_text, cupboard_text_cord,cupboard_text_bb= 0,0,0
        for t_row_num, t_row in enumerate(text_words):
            if t_row_num == text_words_row_num:
                cupboard_text=t_row[0]
                cupboard_text_cord = t_row[1]
                cupboard_text_bb = text_bounding_boxes[t_row_num]

        #--find shortest path(SP) from PL to all contour lines
        #--keep only the lines that has SP intersection point within itself
        contour_lines_with_shortest_paths = self.find_shortest_paths(image_contour_lines,cupboard_text_cord)

        #--find contour lines visible to PL
        #--for each SP if intersects with >1 CL: delete them (not visible)
        visible_contour_lines = self.find_visible_lines(contour_lines_with_shortest_paths,image_contour_lines)

        #---find CL closest to PL
        visible_contour_lines_sorted = sorted(visible_contour_lines, key=itemgetter(3))
        selected_line_data = visible_contour_lines_sorted[0]

        #--extract selected_line data
        selected_cl_number = selected_line_data[0]
        selected_cl = selected_line_data[1]
        sp_of_selected_cl = selected_line_data[2]

        #--- draw line parallel to CL
        #----1.find mirror point to CL's SP (new parallel line should go across this x0,y0)
        x0, y0 = self.find_reflect_line_cords(sp_of_selected_cl)

        #--if mirror point is outside contour: exit method
        contour = self.iterative_function_obj.find_contour_from_contour_lines(image_contour_lines)
        if cv2.pointPolygonTest(contour, tuple(cupboard_text_cord), False) == -1:
            return False, max_line, max_line_floor_plan

        #----2. find mc of CL
        cont_x1, cont_y1 = selected_cl[0]
        cont_x2, cont_y2 = selected_cl[1]
        m, c = self.line_obj.find_mc_of_line(cont_x1,cont_y1,cont_x2,cont_y2)

        #----3. find another point on new parallel line(NPL) and generate virtual NPL
        p1 = [x0, y0]
        p2 = self.find_line_point_using_mc_p1(m,c,p1)
        virtual_new_parallel_line = [p1,p2]

        #---4. find CLs next to selected CL that will intersect with NPL
        #---draw correct new NPL using new contour line intersections
        parallel_line = self.find_parallel_line(
            virtual_new_parallel_line,selected_cl_number,image_contour_lines,avg_door_width)


        #--testing
        # print 'output_path',output_path
        # test_image = ~(np.zeros((3508, 2479, 3), np.uint8))
        # cv2.drawContours(test_image, [contour], -1, (0, 0, 0), 3)
        #
        # test_image1 = test_image.copy()
        # for short_path_row in contour_lines_with_shortest_paths:
        #     sp_line = short_path_row[2]
        #     cv2.line(test_image1, (tuple(sp_line[0])), (tuple(sp_line[1])), (255, 0, 0), 2,
        #              cv2.cv.CV_AA)
        #     cv2.circle(test_image1, (tuple(cupboard_text_cord)), 5, (0, 0, 255), -1)
        # cv2.imwrite(output_path + "step_01.png", test_image1)
        #
        # test_image2 = test_image.copy()
        # for short_path_row in visible_contour_lines:
        #     sp_line = short_path_row[2]
        #     cv2.line(test_image2, (tuple(sp_line[0])), (tuple(sp_line[1])), (255, 0, 0), 2,
        #              cv2.cv.CV_AA)
        #     cv2.circle(test_image2, (tuple(cupboard_text_cord)), 5, (0, 0, 255), -1)
        # cv2.imwrite(output_path + "step_02.png", test_image2)
        #
        # test_image3 = test_image.copy()
        # sp_line = visible_contour_lines_sorted[0][2]
        # cl_line = visible_contour_lines_sorted[0][1]
        # cv2.line(test_image3, (tuple(sp_line[0])), (tuple(sp_line[1])), (255, 0, 0), 2,
        #          cv2.cv.CV_AA)
        # cv2.line(test_image3, (tuple(cl_line[0])), (tuple(cl_line[1])), (255, 255, 0),
        #          2, cv2.cv.CV_AA)
        # cv2.circle(test_image3, (tuple(cupboard_text_cord)), 5, (0, 0, 255), -1)
        # cv2.imwrite(output_path + "step_03.png", test_image3)
        #
        # test_image4 = test_image3.copy()
        # cv2.circle(test_image4, (x0, y0), 5, (0, 0, 0), -1)
        # cv2.circle(test_image4, (tuple(cupboard_text_cord)), 5, (0, 0, 255), -1)
        # cv2.imwrite(output_path + "step_04.png", test_image4)
        #
        # test_image5 = test_image3.copy()
        # cv2.line(test_image5, (tuple(virtual_new_parallel_line[0])), (tuple(virtual_new_parallel_line[1])), (0, 255, 0),
        #                   2, cv2.cv.CV_AA)
        # cv2.circle(test_image5, (x0, y0), 5, (0, 0, 0), -1)
        # cv2.circle(test_image5, (tuple(cupboard_text_cord)), 5, (0, 0, 255), -1)
        # cv2.imwrite(output_path + "step_05.png", test_image5)
        #
        # test_image6 = test_image3.copy()
        # cv2.line(test_image6, (tuple(parallel_line[0])), (tuple(parallel_line[1])), (0, 0, 255),
        #          2,cv2.cv.CV_AA)
        # cv2.line(test_image6, (tuple(virtual_new_parallel_line[0])), (tuple(virtual_new_parallel_line[1])), (0, 255, 0),
        #          2, cv2.cv.CV_AA)
        # cv2.circle(test_image6, (x0, y0), 5, (0, 0, 0), -1)
        # cv2.circle(test_image6, (tuple(cupboard_text_cord)), 5, (0, 0, 255), -1)
        # cv2.imwrite(output_path + "step_06.png", test_image6)
        #--end testing


        # -- draw on max_line image
        cv2.line(max_line_floor_plan, (tuple(parallel_line[0])), (tuple(parallel_line[1])), (0, 0, 0), 2,
                 cv2.cv.CV_AA)
        # -- send data to be processed in next stage
        max_line = [parallel_line]
        return True, max_line, max_line_floor_plan

    def find_shortest_paths(self,contour_lines,point):
        contour_lines_with_shortest_paths = []
        for line_number,each_contour_line in enumerate(contour_lines):
            x1, y1 = each_contour_line[0]
            x2, y2 = each_contour_line[1]
            max_x = max(x1, x2)
            min_x = min(x1, x2)
            max_y = max(y1, y2)
            min_y = min(y1, y2)
            x0, y0 = point

            # ----find a,b (intersection point from x0,y0 to line)
            if x1 == x2:
                intersect_x = x1
                intersect_y = y0
            else:
                m = (y2 - y1) / (x2 - x1)
                if y1 == y2:
                    c = y1
                else:
                    c = y2 - (m * x2)
                intersect_x = (x0 + (m * y0) - (m * c)) / (math.pow(m, 2) + 1)
                intersect_y = (m * intersect_x) + c
            # ----find if intersection point is on the line segment
            if (intersect_x >= min_x and intersect_x <= max_x and intersect_y >= min_y and intersect_y <= max_y):
                shortest_path_line = [[x0,y0],[int(intersect_x),int(intersect_y)]]
                distance_from_text = math.hypot(intersect_x - x0, intersect_y - y0)
                contour_lines_with_shortest_paths.append([line_number,each_contour_line,shortest_path_line,distance_from_text])

        return contour_lines_with_shortest_paths

    def find_visible_lines(self,contour_lines_with_shortest_paths,image_contour_lines):
        visible_contour_lines = []
        for original_list_row in contour_lines_with_shortest_paths:
            shortest_path_line = original_list_row[2]
            x1, y1 = shortest_path_line[0]
            x2, y2 = shortest_path_line[1]

            max_sx = max(x1, x2)
            min_sx = min(x1, x2)
            max_sy = max(y1, y2)
            min_sy = min(y1, y2)
            #--count how many contour lines intersects with sp
            contour_intersection_count = 0
            for each_contour_line in image_contour_lines:
                x3, y3 = each_contour_line[0]
                x4, y4 = each_contour_line[1]
                max_cx = max(x3, x4)
                min_cx = min(x3, x4)
                max_cy = max(y3, y4)
                min_cy = min(y3, y4)
                intersect_x, intersect_y = self.line_obj.find_contour_intersections(x1,y1,x2,y2,x3,y3,x4,y4)
                if (intersect_x >= min_sx and intersect_x <= max_sx and intersect_y >= min_sy and intersect_y <= max_sy) and (
                    intersect_x >= min_cx and intersect_x <= max_cx and intersect_y >= min_cy and intersect_y <= max_cy):
                    contour_intersection_count = contour_intersection_count+1
            #--since sp will intersect with it's related CL once we are checking for 1 intersection
            if contour_intersection_count == 1:
                visible_contour_lines.append(original_list_row)

        return visible_contour_lines

    def find_reflect_line_cords(self, current_line):
        x1, y1 = current_line[0]
        x2, y2 = current_line[1]

        x0 = 2*x1-x2
        y0 = 2*y1-y2

        return x0,y0

    def find_line_point_using_mc_p1(self, m, c, p1):
        p1_x, p1_y = p1

        #--if vertical line
        if m=='a':
            p2_x = p1_x
            p2_y = p1_y - 100
        #--if horizontal line
        elif m==0:
            p2_x = p1_x - 100
            p2_y = p1_y
        else:
            p2_x = p1_x - 100
            p2_y = m*p2_x + c

        p2 = [p2_x,p2_y]
        return p2

    def find_parallel_line(self,new_parallel_line,selected_cl_number,contour_lines,avg_door_width):
        #---find selected_cl from image_contour_lines_list
        #---create 2 lists cont_before_cl, cont_after_cl
        #---combine list so each list will contain all contour lines with cl as starting point
        cont_before_cl = list(reversed(contour_lines[:selected_cl_number]))+list(
                                reversed(contour_lines[selected_cl_number + 1:]))
        #---+1 to exclude selected_cl
        cont_after_cl = contour_lines[selected_cl_number+1:]+contour_lines[:selected_cl_number]

        #--find first intersection with 'cont_before_cl'
        npl_p1,p1_other_interesting_contour_lines = self.find_first_intersection_contour_and_fixed_line(new_parallel_line,cont_before_cl)
        npl_p1_replaceable, point1 = self.check_if_npl_point_is_replaceable(p1_other_interesting_contour_lines,avg_door_width)
        if npl_p1_replaceable:
            npl_p1 = point1

        # --find first intersection with 'cont_after_cl'
        npl_p2,p2_other_interesting_contour_lines = self.find_first_intersection_contour_and_fixed_line(new_parallel_line, cont_after_cl)
        npl_p2_replaceable, point2 = self.check_if_npl_point_is_replaceable(p2_other_interesting_contour_lines, avg_door_width)
        if npl_p2_replaceable:
            npl_p2 = point2

        return [npl_p1,npl_p2]

    def find_first_intersection_contour_and_fixed_line(self,fixed_line, contour_lines):
        intersection_point = 0
        other_interesting_contour_lines = []
        # -- get fixed_line cords
        x1, y1 = fixed_line[0]
        x2, y2 = fixed_line[1]
        #--find first intersection
        for each_contour_line in contour_lines:
            x3, y3 = each_contour_line[0]
            x4, y4 = each_contour_line[1]
            max_cx = max(x3, x4)
            min_cx = min(x3, x4)
            max_cy = max(y3, y4)
            min_cy = min(y3, y4)
            intersect_x, intersect_y = self.line_obj.find_contour_intersections(x1, y1, x2, y2, x3, y3, x4, y4)
            if (intersect_x >= min_cx and intersect_x <= max_cx and intersect_y >= min_cy and intersect_y <= max_cy):
                intersection_point = [intersect_x, intersect_y]
                #---check if [intersection_point,mirror_point] intersects with >1 contour lines
                #---main_point == mirror_point
                main_point = [x1,y1]
                line_intersects = self.find_if_line_intersects_with_contour([main_point,intersection_point],contour_lines)
                if line_intersects==False:
                    break
            else:
                distance_from_line_p1 = math.hypot(intersect_x - x3, intersect_y - y3)
                distance_from_line_p2 = math.hypot(intersect_x - x4, intersect_y - y4)
                if distance_from_line_p1<distance_from_line_p2:
                    other_interesting_contour_lines.append(
                        [each_contour_line,[x3,y3],distance_from_line_p1])
                else:
                    other_interesting_contour_lines.append(
                        [each_contour_line, [x4, y4], distance_from_line_p2])

        return intersection_point, other_interesting_contour_lines

    def find_if_line_intersects_with_contour(self,line_to_check, contour_lines):
        line_intersects = False
        x1, y1 = line_to_check[0]
        x2, y2 = line_to_check[1]
        max_sx = max(x1, x2)
        min_sx = min(x1, x2)
        max_sy = max(y1, y2)
        min_sy = min(y1, y2)
        # --count how many contour lines intersects with sp
        contour_intersection_count = 0
        for each_contour_line in contour_lines:
            x3, y3 = each_contour_line[0]
            x4, y4 = each_contour_line[1]
            max_cx = max(x3, x4)
            min_cx = min(x3, x4)
            max_cy = max(y3, y4)
            min_cy = min(y3, y4)
            intersect_x, intersect_y = self.line_obj.find_contour_intersections(x1, y1, x2, y2, x3, y3, x4, y4)
            if (intersect_x >= min_sx and intersect_x <= max_sx and intersect_y >= min_sy and intersect_y <= max_sy) and (
                                    intersect_x >= min_cx and intersect_x <= max_cx and intersect_y >= min_cy and intersect_y <= max_cy):
                contour_intersection_count = contour_intersection_count + 1
        # --since sp will intersect with it's related CL once we are checking for 1 intersection
        if contour_intersection_count> 1:
            line_intersects = True

        return line_intersects



    def check_if_npl_point_is_replaceable(self,other_interesting_contour_lines,avg_door_width):
        replaceable, point = False, 0
        if len(other_interesting_contour_lines)>1:
            # --sort other_interesting_contour_lines by distance to possible NPL
            other_interesting_contour_lines_sorted = sorted(
                other_interesting_contour_lines, key=itemgetter(2))

            # --check if 'other_interesting_contour_lines' has a line that is very close to NPL
            if other_interesting_contour_lines_sorted[0][2] < avg_door_width/10:
                point = other_interesting_contour_lines_sorted[0][1]
                replaceable = True

        return replaceable,point

    def draw_line_from_point_to_line(self,each_line,point):
        x1, y1 = each_line[0]
        x2, y2 = each_line[1]
        max_x = max(x1, x2)
        min_x = min(x1, x2)
        max_y = max(y1, y2)
        min_y = min(y1, y2)
        x0, y0 = point

        # ----find a,b (intersection point from x0,y0 to line)
        if x1 == x2:
            intersect_x = x1
            intersect_y = y0
        else:
            m = (y2 - y1) / (x2 - x1)
            if y1 == y2:
                c = y1
            else:
                c = y2 - (m * x2)
            intersect_x = (x0 + (m * y0) - (m * c)) / (math.pow(m, 2) + 1)
            intersect_y = (m * intersect_x) + c

        # ----find if intersection point is on the line segment
        if (intersect_x >= min_x and intersect_x <= max_x and intersect_y >= min_y and intersect_y <= max_y):
            contx,conty = intersect_x,intersect_y
        else:
            contx = (x1 + x2) / 2
            conty = (y1 + y2) / 2

        return contx,conty


    def find_relevant_shortest_path(self,text1,text2,shortest_path_data):
        shortest_path = []
        for r_count, row in enumerate(shortest_path_data):
            if (row[0] == text1 and row[1] == text2) or (row[0] == text2 and row[1] == text1):
                shortest_path = row[2:]
                break
        return shortest_path

    def find_lines_intersect_with_sp(self,shortest_path,edge_extensions):
         sp_intersect_eel = []
         for ee_line_num,ee_line in enumerate(edge_extensions):
             x1, y1 = ee_line[0]
             x2, y2 = ee_line[1]
             edge_max_x = max(x1, x2)
             edge_min_x = min(x1, x2)
             edge_max_y = max(y1, y2)
             edge_min_y = min(y1, y2)
             for sp_line in shortest_path:
                 x3, y3 = sp_line[0]
                 x4, y4 = sp_line[1]
                 max_x = max(x3, x4)
                 min_x = min(x3, x4)
                 max_y = max(y3, y4)
                 min_y = min(y3, y4)
                 intersect_x, intersect_y = self.line_obj.find_contour_intersections(x1, y1, x2, y2, x3, y3, x4, y4)
                 if intersect_x >= min_x and intersect_x <= max_x and intersect_y >= min_y and intersect_y <= max_y:
                     if intersect_x >= edge_min_x and intersect_x <= edge_max_x and intersect_y >= edge_min_y and intersect_y <= edge_max_y:
                         sp_intersect_eel.append([ee_line,ee_line_num])
                         break

         return sp_intersect_eel

    def check_if_line_repeats(self,sp_intersect_eel_combined,sp_intersect_eel):
        for new_row in sp_intersect_eel:
            new_line_number = new_row[1]
            line_exists = False
            for old_row in sp_intersect_eel_combined:
                old_line_number = old_row[1]
                if old_line_number == new_line_number:
                    line_exists = True
                    break
            if line_exists == False:
                sp_intersect_eel_combined.append(new_row)
        return sp_intersect_eel_combined

    def find_lines_partitioning_relevant_text(self,text1,text2_set,sp_intersect_eel,
                                              image_contour_lines,text_cordinates,
                                              img_height,img_width,avg_door_width):
        eel_partitioning_text = []
        #--find cordinates_relevant to text1 and text2_set
        text1_cord = text_cordinates[text1]
        text2_set_cords = []
        for text2 in text2_set:
            text2_set_cords.append(text_cordinates[text2])

        # --draw all contour lines to a image
        temp_image = ~(np.zeros((img_height, img_width, 3), np.uint8))
        for cont_line in image_contour_lines:
            cv2.line(temp_image, (tuple(cont_line[0])), (tuple(cont_line[1])), (0, 0, 0), 2, cv2.cv.CV_AA)
        # --check if each EEL partitions text
        for row_num, valid_line in enumerate(sp_intersect_eel):
            line_cord = valid_line[0]
            temp_image_copy = temp_image.copy()
            cv2.line(temp_image_copy, (tuple(line_cord[0])), (tuple(line_cord[1])), (0, 0, 0), 2, cv2.cv.CV_AA)

            # --read image with new line drawn
            temp_image_copy_gray = cv2.cvtColor(temp_image_copy, cv2.COLOR_RGB2GRAY)
            ret, thresh = cv2.threshold(temp_image_copy_gray, 0, 255, 1)
            contours, hierachy = cv2.findContours(thresh, 1, 2)

            text1_partititions = False
            text2_count = 0
            # --check how many contours it create
            if len(contours) > 2:
                for num, cnt in enumerate(contours):
                    if num != len(contours) - 1 and cv2.contourArea(cnt) > avg_door_width * 10:
                        if cv2.pointPolygonTest(cnt, tuple(text1_cord), False) == 1:
                            text1_partititions = True
                        else:
                            for text2_cord in text2_set_cords:
                                if cv2.pointPolygonTest(cnt, tuple(text2_cord), False) == 1:
                                    text2_count = text2_count+1

            # --insert lines that met condition
            if text1_partititions and text2_count==len(text2_set_cords):
                eel_partitioning_text.append(valid_line)

        return eel_partitioning_text



    #--functions from line_optimization straight out
    def order_voronoi_in_cont_order(self,partition_area_lines,voronoi_data):
        voronoi_lines_in_contour_line_order = []
        count = 0
        iterative_obj = iterative_functions_class()
        # --------order the voronoi lines in order of contour lines so that later on voronoi lines can be drawn easily
        for cont, contour in enumerate(partition_area_lines):
            # --take contourID as contour_text_ID
            contour_text_ID = contour[-1]
            voronoi_lines_in_contour_line_order.append([])
            # --add contourID to array
            voronoi_lines_in_contour_line_order[count].append(contour_text_ID)
            # -----iterate through contour[0] because all line details are appended as a single element (elelment 0) in to line
            num_of_lines = len(contour[0])
            # --take contour lines 1 by 1
            for l1, line1 in enumerate(contour[0]):
                x1, y1 = line1[0]
                x2, y2 = line1[1]
                m1, c1 = iterative_obj.find_mc_of_line(x1, y1, x2, y2)
                # --per contour line, compare with voronoi data
                for cont2, contour2 in enumerate(voronoi_data):
                    # --take 2 textIDs in voronoi row
                    text1 = contour2[0]
                    text2 = contour2[1]
                    # --if current contour(contour_text_ID)==text1: we are comparing same contour and its related voronoi lines
                    if contour_text_ID == text1 or contour_text_ID == text2:
                        # --take voronoi line data
                        for line2 in contour2[2:]:
                            x3, y3 = line2[0]
                            x4, y4 = line2[1]
                            m2, c2 = iterative_obj.find_mc_of_line(x3, y3, x4, y4)
                            # --check if current_contour line and current_voronoi is paralell and very close
                            voronoi_line = False
                            if (m1 == 'a' and m2 == 'a') and (abs(x1 - x3) <= 2):
                                voronoi_line = iterative_obj.find_line_subsets(line1, line2)
                            elif (m1 != 'a' and m2 != 'a') and abs(m1 - m2) < 0.02 and (abs(c1 - c2) <= 2):
                                voronoi_line = iterative_obj.find_line_subsets(line1, line2)
                            # ----in line & line2 if gradient is appprox similar, close to each other: add as voronoi
                            if voronoi_line:
                                # --add voronoi line, textID related to it, line number for later use
                                if contour_text_ID == text1:
                                    voronoi_lines_in_contour_line_order[count].append([line1, text2, l1])
                                elif contour_text_ID == text2:
                                    voronoi_lines_in_contour_line_order[count].append([line1, text1, l1])
            voronoi_lines_in_contour_line_order[count].append(num_of_lines)
            count = count + 1

        return voronoi_lines_in_contour_line_order

    def find_boundaries(self,voronoi_lines_in_contour_line_order):
        seperated_voronoi_lines = []
        boundaries = []
        for r, row in enumerate(voronoi_lines_in_contour_line_order):
            text1 = row[0]
            line_data = row[1:-1]
            temp_array_vor = []
            temp_boundaries = []
            last_line_number, last_textID = -1, -1
            x1, y1, x2, y2, x3, y3, x4, y4 = 0, 0, 0, 0, 0, 0, 0, 0
            # ---boundaries list creation
            boundaries.append([])
            boundaries[r].append(text1)
            # ---seperated_voronoi_lines list creation
            seperated_voronoi_lines.append([])
            seperated_voronoi_lines[r].append(text1)
            for e, element in enumerate(line_data):
                # --take cords of voronoi line
                current_line = element[0]
                x1, y1 = current_line[0]
                x2, y2 = current_line[1]
                # --take textID current voronoi line is connected to
                text_ID = element[1]
                # --line number of voronoi line (according to contour line order)
                line_number = element[2]
                # --if first element: just add everything to 2 arrays
                if e == 0:
                    temp_array_vor.append([current_line])
                    temp_boundaries.append([text_ID])
                    last_line_number = line_number
                    x3, y3, x4, y4 = x1, y1, x2, y2

                else:
                    # ---check if last line in array and is sharing a cordinate with very first line (e==0)
                    if e == len(row[1:-1]) - 1 and (
                        (x1 == x3 and y1 == y3 or x1 == x4 and y1 == y4) or (
                                x2 == x4 and y2 == y4 or x2 == x3 and y2 == y3)):
                        if len(row[1:-1]) == 2:
                            if (line_number - last_line_number) == 1:
                                temp_array_vor[-1].append(current_line)
                                exists_flag = False
                                for t_row1 in temp_boundaries[-1]:
                                    if t_row1 == text_ID:
                                        exists_flag = True
                                        break
                                if exists_flag == False:
                                    temp_boundaries[-1].append(text_ID)
                            else:
                                temp_array_vor[0].insert(0, (current_line))
                                exists_flag = False
                                for t_row1 in temp_boundaries[0]:
                                    if t_row1 == text_ID:
                                        exists_flag = True
                                        break
                                if exists_flag == False:
                                    temp_boundaries[0].insert(0, text_ID)
                        # -----check is this last line is also connected to line before it
                        elif (line_number - last_line_number) <= 1:
                            # -----insert current line to last element in temp_array_vor
                            if (line_number - last_line_number) == 1:
                                temp_array_vor[-1].append(current_line)
                            # ---reverse alst element in temp_array_vor, & add each line to temp_array_vor[0]
                            for each_el in list(reversed(temp_array_vor[-1])):
                                temp_array_vor[0].insert(0, each_el)
                            # -----delete last element
                            del temp_array_vor[-1]

                            # -----------------textID array change----------
                            # -------take all textIDs from last element and put them in first one too
                            # -------do the same for text_cordinates array
                            all_last_boundary_texts = temp_boundaries[-1]
                            texts = []
                            for text_num, last_boundary_text in enumerate(list(reversed(all_last_boundary_texts))):
                                exists_flag = False
                                for t_row1 in temp_boundaries[0]:
                                    if t_row1 == last_boundary_text:
                                        exists_flag = True
                                if exists_flag == False:
                                    temp_boundaries[0].insert(0, last_boundary_text)
                                    texts.append(last_boundary_text)
                            if len(list(set(all_last_boundary_texts) - set(texts))) == 0:
                                del temp_boundaries[-1]
                            else:
                                temp_boundaries[-1] == list(set(all_last_boundary_texts) - set(texts))
                        # -------if last line is connected to first line but is not connected to line taken in array immediately before
                        else:
                            # ---add current line to temp_array_vor[0]
                            temp_array_vor[0].insert(0, current_line)
                            exists_flag = False
                            for t_row1 in temp_boundaries[0]:
                                if t_row1 == text_ID:
                                    exists_flag = True
                                    break
                            if exists_flag == False:
                                temp_boundaries[0].insert(0, text_ID)
                        # -----common to both if/else
                        last_line_number = line_number
                    # --------if NOT last line but last entered line and current line is connected
                    elif (line_number - last_line_number) <= 1:
                        if (line_number - last_line_number) == 1:
                            # if ~((x1==x3 and y1==y3 or x1==x4 and y1==y4) or (x2==x4 and y2==y4  or x2==x3 and y2==y3)):
                            temp_array_vor[-1].append(current_line)
                        exists_flag = False
                        for t_row1 in temp_boundaries[-1]:
                            if t_row1 == text_ID:
                                exists_flag = True
                                break
                        if exists_flag == False:
                            temp_boundaries[-1].append(text_ID)
                        last_line_number = line_number
                    # ---- if NOT last line but is creating a new/seperate boundary
                    else:
                        temp_array_vor.append([current_line])
                        temp_boundaries.append([text_ID])
                        last_line_number = line_number
            seperated_voronoi_lines[r].append(temp_array_vor)
            boundaries[r].append(temp_boundaries)

        return seperated_voronoi_lines,boundaries


    def find_special_cases(self,text_words,current_directory):
        corridor_list, room_list, wc_list, bath_list, furniture_list = [], [], [], [], []
        for row_num, row in enumerate(text_words):
            furniture_words_list = self.iterative_function_obj.get_list_from_text_files(
                current_directory + '/Model/floor_plan_text/furniture_list.txt')
            non_open_plan_rooms_words_list = self.iterative_function_obj.get_list_from_text_files(
                current_directory + '/Model/floor_plan_text/non_open_plan_rooms_list.txt')
            wc_words_list = self.iterative_function_obj.get_list_from_text_files(
                current_directory + '/Model/floor_plan_text/wc_list.txt')
            bath_words_list = self.iterative_function_obj.get_list_from_text_files(
                current_directory + '/Model/floor_plan_text/bath_list.txt')
            corridor_words_list = self.iterative_function_obj.get_list_from_text_files(
                current_directory + '/Model/floor_plan_text/corridor_list.txt')


            if row[0] in furniture_words_list:
                furniture_list.append(row_num)
            elif row[0] in non_open_plan_rooms_words_list:
                room_list.append(row_num)
            elif row[0] in wc_words_list:
                wc_list.append(row_num)
            elif row[0] in bath_words_list:
                bath_list.append(row_num)
            elif row[0] in corridor_words_list:
                corridor_list.append(row_num)


        text_categorized_lists = [corridor_list,room_list,wc_list, bath_list, furniture_list]
        return text_categorized_lists


    def populate_lists_by_room_type(self,text_categorized_lists,seperated_voronoi_lines,boundaries):
        corridor_list, room_list, wc_list, bath_list, furniture_list = text_categorized_lists
        seperated_voronoi_lines_copy = seperated_voronoi_lines
        seperated_voronoi_lines = []
        boundaries_copy = boundaries
        boundaries = []
        line_obj = line()

        room_type = 0
        if len(furniture_list) > 0:
            for row in furniture_list:
                seperated_voronoi_lines, boundaries = line_obj.new_list_creation2(seperated_voronoi_lines_copy,
                                                                                  boundaries_copy,
                                                                                  seperated_voronoi_lines, boundaries,
                                                                                  row)
                room_type = 'furniture'
        elif len(room_list) > 0:
            for row in room_list:
                seperated_voronoi_lines, boundaries = line_obj.new_list_creation2(seperated_voronoi_lines_copy,
                                                                                  boundaries_copy,
                                                                                  seperated_voronoi_lines, boundaries,
                                                                                  row)
                room_type = 'NOPRoom'
        elif len(wc_list) > 0:
            for row in wc_list:
                seperated_voronoi_lines, boundaries = line_obj.new_list_creation2(seperated_voronoi_lines_copy,
                                                                                  boundaries_copy,
                                                                                  seperated_voronoi_lines, boundaries,
                                                                                  row)
                room_type = 'wc'
        elif len(bath_list) > 0:
            for row in bath_list:
                seperated_voronoi_lines, boundaries = line_obj.new_list_creation2(seperated_voronoi_lines_copy,
                                                                                  boundaries_copy,
                                                                                  seperated_voronoi_lines, boundaries,
                                                                                  row)
                room_type = 'bath'
        elif len(corridor_list) > 0:
            for row in corridor_list:
                seperated_voronoi_lines, boundaries = line_obj.new_list_creation2(seperated_voronoi_lines_copy,
                                                                                  boundaries_copy,
                                                                                  seperated_voronoi_lines, boundaries,
                                                                                  row)
                room_type = 'corridor'
        else:
            seperated_voronoi_lines = seperated_voronoi_lines_copy
            boundaries = boundaries_copy
            room_type = 'OP'

        return seperated_voronoi_lines,boundaries,room_type


    def calculate_voronoi_score(self,voronoi_penalties):
        voronoi_score = []
        for row_num, voronoi_row in enumerate(voronoi_penalties):
            voronoi_score.append([])
            #--append text1
            voronoi_score[row_num].append(voronoi_row[0])
            all_scores = []
            for i in range(0,len(voronoi_row[1])):
                all_scores.append([0])
            voronoi_score[row_num].append(all_scores)
        return voronoi_score

    def calculate_eel_score(self,voronoi_penalties,edge_extension_penalties):
        edge_extension_scores = []
        for row_num, voronoi_row in enumerate(voronoi_penalties):
            text1 = voronoi_row[0]
            edge_extension_scores.append([])
            edge_extension_scores[row_num].append(text1)
            all_scores = []
            for boundary_num, boundary in enumerate(voronoi_row[1]):
                voronoi_boundary_penalty = boundary[0]
                #---from edge_extension_penalties find row ==text1
                row_edge_penalties = []
                for row in edge_extension_penalties:
                    if row[0] == text1:
                        row_edge_penalties = row[1]
                        break
                boundary_edge_penalties = row_edge_penalties[boundary_num]
                boundary_scores =[]
                for edge_penalty in boundary_edge_penalties:
                    boundary_scores.append(voronoi_boundary_penalty - edge_penalty)
                all_scores.append(boundary_scores)
            edge_extension_scores[row_num].append(all_scores)

        return edge_extension_scores

    def calculate_ivl_score(self,voronoi_penalties,improved_voronoi_penalties):
        improved_voronoi_scores = []
        for row_num, voronoi_row in enumerate(voronoi_penalties):
            text1 = voronoi_row[0]
            improved_voronoi_scores.append([])
            improved_voronoi_scores[row_num].append(text1)
            all_scores = []
            for boundary_num, boundary in enumerate(voronoi_row[1]):
                voronoi_boundary_penalty = boundary[0]

                row_ivl_penalties = []
                for row in improved_voronoi_penalties:
                    if row[0] == text1:
                        row_ivl_penalties = row[1]
                        break
                boundary_ivl_penalties = row_ivl_penalties[boundary_num]
                boundary_scores = []
                for ivl_penalty_element in boundary_ivl_penalties:
                    ivl_penalty = ivl_penalty_element[0]
                    boundary_scores.append([voronoi_boundary_penalty - ivl_penalty])
                all_scores.append(boundary_scores)
            improved_voronoi_scores[row_num].append(all_scores)
        return improved_voronoi_scores

    def calculate_ipl_score(self,voronoi_penalties,ipl_penalties):
        ipl_scores = []
        for row_num, voronoi_row in enumerate(voronoi_penalties):
            text1 = voronoi_row[0]
            ipl_scores.append([])
            ipl_scores[row_num].append(text1)
            all_scores = []
            for boundary_num, boundary in enumerate(voronoi_row[1]):
                voronoi_boundary_penalty = boundary[0]
                #---from edge_extension_penalties find row ==text1
                row_ipl_penalties = []
                for row in ipl_penalties:
                    if row[0] == text1:
                        row_ipl_penalties = row[1]
                        break
                boundary_ipl_penalties = row_ipl_penalties[boundary_num]
                boundary_scores =[]
                for ipl_penalty in boundary_ipl_penalties:
                    boundary_scores.append(voronoi_boundary_penalty - ipl_penalty)
                all_scores.append(boundary_scores)
            ipl_scores[row_num].append(all_scores)

        return ipl_scores


    def print_all_scores(self,boundaries,voronoi_scores,edge_extension_scores,
                         improved_voronoi_scores,seperated_voronoi_lines,
                         seperated_edge_lines,improved_voronoi_lines,
                         seperated_ipl_lines,ipl_scores,
                         open_plan_text_cordinate,score_path):
        y_start_point = 2000
        self.print_voronoi_lines_nd_scores(boundaries,voronoi_scores,
            seperated_voronoi_lines,open_plan_text_cordinate,score_path)
        self.print_EE_lines_nd_scores(voronoi_scores,seperated_edge_lines,
            edge_extension_scores,score_path,y_start_point)
        self.print_iv_lines_nd_scores(voronoi_scores,improved_voronoi_lines,
            improved_voronoi_scores,score_path,y_start_point)
        self.print_ipl_lines_nd_scores(voronoi_scores,seperated_ipl_lines,
            ipl_scores,score_path,y_start_point)


    def print_voronoi_lines_nd_scores(self,boundaries,voronoi_scores,
          seperated_voronoi_lines,open_plan_text_cordinate,score_path):
        for row_num, voronoi_line in enumerate(voronoi_scores):
            text1 = voronoi_line[0]
            for boundary_num, boundary_score in enumerate(voronoi_line[1]):
                floor_plan_boundaries = cv2.imread(
                        score_path + 'Partition ' + str(text1) + ' Boundary ' + str(boundary_num) + 'Line Scores.png',
                        cv2.IMREAD_COLOR)
                text1 = voronoi_line[0]
                b_row = []
                for row in boundaries:
                    if row[0] == text1:
                        b_row = row[1]
                        break
                text2s = b_row[boundary_num]

                text1_cord = open_plan_text_cordinate[text1]
                cv2.circle(floor_plan_boundaries, (tuple(text1_cord)), 7, (0, 0, 255), -1)
                for text2 in text2s:
                    text2_cord = open_plan_text_cordinate[text2]
                    cv2.circle(floor_plan_boundaries, (tuple(text2_cord)), 7, (0, 0, 0), -1)
                # ------ for display purposes draw the partition on image
                vor_row_line = []
                for row in seperated_voronoi_lines:
                    if row[0] == text1:
                        vor_row_line = row[1]
                        break
                for new_b in vor_row_line:
                    for new_l in new_b:
                        cv2.line(floor_plan_boundaries, (tuple(new_l[0])), (tuple(new_l[1])), (0, 51, 102), 2,
                                     cv2.cv.CV_AA)
                # ---------------draw selected voronoi line and line number
                boundary_line = vor_row_line[boundary_num]
                x1, y1 = 0, 0
                for b_line in boundary_line:
                    x1, y1 = b_line[0]
                    cv2.line(floor_plan_boundaries, (tuple(b_line[0])), (tuple(b_line[1])), (0, 0, 255), 2,
                             cv2.cv.CV_AA)
                cv2.putText(floor_plan_boundaries, 'v' + str(text1 + 1) + '-' + str(boundary_num + 1),
                            (x1 + 1, y1 + 1), cv2.FONT_HERSHEY_PLAIN, 4, (0, 0, 255), 5)

                cv2.imwrite(
                    score_path + 'Partition ' + str(text1) + ' Boundary ' + str(boundary_num) + 'Line Scores.png',
                    floor_plan_boundaries)

    def print_EE_lines_nd_scores(self,voronoi_scores,
          seperated_edge_lines,edge_extension_scores,
          score_path,print_start_point):
        for row_num, voronoi_line in enumerate(voronoi_scores):
            text1 = voronoi_line[0]
            for boundary_num, boundary_score in enumerate(voronoi_line[1]):
                floor_plan_boundaries = cv2.imread(
                    score_path + 'Partition ' + str(text1) + ' Boundary ' + str(boundary_num) + 'Line Scores.png',
                    cv2.IMREAD_COLOR)
                edge_lines = []
                for row in seperated_edge_lines:
                    if row[0] == text1:
                        edge_lines = row[1]
                        break
                edge_boundary_lines = edge_lines[boundary_num]
                for e, edge_line1 in enumerate(edge_boundary_lines):
                    edge_line = edge_line1[0]
                    x2, y2 = edge_line[1]
                    cv2.line(floor_plan_boundaries, (tuple(edge_line[0])), (tuple(edge_line[1])), (0, 255, 0), 2,
                             cv2.cv.CV_AA)
                    cv2.putText(floor_plan_boundaries, 'e' + str(e + 1), (x2, y2), cv2.FONT_HERSHEY_PLAIN, 4,
                                (0, 255, 0), 5)
                edge_line_set = []
                for row in edge_extension_scores:
                    if row[0] == text1:
                        edge_line_set = row[1]
                        break
                edge_boundary_scores = edge_line_set[boundary_num]
                point_value = 600
                cv2.putText(floor_plan_boundaries, '------------Edge Extension Scores----------------',
                            (5, print_start_point + point_value), cv2.FONT_HERSHEY_PLAIN, 2, (0, 255, 0), 2)
                point_value = point_value + 30
                for num, e_row in enumerate(edge_boundary_scores):
                    cv2.putText(floor_plan_boundaries,
                                'e ' + str(num + 1) + ' -> Edge Score: ' + str(e_row),
                                (5, print_start_point + point_value), cv2.FONT_HERSHEY_PLAIN, 2, (0, 255, 0), 2)
                    point_value = point_value + 30
                cv2.imwrite(
                    score_path + 'Partition ' + str(text1) + ' Boundary ' + str(boundary_num) + 'Line Scores.png',
                    floor_plan_boundaries)

    def print_iv_lines_nd_scores(self,voronoi_scores,improved_voronoi_lines,
                                 improved_voronoi_scores,score_path,print_start_point):
        for row_num, voronoi_line in enumerate(voronoi_scores):
            text1 = voronoi_line[0]
            for boundary_num, boundary_score in enumerate(voronoi_line[1]):
                floor_plan_boundaries2 = cv2.imread(
                    score_path + 'Partition ' + str(text1) + ' Boundary ' + str(boundary_num) + 'Line Scores.png',
                    cv2.IMREAD_COLOR)
                boundary_line_set = []
                for row in improved_voronoi_lines:
                    if row[0] == text1:
                        boundary_line_set = row[1]
                        break
                improved_lines_set = boundary_line_set[boundary_num]
                for i, improved_line in enumerate(improved_lines_set):
                    x1, y1 = 0, 0
                    for i_line in improved_line:
                        x1, y1 = i_line[0]
                        cv2.line(floor_plan_boundaries2, (tuple(i_line[0])), (tuple(i_line[1])), (255, 0, 0), 1,
                                 cv2.cv.CV_AA)
                    cv2.putText(floor_plan_boundaries2, 'i' + str(i + 1), (x1 + 1, y1 + 1), cv2.FONT_HERSHEY_PLAIN,
                                4, (255, 0, 0), 5)
                improved_line_set = []
                for row in improved_voronoi_scores:
                    if row[0] == text1:
                        improved_line_set = row[1]
                        break
                improved_line_penalty = improved_line_set[boundary_num]
                exists = False
                point_value = 800
                cv2.putText(floor_plan_boundaries2, '------------Voronoi Improved Scores----------------',
                            (5, print_start_point + point_value + 60), cv2.FONT_HERSHEY_PLAIN, 2, (255, 0, 0),
                            2)
                point_value = point_value + 90
                for num, i_row in enumerate(improved_line_penalty):
                    improved_line_penalty = i_row[0]
                    cv2.putText(floor_plan_boundaries2,
                                'i. ' + str(num + 1) + ' -> Improved Voronoi Score' + str(
                                    improved_line_penalty),
                                (5, print_start_point + point_value), cv2.FONT_HERSHEY_PLAIN, 2, (255, 0, 0), 2)
                    point_value = point_value + 30
                cv2.imwrite(
                    score_path + 'Partition ' + str(text1) + ' Boundary ' + str(boundary_num) + 'Line Scores.png',
                    floor_plan_boundaries2)

    def print_ipl_lines_nd_scores(self,voronoi_scores,seperated_ipl_lines,
                                  ipl_scores,score_path,print_start_point):
        for row_num, voronoi_line in enumerate(voronoi_scores):
            text1 = voronoi_line[0]
            for boundary_num, boundary_score in enumerate(voronoi_line[1]):
                floor_plan_boundaries = cv2.imread(
                    score_path + 'Partition ' + str(text1) + ' Boundary ' + str(
                        boundary_num) + 'Line Scores.png', cv2.IMREAD_COLOR)

                ipl_lines = []
                for row in seperated_ipl_lines:
                    if row[0] == text1:
                        ipl_lines = row[1]
                        break
                ipl_boundary_lines = ipl_lines[boundary_num]
                for e, ipl_line1 in enumerate(ipl_boundary_lines):
                    ipl_line = ipl_line1[0]
                    x2, y2 = ipl_line[1]
                    cv2.line(floor_plan_boundaries, (tuple(ipl_line[0])), (tuple(ipl_line[1])),
                             (255, 255, 0), 2, cv2.cv.CV_AA)
                    cv2.putText(floor_plan_boundaries, 'ipl' + str(e + 1), (x2, y2),
                                cv2.FONT_HERSHEY_PLAIN, 4, (255, 255, 0), 5)

                ipl_line_set = []
                for row in ipl_scores:
                    if row[0] == text1:
                        ipl_line_set = row[1]
                        break
                ipl_boundary_scores = ipl_line_set[boundary_num]
                point_value = 1200
                cv2.putText(floor_plan_boundaries, '------------IPL Scores----------------',
                            (5, print_start_point + point_value), cv2.FONT_HERSHEY_PLAIN, 2,
                            (255, 255, 0), 2)
                point_value = point_value + 30

                for num, ipl_penalty in enumerate(ipl_boundary_scores):
                    cv2.putText(floor_plan_boundaries,
                                'ipl ' + str(num + 1) + ' -> IPL Score: ' + str(
                                    ipl_penalty),
                                (5, print_start_point + point_value), cv2.FONT_HERSHEY_PLAIN, 2,
                                (255, 255, 0), 2)
                    point_value = point_value + 30

                cv2.imwrite(score_path + 'Partition ' + str(text1) + ' Boundary ' + str(
                    boundary_num) + 'Line Scores.png', floor_plan_boundaries)
