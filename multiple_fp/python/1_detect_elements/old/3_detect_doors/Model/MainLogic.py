__author__ = 'stefano'


import shutil,os,cv2,time, sys, subprocess,getopt,traceback,copy,pyttsx
from PIL import Image
from natsort import natsorted


from image import Image_class
from iterative_functions import iterative_functions_class
from read_gravvitas_text_files import read_gravvitas_text_files_class


class MainFormLogic():
    #-------initialization method
    def __init__(self):
        self.thresholds = [0,0,0,0,0]
        self.count = 0
        self.name = ''
        self.path_floorplan = ''
        self.converted_floorplan_path = ''
        self.path_output = ''
        self.path_output_d = ''
        self.path_results = ''
        self.path_final_lines = ''
        self.path_gravvitas = ''
        self.path_gravvitas_svg = ''
        self.object_path = ''
        self.object_output_path = ''
        self.current_directory =''
        self.path_component_data = ''
        #--make 'debug_mode' True to see intermediate results-------
        debug_mode = True

        #----get path
        self.current_directory, self.input_path, self.output_path = self.get_path()


        #--create folders
        self.create_output_folders(self.current_directory,debug_mode)


        #--get list of input floorplans
        list_images_floorplan = self.pre_process_images(self.current_directory)


        #--copy back from js state files
        state_filePath = self.output_path+'/2_state_files/'
        state_files = os.listdir(state_filePath)
        for state_file in state_files:
            if os.path.isfile(state_filePath + str(state_file)):
                shutil.copy(state_filePath + str(state_file), self.path_component_data+'/state/')
        #--copy back from js text files
        text_filePath = self.output_path + '/3_detection_files/'
        text_files = os.listdir(text_filePath)
        for text_file in text_files:
            if os.path.isfile(text_filePath + str(text_file)):
                shutil.copy(text_filePath + str(text_file), self.path_component_data)

        ##---take each floorplan name from list of images
        for file_name in list_images_floorplan:
            try:
                # # start_image_process_time = time.time()
                print 'Image is : ',file_name
                img = cv2.imread(self.path_floorplan + file_name, cv2.IMREAD_GRAYSCALE)
                self.floorplan_component_ID(file_name,img,self.path_floorplan,debug_mode)



            except(KeyboardInterrupt,SystemExit):
                print "User interrupted!"
                try:
                    sys.exit(0)
                except SystemExit:
                    os._exit(0)

            except:
                print "---------------------Error occured with-------------------------", str(file_name)
                traceback.print_exc()

        #---copy text files
        svg_files = os.listdir(self.path_gravvitas)
        for svg_file in svg_files:
            if os.path.isfile(self.path_gravvitas + str(svg_file)):
                if '5_door_details.txt' in str(svg_file):
                    shutil.copy(self.path_gravvitas + str(svg_file),
                                self.output_path + '/3_detection_files/5_door_details.txt')
                elif 'svg_door_details.txt' in str(svg_file):
                    shutil.copy(self.path_gravvitas + str(svg_file),
                                self.output_path + '/3_detection_files/5_svg_door_details.txt')
                elif 'wall_details.txt' in str(svg_file):
                    shutil.copy(self.path_gravvitas + str(svg_file),
                                self.output_path + '/3_detection_files/7_wall_details.txt')

        # --copy state files
        state_file_path = os.path.join(self.path_component_data + '/state/')
        state_files = os.listdir(state_file_path)
        for state_file in state_files:
            if os.path.isfile(state_file_path + str(state_file)):
                shutil.copy(state_file_path + str(state_file),
                            self.output_path + '/2_state_files/')


    def copy_to_folder(self,source_folder, destination_folder):
        svg_files = os.listdir(source_folder)
        for svg_file in svg_files:
            if os.path.isfile(source_folder + str(svg_file)):
                if '5_door_details.txt' in str(svg_file):
                    shutil.copy(source_folder + str(svg_file),
                                self.output_path + destination_folder+'/5_door_details.txt')
                elif 'svg_door_details.txt' in str(svg_file):
                    shutil.copy(source_folder + str(svg_file),
                                self.output_path + destination_folder+'/5_svg_door_details.txt')
                elif 'wall_details.txt' in str(svg_file):
                    shutil.copy(source_folder + str(svg_file),
                                self.output_path + destination_folder+'/7_wall_details.txt')


    def create_output_folders(self,current_directory,debug_mode):
        #create folders to store outpuOUTPUT_DEBUG etc.
        self.path_output_d=current_directory+'/OUTPUT_DEBUG/'
        if not(os.path.exists(self.path_output_d)):
                os.mkdir(self.path_output_d)
        self.path_component_data = current_directory + '/Component_Data/'
        if not (os.path.exists(self.path_component_data)):
            os.mkdir(self.path_component_data)
        #--create place to write text for GraVVITTAS
        self.path_gravvitas=current_directory+'/GraVVITAS/'
        if not(os.path.exists(self.path_gravvitas)):
            os.mkdir(self.path_gravvitas)
        # --create place to write text for SVGPrecessing
        self.path_svg_processing = current_directory + '/svg_processing/'
        if not (os.path.exists(self.path_svg_processing)):
            os.mkdir(self.path_svg_processing)

        #--incremental folder creation--------------------------------------
        #OUTPUT folder incremental values for folder name
        list_dir_output = os.listdir(self.path_output_d)
        if len(list_dir_output)>0:
            t = [int(x) for x in list_dir_output]
            t.sort()
            name = str(t[len(t)-1]+1)
            self.path_output_d = self.path_output_d+name+'/'
            os.mkdir(self.path_output_d)
        else:
            self.path_output_d = self.path_output_d+str(1)+'/'
            name="1"
            os.mkdir(self.path_output_d)

        #GRAVVITAS folder incremental values for folder name
        if not(os.path.exists(self.path_gravvitas+ name +"/")):
            os.mkdir(self.path_gravvitas+ name +"/")
            self.path_gravvitas = self.path_gravvitas+ name +"/"
        else:
            shutil.rmtree(self.path_gravvitas+ name +"/")
            os.mkdir(self.path_gravvitas+ name +"/")
            self.path_gravvitas = self.path_gravvitas+ name +"/"
        self.path_gravvitas=self.path_gravvitas+'/TEXT/'
        if not(os.path.exists(self.path_gravvitas)):
            os.mkdir(self.path_gravvitas)

        # path_svg_processing folder incremental values for folder name
        if not (os.path.exists(self.path_svg_processing + name + "/")):
            os.mkdir(self.path_svg_processing + name + "/")
            self.path_svg_processing = self.path_svg_processing + name + "/"
        else:
            shutil.rmtree(self.path_svg_processing + name + "/")
            os.mkdir(self.path_svg_processing + name + "/")
            self.path_svg_processing = self.path_svg_processing + name + "/"
        self.path_svg_processing = self.path_svg_processing + '/TEXT/'
        if not (os.path.exists(self.path_svg_processing)):
            os.mkdir(self.path_svg_processing)



        self.create_path_wall()
        if debug_mode:
            self.create_path_find_door()
        self.create_path_preprocessing()
        self.create_path_stairs()
        self.create_path_merge_all()
        self.create_path_state_files()



    def pre_process_images(self,current_directory):
        input_path_floorplan = self.input_path
        #--check if input folder exists, carry out rest of the program if it does
        if not(os.path.exists(input_path_floorplan)):
            print 'Directory Not Found.'
            sys.exit()
        else:
            # self.path_floorplan = input_path_floorplan
            # print 'input_path_floorplan',input_path_floorplan
            original_fp_path_unsorted = os.listdir(input_path_floorplan)
            original_fp_path = natsorted(original_fp_path_unsorted)
            # for each_image in original_fp_path:


            self.converted_floorplan_path = self.current_directory+'/input_fps/converted_fps/'
            if (os.path.exists(self.converted_floorplan_path)):
                shutil.rmtree(self.converted_floorplan_path)
            os.makedirs(self.converted_floorplan_path)

            for floor in original_fp_path:
                # print 'str(floor)',str(floor)
                if str(floor) == '.DS_Store':
                    os.remove(input_path_floorplan + str(floor))
                else:
                    if floor[-4:] == '.png':
                        shutil.copy(input_path_floorplan + str(floor), self.converted_floorplan_path + str(floor))
                    elif floor[-4:] == '.svg' or floor[-4:] == '.SVG':
                        im_name = str(floor)
                        only_im_name = im_name[:-4]
                        os.system('convert -density 250 ' + input_path_floorplan + str(
                            floor) + ' -set density 250 ' + self.converted_floorplan_path + only_im_name + '.png')

                    else:
                        im_conv = Image.open(input_path_floorplan + str(floor))
                        im_name = str(floor)
                        only_im_name = im_name[:-4]
                        im_conv.save(self.converted_floorplan_path + only_im_name + '.png')

            self.path_floorplan = self.converted_floorplan_path
            list_images_floorplan_unsorted = os.listdir(self.converted_floorplan_path)
            list_images_floorplan = natsorted(list_images_floorplan_unsorted)

            return list_images_floorplan

    def get_image_dimensions(self, input_path_floorplan, only_im_name, floor):
            image_path = self.converted_floorplan_path + 'original_' + only_im_name + '.png'
            os.system('convert ' + input_path_floorplan + str(
                floor) + ' ' + image_path)
            current_image = cv2.imread(image_path, cv2.IMREAD_GRAYSCALE)
            original_height, original_width = current_image.shape
            os.remove(image_path)

            os.system('convert -density 250 ' + input_path_floorplan + str(
                floor) + ' -set density 250 ' + self.converted_floorplan_path + only_im_name + '.png')
            density_changed_image = cv2.imread(self.converted_floorplan_path + only_im_name + '.png',
                                               cv2.IMREAD_GRAYSCALE)
            converted_height, converted_width = density_changed_image.shape

            # --get conversion ratio
            x_ratio = float(original_width) / converted_width
            y_ratio = float(original_height) / converted_height

            return x_ratio, y_ratio

    def floorplan_component_ID(self,file_name,img,orginal_img_path,debug_mode):
        # ---extract only image name without extensions
        only_image_name = file_name[0:-4]
        path = self.path_output_d + only_image_name + '/'
        if not (os.path.exists(path)):
            os.mkdir(path)

        self.image_color = cv2.imread(orginal_img_path + file_name, cv2.IMREAD_COLOR)

        # ---------Otsu's thresholding to clean image and remove smoothing effects
        ret2, otsu = cv2.threshold(img, 0, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)
        cv2.imwrite(self.path_output_d + 'preprocessing/' + only_image_name + '_otsu.png', otsu)


        image = Image_class(self.current_directory,file_name, self.path_output,
                            self.path_output_d,self.path_component_data, otsu, self.image_color)


        print 'Erase Text'
        self.read_gravvitas_text_obj = read_gravvitas_text_files_class()
        all_detected_text = self.read_gravvitas_text_obj.read_text_data(self.path_component_data, only_image_name)
        no_text_image = image.delete_text(otsu, all_detected_text)


        print 'Finding connected components no text'
        image.find_connected_componet_no_text(no_text_image)

        sys.setrecursionlimit(10000)

        print 'Walls classification'
        external_wall_list = image.classification_walls(debug_mode)
        self.write_list_rect('rect', external_wall_list, orginal_img_path + file_name,
                             only_image_name,'_7_wall_details.txt','Wall')

        print 'Doors classification'
        door_list,image_door_rects = image.classification_door(debug_mode)
        self.write_list_rect('rect', door_list, orginal_img_path + file_name,
                             only_image_name,'_5_door_details.txt','Door')
        self.write_detection_for_svg('rect', image_door_rects,
                             only_image_name, '_5_svg_door_details.txt')



    def get_path(self):
        # current_directory, input_path, output_path = self.get_path_from_local_machine()
        current_directory, input_path, output_path = self.get_path_from_js()
        # print 'current_directory',current_directory
        # print 'input_path',input_path
        # print 'output_path',output_path
        return current_directory, input_path, output_path

    def get_path_from_local_machine(self):
        #--/home/ub/Documents/node_js/socketio/python/floor_plan_code
        current_directory = os.path.abspath(os.path.join(os.getcwd(), '..'))
        #--/home/ub/Documents/node_js/socketio/python/floor_plan_code/input_fps/anu/
        input_path = current_directory+ '/input_fps/anu/'
        # input_path = current_directory + '/input_fps/current_working_fps/'
        #--/home/ub/Documents/node_js/socketio/python/floor_plan_code/SVG_results/final_floorplan.svg
        output_path = current_directory+'/Component_Data/'
        return current_directory, input_path, output_path

    def get_path_from_js(self):
        js_message = sys.stdin.readlines()
        input_path, current_directory,output_path = '','',''
        for row_num,each in enumerate(js_message):
            if row_num==0:
                current_directory = each.rstrip()
            elif row_num==1:
                input_path = each.rstrip()+ '1_images/input/'
            else:
                output_path = each.rstrip()
        return current_directory, input_path, output_path


    def write_list_rect(self,type, component_list,orginal_img_path,only_image_name,
                        text_file_name,text):
        # --write to text file for gravvitas
        file = open(self.path_gravvitas + only_image_name + text_file_name, 'a')
        for row in component_list:
            if type == 'rect':
                x1, y1 = row[0]
                x2, y2 = row[1]
                file.write(text+' : ' + str(x1) + ',' + str(y1) + ',' + str(x2) + ',' + str(y2) + '\n')
            elif type == 'contour':
                contour = ''
                for c, cord in enumerate(row):
                    if c == 0:
                        contour = contour + '[' + str(cord[0]) + ',' + str(cord[1]) + ']'
                    else:
                        contour = contour + ',[' + str(cord[0]) + ',' + str(cord[1]) + ']'
                file.write(text + ' : ' + contour + '\n')
        file.close()

    def write_detection_for_svg(self,type, component_list,only_image_name,
                        text_file_name):
        iterative_obj = iterative_functions_class()
        file = open(self.path_svg_processing+only_image_name+text_file_name,'a')
        for row in component_list:
            if type == 'rect':
                x1, y1 = row[0]
                x2, y2 = row[1]
                cx,cy = iterative_obj.find_centre_of_rectangle(x1, y1,x2, y2)
                file.write(str(cx) + ',' + str(cy) + '\n')
                # cv2.rectangle(door_test_image,(x1, y1),(x2,y2),(255,0,0),-1)
                # cv2.circle(door_test_image, (cx, cy), 5, (0, 0, 255), -1)
        file.close()
        # cv2.imwrite(self.path_component_data+'door.png',door_test_image)


    def create_path_stairs(self):
        path = self.path_output_d+'stairs/'
        if not(os.path.exists(path)):
            os.mkdir(path)

    def create_path_merge_all(self):
        path = self.path_output_d+'merge_all/'
        if not(os.path.exists(path)):
            os.mkdir(path)

    def create_path_state_files(self):
        state_path = self.path_component_data + 'state/'
        if not (os.path.exists(state_path)):
            os.mkdir(state_path)

    def create_path_wall(self):
        path_external_wall=self.path_output_d+'wall/'
        if not(os.path.exists(path_external_wall)):
            os.mkdir(path_external_wall)
            # os.mkdir(self.path_output_d+'oblique_walls/')

    def create_path_find_door(self):
        path_external_wall=self.path_output_d+'door/'
        if not(os.path.exists(path_external_wall)):
            os.mkdir(path_external_wall)
            os.mkdir(path_external_wall+'ante/')

    def create_path_preprocessing(self):
        path_preprocessing=self.path_output_d+'preprocessing/'
        if not(os.path.exists(path_preprocessing)):
            os.mkdir(path_preprocessing)




if __name__ == '__main__':
    start = time.time()
    hwl1 = MainFormLogic()
    end = time.time()
    print end - start
