__author__ = 'anu'

import cv2,shutil,os,time
import numpy as np
from scipy import ndimage
from operator import itemgetter

from image import Image_class
from open_plan import planClass
from recompute import recompute
from connectedcomponent import CC
#---used only when we need old voronoi output
from voronoi import voronoi_class
from region_grow import region_grow_class
from line_optimization import line_optimization_class
from iterative_functions import iterative_functions_class
from generate_other_lines import generate_other_lines_class


class room_functions_class:
    def find_door_width(self,current_directory,orginal_image_path,only_image_name):
        ## ------testing section
        # --english
        # new_walls_path = current_directory+'/input_fps/new_walls/online_walls/'
        # --french
        # new_walls_path = current_directory+'/input_fps/new_walls/walls_2/'
        # new_walls_list = os.listdir(new_walls_path)
        # for wall_room_num, wall_room in enumerate(new_walls_list):
        #     wall_image_name = str(wall_room)
        #     # --english
        #     # wall_name = wall_image_name[0:-4]
        #     # --french
        #     wall_name = wall_image_name[0:-11]
        #     if wall_name == only_image_name:
        #         # --english
        #         # self.orginal_image_path = new_walls_path+wall_name+'.png'
        #         # --french
        #         orginal_image_path = new_walls_path + wall_name + '_output.png'
        #         break
        # --------end of testing section
        img = cv2.imread(orginal_image_path,cv2.IMREAD_COLOR)
        dst = cv2.inRange(img, (0,255,0), (0,255,0))
        labels, numlabels = ndimage.label(dst>127)
        avg_width = 0
        count = 0
        for i in range(1,numlabels+1):
            cc = CC(i)
            point= np.argwhere(labels == i)
            cc.y_min=point[:,0].min()
            cc.x_min=point[:,1].min()
            cc.y_max=point[:,0].max()
            cc.x_max=point[:,1].max()
            cc.height= cc.y_max-cc.y_min+1
            cc.width= cc.x_max-cc.x_min+1
            cc.center_x = cc.y_min+(cc.height/2)
            cc.center_y = cc.x_min+(cc.width/2)
            cc.image=(np.where(labels[cc.y_min:cc.y_max+1,cc.x_min:cc.x_max+1] != i, 0, labels[cc.y_min:cc.y_max+1,cc.x_min:cc.x_max+1])*255/i)
            cc.numPixel=(cc.image.sum()/255)
            cc.image = self.add_pad_single_pixel(cc.image)
            cc.set_coordinates()
            cc.find_contour()
            max_dist = cc.get_max_distance()
            if max_dist>20:
                avg_width+=max_dist
                count+=1
        if count > 0:
            avg_width = 2*avg_width/float(count)
        else:
            dst = cv2.inRange(img, (0,255,255),(0,255,255))
            labels, numlabels = ndimage.label(dst>127)
            avg_width = 0
            count = 0
            for i in range(1,numlabels+1):
                cc = CC(i)
                point= np.argwhere(labels == i)
                cc.y_min=point[:,0].min()
                cc.x_min=point[:,1].min()
                cc.y_max=point[:,0].max()
                cc.x_max=point[:,1].max()
                cc.height= cc.y_max-cc.y_min+1
                cc.width= cc.x_max-cc.x_min+1
                cc.center_x = cc.y_min+(cc.height/2)
                cc.center_y = cc.x_min+(cc.width/2)
                cc.image=(np.where(labels[cc.y_min:cc.y_max+1,cc.x_min:cc.x_max+1] != i, 0, labels[cc.y_min:cc.y_max+1,cc.x_min:cc.x_max+1])*255/i)
                cc.numPixel=(cc.image.sum()/255)
                cc.image = self.add_pad_single_pixel(cc.image)
                cc.set_coordinates()
                cc.find_contour()
                max_dist = cc.get_max_distance()
                if max_dist>20:
                    avg_width+=max_dist
                    count+=1
            if count != 0:
                if count > 2:
                    avg_width = avg_width/float(count) #a 0.50 penalty because windows are tipcally larger than doors
                else:
                    avg_width = 2*avg_width/float(count)
        if avg_width < 40:
            avg_width = 40
        return avg_width

    # --- cant call this method from imageOperations.py because it doesnt have a class and has only methods
    def add_pad_single_pixel(self, image):
        s = image.shape
        if len(s) == 2:
            z1 = np.zeros((1, s[1]))
            z2 = np.zeros((s[0] + 2, 1))
        else:
            z1 = np.zeros((1, s[1], s[2]))
            z2 = np.zeros((s[0] + 2, 1, s[2]))
        image = np.concatenate((z1, image), axis=0)
        image = np.concatenate((image, z1), axis=0)
        image = np.concatenate((z2, image), axis=1)
        image = np.concatenate((image, z2), axis=1)
        return image

    def all_room_functions(self,current_directory,path_output_d,path_output,
                           path_gravvitas,path_final_lines,avg_door_width,weight_list,
                           only_image_name,otsu,text_cordinates,debug_mode):
        self.current_directory = current_directory
        self.path_output_d = path_output_d
        self.path_output = path_output
        self.path_gravvitas = path_gravvitas
        self.path_final_lines = path_final_lines


        # ----- line room selections
        inner_directory = self.path_output_d + 'Rooms/' + only_image_name + '/'
        openPlan = planClass(current_directory,self.path_output, inner_directory, otsu, only_image_name, True)

        # ------find all rooms : take all contours in floor plan, remove floor plan borders and select only room contours
        openPlan.find_rooms(debug_mode)


        # ------Find Open Plan:get each room found from 'find_rooms()' and find each rooms number of text labels and store in num_labels_per_room
        print 'find_open_plans'
        all_detected_contour_lines = []
        all_detected_contour_lines, text_data, no_of_rooms = openPlan.find_open_plans(text_cordinates, debug_mode,
                                                                                      avg_door_width,
                                                                                      all_detected_contour_lines,
                                                                                      self.path_gravvitas)

        for text_row in text_data:
            if len(text_row[1]) > 1:
                for text_label_data in text_row[1]:
                    tx0, ty0, tx1, ty1 = text_label_data[2]
                    box_lines = [
                        [[tx0, ty0], [tx1, ty0]],
                        [[tx1, ty0], [tx1, ty1]],
                        [[tx1, ty1], [tx0, ty1]],
                        [[tx0, ty1], [tx0, ty0]]
                    ]
                    for r_num, cont_row in enumerate(all_detected_contour_lines):
                        if cont_row[0] == text_row[0]:
                            all_detected_contour_lines[r_num].append(
                                [text_label_data[0], text_label_data[1], text_label_data[2], box_lines])
                            break

        # --create list to store all open plan partition lines
        all_open_plan_lines = []

        # ----create text file to store open plan line data for evaluation
        op_details_file = open(path_gravvitas + only_image_name + '_8_openPlan_details.txt', 'w')

        # ------store each open plan in 'OpenPlans' folder for future use
        open_plan_path = inner_directory + 'OpenPlans/'
        open_area_list = os.listdir(open_plan_path)
        open_area_list.sort()

        for room_num, room in enumerate(open_area_list):
            # if room_num ==1:
            image_name = str(room)
            room_number = image_name[10:-4]
            current_open_plan_contour_data = []
            for contour_detail_row in all_detected_contour_lines:
                if contour_detail_row[0] == int(room_number):
                    current_open_plan_contour_data = contour_detail_row
                    break
            # ---get only open plan name
            name = image_name[0:-4]
            # --write current open plan to a directory inside current iteration
            output_path = inner_directory + name + '/'
            if (os.path.exists(output_path)):
                shutil.rmtree(output_path)
            os.mkdir(output_path)
            os.mkdir(output_path + 'OpenPlans/')
            open_plan_image = cv2.imread(inner_directory + 'OpenPlans/' + image_name, cv2.IMREAD_GRAYSCALE)
            self.height, self.width = open_plan_image.shape
            cv2.imwrite(output_path + 'OpenPlans/' + image_name, open_plan_image)


            # -----create output folders to store intermediate steps
            os.mkdir(output_path + 'Edge_Extension_Lines/')
            os.mkdir(output_path + 'Voronoi_Lines/')
            os.mkdir(output_path + 'Line_Optimization/')
            best_line_path = output_path + 'Best_Lines/'
            os.mkdir(best_line_path)
            image_path = output_path + 'OpenPlans/' + image_name

            open_plan_text_cordinate = []
            text_words = []
            text_bounding_boxes = []
            open_plan_contour_lines = current_open_plan_contour_data[1]

            for r, row in enumerate(current_open_plan_contour_data[3:]):
                open_plan_text_cordinate.append(row[1])
                text_words.append([row[0], row[1]])
                text_bounding_boxes.append(row[2])

            if debug_mode:
                open_plan_test_image = open_plan_image.copy()
                for r,row in enumerate(text_words):
                    cord1 =row[1][0]
                    cord2 =row[1][1]
                    x_position = (len(row[0])/2)*25
                    text = str(row[0]).upper()
                    cv2.putText(open_plan_test_image, text, (cord1-x_position,cord2+12), cv2.FONT_HERSHEY_COMPLEX, 1, (0, 0, 0), 2,cv2.cv.CV_AA)
                cv2.imwrite(self.path_final_lines+'_Thesis_Step_01_'+only_image_name+'.png',open_plan_test_image)



            # #-----old voronoi diagram drawing
            # voronoi_obj = voronoi_class()
            # voronoi_obj.draw_voronoi(avg_door_width,image_name,text_words,open_plan_text_cordinate,open_plan_contour_lines,open_plan_image,path_final_lines)
            # print 'Voronoi diagram executed'


            original_edges_and_extensions = openPlan.extending_edges(avg_door_width, name, image_name,
                                                                                      image_path,
                                                                                      current_open_plan_contour_data,
                                                                                      open_plan_text_cordinate,
                                                                                      debug_mode)




            generate_other_lines_obj = generate_other_lines_class()
            original_edges_and_extensions,interesting_lines,contour_angle_data = generate_other_lines_obj.generate_lines_from_interesting_points(
                only_image_name+image_name,self.path_final_lines,original_edges_and_extensions,open_plan_contour_lines,
                text_bounding_boxes, open_plan_text_cordinate,avg_door_width,self.height, self.width)

            # #--create orthogonal lines
            # generate_other_lines_obj.find_orthogonal_lines(open_plan_contour_lines,contour_angle_data)

            # print 'Grow_regions'
            # start_region = time.time()
            ring_obj = region_grow_class(output_path, open_plan_image, name)
            ring_obj.grow_regions(current_open_plan_contour_data, open_plan_text_cordinate, name, image_name,
                                  image_path, debug_mode,text_words,text_bounding_boxes, path_final_lines,only_image_name)
            # end_region = time.time()
            # print ('time for region grow', str(end_region - start_region))

            # print 'Get_new_lines'
            # start_get_new_lines = time.time()
            vornoi_data, all_contour_lines,thesis_image = ring_obj.get_new_lines(open_plan_text_cordinate, name, image_name,
                                                                    debug_mode,self.path_final_lines,text_words)
            # end_get_new_lines = time.time()
            # print 'Get_new_lines Time : ', end_get_new_lines - start_get_new_lines

            # print 'Shortest path calculation'
            # start_find_shortest_path = time.time()
            line_opt = line_optimization_class(output_path, best_line_path, only_image_name)
            shortest_path_data = line_opt.find_shortest_path_manhattan(vornoi_data, open_plan_text_cordinate,
                                                                       current_open_plan_contour_data, name, image_name,
                                                                       image_path, debug_mode,self.path_final_lines,text_words,thesis_image)
            # line_opt.test()
            # end_find_shortest_path = time.time()
            # print 'Shortest path calculation Time : ', end_find_shortest_path - start_find_shortest_path

            # # print 'Find_edge_extensions_between_cordinates'
            # # start_Find_edge_extensions_between_cordinates = time.time()
            # edge_extensions_between_coordinates = line_opt.find_edge_extensions_between_cordinates(edge_extensions,
            #                                                                                        shortest_path_data,
            #                                                                                        open_plan_text_cordinate,
            #                                                                                        name, image_name,
            #                                                                                        debug_mode)
            # # end_Find_edge_extensions_between_cordinates = time.time()
            # # print 'Find_edge_extensions_between_cordinates Time ; ', end_Find_edge_extensions_between_cordinates - start_Find_edge_extensions_between_cordinates

            # debug_mode = True
            # print 'Give each Line a Score and Select Maximum Score'
            # start_max_line = time.time()
            max_line_list = []
            max_line = line_opt.find_lines_in_area(self.current_directory, current_open_plan_contour_data,
                                                   avg_door_width, weight_list, vornoi_data, all_contour_lines,
                                                   shortest_path_data, original_edges_and_extensions,interesting_lines,open_plan_text_cordinate, name,
                                                   image_name, only_image_name,open_plan_path, room, text_words, text_bounding_boxes,
                                                   self.path_final_lines,debug_mode)
            max_line_list.append(max_line)
            # end_max_line = time.time()
            # print 'Give each Line a Score and Select Maximum Score Time : ', end_max_line - start_max_line

            # ---------iterate over the process after setting max_score_line
            # -- Do it until partitions created by max_line doesnt have >1 text lables
            # print 'Iterate until complete Open plan is partitioned'
            # start_max_line_iteration = time.time()
            # iter_open_plan_text_cordinate,iter_text_words,iter_text_bounding_boxes =[],[],[]
            iter_open_plan_text_cordinate, iter_text_words, iter_text_bounding_boxes = open_plan_text_cordinate, text_words, text_bounding_boxes
            level = 2
            num_overall_plans = 1
            while num_overall_plans > 0:
                num_overall_plans = 0
                # print '--------------Current Iteration is : ', level
                # ---first iteration over the loop follow this
                if level == 2:
                    # # # --********************* use when running only from 2nd iteration
                    # best_line_path1=self.current_directory+'/input_fps/max/'
                    # best_line_list=[]
                    # image_with_lines =0
                    # #---get image with max_line
                    # for file_l in os.listdir(best_line_path1):
                    #     if file_l.endswith(".png"):
                    #         best_line_list.append(file_l)
                    # if len(best_line_list)==1:
                    #     for img in best_line_list:
                    #         image_with_lines = img
                    # print 'path is....', best_line_path1+str(image_with_lines)
                    # #---read image with max_line
                    # input_image= cv2.imread(best_line_path1+str(image_with_lines),cv2.IMREAD_GRAYSCALE)
                    # #---define path to store max_line in current step
                    # final_output_path = best_line_path+str(level)+'/'
                    # os.mkdir(final_output_path)
                    # #---define path to store intermediate outputs
                    # new_output_path = best_line_path+'output_'+str(level)+'/'
                    # os.mkdir(new_output_path)
                    # #--new addition
                    # # print 'text_words'
                    # # for row in text_words:
                    # #     print row
                    # # print 'open_plan_text_cordinate'
                    # # for row in open_plan_text_cordinate:
                    # #     print row
                    # # print 'text_bounding_boxes'
                    # # for row in text_bounding_boxes:
                    # #     print row
                    #
                    # # ret,thresh = cv2.threshold(input_image,0,255,1)
                    # # contours,hierachy = cv2.findContours(thresh,1,2)
                    # # text_data_iterate =[]
                    # # for cnt_number,contour in enumerate(contours):
                    # #     text_count = 0
                    # #     temp_text_list = []
                    # #     for tr, text_cordinate in enumerate(open_plan_text_cordinate):
                    # #         if cv2.pointPolygonTest(contour,(text_cordinate[0],text_cordinate[1]),False)==1:
                    # #             text_count += 1
                    # #             temp_text_list.append(tr)
                    # #     text_data_iterate.append([cnt_number,temp_text_list,text_count])
                    # #
                    # # # print 'main text_data_iterate'
                    # # # for row in text_data_iterate:
                    # # #     print row
                    # #
                    # # text_data_iterate.sort(key=itemgetter(2),reverse=True)
                    # # for t_num,text_data_row in enumerate(text_data_iterate):
                    # #     #----remove first one -> one with highest num of text labels-> whihc means outer contour
                    # #     if t_num>0:
                    # #     # if text_data_row[2]>1:
                    # #         for textID in text_data_row[1]:
                    # #             iter_text_words.append(text_words[textID])
                    # #             iter_open_plan_text_cordinate.append(open_plan_text_cordinate[textID])
                    # #             iter_text_bounding_boxes.append(text_bounding_boxes[textID])
                    # # # ---********************** end running from 2nd iteration



                    # # --********************* used when running from begining
                    # iter_open_plan_text_cordinate,iter_text_words,iter_text_bounding_boxes = open_plan_text_cordinate,text_words,text_bounding_boxes
                    best_line_list = []
                    image_with_lines = 0
                    # ---get image with max_line
                    for file_l in os.listdir(best_line_path):
                        if file_l.endswith(".png"):
                            best_line_list.append(file_l)
                    if len(best_line_list) == 1:
                        for img in best_line_list:
                            image_with_lines = img
                    # ---read image with max_line
                    input_image = cv2.imread(best_line_path + str(image_with_lines), cv2.IMREAD_GRAYSCALE)
                    # ---define path to store max_line in current step
                    final_output_path = best_line_path + str(level) + '/'
                    os.mkdir(final_output_path)
                    # ---define path to store intermediate outputs
                    new_output_path = best_line_path + 'output_' + str(level) + '/'
                    os.mkdir(new_output_path)
                    # # --********************* end used when running from begining

                    # ---recompute finding max_line for current 'image with max_line'
                    orginal_image_name = image_with_lines[0:-4]
                    recompute_obj = recompute()
                    # number_of_open_plans,max_lines,iter_open_plan_text_cordinate,iter_text_words,iter_text_bounding_boxes = recompute_obj.find_open_plan_area(orginal_image_name,avg_door_width,weight_list,input_image,final_output_path,new_output_path,iter_open_plan_text_cordinate,iter_text_words,level,iter_text_bounding_boxes,debug_mode)
                    number_of_open_plans, max_lines = recompute_obj.find_open_plan_area(self.current_directory,
                                                                                        orginal_image_name,
                                                                                        avg_door_width, weight_list,
                                                                                        input_image, final_output_path,
                                                                                        new_output_path,
                                                                                        iter_open_plan_text_cordinate,
                                                                                        iter_text_words, level,
                                                                                        iter_text_bounding_boxes,
                                                                                        debug_mode)
                    # ---store max_line details
                    if max_lines != 0:
                        for m_row in max_lines:
                            max_line_list.append(m_row)
                    if number_of_open_plans > 0:
                        num_overall_plans = num_overall_plans + 1

                # ---all other iterations follow 'else'
                else:
                    # print '-----------------------iteration 03'
                    # open_plan_text_cordinate,text_words
                    level2 = level - 1
                    # ---define path for earlier outputs
                    earlier_outputs = os.listdir(best_line_path + str(level2) + '/')
                    earlier_outputs.sort()
                    # ---define path to store max_line in current step
                    final_output_path = best_line_path + str(level) + '/'
                    os.mkdir(final_output_path)
                    # ---define path to store intermediate outputs
                    new_output_path = best_line_path + 'output_' + str(level) + '/'
                    os.mkdir(new_output_path)
                    c = 0
                    # --take each image and recompute
                    new_weight = False
                    # op_cord,op_word,op_bb = iter_open_plan_text_cordinate,iter_text_words,iter_text_bounding_boxes
                    for image_with_lines in earlier_outputs:
                        # iter_open_plan_text_cordinate,iter_text_words,iter_text_bounding_boxes = open_plan_text_cordinate,text_words,text_bounding_boxes

                        input_image = cv2.imread(best_line_path + str(level - 1) + '/' + str(image_with_lines),
                                                 cv2.IMREAD_GRAYSCALE)
                        # print 'path', best_line_path+str(level-1)+'/'+str(image_with_lines)
                        orginal_image_name = image_with_lines[0:-4]
                        new_image_output_path = new_output_path + str(c) + '/'
                        os.mkdir(new_image_output_path)
                        recompute_obj = recompute()
                        # number_of_open_plans,max_lines,iter_open_plan_text_cordinate,iter_text_words,iter_text_bounding_boxes = recompute_obj.find_open_plan_area(orginal_image_name,avg_door_width,weight_list,input_image,final_output_path,new_image_output_path,iter_open_plan_text_cordinate,iter_text_words,level,iter_text_bounding_boxes,debug_mode)
                        number_of_open_plans, max_lines = recompute_obj.find_open_plan_area(self.current_directory,
                                                                                            orginal_image_name,
                                                                                            avg_door_width, weight_list,
                                                                                            input_image,
                                                                                            final_output_path,
                                                                                            new_image_output_path,
                                                                                            iter_open_plan_text_cordinate,
                                                                                            iter_text_words, level,
                                                                                            iter_text_bounding_boxes,
                                                                                            debug_mode)
                        if number_of_open_plans > 0:
                            num_overall_plans = num_overall_plans + 1
                        c = c + 1
                        # print ('max_lines', max_lines)
                        if max_lines != 0:
                            for m_row in max_lines:
                                max_line_list.append(m_row)

                level = level + 1
            end_max_line_iteration = time.time()
            # print 'Iterate until complete Open plan is partitioned Time : ', end_max_line_iteration - start_max_line_iteration

            # max_line_list  = [
            #     [[[787, 1683], [787, 1365]], [[787, 1365], [245, 1048]]],
            #     [[[512, 1203], [763, 481]]],
            #     [[[588, 993], [841, 1035]]]
            # ]

            final_image_path = self.path_final_lines + str(only_image_name) + '-' + str(room_num) + '.png'
            bogus_best_line_path = 'bogus_path/'
            line_opt = line_optimization_class(final_image_path, bogus_best_line_path, only_image_name)
            new_max_lines = line_opt.find_closest_contour_point(max_line_list, open_plan_contour_lines)


            # --append to all open plan lines
            all_open_plan_lines.append(new_max_lines)
            #--write open path lne data for evaluation

            # ----get image to draw new max lines
            floor_plan = cv2.imread(open_plan_path + room, cv2.IMREAD_COLOR)
            floor_plan_copy = floor_plan.copy()
            room_extract_copy = floor_plan.copy()
            # ---take each line from max_line_list
            for r, row in enumerate(new_max_lines):
                floor_plan_iteration_copy = floor_plan.copy()
                center_x, center_y = 0,0
                for l, line in enumerate(row):
                    # ---draw text label
                    for t_row in open_plan_text_cordinate:
                        cv2.circle(floor_plan_iteration_copy, (tuple(t_row)), 5, (255, 0, 0), -1)
                    # ---draw each line
                    cv2.line(floor_plan_iteration_copy, (tuple(line[0])), (tuple(line[1])), (0, 0, 255), 2,
                             cv2.cv.CV_AA)
                    cv2.line(floor_plan_copy, (tuple(line[0])), (tuple(line[1])), (0, 0, 255), 2, cv2.cv.CV_AA)
                    cv2.line(room_extract_copy, (tuple(line[0])), (tuple(line[1])), (0, 0, 0), 2, cv2.cv.CV_AA)
                    x1, y1 = line[0]
                    x2, y2 = line[1]
                    center_x, center_y = (x2 + x1) / 2, (y1 + y2) / 2
                cv2.putText(floor_plan_copy, str(r).upper(), (center_x - 10, center_y+10),
                        cv2.FONT_HERSHEY_COMPLEX, 1,
                        (255, 0, 0), 2, cv2.cv.CV_AA)
                # if debug_mode:
                cv2.imwrite(inner_directory + str(room_num) + 'Line' + str(r) + '.png', floor_plan_iteration_copy)
            for r, row in enumerate(text_words):
                cord1 = row[1][0]
                cord2 = row[1][1]
                x_position = (len(row[0]) / 2) * 25
                cv2.putText(floor_plan_copy, str(row[0]).upper(), (cord1 - x_position, cord2 + 12), cv2.FONT_HERSHEY_COMPLEX, 1,
                            (0, 0, 0), 2, cv2.cv.CV_AA)
                # cv2.circle(floor_plan_copy, (cord1-x_position,cord2+12), 5, (255, 0, 0), -1)
            # --write to inside folder
            cv2.imwrite(inner_directory + str(room_num) + '.png', floor_plan_copy)
            # --write to final_lines folder
            cv2.imwrite(self.path_final_lines + str(only_image_name) + '-' + str(room_num) + '.png', floor_plan_copy)
            # --write to extract room contours
            cv2.imwrite(self.path_final_lines + str(only_image_name) + '-' + str(room_num) + 'room.png',
                        room_extract_copy)

            # --extract room conoturs
            gray_room = cv2.imread(self.path_final_lines + str(only_image_name) + '-' + str(room_num) + 'room.png',
                                   cv2.IMREAD_GRAYSCALE)
            ret, thresh = cv2.threshold(gray_room, 0, 255, 1)
            contours, hierachy = cv2.findContours(thresh, 1, 2)
            before_room_lines = []
            for c, cnt in enumerate(contours):
                if c != len(contours) - 1:
                    # print 'after c', c
                    before_room_lines.append([])
                    text, text_cordinate = 0, 0
                    for each_text in text_words:
                        if cv2.pointPolygonTest(cnt, tuple(each_text[1]), False) == 1:
                            text = each_text[0]
                            text_cordinate = each_text[1]
                    if text != 0 and text_cordinate != 0:
                        for row in cnt:
                            before_room_lines[c].append(row[0])
                        before_room_lines[c].append([text, text_cordinate])

            room_lines = []
            #--clean room_lines list
            for r, room_row in enumerate(before_room_lines):
                if len(room_row)>1:
                    room_lines.append(room_row)

            # --write open path line data for evaluation
            op_outer_contour = ''
            for r_count, row in enumerate(open_plan_contour_lines):
                if r_count == 0:
                    op_outer_contour = op_outer_contour + '[' + str(row[0][0]) + ' ' + str(row[0][1]) + ']'
                else:
                    op_outer_contour = op_outer_contour + ',[' + str(row[0][0]) + ' ' + str(row[0][1]) + ']'

            op_details_file.write(
                'Plan ' + str(room_num + 1) + ' : Contour : ' + op_outer_contour + ' : Lines : ')
            for boundary_num, boundary in enumerate(new_max_lines):
                for b_line in boundary:
                    if boundary_num == len(new_max_lines) - 1:
                        op_details_file.write('[' + str(b_line[0]) + ' ' + str(b_line[1]) + ']')
                    else:
                        op_details_file.write('[' + str(b_line[0]) + ' ' + str(b_line[1]) + '], ')

            # --write data to gravvitas
            room_details_file = open(self.path_gravvitas + only_image_name + '_1_room_details.txt', 'a')
            for row in room_lines:
                no_of_rooms += 1
                room_details_file.write('open : plan ' + str(room_num + 1) + '-room ' + str(no_of_rooms) + ' : ' + str(
                    row[-1][0]) + ' : ' + str(row[-1][1]) + ' : ')
                room_details_file.write('contour_details : ')
                # --open plan file write
                op_details_file.write(' : ' + str(row[-1][0]) + ' : ' + str(row[-1][1]) + ' : room_contour_details : ')

                for cord_num, cordinate in enumerate(row[:-1]):
                    if cord_num == len(row[:-1]) - 1:
                        room_details_file.write(str(cordinate))
                        op_details_file.write(str(cordinate))
                    else:
                        room_details_file.write(str(cordinate) + ',')
                        op_details_file.write(str(cordinate) + ',')
                room_details_file.write('\n')
                room_details_file.write('\n')
            room_details_file.close()
            os.remove(self.path_final_lines + str(only_image_name) + '-' + str(room_num) + 'room.png')
            op_details_file.write('\n')

        op_details_file.close()

        return all_open_plan_lines

