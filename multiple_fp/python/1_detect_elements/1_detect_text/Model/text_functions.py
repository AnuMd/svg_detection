author = 'anu'

#--libraries
import cv2, cv, tesseract,os,re,unicodedata,math,sys
from PIL import Image
from bs4 import BeautifulSoup
from operator import itemgetter
from Levenshtein import distance

#---program files
from text_recognition import text_recogntion_class
from iterative_functions import iterative_functions_class


class text_funcitons_class:
    def __init__(self,path_output,path_debug,current_directory,file_name):
        self.path_output = path_output
        self.path_debug = path_debug
        self.current_directory = current_directory
        self.file_name = file_name
        self.iterative_obj = iterative_functions_class()

    def identify_text(self, otsu, language,only_image_name, cc_image_path,path_gravvitas,
                      width, height,debug_mode):
        self.width,self.height = width,height
        otsu_copy = cv2.cvtColor(otsu.copy(), cv2.COLOR_GRAY2BGR)
        path_text_results = self.path_debug + 'Extract_text/density/' + only_image_name

        numbers,text_to_erase = [],[]

        # --1. call tesseract identification method in portrait mode
        text_cordinates_1,text_cc_image,no_text_image,text_to_erase = self.text_detect_process(language,cc_image_path,otsu,numbers,debug_mode,text_to_erase,flipped=False)
        cv2.imwrite(path_text_results + '_iteration1.png',text_cc_image)

        #--2. rotate and re-call tesseract methods
        cc_image_path = self.rotate_image(path_text_results, '_iteration1.png','_iteration2.png')
        text_cordinates_2, text_cc_image, no_text_image,text_to_erase = self.text_detect_process(language,cc_image_path, otsu,numbers,debug_mode,text_to_erase,flipped=True)

        self.write_erasable_text_to_file(text_to_erase,path_gravvitas,only_image_name)

        # final_text_cordinates = []
        # for t_row in text_cordinates_2:
        #     final_text_cordinates.append([t_row[0], t_row[1], t_row[2]])
        # for text_row in final_text_cordinates:
        #     text,centre,bb_points = text_row
        #     text_draw_x, text_draw_y = centre
        #     cv2.rectangle(otsu_copy,(bb_points[0],bb_points[1]),(bb_points[2],bb_points[3]),
        #                   (0,0,255),2)
        #     cv2.putText(otsu_copy,text.upper(),(text_draw_x-10,text_draw_y-30),
        #                 cv2.FONT_HERSHEY_COMPLEX,1,(0,0,255),3,cv2.CV_AA)
        # cv2.imwrite(self.current_directory + '/Final_Lines/'+ only_image_name+ '_detected.png', otsu_copy)


        #--3. get only room names by matching detected words with pre-defined room names
        # text_cordinates_matched = self.match_text_to_rooms(text_cordinates_1+text_cordinates_2)
        #--3. for Gts don't need translated room names: nned all text
        # text_cordinates_matched= text_cordinates_1+text_cordinates_2
        
        #--4. write text to gravvitas
        # self.write_text_to_gravvitas(path_gravvitas,only_image_name,text_to_erase)

        # --5. return only [text, cordinate,bb_box] to the main_method
        # final_text_cordinates = []
        # for t_row in text_cordinates_matched:
        #     final_text_cordinates.append([t_row[0], t_row[1], t_row[2]])

        # if debug_mode:
        # for text_row in final_text_cordinates:
        #     text,centre,bb_points = text_row
        #     text_draw_x, text_draw_y = centre
        #     cv2.rectangle(otsu_copy,(bb_points[0],bb_points[1]),(bb_points[2],bb_points[3]),
        #                   (0,0,255),2)
        #     cv2.putText(otsu_copy,text.upper(),(text_draw_x-10,text_draw_y-30),
        #                 cv2.FONT_HERSHEY_COMPLEX,1,(0,0,255),3,cv2.CV_AA)
        # cv2.imwrite(self.current_directory + '/Final_Lines/'+only_image_name+'_detected.png',otsu_copy)

        # self.image_remove_text = no_text_image
        # name = str(self.file_name[0:len(self.file_name) - 4])
        # path = str(self.path_output + name + '/')
        # if not (os.path.exists(path)):
        #     os.mkdir(path)
        # cv2.imwrite(self.path_output +only_image_name+'/no_text.png', no_text_image)


        # return final_text_cordinates


    def text_detect_process(self,language,cc_image_path,otsu,numbers,debug_mode,text_to_erase,flipped):
        text_cc_image = cv2.imread(cc_image_path,cv2.IMREAD_COLOR)

        textposition = self.tesseract_text_detect(language,cc_image_path)
        tesseract_words,numbers,character_length = self.get_text_from_hocr(textposition,numbers,cc_image_path,debug_mode)
        tesseract_words = self.combine_closeby_words(tesseract_words,character_length)
        tesseract_words,other_tesseract_detections,text_cc_image,no_text_image,text_to_erase = self.other_validation(tesseract_words,text_cc_image,otsu,numbers,text_to_erase,flipped)
        # #--match all tesseract_words with dictionary and find matching floorplan words
        # text_cordinates,other_tesseract_detections = self.dictionary_matching(language,tesseract_words,other_tesseract_detections)

        return [],text_cc_image,no_text_image,text_to_erase


    def tesseract_text_detect(self,language,cc_image_path):
        image = cv.LoadImage(cc_image_path, cv.CV_LOAD_IMAGE_GRAYSCALE)

        api = tesseract.TessBaseAPI()
        api.Init(".", language, tesseract.OEM_DEFAULT)
        tesseract.SetCvImage(image, api)
        tesseract_text = api.GetUTF8Text()
        textposition = api.GetHOCRText(1)
        word_confidence = api.AllWordConfidences()

        # --get word confidences
        confidence_list = []
        for tess_text in tesseract_text:
            for tess_confidence in word_confidence:
                confidence_list.append([tess_text, tess_confidence])

        return textposition

    def get_text_from_hocr(self,textposition,numbers,cc_image_path,debug_mode):
        if debug_mode:
            otsu_copy = cv2.imread(cc_image_path, cv2.IMREAD_COLOR)
        tesseract_fp_words = []
        soup = BeautifulSoup(textposition, "lxml")
        # temp_file = open(self.path_output+'hocr_file.xml','w')
        # for span in soup:
        #     temp_file.write(str(span))
        # temp_file.close()

        word_length_lst = []
        for span in soup.findAll("span", {"class": "ocrx_word"}):
            # no_text_image = self.my_function_remove_text(span,image)
            # --check if hocr file has a string or not '<span...> </span> against <span>abc</span>
            if len(span) > 0:
                bbox_data = (span.get('title'))
                only_numbers = re.findall(r'\d+', bbox_data)
                x1 = int(only_numbers[0])
                y1 = int(only_numbers[1])
                x2 = int(only_numbers[2])
                y2 = int(only_numbers[3])
                word_confidence = int(only_numbers[4])
                bounding_box = [x1, y1, x2, y2]
                #---span.string can be 'none' at times. So check that
                if span.string is not None:
                    #-- get only the text
                    text_name = unicodedata.normalize('NFKD', span.string).encode('ascii', 'ignore')
                    word_length_lst = self.get_character_size(span.string, text_name,
                                      (abs(x1 - x2)),word_length_lst)
                    #--get only the numbers out
                    only_numbers = re.sub("[^0-9]", "", span.string)
                    if text_name=='m2':
                        numbers.append(['m2',bounding_box, word_confidence])
                    elif len(only_numbers)>1:
                        numbers.append([re.sub("[^0-9]",",",span.string),bounding_box, word_confidence])
                    else:
                    #--end
                        if len(text_name) == 0:
                            continue
                        tesseract_fp_words.append([text_name, bounding_box, word_confidence])
                    if debug_mode:
                        cv2.rectangle(otsu_copy, (x1, y1), (x2, y2),(0, 0, 255), 2)
                        cv2.putText(otsu_copy, text_name, (x1 - 5, y1 - 5),
                                    cv2.FONT_HERSHEY_COMPLEX, 1, (0, 0, 255), 2)
        if len(word_length_lst)>0:
            character_length = int(sum(word_length_lst)/float(len(word_length_lst)))
        else:
            character_length = 0

        if debug_mode:
            cv2.imwrite(self.path_debug + 'Extract_text/text_identification/' + '_detected_tess.png',
                    otsu_copy)


        return tesseract_fp_words,numbers,character_length

    def get_character_size(self,string, text_name,bb_width, word_length_lst):
        if len(string)==len(text_name) and len(string)>0:
            word_length_lst.append(bb_width / float(len(string)))
        return word_length_lst


    def combine_closeby_words(self,tesseract_words,character_length):
        #--filter to keep only numbers, ' ',alpha
        tesseract_words_v2 = self.find_alpha_num_space(tesseract_words)

        for r,row in enumerate(tesseract_words_v2):
            text, bounding_box, confidence = row
            tesseract_words_v2[r][0] = text.replace('.',' ')
            # print tesseract_words_v2[r]
            tesseract_words_v2[r][0] = tesseract_words_v2[r][0].replace('-', ' ')
            # print tesseract_words_v2[r]


        #--- find centre of each word and add that to list
        # temp_tesseract_words = []
        # for word_row in tesseract_words_v2:
        #     x1, y1, x2, y2 = word_row[1]
        #     p1,p2,p3,p4 = self.iterative_obj.find_points_rectangle(x1, y1, x2, y2)
        #     cx, cy = self.iterative_obj.find_centre_of_line([p2, p3])
        #     cx1, cy1 = self.iterative_obj.find_centre_of_line([p3, p4])
        #     temp_tesseract_words.append([word_row[0],word_row[1],word_row[2],[cx, cy],[cx1, cy1]])

        #--get distance between 2 text cords and if < threshold, add to combinable words
        already_used,new_tesseract_words = [],[]
        for w_row1, word_1 in enumerate(tesseract_words_v2):
            text1, bounding_box1, confidence1 = word_1

            # --since there cannot be first word with alpha_num, we filter it
            # text1 = ''.join([c for c in text1 if c.isalpha() or c == ' '])
            if w_row1 in already_used or len(text1)==0 or text1==' ' or confidence1<=60 or text1.isdigit():
                continue
            else:
                x1,y1,x2,y2 = bounding_box1
                combinable_words = []
                for w_row2, word_2 in enumerate(tesseract_words_v2):
                    text2, bounding_box2, confidence2 = word_2
                    if w_row2 <= w_row1 or w_row2 in already_used or confidence2<=60:
                        continue
                    else:
                        x3,y3,x4,y4 = bounding_box2
                        # distance = math.hypot(cx2-cx1,cy2-cy1)
                        combinable_flag = False
                        # ---horizontal word combination (distance, same lineish horizontally)
                        if abs(x3-x2) < (character_length*2) and abs(y3 - y1) < (character_length/4):
                            combinable_flag = True

                        # --vertical word combination (distance,same columnish vertically)
                        # elif abs(y3 - y2) < (character_length + (character_length / 3)) and abs(x3 - x1) < (character_length * 3.5):
                        elif abs(y3 - y2) < (character_length*2) and abs(x3 - x1) < (character_length * 4):
                            combinable_flag = True

                        if combinable_flag:
                            x_difference, y_difference = abs(x1 - x4), abs(y1 - y4)
                            if (x_difference - y_difference) > (character_length/9) and x_difference > (character_length)+(character_length/9):
                                combinable_words.append([text2, bounding_box2, confidence2])
                                already_used.append(w_row2)



                #----prepare combinable_words to add to main list
                # combinable_words.sort(key=itemgetter(3))



                #---combine the words
                temp_new_word = []
                for w,single_word in enumerate(combinable_words):
                    s_text, s_bounding_box, s_confidence = single_word
                    x3, y3, x4, y4 = s_bounding_box
                    if w > 0:
                        text1, bounding_box1, confidence1 = temp_new_word
                        x1, y1, x2, y2 = bounding_box1

                    #--'bedroom','1' case->'bedroom 1'
                    if len(text1)>1 and len(s_text)==1 and s_text.isdigit():
                        new_word = text1 + ' ' + s_text
                    #--'bed','room' case-> 'bed room'
                    elif len(text1)>1 and len(s_text)>2:
                        new_word = text1 + ' ' + s_text
                    #--'P','O','R','C','HE' case-> 'PORCHE' - wn't get this since distance from p-he was too far
                    else:
                        new_word = text1 + s_text

                    new_cordinates = x1, y1, x4, y4
                    new_word_confidence = (confidence1 + s_confidence) / 2
                    temp_new_word = [new_word, new_cordinates, new_word_confidence]

                #--add to main list
                if len(temp_new_word) > 0:
                    new_tesseract_words.append(temp_new_word)
                else:
                    new_tesseract_words.append([text1, bounding_box1, confidence1])

        return new_tesseract_words






    def find_alpha_num_space(self,combinable_words):
        combinable_words_v2 = []
        for word_row in combinable_words:
            word = word_row[0]
            new_word = ''.join([c for c in word if c.isalnum() or c==' '])
            if new_word != ' ' and len(new_word)>0:
                combinable_words_v2.append(word_row)
        return combinable_words_v2


    def combine_closeby_words_1(self,tesseract_words):
        temp_tesseract_words = []
        for word in tesseract_words:
            x1, y1, x2, y2 = word[1]
            cx, cy = self.iterative_obj.find_centre_of_line([[x1, y1], [x2, y2]])
            temp_tesseract_words.append([word[0],word[1],word[2],[cx,cy]])

        already_used,new_tesseract_words = [],[]
        for w_row1, word_1 in enumerate(temp_tesseract_words):
            if w_row1 in already_used:
                continue
            else:
                text1, bounding_box1,confidence1,centre1 = word_1
                x1,y1,x2,y2 = bounding_box1
                cx1,cy1 = centre1
                new_words = []
                for w_row2, word_2 in enumerate(temp_tesseract_words):
                    if w_row2 > w_row1:
                        if w_row2 in already_used:
                            continue
                        else:
                            text2, bounding_box2, confidence2,centre2 = word_2
                            x3,y3,x4,y4 = bounding_box2
                            cx2, cy2 = centre2
                            distance = math.hypot(cx2-cx1,cy2-cy1)
                            if distance < 150:
                                # new_word = text1 + ' ' + text2
                                # new_cordinates = x1, y1, x4, y4
                                # new_word_confidence = (confidence1 + confidence2) / 2
                                # ----check word orientation
                                x_difference, y_difference = abs(x1 - x4), abs(y1 - y4)
                                if (x_difference - y_difference) > 2 and x_difference > 20:
                                    new_words.append([text2, bounding_box2, confidence2,distance])
                                    already_used.append(w_row2)
                new_words.sort(key=itemgetter(3))
                temp_new_word,confidence_list = [],[confidence1]
                for w,single_word in enumerate(new_words):
                    text_2,bounding_box_2, confidence_2 = single_word[:-1]
                    x3, y3, x4, y4 = bounding_box_2
                    if w > 0:
                        text1, bounding_box1, confidence1,conf_l = temp_new_word
                        x1, y1, x2, y2 = bounding_box1

                    # else:
                    #     confidence_list.append(confidence1)
                    #     confidence_list.append(confidence2)

                    if text_2.isdigit()==False and len(text_2)<3:
                        new_word = text1 + text_2
                        confidence_list.append(confidence_2)
                    else:
                        new_word = text1 + ' ' + text_2
                        confidence_list.append(0)
                        confidence_list.append(confidence_2)

                    new_cordinates = x1, y1, x4, y4
                    new_word_confidence = (confidence1 + confidence_2) / 2
                    temp_new_word = [new_word, new_cordinates, new_word_confidence,confidence_list]

                if len(temp_new_word) > 0:
                    new_tesseract_words.append(temp_new_word)
                else:
                    new_tesseract_words.append([text1, bounding_box1,confidence1,[]])

        # ----combine words too close to each other
        for w_row1, word_1 in enumerate(tesseract_words):
            before_x1, before_y1, before_x2, before_y2 = word_1[1]
            word_confidence1 = int(word_1[2])
            for w_row2, word_2 in enumerate(tesseract_words):
                if w_row2 > w_row1:
                    current_x1, current_y1, current_x2, current_y2 = word_2[1]
                    word_confidence2 = int(word_2[2])
                    # ---horizontal word combination (distance, same lineish horizontally)
                    if abs(current_x1 - before_x2) < 35 and abs(current_y1 - before_y1) < 5:
                        new_word = word_1[0] + ' ' + word_2[0]
                        new_cordinates = before_x1, before_y1, current_x2, current_y2
                        new_word_confidence = (word_confidence1 + word_confidence2) / 2
                    # --vertical word combination (distance,same columnish vertically)
                    elif abs(current_y1 - before_y2) < 25 and abs(current_x1 - before_x1) < 65:
                        new_word = word_1[0] + ' ' + word_2[0]
                        new_cordinates = before_x1, before_y1, current_x2, current_y2
                        new_word_confidence = (word_confidence1 + word_confidence2) / 2
                    else:
                        new_word = 0
                        new_cordinates = 0
                        new_word_confidence = 0

                    if new_word != 0:
                        # ----check word orientation
                        x_difference, y_difference = abs(new_cordinates[0] - new_cordinates[2]), abs(
                            new_cordinates[1] - new_cordinates[3])
                        if (x_difference - y_difference) > 2 and x_difference > 20:
                            tesseract_words[w_row1] = [new_word, new_cordinates, new_word_confidence]
                            del tesseract_words[w_row2]
                            break


        return new_tesseract_words


    def combine_closeby_words2(self,tesseract_words):
        # ----combine words too close to each other
        for w_row1, word_1 in enumerate(tesseract_words):
            before_x1, before_y1, before_x2, before_y2 = word_1[1]
            word_confidence1 = int(word_1[2])
            for w_row2, word_2 in enumerate(tesseract_words):
                if w_row2 > w_row1:
                    current_x1, current_y1, current_x2, current_y2 = word_2[1]
                    word_confidence2 = int(word_2[2])
                    # ---horizontal word combination (distance, same lineish horizontally)
                    if abs(current_x1 - before_x2) < 35 and abs(current_y1 - before_y1) < 5:
                        new_word = word_1[0] + ' ' + word_2[0]
                        new_cordinates = before_x1, before_y1, current_x2, current_y2
                        new_word_confidence = (word_confidence1 + word_confidence2) / 2
                    # --vertical word combination (distance,same columnish vertically)
                    elif abs(current_y1 - before_y2) < 25 and abs(current_x1 - before_x1) < 65:
                        new_word = word_1[0] + ' ' + word_2[0]
                        new_cordinates = before_x1, before_y1, current_x2, current_y2
                        new_word_confidence = (word_confidence1 + word_confidence2) / 2
                    else:
                        new_word = 0
                        new_cordinates = 0
                        new_word_confidence = 0

                    if new_word != 0:
                        # ----check word orientation
                        x_difference, y_difference = abs(new_cordinates[0] - new_cordinates[2]), abs(
                            new_cordinates[1] - new_cordinates[3])
                        if (x_difference - y_difference) > 2 and x_difference > 20:
                            tesseract_words[w_row1] = [new_word, new_cordinates, new_word_confidence]
                            del tesseract_words[w_row2]
                            break
        return tesseract_words


    def other_validation(self,tesseract_words,text_cc_image,no_text_image,numbers,text_to_erase,flipped):
        final_tesseract_words = []
        other_tessract_detections = []
        for tess_row in tesseract_words:
            detected_text = tess_row[0]
            word_confidence = tess_row[2]
            bounding_box = tess_row[1]
            x1, y1, x2, y2 = bounding_box


            #---C1. check if word is horizontal
            if self.condition_1(x1, y1, x2, y2,detected_text):
                erase_word = False
                # --change cords(for storage) if image is flipped
                # ---xs->heights,ys->width coz image is flipped
                if flipped:
                    bounding_box = [y1, self.height - x1, y2, self.height - x2]
                    tess_row[1] = bounding_box
                    x1,y1,x2,y2 = bounding_box
                # --C2. count number of occurences per character to delete 'OO' like false identification
                # -- this also deletes single chracter words
                if self.condition_2(detected_text):
                    erase_word = True
                    #--C3. check if word is of type 'bedroom 1' if keep word with space
                    has_number, new_word = self.condition_3(detected_text)
                    if has_number:
                        final_word = new_word
                    else:
                        detected_text_1 = ''.join([c for c in detected_text if c.isalpha() or c == ' '])
                        #--C4. replace 1 with 'i' since both are similar
                        #--C5. keep only alpha and '/'
                        final_word = self.condition_4_5(detected_text_1)

                    if len(final_word) > 1 and word_confidence > 60:
                        final_tesseract_words.append([final_word, bounding_box, word_confidence])
                        erase_word = True
                    #--for later use
                    else:
                        other_tessract_detections.append(tess_row)

                #---for later use
                elif word_confidence > 60:
                    erase_word = True
                    other_tessract_detections.append(tess_row)

                #--if condition_2,3,4,5 passed or word_confidence > 60: erase that from image as a word
                if erase_word:
                    # --erase horizontal word from 'text_cc_image' to be rotated later
                    cv2.rectangle(text_cc_image, (x1, y1), (x2, y2), (255, 255, 255), -1)
                    # --remove all horizontal text from image for piers use later
                    no_text_image = self.anu_function_remove_text(no_text_image, x1, y1, x2, y2)
                    text_to_erase.append([detected_text,bounding_box,word_confidence])

        for num_row in numbers:
            detected_text = num_row[0]
            x1, y1, x2, y2 = num_row[1]
            word_confidence = num_row[2]

            # ---C1. check if word is horizontal
            if self.condition_1(x1, y1, x2, y2, detected_text):
                erase_word = False
                if word_confidence > 40:
                    erase_word = True
                    other_tessract_detections.append(num_row)

                    # --if condition_2 passed or word_confidence > 60: erase that from image as a word
                if erase_word:
                    # --erase horizontal word from 'text_cc_image' to be rotated later
                    cv2.rectangle(text_cc_image, (x1, y1), (x2, y2), (255, 255, 255), -1)
                    # --remove all horizontal text from image for piers use later
                    no_text_image = self.anu_function_remove_text(no_text_image, x1, y1, x2, y2)
                    text_to_erase.append([detected_text, num_row[1], word_confidence])

        return final_tesseract_words,other_tessract_detections,text_cc_image,no_text_image,text_to_erase


    def condition_1(self,x1, y1, x2, y2,detected_text):
        x_difference, y_difference = abs(x1 - x2), abs(y1 - y2)
        if (x_difference - y_difference) >= 0 and x_difference > 20 and detected_text != None:
            return True
        else:
            return False

    def condition_2(self,text_name):
        count_letters = []
        for L in text_name:
            chracter_exists = False
            if len(count_letters) > 0:
                for cur_row, C in enumerate(count_letters):
                    if C[0] == L:
                        chracter_exists = True
                        break
            if chracter_exists == False:
                count_letters.append([L, text_name.count(L)])
            else:
                continue

        condition_2 = True
        # calculate if same letter is repeated or after removing '/' such,
        # still only one letter is repeated
        alpha_character_count = 0
        for character_count in count_letters:
            if len(count_letters) == 1:
                if character_count[1] == len(text_name):
                    condition_2 = False
            else:
                if character_count[0].isalpha():
                    alpha_character_count += 1
        if alpha_character_count < 2:
            condition_2 = False

        return condition_2

    def condition_3(self,detected_text):
        # --break the word by space
        text_word = detected_text.split()
        if len(text_word) == 2 and len(text_word[1]) == 1 and text_word[1].isdigit():
            returnable_word = ''.join([c for c in text_word[0] if c.isalpha() or c == ' '])
            returnable_word = returnable_word + ' '+ text_word[1]
            # print returnable_word
            return True,returnable_word
        else:
            return False,''

    def condition_4_5(self,detected_text):
        # ---check each character and take them only if is letter or a '/'
        final_word = ''
        for character in detected_text:
            # --replace 1 with 'i' since both are similar
            if character == '1':
                new_char = 'i'
            else:
                new_char = character
            if new_char.isalpha() or new_char == '/':
                # -- add character to word after converting to lower case
                final_word = final_word + new_char.lower()
        return final_word

    def condition_6(self,detected_text,confidence_list,total_confidence):
        # if len(confidence_list)==0:
        #     return ''.join([c for c in detected_text if c.isalpha()]),total_confidence
        # else:
        final_word, new_confidence = '', 0
        for c_num,c in enumerate(detected_text):
            if c.isalpha() or c==' ':
                final_word = final_word+c
                if len(confidence_list)==len(detected_text):
                    new_confidence = new_confidence+confidence_list[c_num]
                else:
                    new_confidence = total_confidence
        return final_word,new_confidence

    def dictionary_matching(self, language,tesseract_words,other_tessract_detections):
        dict_matched_words_list = []
        for row_num,row in enumerate(tesseract_words):
            detected_word = row[0]
            coordinates = row[1]
            confidence = row[2]
            # ----check if word contains a slash
            slash_position = detected_word.find('/')
            # ---check if word has 'bed room 1, bedroom 2,etc'
            word_list = detected_word.split()

            #--case_01: for '/' seperated word :a/c, dining/living
            if slash_position != -1:
                found_match,dict_matched_word_data = self.contains_slash(language,detected_word,slash_position)
            #--case_02: for bedroom 1
            elif len(word_list) == 2:
                found_match,dict_matched_word_data = self.contains_digit(language,word_list)
            #--case_03: all else
            else:
                found_match,dict_matched_word_data = self.contains_only_alpha(language,detected_word)


            #--if we find a match to the word, add it to list, else add to other_words[
            if found_match:
                dict_matched_words_list.append([coordinates]+[confidence]+[dict_matched_word_data])
            else:
                other_tessract_detections.append(row)

        text_coordinates = self.insert_dictionary_matches_to_final_list(dict_matched_words_list)

        return text_coordinates,other_tessract_detections




    def contains_slash(self,language,detected_word,slash_position):
        unslashed_words = self.generate_unslashed_words(detected_word,slash_position)
        dict_matched_word_data_list = []
        #--for each 'living' and 'dining' in 'living/dining' find a match
        for word_to_check in unslashed_words:
            found_match,dict_matched_word_data = self.find_matching_word(language,word_to_check)
            if found_match:
                dict_matched_word_data_list.append(dict_matched_word_data)
        #--condition: if atleast one slashed word had a match
        if len(dict_matched_word_data_list) >0:
            dict_matched_word_data_edited = self.regenerate_dict_word_data(detected_word,dict_matched_word_data_list)
            found_match = True
        else:
            dict_matched_word_data_edited = []
            found_match = False

        return found_match, dict_matched_word_data_edited

    def generate_unslashed_words(self,detected_word,slash_position):
        if len(detected_word)<4:
            slashed_words = detected_word[:slash_position + slash_position + 1:]
        else:
            slashed_words = [detected_word[:slash_position],
                             detected_word[slash_position + 1:]]
        return slashed_words

    def regenerate_dict_word_data(self,detected_word,dict_matched_word_data_list):
        regenerated_dict_word,regenerated_edit_distance_value = '',0
        #--recreate the word by adding '/' to it
        for t, temp_row in enumerate(dict_matched_word_data_list):
            temp_word = temp_row[2]
            temp_edit_distance = temp_row[3]
            if t == 0:
                regenerated_dict_word = temp_word + '/'
                regenerated_edit_distance_value = temp_edit_distance
            elif t == len(dict_matched_word_data_list) - 1:
                regenerated_dict_word = regenerated_dict_word + temp_word
                regenerated_edit_distance_value = regenerated_edit_distance_value + temp_edit_distance
            else:
                regenerated_dict_word = regenerated_dict_word + temp_word + '/'
                regenerated_edit_distance_value = regenerated_edit_distance_value + temp_edit_distance
        #--add '/' addded word and added-ED to list
        dict_matched_word_data_edited = [detected_word, 'D', regenerated_dict_word, regenerated_edit_distance_value]

        return dict_matched_word_data_edited

    def contains_digit(self,language,word_list):
        word_to_check = word_list[0]
        found_match, dict_matched_word_data =  self.find_matching_word(language,word_to_check.lower())
        #--if has a match add room number (1) to the detected word (chambre) -> chambre 1
        if found_match:
            dict_matched_word_data[0] = dict_matched_word_data[0] +' '+ word_list[1]
            if dict_matched_word_data[1] == 'SG':
                dict_matched_word_data[2] = dict_matched_word_data[2] +' '+ word_list[1]

        return found_match, dict_matched_word_data

    def contains_only_alpha(self,language,detected_word):
        # --remove all spaces and check
        word_to_check = "".join(c for c in detected_word if c not in (' '))
        return self.find_matching_word(language,word_to_check)

    def find_matching_word(self,language,word_to_check):
        matched_word, is_direct_match, suggestion_list = self.match_word_with_dictionary(
                                                            language,word_to_check)
        if is_direct_match:
            dict_matched_word_data = [matched_word, 'DM', '', 0]
            found_match = True
        elif len(suggestion_list) > 0:
            suggestion_list.sort(key=itemgetter(1))
            if suggestion_list[0][1]==1:
                suggested_word = suggestion_list[0][0]
                edit_distance_value = 1
                dict_matched_word_data = [word_to_check, 'SG', suggested_word, edit_distance_value]
                found_match = True
            else:
                #--suggestion_list[] filter 1: find suggestions wit same first letter as word_to_check
                filtered_suggestion_list = self.suggestion_list_filter_1(suggestion_list,word_to_check)
                #--suggestion_list[] filter 2: find_suggestion_of_closest_length_to_word_to_check
                best_match_suggestion = self.suggestion_list_filter_2(word_to_check,filtered_suggestion_list)
                if len(filtered_suggestion_list) > 0:
                    dict_matched_word_data = best_match_suggestion
                    found_match = True
                else:
                    found_match = False
                    dict_matched_word_data = []
        else:
            found_match = False
            dict_matched_word_data = []
        return found_match, dict_matched_word_data

    def match_word_with_dictionary(self,language,word_to_check):
        # --read dictionary.txt file and get its content as a list based on langugage
        if language=="fra":
            dict_path = self.current_directory + '/Model/dict_french.txt'
        elif language=="eng":
            dict_path = self.current_directory + '/Model/dict_english.txt'
        else:
            print 'Unknown language ERROR'
            sys.exit()
        with open(dict_path) as f:
            dictionary_content = f.read().splitlines()

        is_direct_match = False
        matched_word = ''
        #--define thresholds: 1-word_lengths, 2-commonLetterCount
        word_length_threshold = (len(word_to_check) / 100) * 90
        no_common_letter_threshold = (len(word_to_check) / 100) * 80
        suggestion_list = []
        for dictionary_word in dictionary_content:
            #---calculate leveinstein edit distance()
            edit_distance = distance(str(dictionary_word), str(word_to_check))
            #---if is a is_direct_match
            if edit_distance == 0:
                matched_word = word_to_check
                is_direct_match = True
                break
            #--else if difference is < 5
            elif edit_distance < 5:
                #--Condition 01: length similarity check (if dictionary_word && word_to_check have approx.similar no of letters)
                if abs(len(word_to_check) - len(dictionary_word)) <= word_length_threshold:
                    # --Condition 02: common letter similarity check
                    letter_similarity_count = self.find_no_of_common_letters(dictionary_word,word_to_check)
                    if letter_similarity_count > no_common_letter_threshold:
                        #--find place to insert sugggestion
                        index_to_insert = len(suggestion_list)
                        suggestion_list.append([])
                        suggestion_list[index_to_insert].append(dictionary_word)
                        suggestion_list[index_to_insert].append(edit_distance)
                    #-- Condition 03: if both words are too small getting a % of common letters is not sensible
                    elif len(word_to_check) < 3 and len(dictionary_word) < 3:
                        if letter_similarity_count > 0:
                            index_to_insert = len(suggestion_list)
                            suggestion_list.append([])
                            suggestion_list[index_to_insert].append(dictionary_word)
                            suggestion_list[index_to_insert].append(edit_distance)
        #---if found a direct match later: remove all earlier suggestions
        if is_direct_match:
            suggestion_list = []
        return matched_word, is_direct_match, suggestion_list

    def find_no_of_common_letters(self,dictionary_word,word_to_check):
        letter_similarity_count = 0
        copy_dictionary_word = list(dictionary_word)
        for image_letter in word_to_check:
            for num, dict_letter in enumerate(copy_dictionary_word):
                if dict_letter == image_letter:
                    letter_similarity_count = letter_similarity_count + 1
                    del copy_dictionary_word[num]
                    break
        return letter_similarity_count

    def suggestion_list_filter_1(self,suggestion_list,word_to_check):
        possible_matches = []
        for each_suggestion in suggestion_list:
            if each_suggestion[0] == word_to_check[0]:
                possible_matches.append(each_suggestion)
            else:
                continue
        return possible_matches

    def suggestion_list_filter_2(self,word_to_check,possible_matches_temp_list):
        len_word_check = len(word_to_check)
        least_difference_threshold = 100
        best_match = []
        for each_match in possible_matches_temp_list:
            suggestion = each_match[0]
            if len(suggestion)-len_word_check < least_difference_threshold:
                best_match = [word_to_check, 'SG',suggestion,each_match[1]]
        return best_match

    def insert_dictionary_matches_to_final_list(self,dict_matched_words_list):
        text_coordinates = []
        # ---append identified words to text_coordinates []
        for row_num, each_row in enumerate(dict_matched_words_list):
            numbers,confidence,detection_data = each_row[0],each_row[1],each_row[2]
            original_word,dict_check,suggestion,edit_distance_value = detection_data[0],detection_data[1],detection_data[2],detection_data[3]
            detected_word = ''
            #--if is a Direct Match add original word as the correct word
            if dict_check == 'DM':
                correct_word = original_word
            #--else take suggestion as the correct word
            else:
                correct_word = suggestion
                detected_word = original_word
            x1, y1, x2, y2 = int(numbers[0]), int(numbers[1]), int(numbers[2]), int(numbers[3])
            x_centre = x1 + ((x2 - x1) / 2)
            y_centre = y1 + ((y2 - y1) / 2)
            text_coordinates.append(
                [correct_word, [x_centre, y_centre], [x1, y1, x2, y2], confidence, dict_check,
                 detected_word, edit_distance_value])

        return text_coordinates






    def anu_function_remove_text(self,img,x1,y1,x2,y2):
        #--check later
        cv2.rectangle(img, (x1-1, y1-1), (x2+2, y2+2), (255,255,255), -1)
        return img


    def rotate_image(self,path_text_results,image1,image2):
        img_to_rotate = Image.open(path_text_results+image1)
        rotated_img = img_to_rotate.rotate(270,expand=True)
        rotated_img.save(path_text_results+image2)
        # image2 = cv.LoadImage(path_text_results + image2,cv.CV_LOAD_IMAGE_GRAYSCALE)
        cc_image_path = path_text_results + image2
        return cc_image_path

    def match_text_to_rooms(self,text_cordinates):
        text_files_path = self.current_directory+'/Model/floor_plan_text/match_words.txt'
        room_data_file = open(text_files_path,'r')

        #--get text file data to a list
        rooms_connection = []
        for row_num, text_line in enumerate(room_data_file):
            line_seperate = text_line.split(':')
            if len(line_seperate) > 1:
                rooms_connection.append([])
                rooms_connection[len(rooms_connection) - 1].append(line_seperate[0].strip())
                rooms_connection[len(rooms_connection) - 1].append(line_seperate[1].strip().split(','))

        #--match detected words with rooms in text file
        for current_row, tesseract_word_row in enumerate(text_cordinates):
            #--get detected word without digits
            detected_whole_word = tesseract_word_row[0]
            text_word = detected_whole_word.split()
            # --check if the word has two words; 2nd word is len()==1 and is a digit
            #--- if so take only first word ex: chambre 1 ;_ we need only 'chambre'
            if len(text_word)>1 and text_word[1].isdigit():
                detected_word = text_word[0]
                digit_exists = True
            else:
                detected_word = detected_whole_word
                digit_exists = False

            word_matched = False
            for room_data_row in rooms_connection:
                room_name_to_assign = room_data_row[0]
                room_names_to_check = room_data_row[1]
                for single_room_name in room_names_to_check:
                    if single_room_name==detected_word:
                        #--if is in pattern 'bedroom 1': add digit to the end of word
                        if digit_exists:
                            text_cordinates[current_row][0] = room_name_to_assign+' '+text_word[1]
                        else:
                            text_cordinates[current_row][0] = room_name_to_assign
                        word_matched = True
                        break
                if word_matched:
                    break

        return text_cordinates

    def write_text_to_gravvitas(self,path_gravvitas,only_image_name,text_cordinates_matched):
        # --write text to gravvitas
        text_details_file = open(path_gravvitas + only_image_name + '_6_text_details.txt', 'w')
        # unknown_details_file = open(path_gravvitas + only_image_name + '_9_unknown_details.txt', 'w')
        for text_row in text_cordinates_matched:
            # print text_row[0]
            tx, ty = text_row[1]
            # --cordinates
            text_and_cordinate = str(text_row[0]) + ' : [' + str(tx) + ',' + str(ty) + '] : '
            # ---bb_box
            x1, y1, x2, y2 = text_row[2]
            text_bounding_box = '[' + str(x1) + ',' + str(y1) + ',' + str(x2) + ',' + str(y2) + '] : '
            if len(text_row[5]) == 0:
                text_row[5] = 'None'
            confidence_and_others = 'confidence,dict,original_word,difference : [' + str(text_row[3]) + ',' + str(text_row[4]) + ',' + str(text_row[5]) + ',' + str(text_row[6]) + ']'
            # if text_row[3] >30:
            text_details_file.write(text_and_cordinate + text_bounding_box + confidence_and_others + '\n')
            # else:
            #     unknown_details_file.write(text_and_cordinate + text_bounding_box + confidence_and_others + '\n')

            # if str(text_row[4])=='dict':
            #     print image_name_without_extension,str(text_row[0]),text_row[5],text_row[3],len(text_row[0]),text_row[6]
        if os.stat(path_gravvitas + only_image_name + '_6_text_details.txt').st_size == 0:
            text_details_file.write('No Text Detected' + '\n')
        text_details_file.close()

    def write_erasable_text_to_file(self,text_to_erase,path_gravvitas,only_image_name):
        text_details_file = open(path_gravvitas + only_image_name + '_6_text_details.txt', 'w')
        for text_row in text_to_erase:
            text, text_bb, confidence = text_row
            # ---bb_box
            x1, y1, x2, y2 = text_bb
            tx, ty = self.iterative_obj.find_centre_of_line([[x1, y1],[x2, y2]])

            # --cordinates
            text_and_cordinate = str(text) + ' : [' + str(tx) + ',' + str(ty) + '] : '
            # ---bb_box
            text_bounding_box = '[' + str(x1) + ',' + str(y1) + ',' + str(x2) + ',' + str(y2) + '] : '
            text_details_file.write(text_and_cordinate + text_bounding_box + str(confidence) + '\n')
        if os.stat(path_gravvitas + only_image_name + '_6_text_details.txt').st_size == 0:
            text_details_file.write('No Text Detected' + '\n')
        text_details_file.close()