authour = 'anu'

import cv2,os
from PIL import Image
from natsort import natsorted
import numpy as np
import itertools

from iterative_functions import iterative_functions_class


class close_cont_detect_elements_class:

    def __init__(self,avg_wall_width,current_directory,img_width,img_height):
        self.avg_wall_width = avg_wall_width
        self.current_directory = current_directory
        self.img_width,self.img_height = img_width,img_height
        self.iterative_obj = iterative_functions_class()

    def identify_elements(self,connection_lines,orginal_img_path,outer_cont,connection_rects,
                          only_image_name,path_output_dir):
        path_output = path_output_dir+only_image_name

        # self.win_path = path_output_dir + '/windows/'
        # self.door_path = path_output_dir + '/doors/'
        # self.all_rects = path_output_dir + '/rects/'
        self.only_image_name = only_image_name
        # self.iterative_obj.make_directory(self.win_path,False)
        # self.iterative_obj.make_directory(self.door_path,False)
        # self.iterative_obj.make_directory(self.all_rects, False)


        org_image = cv2.imread(orginal_img_path,cv2.IMREAD_COLOR)

        # connection_data = [
        #     [[714, 1474], [723, 1568]],
        #     [[1345, 1562], [1300, 1553]]
        # ]

        rect_width = (self.avg_wall_width*5)/2

        connection_components = []

        for connct_row,connection_line in enumerate(connection_lines):
            component_rect = connection_rects[connct_row]
            is_wall = self.check_rect_is_wall(component_rect,org_image,path_output)
            if is_wall:
                connection_components.append([component_rect,'wl'])
            else:
                x1,y1 = connection_line[0]
                x2, y2 = connection_line[1]
                #---find parelell lines
                parallel_lines = self.iterative_obj.find_paralell_lines(connection_line,rect_width)
                for parllel in parallel_lines:
                    # for parllel in parallel_lines:
                    x3,y3 = parllel[0]
                    x4, y4 = parllel[1]
                    # cv2.line(org_image,(tuple(parllel[0])),(tuple(parllel[1])),(0,0,255),5)
                    #--find which line in inside contour
                    cx,cy = self.iterative_obj.find_centre_of_line(parllel)
                    if cv2.pointPolygonTest(outer_cont,(cx,cy),False) ==1:
                        rect = [
                            [x1,y1],[x2,y2],[x3,y3],[x4,y4]
                        ]
                        cx1,cy1 = self.iterative_obj.find_centre_of_line(connection_line)
                        rcx, rcy = self.iterative_obj.find_centre_of_line([[cx,cy],[cx1,cy1]])
                        sorted_points = self.iterative_obj.order_polygon_points(rect, [rcx, rcy])
                        rect_cont = self.iterative_obj.convert_points_to_contour(sorted_points)


                        is_door = self.check_rect_is_door(sorted_points,org_image,path_output)
                        #-- is_door ==-1 means a invalid connection -that was too small to get
                        #-- contours in the cropped image, so most prob is invalid
                        if is_door:
                            connection_components.append([component_rect, 'd'])
                        else:
                            connection_components.append([component_rect, 'win'])

                    # cv2.drawContours(org_image,[rect_cont],-1,(0,0,255),5)

        # cv2.imwrite(path_output+'_parallel.png',org_image)

        return connection_components


    def check_rect_is_wall(self,rectangle,org_image,path_output):
        is_wall = False

        #--get a rectangle from rect shape used to draw connection
        imCrop = self.crop_image_by_rectangle(rectangle,org_image,path_output,wall=True)
        imcrop_height, imcrop_width, imcrop_d = imCrop.shape
        if imcrop_height!=0 and imcrop_width!=0:
            cv2.imwrite(path_output +'temp.png', imCrop)

            #--get colors in rectangle
            newimg = Image.open(path_output +'temp.png')
            os.remove(path_output + 'temp.png')
            color_list = newimg.getcolors()

            total_pixels = sum([row[0]for row in color_list])
            #--if has high black pixels amount : then is wall
            for col_row in color_list:
                if (0,0,0) in col_row:
                    if col_row[0] > (total_pixels/3)*2:
                        is_wall = True
                        break


        return is_wall


    def crop_image_by_rectangle(self,rectangle,org_image,path_output,wall):
        if wall:
            xs = [row[0] for row in rectangle]
            ys = [row[1] for row in rectangle]
            min_x,max_x = min(xs), max(xs)
            min_y, max_y = min(ys), max(ys)
            # --get a rectangle from rect shape used to draw connection
            imCrop = org_image[min_y:max_y, min_x:max_x]
            return imCrop

        else:
            xs = [row[0] for row in rectangle]
            ys = [row[1] for row in rectangle]
            min_x, max_x = min(xs), max(xs)
            min_y, max_y = min(ys), max(ys)

            # s_width = max_x - min_x
            # s_height = max_y - min_y

            # inverse_org_image = cv2.bitwise_not(org_image)
            # cv2.imwrite(path_output + 'inverse_org_image.png', inverse_org_image)

            contour = self.iterative_obj.convert_points_to_contour(rectangle)

            mask = np.zeros((self.img_height, self.img_width, 3), np.uint8)
            cv2.drawContours(mask, [contour], -1, (255, 255, 255), cv2.cv.CV_FILLED)
            # cv2.imwrite(path_output + 'mask.png', mask)
            only_cont_img = cv2.bitwise_and(org_image, mask)
            # cv2.imwrite(path_output + 'only_cont_img.png', only_cont_img)
            imCrop = only_cont_img[min_y:max_y, min_x:max_x]

            # --get a rectangle from rect shape used to draw connection
            # x1,y1 = rectangle[0]
            # x3,y3 = rectangle[2]
            #
            # imCrop = org_image[y1:y3, x1:x3]
            return imCrop


    def check_rect_is_door(self,rectangle,org_image,path_output):
        is_door = False
        imCrop = self.crop_image_by_rectangle(rectangle,org_image,path_output,wall=False)
        # cv2.imwrite(path_output +'-'+str(rectangle[0][0])+ '-door-temp.png', imCrop)
        crop_height, crop_width, d = imCrop.shape

        gray_image = cv2.cvtColor(imCrop, cv2.COLOR_RGB2GRAY)
        ret, thresh = cv2.threshold(gray_image, 127, 255, 0)
        contours, hierachy = cv2.findContours(thresh, cv2.RETR_TREE, cv2.CHAIN_APPROX_TC89_KCOS)

        max_contour,max_area,width,height = 0,0,0,0
        for c,cnt in enumerate(contours):
            rect = cv2.minAreaRect(cnt)
            # box = cv2.cv.BoxPoints(rect)
            # box = np.int0(box)
            if cv2.contourArea(cnt)> max_area:
                max_area = cv2.contourArea(cnt)
                width,height = rect[1]

            # current_cont = ~(np.zeros((crop_height, crop_width, 3), np.uint8))
            # cv2.drawContours(current_cont, [cnt], -1, (0, 0, 0), 2)
            # print str(rectangle[0][0])+'-'+str(c)+'-'+str(cv2.contourArea(cnt))
            # cv2.imwrite(path_output+'-'+str(rectangle[0][0])+'-'+str(c)+'.png',current_cont)
        if len(contours)>0 and max_area>0:
                # current_cont = ~(np.zeros((crop_height, crop_width, 3), np.uint8))
                # cv2.drawContours(current_cont, [max_contour], -1, (0, 0, 255), 2)
                # cv2.imwrite(self.all_rects + '-' +self.only_image_name+'-'+ str(rectangle[0][0]) + 'temp2.png', current_cont)



                # print str(rectangle[0][0]),'----',len(contours)
                # cv2.imwrite(path_output+'-'+str(rectangle[0][0])+'temp.png', imCrop)



                min_dimension = min(height,width)
                max_dimension = max(width,height)

                ratio = min_dimension/float(max_dimension)
                # print str(rectangle[0][0]),'---',ratio

                if ratio > 0.8:
                    is_door = True
                    # cv2.imwrite(self.door_path + self.only_image_name +'-'+ str(rectangle[0][0]) + '-door.png', imCrop)
                # else:
                    # cv2.imwrite(self.win_path + self.only_image_name + '-' + str(rectangle[0][0]) + '-win.png', imCrop)

                # if rectangle[0][0]==1261:
                #     template_path = self.current_directory+'/input_fps/contour_close_test/DW/'
                #     for file in natsorted(os.listdir(template_path)):
                #         img = cv2.imread(path_output+'temp.png')
                #         temp = cv2.imread(template_path+str(file))
                #         dist = 200
                #         num = -1
                #         skp, tkp = self.findKeyPoints(img, temp, dist)
                #         newimg = self.drawKeyPoints(img, temp, skp, tkp, num)
                #         cv2.imwrite(path_output+str(file)+'.png',newimg)

        return is_door


    def add_detections(self,old_components,connection_components,orginal_img_height,
                        orginal_img_width,only_image_name, path_output,
                       path_gravvitas_text,debug_mode,testing_mode):
        #---non test path
        all_comps_image = cv2.imread(path_output+only_image_name+'/Stefano_output.png',cv2.IMREAD_COLOR)
        # ---tst path
        if testing_mode:
            all_comps_image = cv2.imread(self.current_directory + '/OUTPUT/45/' + only_image_name + '/Stefano_output.png', cv2.IMREAD_COLOR)
            # all_comps_image = cv2.imread(self.current_directory+'/input_fps/temp/' + only_image_name + '_output.png', cv2.IMREAD_COLOR)

        window_details_file = open(path_gravvitas_text + only_image_name + '_4_window_details.txt', 'a')
        door_details_file = open(path_gravvitas_text + only_image_name + '_5_door_details.txt', 'a')
        wall_details_file = open(path_gravvitas_text + only_image_name + '_7_wall_details.txt', 'a')

        for new_row in connection_components:
            points, element_type = new_row
            contour = self.iterative_obj.convert_points_to_contour(points)
            if element_type == 'win':
                cv2.drawContours(all_comps_image, [contour], -1, (0, 255, 255), -1)
                if testing_mode==False:
                    point_string = self.iterative_obj.generate_string_from_point_list(points)
                    window_details_file.write('Window : ' + point_string + '\n')
            elif element_type == 'd':
                cv2.drawContours(all_comps_image, [contour], -1, (0, 255, 0), -1)
                if testing_mode==False:
                    point_string = self.iterative_obj.generate_string_from_point_list(points)
                    door_details_file.write('Door : ' + point_string + '\n')
            else:
                cv2.drawContours(all_comps_image, [contour], -1, (0, 0, 255), -1)
                if testing_mode==False:
                    point_string = self.iterative_obj.generate_string_from_point_list(points)
                    wall_details_file.write('Wall : ' + point_string + '\n')
        #--testing
        if testing_mode:
            cv2.imwrite(path_output + only_image_name + '_new_walls_windows_door_1.png', all_comps_image)
        #--non-testing
        else:
            cv2.imwrite(path_output + only_image_name + '/new_walls_windows_door_1.png',all_comps_image)
            window_details_file.close()
            door_details_file.close()
            wall_details_file.close()


    def drawKeyPoints(self,img, template, skp, tkp, num=-1):
        h1, w1 = img.shape[:2]
        h2, w2 = template.shape[:2]
        nWidth = w1 + w2
        nHeight = max(h1, h2)
        hdif = (h1 - h2) / 2
        newimg = np.zeros((nHeight, nWidth, 3), np.uint8)
        newimg[:h2, :w2] = template
        newimg[:h1, w2:w1 + w2] = img

        maxlen = min(len(skp), len(tkp))
        if num < 0 or num > maxlen:
            num = maxlen
        for i in range(num):
            pt_a = (int(tkp[i].pt[0]), int(tkp[i].pt[1] + hdif))
            pt_b = (int(skp[i].pt[0] + w2), int(skp[i].pt[1]))
            cv2.line(newimg, pt_a, pt_b, (255, 0, 0))
        return newimg

    def findKeyPoints(self,img, template, distance=50):
        detector = cv2.FeatureDetector_create("SIFT")
        descriptor = cv2.DescriptorExtractor_create("SIFT")

        skp = detector.detect(img)
        skp, sd = descriptor.compute(img, skp)

        tkp = detector.detect(template)
        tkp, td = descriptor.compute(template, tkp)

        flann_params = dict(algorithm=1, trees=4)
        flann = cv2.flann_Index(sd, flann_params)
        idx, dist = flann.knnSearch(td, 1, params={})
        del flann

        dist = dist[:, 0] / 2500.0
        dist = dist.reshape(-1, ).tolist()
        idx = idx.reshape(-1).tolist()
        indices = range(len(dist))
        indices.sort(key=lambda i: dist[i])
        dist = [dist[i] for i in indices]
        idx = [idx[i] for i in indices]
        skp_final = []
        for i, dis in itertools.izip(idx, dist):
            if dis < distance:
                skp_final.append(skp[i])

        flann = cv2.flann_Index(td, flann_params)
        idx, dist = flann.knnSearch(sd, 1, params={})
        del flann

        dist = dist[:, 0] / 2500.0
        dist = dist.reshape(-1, ).tolist()
        idx = idx.reshape(-1).tolist()
        indices = range(len(dist))
        indices.sort(key=lambda i: dist[i])
        dist = [dist[i] for i in indices]
        idx = [idx[i] for i in indices]
        tkp_final = []
        for i, dis in itertools.izip(idx, dist):
            if dis < distance:
                tkp_final.append(tkp[i])

        return skp_final, tkp_final

