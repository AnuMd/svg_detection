import cv2,shutil,os, re, random, math, numpy as np,PIL.ImageOps
from operator import itemgetter
from PIL import Image

from iterative_functions import iterative_functions_class

class pre_process_class():

    def __init__(self,current_directory,path_output,file_name,avg_door_width,
                 path_gravvitas,orginal_img_height, orginal_img_width):
        self.iterative_obj = iterative_functions_class()
        self.current_directory = current_directory
        self.path_output = path_output
        self.file_name = file_name
        self.avg_door_width = avg_door_width
        self.path_gravvitas = path_gravvitas
        self.orginal_img_height, self.orginal_img_width = orginal_img_height, orginal_img_width

    def pre_process_detection(self):
        # --- 1. get outer contour as a contour and as a list of lines
        outer_contour, outer_contour_lines = self.get_fp_outer_contour(
            self.current_directory, self.path_output, self.file_name, self.avg_door_width)
        # --- 2. crop floorplan to fit only floorplan contour and remove outside space
        # ----- and modify all fp elements to the cropped floorplan
        all_fp_comps, fp_heig_wid, outer_cont_data = self.modify_cordinates(
            self.current_directory, self.path_gravvitas, self.file_name, outer_contour,
            outer_contour_lines, self.orginal_img_height, self.orginal_img_width)
        new_img_height, new_img_width = fp_heig_wid
        modified_outer_contour, modified_outer_contour_lines = outer_cont_data

        # --- 3. extract walls based on all other fp components
        all_fp_comps = self.extract_walls(self.current_directory, self.path_gravvitas,
                                                          modified_outer_contour, new_img_height, new_img_width,
                                                          self.file_name, all_fp_comps)

        # --- 7. categorize fp data by rooms
        room_data = self.assign_fp_data_to_rooms(all_fp_comps, self.path_gravvitas,
                                                 new_img_height, new_img_width)
        # --- 8. crop room image to fit only room and map cordinates to fit in to cropped image
        modified_room_cordinates = self.modify_room_cordinates(room_data)

        results = [modified_outer_contour, modified_outer_contour_lines,new_img_height,
                   new_img_width, all_fp_comps,modified_room_cordinates]
        return results

    # ----- all methods related to preprocessing detected data
    def get_fp_outer_contour(self,current_directory, path_output,file_name,avg_door_width):

        # #--iterative running test paths
        # path_output = current_directory+'/input_fps/new_walls/walls_2/'
        # only_image_name = file_name[0:-4]
        # outer_contour_image_path = path_output + only_image_name+'_output.png'
        # # print 'outer_contour_image_path', outer_contour_image_path
        # outer_cont_image = cv2.imread(outer_contour_image_path, cv2.IMREAD_GRAYSCALE)
        # --end test


        # --comment test to work
        only_image_name = file_name[0:-4]
        outer_contour_image_path = path_output + only_image_name + '/'
        # --extract outer contour and get improved contour lines
        outer_cont_image = cv2.imread(outer_contour_image_path + 'new_walls_windows_door.png', cv2.IMREAD_GRAYSCALE)
        # --end comment test to work


        # ---get outer contour to be used : for finding entrance door
        ret, thresh = cv2.threshold(outer_cont_image, 0, 255, 1)
        contours, hierachy = cv2.findContours(thresh, 1, 2)
        # ---"outer_contour_wth_point_data" has contour points
        # ---"outer_contour_lines" has contour lines
        outer_contour_wth_point_data = 0
        img_height, img_width = outer_cont_image.shape
        img_size = img_height * img_width
        contour_area_list = []
        for cont, contour in enumerate(contours):
            contour_size = cv2.contourArea(contour)
            contour_ratio = contour_size / img_size
            contour_area_list.append([contour,contour_ratio])
            # #--test
            # contour_image = ~(np.zeros((3508, 2480, 3), np.uint8))
            # cv2.drawContours(contour_image, [contour], -1, (0, 0, 0), 5)
            # cv2.imwrite(current_directory+'/RESULTS_TEST/' + str(cont) + '_contour.png', contour_image)

            # contour_size = cv2.contourArea(contour)
            # contour_ratio = contour_size / img_size
            # print cont, contour_ratio
            # if contour_ratio > 0.2 and contour_ratio < 1.0:
            #     # if cont==len(contours)-2:
            #     outer_contour_wth_point_data = contour
            #     break
        #---sort contour list by contour area
        contour_area_list.sort(key=itemgetter(1),reverse=True)
        #---get 2nd largest since 1st=image border, 2nd=fp_border
        outer_contour_wth_point_data = contour_area_list[1][0]

        simplified_outer_contour, simplified_outer_contour_lines = self.iterative_obj.simplify_contour(outer_contour_wth_point_data,avg_door_width,20,15)


        return simplified_outer_contour, simplified_outer_contour_lines

    def modify_cordinates(self,current_directory,path_gravvitas,file_name,outer_contour, outer_contour_lines,img_height,img_width):

        only_image_name = file_name[0:-4]

        rooms_point_list = self.read_room_detail_file(path_gravvitas,only_image_name)
        object_point_list = self.read_object_detail_file(path_gravvitas, only_image_name)
        stairs_point_list = self.read_stairs_detail_file(path_gravvitas, only_image_name)
        windows_point_list = self.read_windows_detail_file(path_gravvitas, only_image_name)
        doors_rect_list = self.read_doors_detail_file(path_gravvitas, only_image_name)
        wall_point_list = self.read_walls_detail_file(path_gravvitas, only_image_name)
        text_data_list = self.read_text_detail_file(path_gravvitas, only_image_name)

        #-- get bounding box of the outer_contour
        x, y, bb_w, bb_h = cv2.boundingRect(outer_contour)

        #--calculate shift_x and shift_y(with a safe margin/border of 50)
        shift_x, shift_y = x-50, y-50

        #--calculate new max img_height and max_width
        new_width, new_height = bb_w + 100, bb_h + 100


        modified_rooms_point_list = self.get_new_points_for_text_and_point_list(rooms_point_list,shift_x, shift_y)
        modified_object_point_list = self.get_new_points_for_text_and_point_list(object_point_list, shift_x, shift_y)
        modified_stairs_point_list = self.get_new_points_for_only_point_list(stairs_point_list, shift_x, shift_y)
        modified_windows_point_list = self.get_new_points_for_only_point_list(windows_point_list, shift_x, shift_y)
        modified_doors_rect_list = self.get_new_points_for_only_point_list(doors_rect_list, shift_x, shift_y)
        modified_wall_point_list = self.get_new_points_for_only_point_list(wall_point_list, shift_x, shift_y)
        modified_text_data_list = self.get_new_points_for_text_data(text_data_list,shift_x, shift_y)

        #--modify outer conotur data too
        modified_outer_contour = self.get_new_contour(outer_contour,shift_x, shift_y)
        modified_outer_contour_lines = self.get_new_conotur_lines(outer_contour_lines,shift_x, shift_y)


        all_floorplan_components = [modified_rooms_point_list,modified_object_point_list,
                                    modified_stairs_point_list,modified_windows_point_list,
                                    modified_doors_rect_list,modified_wall_point_list,
                                    modified_text_data_list]
        fp_new_dimensions = [new_height,new_width]
        outer_contour_data = [modified_outer_contour,modified_outer_contour_lines]

        return all_floorplan_components,fp_new_dimensions,outer_contour_data

    def extract_walls(self, current_directory, path_gravvitas, outer_contour_line,
                      orginal_img_height, orginal_img_width, file_name, all_floorplan_components):
        # path_gravvitas = current_directory+'/input_fps/test_svg/text/'
        results_path = path_gravvitas + 'temp/'
        self.iterative_obj.make_directory(path_gravvitas + 'temp/', True)

        # only_image_name = file_name[0:-4]
        # --extract floorplan_data
        rooms_point_list = all_floorplan_components[0]
        object_point_list = all_floorplan_components[1]
        stairs_point_list = all_floorplan_components[2]
        windows_point_list = all_floorplan_components[3]
        doors_rect_list = all_floorplan_components[4]
        contour_image = ~(np.zeros((orginal_img_height, orginal_img_width, 3), np.uint8))
        cv2.drawContours(contour_image, [outer_contour_line], -1, (0, 0, 0), -1)

        dilate_components_image = np.zeros((orginal_img_height, orginal_img_width, 3), np.uint8)

        for room_row in rooms_point_list:
            room_points = room_row[-1]
            room_contour = self.iterative_obj.convert_points_to_contour(room_points)
            cv2.drawContours(contour_image, [room_contour], -1, (255, 255, 255), -1)

        for object_row in object_point_list:
            object_points = object_row[1]
            object_contour = self.iterative_obj.convert_points_to_contour(object_points)
            cv2.drawContours(dilate_components_image, [object_contour], -1, (255, 255, 255), -1)

        for stair_row in stairs_point_list:
            stair_contour = self.iterative_obj.convert_points_to_contour(stair_row)
            cv2.drawContours(dilate_components_image, [stair_contour], -1, (255, 255, 255), -1)

        for window_row in windows_point_list:
            window_contour = self.iterative_obj.convert_points_to_contour(window_row)
            cv2.drawContours(dilate_components_image, [window_contour], -1, (255, 255, 255), -1)

        for door_row in doors_rect_list:
            door_contour = self.iterative_obj.convert_points_to_contour(door_row)
            cv2.drawContours(dilate_components_image, [door_contour], -1, (255, 255, 255), -1)

        cv2.imwrite(results_path + 'only_components.png', dilate_components_image)
        img = cv2.imread(results_path + 'only_components.png', 0)
        kernel = np.ones((5, 5), np.uint8)
        dilation = cv2.dilate(img, kernel, iterations=1)
        cv2.imwrite(results_path + 'only_components.png', dilation)
        image = Image.open(results_path + 'only_components.png')
        inverted_image = PIL.ImageOps.invert(image)
        inverted_image.save(results_path + 'only_components.png')

        dilated_inverted = cv2.imread(results_path + 'only_components.png', 0)
        ret, thresh = cv2.threshold(dilated_inverted, 0, 255, 1)
        contours, hierachy = cv2.findContours(thresh, 1, 2)
        for contour in contours:
            cv2.drawContours(contour_image, [contour], -1, (255, 255, 255), -1)
        cv2.imwrite(results_path + 'all_components.png', contour_image)

        # --extract for text file and for wall_point_list
        wall_point_list = []
        # wall_details_file = open(path_gravvitas+only_image_name+'_7_wall_details.txt', 'w')
        all_components = cv2.imread(results_path + 'all_components.png', 0)
        ret, thresh = cv2.threshold(all_components, 0, 255, 1)
        contours, hierachy = cv2.findContours(thresh, 1, 2)
        # wall_num = 0
        for contour in contours:
            # contour_data = ''
            single_wall_data = []
            for c, cont_point in enumerate(contour):
                single_wall_data.append([cont_point[0][0], cont_point[0][1]])
            #     if c==0:
            #         contour_data  = contour_data+'['+str(cont_point[0][0])+' '+str(cont_point[0][1])+']'
            #     else:
            #         contour_data  = contour_data+',['+str(cont_point[0][0])+' '+str(cont_point[0][1])+']'
            # wall_details_file.write('Wall '+str(wall_num)+': ')
            # wall_details_file.write(contour_data+'\n')
            # wall_num += 1
            wall_point_list.append(single_wall_data)

        all_floorplan_components.append(wall_point_list)
        shutil.rmtree(results_path)

        return all_floorplan_components

    def assign_fp_data_to_rooms(self,all_floorplan_components,path_gravvitas,
                                img_height,img_width):
        # tactile_room_data = []
        # --extract floorplan_data
        rooms_point_list = all_floorplan_components[0]
        object_point_list = all_floorplan_components[1]
        windows_point_list = all_floorplan_components[3]
        doors_rect_list = all_floorplan_components[4]


        #--get rooms
        room_data = []
        #--structure [room_text,room_points,object_data]
        for room_num,room_row in enumerate(rooms_point_list):
            room_name = str(room_row[0])
            room_type = room_row[1]
            room_points = room_row[-1]
            # --temp image to get walls
            temp_wall_image = np.zeros((img_height, img_width, 3), np.uint8)

            # --to check if objects and others are inside room
            room_contour = self.iterative_obj.convert_points_to_contour(room_points)
            cv2.drawContours(temp_wall_image, [room_contour], -1, (255, 255, 255), -1)

            object_data = []
            for objrow_num, each_object_row in enumerate(object_point_list):
                object_contour = self.iterative_obj.convert_points_to_contour(each_object_row[1])
                M = cv2.moments(object_contour)
                cx = int(M['m10'] / M['m00'])
                cy = int(M['m01'] / M['m00'])
                object_place = cv2.pointPolygonTest(room_contour, (cx, cy), False)

                if object_place == 1:
                    object_data.append(each_object_row)
                    cv2.drawContours(temp_wall_image, [object_contour], -1, (255, 255, 255), -1)


            window_data = []
            for win_num, window_row in enumerate(windows_point_list):
                for each_win_point in window_row:
                    cx, cy = each_win_point
                    window_dist = cv2.pointPolygonTest(room_contour, (cx, cy), True)
                    if window_dist < 1 and window_dist > -1:
                        window_data.append(window_row)
                        window_contour = self.iterative_obj.convert_points_to_contour(window_row)
                        cv2.drawContours(temp_wall_image, [window_contour], -1, (255, 255, 255), -1)
                        break

            door_data = []
            for door_num,door_row in enumerate(doors_rect_list):
                for each_door_point in door_row:
                    cx, cy = each_door_point
                    door_dist = cv2.pointPolygonTest(room_contour, (cx, cy), True)
                    if door_dist < 1 and door_dist > -1:
                        door_data.append(door_row)
                        door_contour = self.iterative_obj.convert_points_to_contour(door_row)
                        cv2.drawContours(temp_wall_image, [door_contour], -1, (255, 255, 255), -1)
                        break
                # #--get door rect corner points
                # x1,y1 = door_row[0]
                # x2,y2 = door_row[1]
                #
                # #--get lines from door_rect
                # L1, L2, L3, L4 = self.iterative_obj.find_lines_rectangle(x1,y1,x2,y2)
                # #--- check if centre point of any door line is inside rooms
                # for L in (L1, L2, L3, L4):
                #     #---get centre point of each line
                #     cx, cy = self.iterative_obj.find_centre_of_line(L)
                #     door_place = cv2.pointPolygonTest(room_contour, (cx, cy), True)
                #     if door_place > -2:
                #         door_data.append(door_row)
                #         cv2.rectangle(temp_wall_image, (x1, y1), (x2, y2), (255, 255, 255), -1)
                #         break


            cv2.imwrite(path_gravvitas +'all_components.png', temp_wall_image)


            #---need to dilate walls or get only wall rectangles
            #--dilating walls to get outside_contour
            img = cv2.imread(path_gravvitas + 'all_components.png', 0)
            kernel = np.ones((30, 30), np.uint8)
            dilation = cv2.dilate(img, kernel, iterations=1)
            cv2.imwrite(path_gravvitas + 'dilated.png', dilation)
            image = Image.open(path_gravvitas + 'dilated.png')
            inverted_image = PIL.ImageOps.invert(image)
            inverted_image.save(path_gravvitas + 'dilated.png')
            dilated_inverted = cv2.imread(path_gravvitas + 'dilated.png', 0)
            ret, thresh = cv2.threshold(dilated_inverted, 0, 255, 1)
            contours, hierachy = cv2.findContours(thresh, 1, 2)

            if len(contours)>=1:
                outside_contour = min(contours, key=cv2.contourArea)

                # --get inside_contour
                small_contour_image = cv2.imread(path_gravvitas + 'all_components.png', 0)
                ret, thresh = cv2.threshold(small_contour_image, 0, 255, 1)
                contours, hierachy = cv2.findContours(thresh, 1, 2)
                inside_contour = min(contours, key=cv2.contourArea)

                #--convert inside_contour to list -> reverse the list -> turn back to contour
                #--paths has to be in 2 different directions for clipping to work
                inside_contour_point_list = self.iterative_obj.get_points_from_contour(inside_contour)
                inside_contour_reversed_point_list = inside_contour_point_list[::-1]
                reveresed_inside_contour = self.iterative_obj.convert_points_to_contour(inside_contour_reversed_point_list)

                wall_data = [outside_contour,reveresed_inside_contour]

                room_data.append([room_type,room_name, room_points,
                                  object_data, window_data, door_data,wall_data])

        return room_data

    def modify_room_cordinates(self,room_data):
        modified_cordinates = []
        for room_row in room_data:
            # room_name = str(room_row[0])
            room_points_modify = [room_row[1],room_row[2]]
            object_data = room_row[3]
            window_data = room_row[4]
            door_data = room_row[5]
            wall_data = room_row[6]

            outside_contour,inner_contour = wall_data

            # -- get bounding box of the outer_contour
            x, y, bb_w, bb_h = cv2.boundingRect(outside_contour)

            # --calculate shift_x and shift_y(with a safe margin/border of 50)
            shift_x, shift_y = x - 50, y - 50

            # --calculate new max img_height and max_width
            new_width, new_height = bb_w + 100, bb_h + 100
            modified_rooms_point_list = self.get_new_points_for_text_and_point_list([room_points_modify], shift_x, shift_y)
            modified_object_point_list = self.get_new_points_for_text_and_point_list(object_data, shift_x,
                                                                                     shift_y)
            modified_windows_point_list = self.get_new_points_for_only_point_list(window_data, shift_x, shift_y)
            modified_doors_rect_list = self.get_new_points_for_only_point_list(door_data, shift_x, shift_y)

            # --modify outer conotur data too
            modified_outside_contour = self.get_new_contour(outside_contour, shift_x, shift_y)
            modified_inner_contour = self.get_new_contour(inner_contour, shift_x, shift_y)

            modified_wall_data = [modified_outside_contour,modified_inner_contour]
            modified_dimensions = [new_width, new_height]

            modified_room_data = [room_row[0],room_row[1],modified_rooms_point_list[0][1]]
            modified_cordinates.append([modified_room_data,modified_object_point_list,
                                        modified_windows_point_list,
                                        modified_doors_rect_list,modified_wall_data,
                                        modified_dimensions])
        return modified_cordinates

    # -----each method sub-methods
    def read_room_detail_file(self,path_gravvitas,only_image_name):
        #----read all rooms data
        rooms_point_list = []
        room_details_file = open(path_gravvitas + only_image_name + '_1_room_details.txt', 'r')
        for row_num, text_line in enumerate(room_details_file):
            line_seperate = text_line.split(':')
            if len(line_seperate) > 3:
                #---get room contour points
                contour_details = re.findall(r"[\w']+", line_seperate[5])
                int_contour_details = [int(x) for x in contour_details]
                room_contour = [int_contour_details[x:x + 2] for x in xrange(0, len(int_contour_details), 2)]
                #---get room text
                room_text = line_seperate[2].strip()
                #---get room type details
                room_type_str = line_seperate[1].strip()
                if '-' in room_type_str:
                    room_type = (room_type_str.split('-'))[0].strip()
                else:
                    room_type = 'single'
                rooms_point_list.append([room_text, room_type,room_contour])

        #-- get open plan data in teh same format as rooms
        all_op_data = self.read_open_plan_file(path_gravvitas,only_image_name)
        for op_row in all_op_data:
            rooms_point_list.append(op_row)

        return rooms_point_list


    def read_open_plan_file(self,path_gravvitas,only_image_name):
        # --find lines for all rooms and store in "room_lines_list[room_name,room_contour_lines_list]"
        all_op_data = []
        room_details_file = open(path_gravvitas + only_image_name + '_8_openPlan_details.txt', 'r')
        for op_number, text_line in enumerate(room_details_file):
            contour_seperate = text_line.split(':')
            if len(contour_seperate)>3:
                # ---get op contour points
                contour_details = re.findall(r"[\w']+", contour_seperate[2])
                int_contour_details = [int(x) for x in contour_details]
                open_plan_contour_points = [int_contour_details[x:x + 2] for x in xrange(0, len(int_contour_details), 2)]
                all_op_data.append(['plan'+str(op_number),'OP',open_plan_contour_points])
        return all_op_data

    def read_object_detail_file(self,path_gravvitas,only_image_name):
        object_point_list = []
        object_text_file_path = path_gravvitas + only_image_name + '_2_object_details.txt'
        if os.path.exists(object_text_file_path):
            object_details_file = open(path_gravvitas + only_image_name + '_2_object_details.txt', 'r')
            for text_line in object_details_file:
                line_seperate = text_line.split(':')
                if len(line_seperate) > 1:
                    object_contour = re.findall(r"[\w']+", line_seperate[1])
                    int_object_details = [int(x) for x in object_contour]
                    object_chunks = [int_object_details[x:x + 2] for x in xrange(0, len(int_object_details), 2)]
                    # ---temp fix delete later
                    x1, y1 = object_chunks[0]
                    x2, y2 = object_chunks[1]
                    object_contour_new = [[x1, y1], [x2, y1], [x2, y2], [x1, y2]]

                    # --get object_name
                    object_name = line_seperate[0].split()[0]
                    # print object_name
                    if len(object_point_list) == 0:
                        object_point_list.append([])
                        object_point_list[0].append(object_name)
                        object_point_list[0].append(object_contour_new)
                    else:
                        row_ID = -1
                        for obj_row, each_object in enumerate(object_point_list):
                            if each_object[0] == object_name:
                                row_ID = obj_row
                                break
                        if row_ID != -1:
                            object_point_list[row_ID].append(object_contour_new)
                        else:
                            object_point_list.append([])
                            object_point_list[len(object_point_list) - 1].append(object_name)
                            object_point_list[len(object_point_list) - 1].append(object_contour_new)
        return object_point_list

    def read_stairs_detail_file(self,path_gravvitas,only_image_name):
        stairs_point_list = []
        stair_details_file = open(path_gravvitas + only_image_name + '_3_stairs_details.txt', 'r')
        for stair_num, text_line in enumerate(stair_details_file):
            line_seperate = text_line.split(':')
            if len(line_seperate) == 2:
                contour_details = re.findall(r"[\w']+", line_seperate[1])
                int_contour_details = [int(x) for x in contour_details]
                stair_points = [int_contour_details[x:x + 2] for x in xrange(0, len(int_contour_details), 2)]
                stairs_point_list.append(stair_points)
        return stairs_point_list

    def read_windows_detail_file(self,path_gravvitas,only_image_name):
        windows_point_list = []
        window_details_file = open(path_gravvitas + only_image_name + '_4_window_details.txt', 'r')
        for win_num, text_line in enumerate(window_details_file):
            contour_seperate = text_line.split(':')
            if len(contour_seperate) == 2:
                contour_details = re.findall(r"[\w']+", contour_seperate[1])
                int_contour_details = [int(x) for x in contour_details]
                window_data = [int_contour_details[x:x + 2] for x in xrange(0, len(int_contour_details), 2)]
                windows_point_list.append(window_data)
        return windows_point_list

    def read_doors_detail_file(self,path_gravvitas,only_image_name):
        doors_rect_list = []
        door_details_file = open(path_gravvitas + only_image_name + '_5_door_details.txt', 'r')
        for door_num, text_line in enumerate(door_details_file):
            line_seperate = text_line.split(':')
            if len(line_seperate) == 2:
                contour_details = re.findall(r"[\w']+", line_seperate[1])
                int_contour_details = [int(x) for x in contour_details]
                door_rectangle = [int_contour_details[x:x + 2] for x in xrange(0, len(int_contour_details), 2)]
                doors_rect_list.append(door_rectangle)
        return doors_rect_list

    def read_walls_detail_file(self,path_gravvitas,only_image_name):
        wall_point_list = []
        wall_details_file = open(path_gravvitas + only_image_name + '_7_wall_details.txt', 'r')
        for wall_num, text_line in enumerate(wall_details_file):
            line_seperate = text_line.split(':')
            if len(line_seperate) == 2:
                contour_details = re.findall(r"[\w']+", line_seperate[1])
                int_contour_details = [int(x) for x in contour_details]
                wall_contour = [int_contour_details[x:x + 2] for x in xrange(0, len(int_contour_details), 2)]
                wall_point_list.append(wall_contour)
        return wall_point_list

    def read_text_detail_file(self,path_gravvitas,only_image_name):
        text_data_list = []
        text_details_file = open(path_gravvitas + only_image_name + '_6_text_details.txt', 'r')
        for text_num, text_line in enumerate(text_details_file):
            detail_seperate = text_line.split(':')
            if len(detail_seperate) > 1:
                word,centre_point, bb_data = detail_seperate[0].strip(),detail_seperate[1],detail_seperate[2]

                # --seperate and convert text cordinate to int
                cordinates_string_2 = re.findall(r"[\w']+", centre_point)
                int_cordinates = [int(x) for x in cordinates_string_2]
                centre_point_chunks = [int_cordinates[x:x + 2] for x in xrange(0, len(int_cordinates), 2)]

                # --seperate and convert BB-box to int
                bb_cordinates_string_2 = re.findall(r"[\w']+", bb_data)
                int_bb_cordinates = [int(x) for x in bb_cordinates_string_2]
                bb_cordintes_chunks = [int_bb_cordinates[x:x + 4] for x in xrange(0, len(int_bb_cordinates), 4)]

                text_data_list.append([word,centre_point_chunks,bb_cordintes_chunks])
        return text_data_list

    def get_new_points_for_text_and_point_list(self,point_list,shift_x, shift_y):
        new_point_list = []
        for row_num, row in enumerate(point_list):
            text = row[0]
            room_contour_points = row[-1]
            new_point_list.append([])
            new_point_list[row_num].append(text)
            if len(row) > 2:
                new_point_list[row_num].append(row[1])
            new_individual_points = []
            for room_point in room_contour_points:
                x, y = room_point
                new_individual_points.append([x-shift_x,y-shift_y])
            new_point_list[row_num].append(new_individual_points)
        return new_point_list

    def get_new_points_for_only_point_list(self,point_list,shift_x, shift_y):
        new_point_list = []
        for row_num, row in enumerate(point_list):
            new_row = []
            for each_cord in row:
                x, y = each_cord
                new_row.append([x-shift_x,y-shift_y])
            new_point_list.append(new_row)
        return new_point_list

    def get_new_points_for_text_data(self,text_data_list,shift_x, shift_y):
        new_point_list = []
        for row_num, row in enumerate(text_data_list):
            text,centre_points,bb_cords = row[0],row[1][0],row[2][0]
            x1,y1 = centre_points
            new_cx, new_cy = x1 - shift_x, y1 - shift_y
            x1,y1,x2,y2 = bb_cords
            new_bbx1, newbby1,new_bbx2, newbby2 = x1 - shift_x, y1 - shift_y, x2 - shift_x, y2 - shift_y

            new_point_list.append([text,[new_cx,new_cy],[new_bbx1, newbby1,new_bbx2, newbby2]])
        return new_point_list

    def get_new_contour(self,outer_contour,shift_x, shift_y):
        cont_point_list = []
        for con_point_l1 in outer_contour:
            for con_point_l2 in con_point_l1:
                cont_x, cont_y = con_point_l2
                cont_point_list.append([cont_x-shift_x, cont_y-shift_y])
        new_contour = self.iterative_obj.convert_points_to_contour(cont_point_list)
        return new_contour

    def get_new_conotur_lines(self,outer_contour_lines,shift_x, shift_y):
        new_outer_contour_lines = []
        for each_contour_line in outer_contour_lines:
            x1,y1 = each_contour_line[0]
            x2, y2 = each_contour_line[1]
            new_outer_contour_lines.append([[x1-shift_x,y1-shift_y],[x2-shift_x, y2-shift_y]])
        return new_outer_contour_lines


    # def get_new_points_for_text_and_point_list(self, point_list, shift_x, shift_y):
    #     new_point_list = []
    #     for row_num, row in enumerate(point_list):
    #         text = row[0]
    #         room_contour_points = row[-1]
    #         new_point_list.append([])
    #         new_point_list[row_num].append(text)
    #         if len(row) > 2:
    #             new_point_list[row_num].append(row[1])
    #         new_individual_points = []
    #         for room_point in room_contour_points:
    #             x, y = room_point
    #             new_individual_points.append([x - shift_x, y - shift_y])
    #         new_point_list[row_num].append(new_individual_points)
    #     return new_point_list
    #
    # def get_new_points_for_only_point_list(self, point_list, shift_x, shift_y):
    #     new_point_list = []
    #     for row_num, row in enumerate(point_list):
    #         new_row = []
    #         for each_cord in row:
    #             x, y = each_cord
    #             new_row.append([x - shift_x, y - shift_y])
    #         new_point_list.append(new_row)
    #     return new_point_list
    #
    # def get_new_contour(self, outer_contour, shift_x, shift_y):
    #     cont_point_list = []
    #     for con_point_l1 in outer_contour:
    #         for con_point_l2 in con_point_l1:
    #             cont_x, cont_y = con_point_l2
    #             cont_point_list.append([cont_x - shift_x, cont_y - shift_y])
    #     new_contour = self.iterative_obj.convert_points_to_contour(cont_point_list)
    #     return new_contour