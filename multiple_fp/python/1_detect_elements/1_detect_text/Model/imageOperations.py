__author__ = 'pierpaolo'

from connectedcomponent import CC
import numpy as np
from scipy import ndimage
import cv2
# import cv2.cv as cv
from copy import deepcopy
from scipy.spatial import Voronoi
import matplotlib.pyplot as plt

def voronoi(labels, contour, red_img, path):
    points = []
    image = restore_original_size(contour, red_img)
    # points.append([0,0])
    s = image.shape
    h = s[0]
    w = s[1]
    rect = (0, 0, w, h)
    subdiv = cv2.Subdiv2D(rect)
    # points.append([w,h])
    for lab in labels:
        points.append((lab.cx, lab.cy))
    # #print str(points)
    for p in points:
        subdiv.insert(p)
    img_voronoi = np.zeros(image.shape, dtype=image.dtype)
    img_voronoi.fill(255)
    img_voronoi = np.array(img_voronoi, dtype=image.dtype)
    draw_voronoi(img_voronoi, subdiv)
    cv2.imwrite(path+'voronoi.png',img_voronoi)
    cv2.imwrite(path+'contour.png',image)
    img_voronoi = cv2.imread(path+'voronoi.png',0)
    image = cv2.imread(path+'contour.png',0)
    dst = cv2.bitwise_and(img_voronoi,image,dst=None,mask=None)
    cv2.imwrite(path+'dst.png',dst)
    return dst

# Draw voronoi diagram
def draw_voronoi(img, subdiv):
    (facets, centers) = subdiv.getVoronoiFacetList([])
    for i in xrange(0,len(facets)):
        ifacet_arr = []
        for f in facets[i]:
            ifacet_arr.append(f)
        ifacet = np.array(ifacet_arr, np.int)
        #cv2.drawContours(img, ifacet, -1, (0,255,0), 3)
        cv2.fillConvexPoly(img, ifacet, (255,255,255), cv2.CV_AA, 0)
        ifacets = np.array([ifacet])
        cv2.polylines(img, ifacets, True, (0, 0, 0), 1, cv2.CV_AA, 0)
        # cv2.circle(img, (centers[i][0], centers[i][1]), 3, (0, 0, 0), cv2.cv.CV_FILLED, cv2.CV_AA, 0)


def restore_original_size(cc, img_original):
        s = img_original.shape
        cc.set_coordinates()
        padup = cc.y_min
        paddown = s[0]-cc.y_max+1
        padleft = cc.x_min
        padright = s[1]-cc.x_max+1
        img = add_pad(cc,padup,paddown,padleft,padright)
        return img

def calculate_avg_width(image):
        img = np.array(image)
        labels, numlabels =ndimage.label(img<=127)
        threshold = 8
        mean = 0
        numPixel = 0
        for i in range(1,numlabels+1):
            cc=CC(i)
            point= np.argwhere(labels == i)

            # find x_max, x_min, y_max, y_min
            cc.y_min=point[:,0].min()
            cc.x_min=point[:,1].min()
            cc.y_max=point[:,0].max()
            cc.x_max=point[:,1].max()
            cc.image=(np.where(labels[cc.y_min:cc.y_max+1,cc.x_min:cc.x_max+1] != i, 0, labels[cc.y_min:cc.y_max+1,cc.x_min:cc.x_max+1])*255/i)
            #need for padding
            cc.image=add_pad_single_pixel(cc.image)
            cc.set_coordinates()
            cc.find_contour()
            # cc.numPixel=(cc.image.sum()/255)
            # if cc.area>threshold:
            cc.numPixel=cv2.countNonZero(cc.image)
            if cc.numPixel>threshold:
                cc.image_dist_tran=ndimage.morphology.distance_transform_cdt(cc.image)
                # tmp = ndimage.measurements.mean(cc.image_dist_tran,cc.image)
                tmp = np.amax(cc.image_dist_tran)
                numPixel += cc.numPixel
                # mean += tmp*cc.numPixel
                mean+= np.sqrt(tmp)*cc.numPixel
        #calculate avg value minus the main component
        if numPixel != 0:
            width = float(mean)/float(numPixel)
        else:
            width = 1
        # print 'average width of the contours = '+str(width)
        return width

def find_connected_componet_rooms(img_walls, img_no_text, path):
    #--img_walls is stefanos image in gray scale
    labels, numlabels =ndimage.label(img_walls<10)
    original, num = ndimage.label(img_no_text>127)

    #cc of the original external ambient
    ext_original = CC(0)
    point= np.argwhere(original == 1)
    ext_original.y_min=point[:,0].min()
    ext_original.x_min=point[:,1].min()
    ext_original.y_max=point[:,0].max()
    ext_original.x_max=point[:,1].max()
    ext_original.image=np.where(original[ext_original.y_min:ext_original.y_max+1,ext_original.x_min:ext_original.x_max+1] == 1, 0, 255)
    ext_original.numPixel=(ext_original.image.sum()/255)
    ext_original.image = add_pad_single_pixel(ext_original.image)
    ext_original.set_coordinates()
    ext_original.find_contour()
    cv2.imwrite(path+'extern.png', ext_original.image)
    list_cc = []
    for i in range(1,numlabels+1):
        cc=CC(i)
        point= np.argwhere(labels == i)
        # find x_max, x_min, y_max, y_min
        cc.y_min=point[:,0].min()
        cc.x_min=point[:,1].min()
        cc.y_max=point[:,0].max()
        cc.x_max=point[:,1].max()


        #I create image of the connected compis an open image with black background and white lines, i want as output an image with white background and black linesonent
        cc.image=np.where(labels[cc.y_min:cc.y_max+1,cc.x_min:cc.x_max+1] != i, 0, 255)
        cc.image = add_pad_single_pixel(cc.image)
        cc.set_coordinates()
        # cc.image_dist_tran=ndimage.morphology.distance_transform_cdt(cc.image)
        cc.find_contour()
        if i == 1:
            # cv2.imwrite(path+'room_'+str(i)+'_before_and.png', cc.image)
            boolean, cc.image = logic_and(cc, ext_original)
            # cv2.imwrite(path+'room_'+str(i)+'_after_and.png', cc.image)
            cc.x_min = 0
            cc.y_min = 0
            cc.set_coordinates()
        cc.numPixel=(cc.image.sum()/255)
        #--if component area>400 then only consider it as a room
        if cc.numPixel>400:
            cc.find_contour()
            # I add the connected component to the list
            list_cc.append(cc)
    # list_cc.append(ext_after)
    # img = np.where(labels>1,255,0)
    # cv2.imwrite(path+'room_internal.png', img)
    #print 'I found ' +str(len(list_cc)) +' Rooms!'
    return list_cc

def update_room(contour, no_wall, external_graph, ymax, xmax, width):
#contour is a cc, no_wall is an image, external_graph is a GraphCC
    image = np.zeros(contour.image.shape,dtype=np.uint8)
    external = np.zeros(contour.image.shape,dtype=np.uint8)
    image.fill(255)
    for cc in external_graph.get_indices():
        img1, img2 = same_size(cc, contour)
        # s = cc.image.shape
        # h = s[0]
        # w = s[1]
        # padup = cc.y_min-1
        # paddown = ymax-padup-h+2
        # padleft = cc.x_min-1
        # padright = xmax-padleft-w+2
        # img1 = add_pad(cc,padup,paddown,padleft,padright)
        # #print 'cc.image.shape = '+str(cc.image.shape)
        # #print 'img1.shape = '+str(img1.shape)
        # #print 'image.shape = '+str(image.shape)
        image = np.where(img1 == 255, 0, image)
        external = np.where(img1 == 255, 255, external)
    cc = CC(1)
    cc.image = image
    # cc.image = add_pad_single_pixel(image)
    cc.x_min = 0
    cc.y_min = 0
    cc.set_coordinates()
    # external = add_pad_single_pixel(external)
    contour.set_coordinates()
    # path = '/home/pierpaolo/chiusura_esterna/'
    # cv2.imwrite(path+'extern.png', contour.image)
    # cv2.imwrite(path+'ext_cc.png', cc.image)
    logic, new_room = logic_and(cc, contour)
    # cv2.imwrite(path+'logic_and.png', new_room)
    width = int(2*width)+3
    kernel = np.ones((width,width),np.uint8)
    opening = cv2.morphologyEx(new_room, cv2.MORPH_OPEN, kernel)
    # cv2.imwrite(path+'new_rooms.png', opening)
    contour.image = external-opening
    contour.image = cv2.morphologyEx(contour.image, cv2.MORPH_OPEN, kernel)
    # cc.image = np.where(opening == 0, 255, 0)
    # cc.set_coordinates()
    # contour.image, cc.image = same_size(contour, cc)
    # cc.image = np.array(cc.image, dtype=np.uint8)
    # contour.image = np.array(contour.image, dtype=np.uint8)
    # cv2.imwrite('/home/pierpaolo/negative.png', cc.image)
    # #print 'contour.shape = '+str(contour.image.shape)
    # #print str(contour.image)
    # #print 'cc.shape = '+str(cc.image.shape)
    # #print str(cc.image)
    # contour.image = cv2.bitwise_and(contour.image, cc.image ,dst=None,mask=None)
    # contour.image = cv2.morphologyEx(contour.image, cv2.MORPH_OPEN, kernel)
    # cv2.imwrite('/home/pierpaolo/contour_end.png', contour.image)
    contour.x_min = 0
    contour.y_min = 0
    contour.set_coordinates()
    # internal_pad(contour.image)
    # contour.find_contour()
    # cv2.imwrite('/home/pierpaolo/new_extern.png', contour.image)
    labels, numlabels = ndimage.label(opening>127)
    rooms_cc = []
    for i in range(1,numlabels+1):
            cc=CC(i)
            point= np.argwhere(labels == i)
            cc.y_min=point[:,0].min()
            cc.x_min=point[:,1].min()
            cc.y_max=point[:,0].max()
            cc.x_max=point[:,1].max()
            cc.image=np.where(labels[cc.y_min:cc.y_max+1,cc.x_min:cc.x_max+1] != i, 0, 255)
            cc.image = add_pad_single_pixel(cc.image)
            cc.set_coordinates()
            # cc.image_dist_tran=ndimage.morphology.distance_transform_cdt(cc.image)
            cc.find_contour()
            cc.image_dist_tran = ndimage.morphology.distance_transform_cdt(cc.image)
            width = ndimage.measurements.mean(cc.image_dist_tran,cc.image)
            cc.numPixel=(cc.image.sum()/255)
            if cc.numPixel>400 and width > 20:
                cc.find_contour()
                rooms_cc.append(cc)

    #print 'I found ' +str(len(rooms_cc)) +' more Rooms!'
    return rooms_cc, contour

def check_wall_around(img, cc, label):
    if label == 'wc':
        if cc.height > cc.width:
            img1 = img[cc.y_min:cc.y_max, cc.center_x-cc.width:cc.center_x]
            img2 = img[cc.y_min:cc.y_max, cc.center_x:cc.center_x+cc.width]
        else:
            img1 = img[cc.center_y-cc.height:cc.center_y, cc.x_min: cc.x_max]
            img2 = img[cc.center_y: cc.center_y+cc.height, cc.x_min: cc.x_max]
        dst1 = dst = cv2.inRange(img1, (0,0,255), (0,0,255))
        dst2 = dst = cv2.inRange(img2, (0,0,255), (0,0,255))
        white1 = cv2.countNonZero(dst1)
        white2 = cv2.countNonZero(dst2)
        if min(white1,white2)>20 or max(white1,white2)<=20:
            return 0
        if white1 < white2:
            return -1
        else:
            return 1

def avg_doors_width(img):
    #--take all green components from Stefano output
    dst = cv2.inRange(img, (0,255,0), (0,255,0))
    labels, numlabels = ndimage.label(dst>127)
    avg_width = 0
    count = 0
    #--calculate distance from centroid in green CC to its outside contour
    #--then get its average and *2
    for i in range(1,numlabels+1):
        cc = CC(i)
        point= np.argwhere(labels == i)
        cc.y_min=point[:,0].min()
        cc.x_min=point[:,1].min()
        cc.y_max=point[:,0].max()
        cc.x_max=point[:,1].max()
        cc.height= cc.y_max-cc.y_min+1
        cc.width= cc.x_max-cc.x_min+1
        cc.center_x = cc.y_min+(cc.height/2)
        cc.center_y = cc.x_min+(cc.width/2)
        cc.image=(np.where(labels[cc.y_min:cc.y_max+1,cc.x_min:cc.x_max+1] != i, 0, labels[cc.y_min:cc.y_max+1,cc.x_min:cc.x_max+1])*255/i)
        cc.numPixel=(cc.image.sum()/255)
        cc.image = add_pad_single_pixel(cc.image)
        cc.set_coordinates()
        cc.find_contour()
        max_dist = cc.get_max_distance()
        #----discard small green components because they cant be doors
        if max_dist>20:
            avg_width+=max_dist
            count+=1
    #---if stefano has atleast more than 1 dor do it
    if count > 1:
        avg_width = 2*avg_width/float(count)
    #---else get average using windows
    else:
        #find windows
        count = 0
        avg_width = 0
        dst = cv2.inRange(img, (0,255,255),(0,255,255))
        labels, numlabels = ndimage.label(dst>127)
        for i in range(1,numlabels+1):
            cc = CC(i)
            point= np.argwhere(labels == i)
            cc.y_min=point[:,0].min()
            cc.x_min=point[:,1].min()
            cc.y_max=point[:,0].max()
            cc.x_max=point[:,1].max()
            cc.height= cc.y_max-cc.y_min+1
            cc.width= cc.x_max-cc.x_min+1
            cc.center_x = cc.y_min+(cc.height/2)
            cc.center_y = cc.x_min+(cc.width/2)
            cc.image=(np.where(labels[cc.y_min:cc.y_max+1,cc.x_min:cc.x_max+1] != i, 0, labels[cc.y_min:cc.y_max+1,cc.x_min:cc.x_max+1])*255/i)
            cc.numPixel=(cc.image.sum()/255)
            cc.image = add_pad_single_pixel(cc.image)
            cc.set_coordinates()
            cc.find_contour()
            max_dist = cc.get_max_distance()
            if max_dist>20:
                avg_width+=max_dist
                count+=1
        if count != 0:
            if count > 2:
                avg_width = avg_width/float(count) #a 0.50 penalty because windows are tipcally larger than doors
            else:
                avg_width = 2*avg_width/float(count)
    #---if avgDW<40 fix to 40
    if avg_width < 40:
        avg_width = 40
    return avg_width




def over_wall(image,cc1,cc2):
    ymax = max(cc1.center_y, cc2.center_y)
    ymin = min(cc1.center_y, cc2.center_y)
    xmax = max(cc1.center_x, cc2.center_x)
    xmin = min(cc1.center_x, cc2.center_x)
    if ymin == ymax:
        ymax+=1
    if xmin == xmax:
        xmax+=1
    red = 0
    if (ymax-ymin)/4>0 and (xmax-xmin)/4>0:
        y = (ymax-ymin)/4
        x = (xmax-xmin)/4
        for i in range(0,4):
            red+=count_red(image, ymin+i*y, ymax-(3-i)*y, xmin+i*x, xmax-(3-i)*x)
    else:
        red = count_red(image, ymin, ymax, xmin, xmax)

    # #print 'confronto le cc '+str(cc1.id)+' e '+str(cc2.id)
    # #print 'xmin = '+str(xmin)+' xmax = '+str(xmax)+' ymin = '+str(ymin)+' ymax = '+str(ymax)
    # crop = self.img_red_wall[ymin:ymax, xmin:xmax]
    # crop2 = self.img_red_wall[(cc1.center_y-cc1.height/2):(cc1.center_y+cc1.height/2),(cc1.center_x-cc1.width/2):(cc1.center_x+cc1.width/2)]
    # #print 'dimensioni crop = '+str(crop.shape)
    # cv2.imwrite('/home/pierpaolo/TesiMagistrale/OUTPUT_DEBUG/output/'+str(cc1.id)+'_'+str(cc2.id)+'_0.png',crop)

    # cv2.imwrite('/home/pierpaolo/TesiMagistrale/OUTPUT_DEBUG/output/'+str(cc1.id)+'_cc.png',crop2)

    if red is not 0:
        # #print 'evitatu un edge inutile passante un muru tra le cc '+str(cc1.id)+' e '+str(cc2.id)
        return True
    else:
        return False

def over_wall2(wall_node, dst, cc1, cc2):
    x_min = min(cc1.x_min, cc2.x_min)-1
    y_min = min(cc1.y_min, cc2.y_min)-1
    h,w = dst.shape
    image = wall_node.image[y_min:y_min+h,x_min:x_min+w]
    #print 'dst = '+str(dst.shape)
    #print 'cropped wall = '+str(image)
    diff = dst-image
    white = cv2.countNonZero(diff)
    if white > 0:
        return True
    return False




def internal_pad(image):
    s = image.shape
    image[0:,0].fill(0)
    image[0,0:].fill(0)
    image[s[0]-1,0:].fill(0)
    image[0:,s[1]-1].fill(0)
    return image

def count_red(image, ymin, ymax, xmin, xmax):
        crop = image[ymin:ymax, xmin:xmax]
        dst = cv2.inRange(crop, (0,0,255), (0,0,255))
        return cv2.countNonZero(dst)

def crop_image(mask, img):
        #mask is a Cc, img is an image
        y_min=mask.y_min
        x_min=mask.x_min
        y_max=mask.y_max
        x_max=mask.x_max
        image=np.where(mask.image< 10 , 0, img[y_min:y_max+1,x_min:x_max+1])
        return image


def add_pad_single_pixel(image):
        s=image.shape
        if len(s) == 2:
            z1=np.zeros((1,s[1]))
            z2=np.zeros((s[0]+2,1))
        else:
            z1=np.zeros((1,s[1],s[2]))
            z2=np.zeros((s[0]+2,1,s[2]))
        image=np.concatenate((z1,image),axis=0)
        image=np.concatenate((image,z1),axis=0)
        image=np.concatenate((z2,image),axis=1)
        image=np.concatenate((image,z2),axis=1)
        return image

def add_pad(cc,padup,paddown,padleft,padright):
        s = cc.image.shape
        h = s[0]
        w = s[1]
        image = deepcopy(cc.image)
        if padup > 0:
            z1=np.zeros((padup,w))
            # #print 'z1 =  '+str(z1.shape)
            # #print 'image = '+str(cc.image.shape)
            image=np.concatenate((z1,image),axis=0) #aggiungo padding sopra
        if paddown > 0:
            z2=np.zeros((paddown,w))
            image=np.concatenate((image,z2),axis=0) #aggiungo padding sotto
        s = image.shape
        h = s[0]
        w = s[1]
        if padleft > 0:
            z3=np.zeros((h,padleft))
            image=np.concatenate((z3,image),axis=1)  #aggiungo padding a sinistra
        if padright > 0:
            z4=np.zeros((h, padright))
            image=np.concatenate((image,z4),axis=1) #aggiungo padding a destra
        # h1,w1=image.shape
        return image

def touching_dilation(cc1,cc2,width):
        # cc3= cv2.inRange(cc1, (0,255,0), (0,255,0))
        # cc4 = cv2.inRange(cc2, (0,255,0), (0,255,0))
        # cc3 = cv2.inRange(cc1, (0,255,0), (0,255,0)) or cv2.inRange(cc1, (255,0,255), (255,0,255)) or cv2.inRange(cc1, (0,0,255), (0,0,255))
        # cc4 = cv2.inRange(cc2, (0,255,0), (0,255,0)) or cv2.inRange(cc1, (255,0,255), (255,0,255)) or cv2.inRange(cc1, (0,0,255), (0,0,255))
        # img1, img2 = same_size(cc3,cc4)
        img1, img2 = same_size(cc1,cc2)
        img = img1+img2
        # width = int(2*width)+2
        width = int(round(2*width))+2
        kernel = np.ones((width,width),np.uint8)
        img1 = cv2.dilate(img1,kernel,iterations=1)
        img2 = cv2.dilate(img2,kernel,iterations = 1)
        img1 = add_pad_single_pixel(img1)
        img2 = add_pad_single_pixel(img2)
        img = img1+img2
        img1 = np.array(img1, dtype=np.uint8)
        img2 = np.array(img2, dtype=np.uint8)
        dst = cv2.bitwise_and(img1,img2,dst=None,mask=None)
        white = cv2.countNonZero(dst)
        del img1
        del img2
        if white > 1:
            return True, dst
        else:
            return False, dst

def touching_dilation2(cc1,cc2):
        # cv2.imwrite('/home/pierpaolo/DEBUG/'+str(cc1.id)+'.png', cc1.image)
        # cv2.imwrite('/home/pierpaolo/DEBUG/'+str(cc2.id)+'.png', cc2.image)
        img1, img2 = same_size(cc1,cc2)
        img = img1+img2
        # cv2.imwrite('/home/pierpaolo/DEBUG/'+str(cc1.id)+'_'+str(cc2.id)+'_plus.png', img)
        width1 = int(cc1.get_max_distance())
        width2 = int(cc2.get_max_distance())
        kernel1 = np.ones((width1,width1),np.uint8)
        kernel2 = np.ones((width2,width2),np.uint8)
        img1 = cv2.dilate(img1,kernel1,iterations=1)
        img2 = cv2.dilate(img2,kernel2,iterations = 1)
        img1 = add_pad_single_pixel(img1)
        img2 = add_pad_single_pixel(img2)
        img = img1+img2
        # cv2.imwrite('/home/pierpaolo/DEBUG/'+str(cc1.id)+'_'+str(cc2.id)+'_plus_width_'+str(width1)+'_'+str(width2)+'.png', img)
        img1 = np.array(img1, dtype=np.uint8)
        img2 = np.array(img2, dtype=np.uint8)
        # #print 'img1.shape = '+str(img1.shape)
        # #print 'img2.shape = '+str(img2.shape)
        dst = cv2.bitwise_and(img1,img2,dst=None,mask=None)
        # asd = cv.fromarray(dst)
        white = cv2.countNonZero(dst)
        del img1
        del img2
        # cv2.imwrite('/home/pierpaolo/DEBUG/'+str(cc1.id)+'_'+str(cc2.id)+'_and_width_'+str(width1)+'_'+str(width2)+'.png', dst)

        ##print 'pixel bianchi intersecando le due immagini: ' +str(white)
        if white > 1:

            return True, dst
        else:
            return False, dst

def check_wall_adjacence(img_red_wall, cc, thresh):
    cc_red = CC(0)
    cc_red.image = cv2.inRange(img_red_wall, (0,0,255), (0,0,255))
    cc_red.x_min = 0
    cc_red.y_min = 0
    cc_red.set_coordinates()
    img, img2 = same_size(cc, cc_red)
    width = int(3*cc.get_min_distance())+1
    kernel = np.ones((width,width),np.uint8)
    img1 = cv2.dilate(img,kernel,iterations=1)
    # if cc.id == 264:
    #     cv2.imwrite('/home/pierpaolo/images/'+str(cc.id)+'.png', img)
    #     cv2.imwrite('/home/pierpaolo/images/'+str(cc.id)+'_dilate.png', img1)
    #     cv2.imwrite('/home/pierpaolo/images/'+str(cc.id)+'red_wall.png', img2)
    #     cv2.imwrite('/home/pierpaolo/images/'+str(cc.id)+'sum.png', img2+img1)
    # #print 'img1 = '+str(img1.shape)
    # #print 'img2 = '+str(img2.shape)
    img1 = np.array(img1, dtype=np.uint8)
    img2 = np.array(img2, dtype=np.uint8)
    dst = cv2.bitwise_and(img1,img2,dst=None,mask=None)
    white = cv2.countNonZero(dst)
    # if cc.id == 264:
    #     #print 'red pixels = '+str(white)
    del img1
    del img2
    if white > thresh:
        return True, dst
    else:
        return False, dst

def same_size(cc1,cc2):
        ymax = max(cc1.y_max, cc2.y_max)
        ymin = min(cc1.y_min, cc2.y_min)
        xmax = max(cc1.x_max, cc2.x_max)
        xmin = min(cc1.x_min, cc2.x_min)

        padup1 = cc1.y_min-ymin
        paddown1 = ymax-cc1.y_max
        padleft1 = cc1.x_min-xmin
        padright1 = xmax-cc1.x_max
        padup2 = cc2.y_min-ymin
        paddown2 = ymax-cc2.y_max
        padleft2 = cc2.x_min-xmin
        padright2 = xmax-cc2.x_max
        img1 = add_pad(cc1,padup1,paddown1,padleft1,padright1)
        img2 = add_pad(cc2,padup2,paddown2,padleft2,padright2)
        return img1, img2

#check if cc1 is inside cc2
def is_inside(cc1, cc2):
        if (cc1.center_x > cc2.x_min and cc1.center_x < cc2.x_max) and (cc1.center_y > cc2.y_min and cc1.center_y < cc2.y_max):
            isin, dst = logic_and(cc1, cc2)
            return isin
        else:
            return False


def logic_and(cc1,cc2):
        # #print 'logic and: before same_size'
        # #print 'img1 = '+str(cc1.image.shape)
        # #print 'img2 = '+str(cc2.image.shape)
        img1, img2 = same_size(cc1,cc2)
        # #print 'after same_size'
        # #print 'img1 = '+str(img1.shape)
        # #print 'img2 = '+str(img2.shape)
        img1 = np.array(img1, dtype=np.uint8)
        img2 = np.array(img2, dtype=np.uint8)
        dst = cv2.bitwise_and(img1,img2,dst=None,mask=None)
        # asd = cv.fromarray(dst)
        white = cv2.countNonZero(dst)
        del img1
        del img2
        cc2.numPixel=(cc2.image.sum()/255)
        cc1.numPixel=(cc1.image.sum()/255)
        thresh = min(cc1.numPixel, cc2.numPixel)*70/100
        # #print 'white = '+str(white)
        # #print 'thresh = '+str(thresh)
        # #print 'areas = '+str(cc1.area)+' & '+str(cc2.area)
        # #print 'cc =' +str(cc1.id)
        # #print 'thresh = '+str(thresh)
        # #print 'white = '+str(white)
        if white > thresh:
            return True, dst
        else:
            return False, dst

def logic_match(cc,cc_gt):
        # #print 'logic and: before same_size'
        # #print 'img1 = '+str(cc1.image.shape)
        # #print 'img2 = '+str(cc2.image.shape)
        img1, img2 = same_size(cc,cc_gt)
        # #print 'after same_size'
        # #print 'img1 = '+str(img1.shape)
        # #print 'img2 = '+str(img2.shape)
        img1 = np.array(img1, dtype=np.uint8)
        img2 = np.array(img2, dtype=np.uint8)
        dst = cv2.bitwise_and(img1,img2,dst=None,mask=None)
        # asd = cv.fromarray(dst)
        white = cv2.countNonZero(dst)
        del img1
        del img2
        cc_gt.numPixel=cv2.countNonZero(cc_gt.image)
        cc.numPixel=cv2.countNonZero(cc.image)
        thresh = cc_gt.numPixel*0.40
        thresh2 = cc_gt.numPixel*2.1
        # #print 'Logic Match: cc.id = '+str(cc.id)+' and cc_gt.id = '+str(cc_gt.id)
        # #print 'white = '+str(white)
        # #print 'thresh = '+str(thresh)
        # #print 'thresh2 = '+str(thresh2)
        # #print 'cc.pixels = '+str(cc.numPixel)
        # #print 'cc_gt.pixels = '+str(cc_gt.numPixel)
        if white > thresh and cc.numPixel < thresh2:
            # #print 'match!'
            return True
        else:
            # #print 'nothing...'
            return False


def check_over_90(cc, cx, cy):
        if cx >= cc.x_min and cx <= cc.x_max and cy >= cc.y_min and cy <= cc.y_max:
            #print 'centro dentro il rettangolo del contorno'
            return True

        image = deepcopy(cc.image)
        s = cc.image.shape
        h = s[0]
        w = s[1]
        white = 0
        white = count_zero(image[cc.center_y:cc.center_y+1, 0:cc.center_x])
        white = count_zero(image[cc.center_y:cc.center_y+1, cc.center_x:w])
        white = count_zero(image[0:cc.center_y, cc.center_x:cc.center_x+1])
        white = count_zero(image[cc.center_y:h, cc.center_x:cc.center_x+1])
        if white == 0:
            return False
        if white > 1:
            return True
        else:
            return False


def count_zero(img):
        n = cv2.countNonZero(img)
        if n>0:
            return 1
        else:
            return 0

