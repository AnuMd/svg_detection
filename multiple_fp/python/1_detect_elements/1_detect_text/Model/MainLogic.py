__author__ = 'stefano'


from connectedcomponent import eps_circle, eps_ellipse, eps_rect
import shutil,os,cv2,time, sys, subprocess,getopt,traceback,copy,signal
import numpy as np
from PIL import Image
from distutils.dir_util import copy_tree
from franges import frange
from franges import drange
from scipy import ndimage
from operator import itemgetter
from natsort import natsorted
from shutil import copyfile
from xlwt import Workbook


from image import Image_class
from graphCC import GraphCC
from room_object_match import room_object_match_class
import imageOperations as imop
from copy import deepcopy
from connectedcomponent import CC
from pre_process import pre_process_class
from iterative_functions import iterative_functions_class
from room_functions import room_functions_class
from evaluate import evaluation_class
from cupboard_find import cupboard_class
from accessible_fp_generation import accessible_fp_class
from close_contour import contour_closing_functions
from close_cont_detect_elements import close_cont_detect_elements_class
from text_functions import text_funcitons_class


class MainFormLogic():
    #-------initialization method
    def __init__(self):
        self.thresholds = [0,0,0,0,0]
        self.count = 0
        self.name = ''
        self.path_floorplan = ''
        self.converted_floorplan_path = ''
        self.path_output = ''
        self.path_output_d = ''
        self.path_results = ''
        self.path_final_lines = ''
        self.path_gravvitas = ''
        self.path_gravvitas_text = ''
        self.path_gravvitas_svg = ''
        self.object_path = ''
        self.object_output_path = ''
        self.current_directory =''
        self.name = ''
        #--make 'debug_mode' True to see intermediate results-------
        debug_mode = False
        #--fra,eng
        self.language = "eng"
        self.iterative_obj = iterative_functions_class()
        signal.signal(signal.SIGALRM, self.handler)

        #----get path
        self.current_directory, self.input_path, self.output_path = self.get_path()



        #--create folders
        self.create_output_folders(self.current_directory,debug_mode)


        #--get list of input floorplans
        list_images_floorplan = self.pre_process_images(self.current_directory)

        #--define weight_lists
        criteria_weight_list = [['line_length', 0.6], ['Gradient', 0.75], ['TxtDistance', 1.0],
                                ['EELengthRatio', 0.4], ['ConvexityDefect', 0.5],['IplWeight',0.5]]

        weight_list = [row[1] for row in criteria_weight_list]


        ##---take each floorplan name from list of images
        for file_name in list_images_floorplan:
            signal.alarm(1200)
            try:
                # start_image_process_time = time.time()
                print 'Image is : ',file_name
                img = cv2.imread(self.path_floorplan + file_name, cv2.IMREAD_GRAYSCALE)
                orginal_img_height, orginal_img_width = img.shape
                self.floorplan_component_ID(file_name,img,weight_list,self.path_floorplan,orginal_img_height, orginal_img_width,debug_mode)



            except(KeyboardInterrupt,SystemExit):
                print "User interrupted!"
                try:
                    sys.exit(0)
                except SystemExit:
                    os._exit(0)

            # except Exception,exc:
                # print exc
                # print "---------------------Timeout Error occured with-------------------------", str(file_name)

            except:
                print "---------------------Error occured with-------------------------", str(file_name)
                traceback.print_exc()

        svg_files = os.listdir(self.path_gravvitas_text)
        for svg_file in svg_files:
            if os.path.isfile(self.path_gravvitas_text + str(svg_file)):
                if 'text_details.txt' in str(svg_file):
                    shutil.copyfile((self.path_gravvitas_text + str(svg_file)),
                                    self.output_path + '/6_text_details.txt')
        copy_tree(self.path_gravvitas_text, self.output_path + '/../../../results_collected/')
	

    def handler(self,signum, frame):
        print "Forever is over!"
        raise Exception("end of time")
    
	#shows the total execution time for the function
    def contatempo(fn):
        def interna(*args,**kw):
            start=time.time()
            result=fn(*args,**kw)
            tempo=time.time()-start
            print "Function: %s - Execution time: %4.2f" % (fn.__name__,tempo)
            return result
        return interna

    def create_output_folders(self,current_directory,debug_mode):
        #create folders to store output - OUTPUT/OUTPUT_DEBUG/RESULTS_TEST/EDGES folders
        self.path_output=current_directory+'/OUTPUT/'
        if not(os.path.exists(self.path_output)):
                os.mkdir(self.path_output)
        self.path_output_d=current_directory+'/OUTPUT_DEBUG/'
        if not(os.path.exists(self.path_output_d)):
                os.mkdir(self.path_output_d)
        self.path_final_lines = current_directory+'/Final_Lines/'
        if (os.path.exists(self.path_final_lines)):
            shutil.rmtree(self.path_final_lines)
        os.mkdir(self.path_final_lines)
        #--create place to write text for GraVVITTAS
        self.path_gravvitas=current_directory+'/GraVVITAS/'
        if not(os.path.exists(self.path_gravvitas)):
            os.mkdir(self.path_gravvitas)

        #--incremental folder creation--------------------------------------
        #OUTPUT folder incremental values for folder name
        list_dir_output = os.listdir(self.path_output)
        if len(list_dir_output)>0:
            t = [int(x) for x in list_dir_output]
            t.sort()
            name = str(t[len(t)-1]+1)
            self.path_output = self.path_output+name+'/'
            os.mkdir(self.path_output)
        else:
            self.path_output = self.path_output+str(1)+'/'
            name="1"
            os.mkdir(self.path_output)

        #OUTPUT_DEBUG folder incremental values for folder name
        if not(os.path.exists(self.path_output_d+ name +"/")):
            os.mkdir(self.path_output_d+ name +"/")
            self.path_output_d = self.path_output_d+ name +"/"
        else:
            shutil.rmtree(self.path_output_d+ name +"/")
            os.mkdir(self.path_output_d+ name +"/")
            self.path_output_d = self.path_output_d+ name +"/"

        #GRAVVITAS folder incremental values for folder name
        if not(os.path.exists(self.path_gravvitas+ name +"/")):
            os.mkdir(self.path_gravvitas+ name +"/")
            self.path_gravvitas = self.path_gravvitas+ name +"/"
        else:
            shutil.rmtree(self.path_gravvitas+ name +"/")
            os.mkdir(self.path_gravvitas+ name +"/")
            self.path_gravvitas = self.path_gravvitas+ name +"/"
        self.path_gravvitas_svg=self.path_gravvitas+'/SVG/'
        if not(os.path.exists(self.path_gravvitas_svg)):
            os.mkdir(self.path_gravvitas_svg)
        self.path_gravvitas_text=self.path_gravvitas+'/TEXT/'
        if not(os.path.exists(self.path_gravvitas_text)):
            os.mkdir(self.path_gravvitas_text)





        #create subfolders inside main OUTPUT and OUTPUT_DEBUG folders
        self.create_path_extract_text()
        self.create_path_preprocessing()
        self.create_path_output()



    def pre_process_images(self,current_directory):
        input_path_floorplan = self.input_path

        #--check if input folder exists, carry out rest of the program if it does
        if not(os.path.exists(input_path_floorplan)):
            print 'Directory Not Found.'
            sys.exit()
        else:
            # self.path_floorplan = input_path_floorplan
            # print 'input_path_floorplan',input_path_floorplan
            original_fp_path_unsorted = os.listdir(input_path_floorplan)
            original_fp_path = natsorted(original_fp_path_unsorted)
            # for each_image in original_fp_path:


            self.converted_floorplan_path = self.current_directory+'/input_fps/converted_fps/'
            if (os.path.exists(self.converted_floorplan_path)):
                shutil.rmtree(self.converted_floorplan_path)
            os.makedirs(self.converted_floorplan_path)


            for floor in original_fp_path:
                # print 'str(floor)',str(floor)
                if str(floor)=='.DS_Store':
                    os.remove(input_path_floorplan+str(floor))
                else:
                    if floor[-4:]=='.png':
                        shutil.copy(input_path_floorplan+str(floor),self.converted_floorplan_path+str(floor))
                    elif floor[-4:]=='.svg' or floor[-4:]=='.SVG':
                        im_name = str(floor)
                        only_im_name = im_name[:-4]
                        # os.system('convert -density 20 '+input_path_floorplan+str(floor)+' -set density 20 '+self.converted_floorplan_path+only_im_name+'_20.png')
                        # os.system('convert -density 50 '+input_path_floorplan+str(floor)+' -set density 50 '+self.converted_floorplan_path+only_im_name+'_50.png')
                        # os.system('convert -density 60 '+input_path_floorplan+str(floor)+' -set density 60 '+self.converted_floorplan_path+only_im_name+'_60.png')
                        # os.system('convert -density 70 '+input_path_floorplan+str(floor)+' -set density 70 '+self.converted_floorplan_path+only_im_name+'_70.png')
                        # os.system('convert -density 100 '+input_path_floorplan+str(floor)+' -set density 100 '+self.converted_floorplan_path+only_im_name+'_100.png')
                        # os.system('convert -density 150 '+input_path_floorplan+str(floor)+' -set density 150 '+self.converted_floorplan_path+only_im_name+'_150.png')
                        # os.system('convert -density 200 '+input_path_floorplan+str(floor)+' -set density 200 '+self.converted_floorplan_path+only_im_name+'_200.png')
                        os.system('convert -density 250 '+input_path_floorplan+str(floor)+' -set density 250 '+self.converted_floorplan_path+only_im_name+'.png')
                        # os.system('convert -density 500 ' + input_path_floorplan + str(
                        #     floor) + ' -set density 500 ' + self.converted_floorplan_path + only_im_name + '_500.png')

                    else:
                        print '----------'+input_path_floorplan+str(floor)
                        im_conv = Image.open(input_path_floorplan+str(floor))
                        im_name = str(floor)
                        only_im_name = im_name[:-4]
                        im_conv.save(self.converted_floorplan_path+only_im_name+'.png')

            self.path_floorplan = self.converted_floorplan_path
            list_images_floorplan_unsorted = os.listdir(self.converted_floorplan_path)
            list_images_floorplan = natsorted(list_images_floorplan_unsorted)

            self.create_path_open_plan(list_images_floorplan)
            return list_images_floorplan

    def floorplan_component_ID(self,file_name,img,weight_list, orginal_img_path,orginal_img_height, orginal_img_width,debug_mode):
        # ---extract only image name without extensions
        only_image_name = file_name[0:-4]
        path = self.path_output_d + only_image_name + '/'
        if not (os.path.exists(path)):
            os.mkdir(path)
        path_output = self.path_output + only_image_name + '/'
        if not (os.path.exists(path_output)):
            os.mkdir(path_output)



        image_color = cv2.imread(orginal_img_path + file_name, cv2.IMREAD_COLOR)

        # ---------Otsu's thresholding to clean image and remove smoothing effects
        ret2, otsu = cv2.threshold(img, 0, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)
        cv2.imwrite(self.path_output_d + 'preprocessing/' + only_image_name + '_otsu.png', otsu)


        image = Image_class(self.current_directory, file_name, self.path_output,
                            self.path_output_d, otsu, image_color)

        testing_flag = False
        if testing_flag:
            cc_image_path = image.test_image_process()
        else:
            print '1. Finding connected components and classify'
            list_cc_text, list_cc_no_text = image.find_CC_and_classify()

            print '2. Thin lines removal and clean image'
            cleaned_image = image.remove_thin_lines_and_clean_image(list_cc_no_text,
                                            self.path_final_lines+only_image_name)

            print '3. Refine text CC'
            list_cc_text = image.improve_textCC_accuracy(cleaned_image, list_cc_text)
            cc_image_path = image.generate_and_improve_text_image(list_cc_text)


        print '4. Text Recognition'
        self.text_functions_obj = text_funcitons_class(self.path_output, self.path_output_d,self.current_directory, file_name)
        self.text_functions_obj.identify_text(otsu,self.language,only_image_name, cc_image_path,self.path_gravvitas_text, orginal_img_width, orginal_img_height, debug_mode)






    def get_path(self):
        # current_directory, input_path, output_path = self.get_path_from_local_machine()
        current_directory, input_path, output_path = self.get_path_from_js()
        # print 'current_directory',current_directory
        # print 'input_path',input_path
        # print 'output_path',output_path
        return current_directory, input_path, output_path

    def get_path_from_local_machine(self):
        #--/home/ub/Documents/node_js/socketio/python/floor_plan_code
        current_directory = os.path.abspath(os.path.join(os.getcwd(), '..'))
        #--/home/ub/Documents/node_js/socketio/python/floor_plan_code/input_fps/anu/
        input_path = current_directory+ '/input_fps/anu/'
        # input_path = current_directory + '/input_fps/current_working_fps/'
        #--/home/ub/Documents/node_js/socketio/python/floor_plan_code/SVG_results/final_floorplan.svg
        output_path = current_directory+'/SVG_results/final_floorplan.png'
        return current_directory, input_path, output_path

    def get_path_from_js(self):
        js_message = sys.stdin.readlines()
        input_path, current_directory,output_path = '','',''
        for row_num,each in enumerate(js_message):
            if row_num==0:
                current_directory = each.rstrip()
            elif row_num == 1:
                input_path = each.rstrip() + '1_images/input/'
            elif row_num == 2:
                output_path = each.rstrip() + '/3_detection_files/'
        return current_directory, input_path, output_path


    def write_list_rect(self,type, component_list,orginal_img_path,only_image_name,
                        text_file_name,text):
        # --write to text file for gravvitas
        file = open(self.path_gravvitas_text + only_image_name + text_file_name, 'a')
        #---write to image
        image_to_write = cv2.imread(orginal_img_path)
        for row in component_list:
            if type == 'rect':
                x1, y1 = row[0]
                x2, y2 = row[1]
                cv2.rectangle(image_to_write, (int(x1) - 1, int(y1) - 1), (int(x2) + 1, int(y2) + 1),
                              (255, 255, 255), -1)
                file.write(text+' : ' + str(x1) + ',' + str(y1) + ',' + str(x2) + ',' + str(y2) + ' : [1] \n')
            elif type == 'contour':
                contour = ''
                for c, cord in enumerate(row):
                    if c == 0:
                        contour = contour + '[' + str(cord[0]) + ',' + str(cord[1]) + ']'
                    else:
                        contour = contour + ',[' + str(cord[0]) + ',' + str(cord[1]) + ']'
                file.write(text + ' : ' + contour + ' : [1] \n')
        file.close()
        cv2.imwrite(self.path_output_d+'results/'+only_image_name+'_'+text+'.png', image_to_write)







    def create_path_find_oblique_walls(self):
        path = self.path_output_d+'oblique_walls/'
        if not(os.path.exists(path)):
            os.mkdir(path)

    def create_path_stairs(self):
        path = self.path_output_d+'stairs/'
        if not(os.path.exists(path)):
            os.mkdir(path)

    def create_path_merge_all(self):
        path = self.path_output_d+'merge_all/'
        if not(os.path.exists(path)):
            os.mkdir(path)

    def create_path_merge_wwd(self):
        path_merge_wall=self.path_output_d+'merge_walls_doors_windows/'
        if not(os.path.exists(path_merge_wall)):
            os.mkdir(path_merge_wall)
       # os.mkdir(path_merge_wall+'step1/')

    def create_path_find_windows(self):
        path_window=self.path_output_d+'windows/'
        if not(os.path.exists(path_window)):
            os.mkdir(path_window)
       # os.mkdir(path_window+'step1/')

    def create_path_output(self):
        path_out=self.path_output_d+'output/'
        if not(os.path.exists(path_out)):
            os.mkdir(path_out)
        path_results = self.path_output_d+'results/'
        if not(os.path.exists(path_results)):
            os.mkdir(path_results)

    def create_path_wall(self):
        path_external_wall=self.path_output_d+'wall/'
        if not(os.path.exists(path_external_wall)):
            os.mkdir(path_external_wall)
            os.mkdir(self.path_output_d+'oblique_walls/')

    def create_path_find_door(self):
        path_external_wall=self.path_output_d+'door/'
        if not(os.path.exists(path_external_wall)):
            os.mkdir(path_external_wall)
            os.mkdir(path_external_wall+'ante/')


    def create_path_extract_text(self):
        extract_text_path = self.path_output_d+'Extract_text/'
        if not(os.path.exists(extract_text_path)):
            os.mkdir(extract_text_path)
            os.mkdir(extract_text_path+'area_dim/')
            os.mkdir(extract_text_path+'area_dim_2/')
            os.mkdir(extract_text_path+'asp_ratio/')
            os.mkdir(extract_text_path+'density/')
            os.mkdir(extract_text_path+'final_images/')
            os.mkdir(extract_text_path+'text_identification/')

         #in the path "path_clean_image", I save the output of the function clean_image (image.py)
        path_clean_image=extract_text_path+'clean_image/'
        if os.path.exists(path_clean_image):
            shutil.rmtree(path_clean_image)
        else:
            os.mkdir(path_clean_image)
            os.mkdir(path_clean_image+'Canny/')
            os.mkdir(path_clean_image+'Hough/')
            os.mkdir(path_clean_image+'Clean/')
            os.mkdir(path_clean_image+'Open/')# in this path there are the final image

    def create_path_preprocessing(self):
        path_preprocessing=self.path_output_d+'preprocessing/'
        if not(os.path.exists(path_preprocessing)):
            os.mkdir(path_preprocessing)


    def create_path_open_plan(self,list_images_floorplan):
        path_open_plan=self.path_output_d+'Rooms/'
        if not(os.path.exists(path_open_plan)):
            os.mkdir(path_open_plan)
        for each_image in list_images_floorplan:
            image_name = each_image[0:-4]
            image_folder_path = path_open_plan+image_name+'/'
            if not(os.path.exists(image_folder_path)):
                os.mkdir(image_folder_path)
                os.mkdir(image_folder_path+'All_Rooms/')
                os.mkdir(image_folder_path+'OpenPlans/')





    # Pierpaolo's:
    def add_new_objects(self):
        # self.usage=raw_input('Do you want to create new dictionaries? y for Yes, any digit for no: ')
        a = self.current_directory
        self.usage = 'y'
        if self.usage == 'y':
            self.__buildDictionary(a+'/dictionary/')
            self.__buildDictionary(a+'/secondary dictionary/')
            self.__buildDictionary(a+'/noise/')
        self.calculate_gt_features()
        print '--- finding objects in floorplans ----'
        self.colors_label = []
        self.gt_path = a+'/GT_Files/'
        self.read_color()
        self.num_class = len(self.colors_label)
        self.true_pos = np.zeros((self.num_class, 1))
        self.false_pos = np.zeros((self.num_class, self.num_class))
        self.objects = np.zeros((self.num_class, 1))
        self.avg = []
        for i in range(0, self.num_class):
            self.avg.append([])
            self.avg[i].append([])
            self.avg[i].append([])

        # self.usage=raw_input('Do you want to create new dictionaries? y for Yes, any digit for no: ')
        # self.usage = 'y'
        # if self.usage == 'y':
        #     self.__buildDictionary(self.current_directory+'/dictionary/')
        #     self.__buildDictionary(self.current_directory+'/secondary dictionary/')
        #     self.__buildDictionary(self.current_directory+'/noise/')
        # self.colors_label = []
        # self.gt_path = self.current_directory+'/GT_Files/'
        # #--read "self.current_directory+'/GT_Files/colors.txt'"
        # #-- get its each line "'crossed', '000040'"
        # #-- add to self.colors_label list
        # self.read_color()
        # for row in self.colors_label:
        #     print row
        # #--10 lines from "colors.txt" file + 'nothing', '000000' = len(self.colors_label) is 11
        # self.num_class = len(self.colors_label)
        # print self.num_class
        # self.true_pos = np.zeros((self.num_class, 1))
        # print 'true_pos'
        # for row in self.true_pos:
        #     print row
        # self.false_pos = np.zeros((self.num_class, self.num_class))
        # self.objects = np.zeros((self.num_class, 1))
        # self.avg = []
        # for i in range(0, self.num_class):
        #     self.avg.append([])
        #     self.avg[i].append([])
        #     self.avg[i].append([])
        # self.calculate_gt_features()
        # print '--- finding objects in floorplans ----'
        # self.colors_label = []
        # self.gt_path = self.current_directory+'/GT_Files/'
        # self.read_color()
        # self.num_class = len(self.colors_label)
        # self.true_pos = np.zeros((self.num_class, 1))
        # self.false_pos = np.zeros((self.num_class, self.num_class))
        # self.objects = np.zeros((self.num_class, 1))
        # self.avg = []
        # for i in range(0, self.num_class):
        #     self.avg.append([])
        #     self.avg[i].append([])
        #     self.avg[i].append([])

    def __buildDictionary(self, path):
        # a=os.path.dirname(os.getcwd())
        a = self.current_directory
        # dictionary_path = raw_input('Insert dictionary input directory: ')
        dictionary_path = path
        if not(os.path.exists(dictionary_path)):
            print 'Directory Not Found.'
            sys.exit()
        else:
            list_object_folder = os.listdir(dictionary_path)
            for folder in list_object_folder:
                if '.' not in folder:
                    object_path = dictionary_path+folder+'/'
                    #--object_path=dictionary/001 bath/
                    output_path = object_path+'output/'
                    debug_path = object_path+'preprocessing/'
                    #--creates 'dictionary/output/'
                    #--creates 'dictionary/preprocessing/'
                    if not(os.path.exists(output_path)):
                        os.mkdir(output_path)
                    if not(os.path.exists(debug_path)):
                        os.mkdir(debug_path)
                    list_images_o = os.listdir(object_path)
                    # find only image .png in directory
                    list_object_floorplan=[]
                    i = 1
                    for object in list_images_o:
                        #get image extension
                        #--object_path=dictionary/001 bath/bath001.png or 'dictionary/001 bath/bath002.png'
                        typefile=object[len(object)-4:len(object)]
                        if typefile=='.png':
                            list_object_floorplan.append(object)
                    list_object_floorplan.sort()
                    for f in list_object_floorplan:
                        #--for each bath001.png and bath002.png create a folder in 'dictionary/output/'
                        out_path = output_path+str(i)+'/'
                        if not(os.path.exists(out_path)):
                            os.mkdir(out_path)
                        # print '\n'+str(f)
                        s=f[0:-4]
                        img = cv2.imread(object_path+f,cv2.IMREAD_GRAYSCALE)
                        image_color =  cv2.imread(object_path+f,cv2.IMREAD_COLOR )
                        # Otsu's thresholding to clean image and remove smoothing effects
                        ret2,otsu = cv2.threshold(img,0,255,cv2.THRESH_BINARY+cv2.THRESH_OTSU)
                        image = Image_class(self.current_directory,f,out_path, debug_path, otsu,image_color)
                        #--create objects
                        image.find_connected_componet_dictionary()
                        i+=1

    def save_objects_recognition(self, path):
        file = open(path+'results.csv', 'w')
        file.write(' ;Objects found;TP:\n')
        for i in range(0, len(self.objects)):
            label = self.colors_label[i][0]
            file.write(label+'; '+str(int(self.objects[i][0]))+';'+str(int(self.true_pos[i][0]))+';\n')
        file.write('Total = '+str(int(self.objects.sum()))+';'+str(int(self.true_pos.sum()))+';\n')
        file.write('\nConfusion table:\n;')
        for i in range(0, len(self.false_pos)):
            label = self.colors_label[i][0]
            file.write(label+';')

        for i in range(0, len(self.false_pos)):
            file.write('\n')
            label = self.colors_label[i][0]
            file.write(label+';')
            for j in range(0, len(self.false_pos[i])):
                file.write(str(int(int(self.false_pos[i][j])))+';') #object j confused with object i
        file.write('\n')
        file.write('Total;')
        for i in range(0, len(self.objects)):
            file.write(str(int(self.false_pos[i].sum()))+';')
        file.write('\nTotal FP;'+str(int(self.false_pos.sum()))+'\n')
        # file.write('Similiar = '+str(self.similiar)+'\n')
        file.close()

    def read_color(self):
        file = open(self.gt_path+'colors.txt', 'r')
        line = file.readline()
        while len(line)>2:
            color = []
            color.append(line[0:-1])
            line = file.readline()
            color.append(line[0:-1])
            self.colors_label.append(color)
            line = file.readline()
        self.colors_label.append(['nothing', '000000'])

    def get_label_from_color(self, col):
        for color in self.colors_label:
            if color[1] == col:
                return color

    def calculate_gt_features(self):
        a = self.current_directory
        # a=os.path.dirname(os.getcwd())
        gt_path = a+'/GT_Files/'
        list_gt = os.listdir(gt_path)
        list_gt.sort()
        total_door = []
        for gt_file in list_gt:
            if gt_file[-3:] == 'svg':
                name = gt_file[0:-4]
                file = open(gt_path+'doors_width.txt', 'r')
                line = file.readline()
                end = line.find(':')
                count = 0
                while name != line[0:end] and count < 100:
                    line = file.readline()
                    end = line.find(':')
                    count+=1
                file.close()
                avg_door = float(line[end+2:])
                if avg_door > 40:
                    # print line[0:end]+': '+str(avg_door)
                    self.read_gt_and_weight_size(gt_path+name, avg_door)
                    total_door.append(avg_door)
        file = open(gt_path+'objects_proportions.csv', 'w')
        file.write(' ;min ;dev ;max ; dev\n')
        for i in range(0, len(self.avg)):
            label = self.colors_label[i][0]
            file.write(label+';')
            path_class = gt_path+label
            if not os.path.exists(path_class):
                os.mkdir(path_class)
            prop = open(path_class+'/proportions.txt', 'w')
            for k in range(0, 2):
                # print str(self.avg[i][k])
                avg = sum(self.avg[i][k])/len(self.avg[i][k])
                var = 0
                for j in self.avg[i][k]:
                    var+=(avg-j)**2
                var = (var/(len(self.avg[i][k])-1))
                dev = np.sqrt(var)
                file.write(str(avg)+';'+str(dev)+';')
                thresh1 = avg-dev
                thresh2 = avg+dev
                prop.write(str(thresh1)+'\n')
                prop.write(str(thresh2)+'\n')
            prop.close()
            file.write('\n')
        file.write('\n')
        bound = int(round(len(total_door)*0.05))
        for i in range(0, bound):
            total_door.remove(max(total_door))
            total_door.remove(min(total_door))
        avg = sum(total_door)/len(total_door)
        var = 0
        file.write('doors;')
        for j in total_door:
            var+=(avg-j)**2
        var = (var/(len(self.avg[i][k])-1))
        dev = np.sqrt(var)
        file.write(str(avg)+';'+str(dev)+';')
        file.close()

    def get_id_color(self, name):
        # print 'get id color for '+name[0:3]
        for i in range(0, len(self.colors_label)):
            # print 'self.colors_label[i][0][0:3] = '+self.colors_label[i][0][0:3]
            if self.colors_label[i][0][0:3] == name[0:3]:
                return i
        return i

    def match_gt(self, obj, gt):
        id = self.get_id_color(obj.label)
        cc = CC(id)
        xmin, xmax, ymin, ymax = obj.get_extremes()
        cc.x_min = xmin
        cc.y_min = ymin
        # print 'match object '+obj.label
        # print 'coordinates = '+str(obj.get_extremes())
        img = np.ones((ymax-ymin-2, xmax-xmin-2))
        cc.image = imop.add_pad_single_pixel(img)
        cc.set_coordinates()
        hit = False
        for cc_gt in gt.get_indices():
            found = imop.logic_match(cc, cc_gt)
            if found:
                if cc.id == cc_gt.id:
                    self.true_pos[cc_gt.id]+=1
                else:
                    self.false_pos[cc.id][cc_gt.id]+=1
                hit = True
        if not hit:
            self.false_pos[cc.id][-1]+=1

    def read_gt_and_weight_size(self, path, avg_door):
        file = open(path+'.svg')
        line = file.readline()
        while line.find('</svg>') == -1:
            #print line
            if '<rect' in line:
                line = file.readline()
                # print line
                stroke = line.find('stroke:#')
                color = line[stroke+8:stroke+14]
                i = len(self.colors_label)-1
                for j in range(0, len(self.colors_label)):
                    if self.colors_label[j] == color:
                        i = j
                cc = CC(i)
                self.objects[i]+=1
                # print 'color is '+color
                line = file.readline()
                # print line
                line = file.readline()
                # print line
                eq = line.find('=')
                w = float(line[eq+2:-2])
                line = file.readline()
                eq = line.find('=')
                h = float(line[eq+2:-2])
                max_axis = float(max(h,w))
                min_axis = float(min(h,w))
                self.avg[i][0].append(min_axis/avg_door)
                self.avg[i][1].append(max_axis/avg_door)
            line = file.readline()
        file.close()

    def read_gt(self, path):
        file = open(path+'.svg')
        line = file.readline()
        graph = GraphCC()
        while line.find('</svg>') == -1:
            #print line
            if '<rect' in line:
                line = file.readline()
                # print line
                stroke = line.find('stroke:#')
                color = line[stroke+8:stroke+14]
                i = len(self.colors_label)-1
                # print 'color = '+color
                for j in range(0, len(self.colors_label)):
                    if self.colors_label[j][1] == color:
                        i = j
                cc = CC(i)
                self.objects[i]+=1
                # print 'color is '+color
                line = file.readline()
                # print line
                line = file.readline()
                # print line
                eq = line.find('=')
                w = float(line[eq+2:-2])
                line = file.readline()
                eq = line.find('=')
                h = float(line[eq+2:-2])
                line = file.readline()
                eq = line.find('=')
                x = float(line[eq+2:-2])
                line = file.readline()
                eq = line.find('=')
                eq2 = line[eq+2:].find('"')
                y = float(line[eq+2:eq+2+eq2])
                cc.x_min = int(x)
                cc.y_min = int(y)
                img = np.ones((int(h-2), int(w-2)))
                cc.image = imop.add_pad_single_pixel(img)
                cc.set_coordinates()
                # print 'w = '+str(int(w)) +', h = '+str(h)+', x = '+str(x)+', y = '+str(y)
                graph.add_node(cc)
            line = file.readline()
        file.close()
        return graph

    # def increase_thresholding(self):
    #     if self.count%5 == 0:
    #         self.thresholds[0] += 0.002
    #     if self.count%5 == 1:
    #         self.thresholds[1] += 0.002
    #     if self.count%5 == 2:
    #         self.thresholds[2] += 0.01
    #     if self.count%5 == 3:
    #         self.thresholds[3] += 0.01
    #     if self.count%5 == 4:
    #         self.thresholds[4] += 0.05
    #     self.count+=1
    #     self.true_pos = np.zeros((self.num_class, 1))
    #     self.false_pos = np.zeros((self.num_class, self.num_class))
    #     self.objects = np.zeros((self.num_class, 1))
    #     return self.thresholds






if __name__ == '__main__':
    # start = time.time()
    hwl1 = MainFormLogic()
    # end = time.time()
    # print end - start
