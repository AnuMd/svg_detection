__author__ = 'stefano'

import numpy as np
from scipy import ndimage
from scipy import stats
import time
import random
import cv2
# import cv2.cv as cv
from rectangle_in_cc import Rectangle
from matplotlib import pyplot as plt
import sys
import math
import mathMethods as mm
from angle import Angle
import matplotlib.path as mplPath
from copy import deepcopy

bins1 = 25
bins2 = 50
eps_ellipse = 0.5
eps_rect = 0.85
eps_circle = 0.85

class CC():
    def __init__(self,id):
        self.list_image=None
        self.id = id
        self.image=None
        self.image_dist_tran=None
        self.numPixel=None # numpy array that contain the number of pixel of connected component
        self.x_min=0
        self.x_max=0
        self.y_min=0
        self.y_max=0
        self.width=0
        self.height=0
        self.image_dt_1 = None
        self.image_dt_2 = None

        #list of rectangles in the identified connected component
        self.list_rect=[]
        self.list_obl=[]
        self.list_print_obl=[]

        #Pierpaolo's
        self.contourNone = []
        self.contourSimple = []
        self.area = 0
        #centroid coordinates (absolute coordinates)
        self.cx = []
        self.cy = []
        #centroid's coordinates relative tp the image
        self.center_x = 0
        self.center_y = 0
        self.max_distance = 0
        self.min_distance = 999999999
        self.max_point = []
        self.min_point = []
        self.shape = ''
        self._list_linked = []
        self._indices = {}
        self._list_notlinked = []
        self._notindices = {}
        self.add_link(self)
        self.histogram = []
        self.histogram2 = []
        self.index_hist = {}
        self.polygon = []
        self.polygon_rec = []
        self.rotated = 0
        #end Pierpaolo's

    # contatempo: timekeeper
    def contatempo(fn):
        def interna(*args,**kw):
            start=time.time()
            result=fn(*args,**kw)
            tempo=time.time()-start
            print "Function: %s - Execution time: %4.2f" % (fn.__name__,tempo)
            return result
        return interna

    def getCentroid(self):
        return [self.y_min+(self.height/2),self.x_min+(self.width/2)]

    def getArea(self):
        return self.height*self.width

    def getDensity(self):
        return float(self.numPixel)/float(self.height*self.width)

    def getAspectRatio(self):
        return float(self.width)/float(self.height)

    def getLengthSide(self):
        return max(self.height,self.width)

    def extract_rectangle_in_cc_3(self, index_inc):
        id = 1

        image = np.array(self.image,dtype=np.int32)

        max_dt = self.image_dt_1.max()
        while(max_dt>1):

            vet_max_point = np.argwhere(self.image_dt_1 == max_dt)
            # vet_max_point contains the pixels with maximum value
            # in the distance transform.
            # If the CC contains a rectangle these points are in the central part
            l = vet_max_point.tolist()
            r = random.choice(l)
            #r is one of the vet_max_points randomly selected
            l.pop(l.index(r))
            y = float(r[0])
            x = float(r[1])
            #x,y coordinates of r
            # (x+self.x_min) > 612 and (x+self.x_min) < 915 and (y+self.y_min) > 1296 and (y+self.y_min) < 1303
            # coef. angolare, intersezione asse x, coefficiente di corr,


            # print 'coor_x: '+str(x+self.x_min)+'   coor_y: '+str(y+self.y_min)+ '  max_dt: ' + str(max_dt)
            m,q,r,p,s = self.find_orientation_rectangle_3(self.image_dt_1,max_dt,x,y,20)

            # print 'coefficente angolare : '+str(m)+'   intersezione con gli assi: '+str(q)+'   coeff. di regressione: '+str(r)
            # plt.imshow(i, cmap = 'gray', interpolation = 'bicubic')
            # plt.xticks([]), plt.yticks([])  # to hide tick values on X and Y axis
            # plt.show()

            if np.abs(r)>=0.9 and np.abs(m) > 0.1 and np.abs(m) < 1000:
                l_rect = self.extract_oblique_rect_in_CC_3(x,y,m,q)
                if l_rect[0]>0:
                    rec = Rectangle(id+index_inc)
                    id += 1
                    if m>0:
                        rec.x1 = l_rect[0] + self.x_min
                        rec.y1 = l_rect[1] + self.y_min + 1

                        rec.x2 = l_rect[2] + self.x_min + 1
                        rec.y2 = l_rect[3] + self.y_min

                        rec.x3 = l_rect[6] + self.x_min
                        rec.y3 = l_rect[7] + self.y_min - 1

                        rec.x4 = l_rect[4] + self.x_min - 1
                        rec.y4 = l_rect[5] + self.y_min
                        p = np.array([[[l_rect[0], l_rect[1]+1],[l_rect[2]+1, l_rect[3]],[l_rect[6], l_rect[7]-1],[l_rect[4]-1, l_rect[5]]]], dtype=np.int32)
                    else:
                        rec.x1 = l_rect[0] + self.x_min
                        rec.y1 = l_rect[1] + self.y_min + 1

                        rec.x2 = l_rect[2] + self.x_min - 1
                        rec.y2 = l_rect[3] + self.y_min

                        rec.x3 = l_rect[6] + self.x_min
                        rec.y3 = l_rect[7] + self.y_min - 1

                        rec.x4 = l_rect[4] + self.x_min + 1
                        rec.y4 = l_rect[5] + self.y_min
                        p = np.array([[[l_rect[0] , l_rect[1]+1],[l_rect[2]-1, l_rect[3]],[l_rect[6], l_rect[7]-1],[l_rect[4]+1, l_rect[5]]]], dtype=np.int32)

                    rec.type = "oblique"
                    rec.set_skeleton()
                    self.list_rect.append(rec)

                    i_app = np.ones((self.height+2,self.width+2))
                  #  p = np.array([[[l_rect[0], l_rect[1]],[l_rect[2], l_rect[3]],[l_rect[6], l_rect[7]],[l_rect[4], l_rect[5]]]], dtype=np.int32)
                    cv2.drawContours(i_app, [p], -1, 0, -1)

                    self.image_dt_1 = self.image_dt_1 * i_app
                    self.image_dt_1 = ndimage.morphology.distance_transform_cdt(self.image_dt_1)
                    max_dt = self.image_dt_1.max()
                else:
                    id = self.find_horizontal_and_vertical_rectangle(x,y,max_dt,index_inc,id)
                    max_dt = self.image_dt_1.max()
                # plt.imshow(i, cmap = 'gray', interpolation = 'bicubic')
                # plt.xticks([]), plt.yticks([])  # to hide tick values on X and Y axis
                # plt.show()

                # cv2.drawContours(image, [p], -1, 125, -1)
                # plt.imshow(image, cmap = 'gray', interpolation = 'bicubic')
                # plt.xticks([]), plt.yticks([])  # to hide tick values on X and Y axis
                # plt.show()
            elif np.abs(m) < 0.1 or np.abs(m) > 1000 or np.abs(r)<0.9 :
                id = self.find_horizontal_and_vertical_rectangle(x,y,max_dt,index_inc,id)
                max_dt = self.image_dt_1.max()

                    # p = np.array([[[rec.x1, rec.y1],[rec.x2, rec.y2],[rec.x3, rec.y3],[rec.x4, rec.y4]]], dtype=np.int32)
                    # cv2.drawContours(self.image, [p], -1, (0,0,255), 2)


            # plt.imshow(i, cmap = 'gray', interpolation = 'bicubic')
            # plt.xticks([]), plt.yticks([])  # to hide tick values on X and Y axis
            # plt.show()


        #when the max_dt is 1 use this method, otherwise use earlier method
        c = np.argwhere(self.image_dt_1 == 1)
        l = c.tolist()

        while (len(l)!=0):

            rect=[]
            px=l.pop(0)
            rect.append(px)
            x=px[1]
            y=px[0]
            x_max=x
            y_max=y
            x_min=x
            y_min=y
            ver=True
            x1=x
            y1=y+1

            if([y+1,x] in l):
                rect.append(l.pop(l.index([y+1,x])))
                y_max=y+1
            if([y+1,x-1] in l):
                rect.append(l.pop(l.index([y+1,x-1])))
                y_min=y-1
                x_min=x-1

            while([y,x1+1] in l or [y+1,x1+1] in l):
                x1=x1+1
                if([y,x1] in l):
                    rect.append(l.pop(l.index([y,x1])))
                    x_max=x1
                if([y+1,x1] in l):
                    rect.append(l.pop(l.index([y+1,x1])))
                    y_max=y+1

            if((x1-x)>2):
                #x1=x-1
                ver=False


            if(ver):
                rect=[]
                rect.append(px)
                while([y1+1,x] in l or [y1+1,x-1] in l or [y1+1,x+1] in l):
                    y1=y1+1
                    y_max=y1
                    if([y1,x] in l):
                        rect.append(l.pop(l.index([y1,x])))

                    if([y1,x+1] in l):
                        rect.append(l.pop(l.index([y1,x+1])))
                        x_max=x+1

                    if([y1,x-1] in l):
                        rect.append(l.pop(l.index([y1,x-1])))
                        x_min=x-1


            rec=Rectangle(id+index_inc)
            id=id+1

            rec.x1=x_min+self.x_min
            rec.x2=x_max+self.x_min
            rec.y1=y_min+self.y_min
            rec.y2=y_min+self.y_min

            rec.x3=x_max+self.x_min
            rec.x4=x_min+self.x_min
            rec.y3=y_max+self.y_min
            rec.y4=y_max+self.y_min


            # id=id+1
            # rec.id=id+index_inc
            #rec.list_point=rect
            self.list_rect.append(rec)
            rec.set_skeleton()

        return self.list_rect[:]

    def find_orientation_rectangle_3(self,i,max_dt,x1,y1,n_points):
        list_point = [[x1,y1]]
        list_final_point_x= [x1]
        list_final_point_y= [y1]
        list_point_2=[[x1,y1]]
        img = np.array(self.image,dtype=np.uint8)
        # print 'max_dt iniaziale : ' + str(max_dt)
        # plt.imshow(self.image, cmap = 'gray', interpolation = 'bicubic')
        # plt.xticks([]), plt.yticks([])  # to hide tick values on X and Y axis
        # plt.show()
        max_dt_ori = max_dt
        stop = True
        if max_dt>10:
            count = 100
        else:
            count = 100

        check_ad = False

        while(len(list_point)>0 and stop):
            count = count-1
            if count==0:
                stop=False

            p = list_point.pop()
            x = p[0]
            y = p[1]
            h,w = i.shape


           ## aa= i[y-1:y+2,x-1:x+2]
           ## print aa


            if ((y>1 and y<(h-1)) and (x>1 and x<(w-1))):

                # if (i[y-1,x-1] >= max_dt and (not(x-1 in list_final_point_x) or not(y-1 in list_final_point_y))):
                if (i[y-1,x-1] == max_dt  and not([x-1,y-1] in list_point_2)):
                    list_point.append([x-1,y-1])
                    list_point_2.append([x-1,y-1])
                    list_final_point_x.append(x-1)
                    list_final_point_y.append(y-1)
                    check_ad = True
                # if (i[y-1,x] >= max_dt and (not(x in list_final_point_x) or not(y-1 in list_final_point_y))):
                if (i[y-1,x] == max_dt  and (not([x,y-1] in list_point_2))):
                    list_point.append([x,y-1])
                    list_point_2.append([x,y-1])
                    list_final_point_x.append(x)
                    list_final_point_y.append(y-1)
                    check_ad = True
                # if (i[y-1,x+1] >= max_dt and (not(x+1 in list_final_point_x) or not(y-1 in list_final_point_y))):
                if (i[y-1,x+1] == max_dt and (not([x+1,y-1] in list_point_2))):
                    list_point.append([x+1,y-1])
                    list_point_2.append([x+1,y-1])
                    list_final_point_x.append(x+1)
                    list_final_point_y.append(y-1)
                    check_ad = True
                #if (i[y,x-1] >= max_dt and (not(x-1 in list_final_point_x) or not(y in list_final_point_y))):
                if (i[y,x-1] == max_dt and (not([x-1,y] in list_point_2))):
                    list_point.append([x-1,y])
                    list_point_2.append([x-1,y])
                    list_final_point_x.append(x-1)
                    list_final_point_y.append(y)
                    check_ad = True
                # if (i[y,x+1] >= max_dt and (not(x+1 in list_final_point_x) or not(y in list_final_point_y))):
                if (i[y,x+1] == max_dt and (not([x+1,y] in list_point_2))):
                    list_point.append([x+1,y])
                    list_point_2.append([x+1,y])
                    list_final_point_x.append(x+1)
                    list_final_point_y.append(y)
                    check_ad = True
                # if (i[y+1,x-1] >= max_dt and (not(x-1 in list_final_point_x) or not(y+1 in list_final_point_y))):
                if (i[y+1,x-1] == max_dt and (not([x-1,y+1] in list_point_2))):
                    list_point.append([x-1,y+1])
                    list_point_2.append([x-1,y+1])
                    list_final_point_x.append(x-1)
                    list_final_point_y.append(y+1)
                    check_ad = True
                #if (i[y+1,x] >= max_dt and (not(x in list_final_point_x) or not(y+1 in list_final_point_y))):
                if (i[y+1,x] == max_dt and (not([x,y+1] in list_point_2))):
                    list_point.append([x,y+1])
                    list_point_2.append([x,y+1])
                    list_final_point_x.append(x)
                    list_final_point_y.append(y+1)
                    check_ad = True
                #if (i[y+1,x+1] >= max_dt and (not(x+1 in list_final_point_x) or not(y+1 in list_final_point_y))):
                if (i[y+1,x+1] == max_dt and (not([x+1,y+1] in list_point_2))):
                    list_point.append([x+1,y+1])
                    list_point_2.append([x+1,y+1])
                    list_final_point_x.append(x+1)
                    list_final_point_y.append(y+1)
                    check_ad = True

                if not(check_ad):
                    check_ad =True
                  #  count = 100
                    max_dt -=1
                    n_points = 100
                    list_point.append([x1,y1])

            if len(list_point_2)<n_points and len(list_point)==0:
                list_point = list_point_2[:]
                max_dt -=1
                 #list_point.append([x1,y1])
            #     count_1 = 0
            #     count_2 = 0
            #     count_3 = 0
            #     count_4 = 0
            #     list_supp_x = []
            #     list_inf_x = []
            #     list_supp_y = []
            #     list_inf_y = []
            #     for pp in list_point_2:
            #         if pp[0] > x1:
            #             count_1 += 1
            #             list_supp_x.append(pp)
            #         elif pp[0] < x1:
            #             count_2 += 1
            #             list_inf_x.append(pp)
            #         elif pp[1] > y1:
            #             count_3 += 1
            #             list_supp_y.append(pp)
            #         elif pp[1] < x1:
            #             count_4 += 1
            #             list_inf_y.append(pp)
            #
            #     if count_1 > count_2:
            #         if count_3 >= count_4:
            #             for p1 in list_supp_x:
            #                 if p1 in list_supp_y:
            #                     list_point.append(p1)
            #         if count_3 < count_4:
            #             for p1 in list_supp_x:
            #                 if p1 in list_inf_y:
            #                     list_point.append(p1)
            #     elif count_1 < count_2:
            #         if count_3 >= count_4:
            #             for p1 in list_inf_x:
            #                 if p1 in list_supp_y:
            #                     list_point.append(p1)
            #         if count_3 < count_4:
            #             for p1 in list_inf_x:
            #                 if p1 in list_inf_y:
            #                     list_point.append(p1)
            #
            #     if count_3 > count_4:
            #         if count_1 >= count_2:
            #             for p1 in list_supp_y:
            #                 if p1 in list_supp_x:
            #                     list_point.append(p1)
            #         if count_1 <= count_2:
            #             for p1 in list_supp_y:
            #                 if p1 in list_inf_x:
            #                     list_point.append(p1)
            #     elif count_3 < count_4:
            #         if count_1 >= count_2:
            #             for p1 in list_inf_y:
            #                 if p1 in list_supp_x:
            #                     list_point.append(p1)
            #         if count_1 < count_2:
            #             for p1 in list_inf_y:
            #                 if p1 in list_inf_x:
            #                     list_point.append(p1)
            #     else:




        len_max = list_final_point_x.count(max(list_final_point_x))
        len_min = list_final_point_x.count(min(list_final_point_x))
        if len(list_final_point_x)>1:
            if (len_max < (len(list_final_point_x)*0.7)) and (len_min < (len(list_final_point_x)*0.7)):
                # coef. angolare, intersezione asse x, coefficiente di corr,
                m,q,r,p,s = stats.linregress(np.asarray(list_final_point_x),np.asarray(list_final_point_y))
                # print 'coor_x: '+str(x1+self.x_min)+'   coor_y: '+str(y1+self.y_min)
                # print 'max_dt finale : ' + str(max_dt)
                # print 'coefficente angolare : '+str(m)+'   intersezione con gli assi: '+str(q)+'   coeff. di regressione: '+str(r)
                # print '--------------------------------------------------'
                #print "x: " +str(x)+"; y: "+str(y)+"; m: " +str(m)+"; q: "+str(q)+"; r: "+str(r)+"; s: "+str(p)+"; "
                # if np.abs(m)>0.1 and  np.abs(r)<0.9:
                #     gooo = True
                #     find = False
                #     while gooo:
                #         if len(list_point_2)>0:
                #             app = list_point_2.pop()
                #             if (i[app[1],app[0]]==max_dt):
                #                 x1 = app[0]
                #                 y1 = app[1]
                #                 gooo = False
                #                 find = True
                #         else:
                #              gooo = False
                #     if find:
                #         m,q,r,p,s = self.find_orientation_rectangle_3(i,max_dt_ori-1,x1,y1,n_points*2)
                #
                #     return m,q,r,p,s
                # el
                if np.abs(m) < 0.1:
                    return 0,-1,-1,-1,-1

                return m,q,r,p,s
            else:
                return 10000,-1,-1,-1,-1

        return -1,-1,-1,-1,-1

    def extract_oblique_rect_in_CC_3(self,x,y,m,q):
        ang = int(math.degrees(math.atan(m)))
        if m>0:
            ang1 = ang + 1
            m1 = math.tan(math.radians(ang1))
            q1 = y - float(m1 * x)
            ang2 = ang - 1
            m2 = math.tan(math.radians(ang2))
            q2 = y - float(m2 * x)
            l_ori = self.find_oblique_rectangle(x,y,m,q)
            area_ori = self.area_oblique_rectangle(l_ori)
            # if l_ori[0]>0:
            repeat_inc = True
            while repeat_inc:
                q1 = y - float(m1*x)
                l_inc = self.find_oblique_rectangle(x,y,m1,q1)
                if l_inc[0]>0:
                    area_inc = self.area_oblique_rectangle(l_inc)
                    if area_inc < area_ori:
                        repeat_inc = False
                    else:
                        l_ori = l_inc[:]
                        area_ori = area_inc
                        ang1 += 1
                        m1 = math.atan(ang1)
                        q1 = y - float(m1*x)
                else:
                    ang1 += 1

                if ang1 > ang + 10:
                    repeat_inc = False

            repeat_dec = True
            while repeat_dec:
                l_dec = self.find_oblique_rectangle(x,y,m2,q2)
                if l_dec[0]>0:
                    area_dec = self.area_oblique_rectangle(l_dec)
                    if area_dec < area_ori:
                        repeat_dec = False
                    else:
                        l_ori = l_dec[:]
                        area_ori = area_dec
                        ang2 -= 1
                        m2 = math.atan(ang2)
                        q2 = y - float(m2*x)
                else:
                    ang2 -= 1

                if ang2 < ang - 10:
                    repeat_dec = False

        else:
            ang1 = ang - 1
            m1 = math.tan(math.radians(ang1))
            q1 = y - float(m1 * x)
            ang2 = ang + 1
            m2 = math.tan(math.radians(ang2))
            q2 = y - float(m2 * x)

            l_ori = self.find_oblique_rectangle(x,y,m,q)
            area_ori = self.area_oblique_rectangle(l_ori)
            #if l_ori[0]>0:
            repeat_inc = True
            while repeat_inc:
                q1 = y - float(m1*x)
                l_inc = self.find_oblique_rectangle(x,y,m1,q1)
                if l_inc[0]>0:
                    area_inc = self.area_oblique_rectangle(l_inc)
                    if area_inc < area_ori:
                        repeat_inc = False
                    else:
                        l_ori = l_inc[:]
                        area_ori = area_inc
                        ang1 -= 1
                        m1 = math.atan(ang1)
                        q1 = y - float(m1*x)
                else:
                    ang1 -= 1

                if ang1 < ang - 10:
                    repeat_inc = False

            repeat_dec = True
            while repeat_dec:
                l_dec = self.find_oblique_rectangle(x,y,m2,q2)
                if l_dec[0]>0:
                    area_dec = self.area_oblique_rectangle(l_dec)
                    if area_dec < area_ori:
                        repeat_dec = False
                    else:
                        l_ori = l_dec[:]
                        area_ori = area_dec
                        ang2 += 1
                        m2 = math.atan(ang2)
                        q2 = y - float(m2*x)
                else:
                    ang2 += 1

                if ang2 > ang + 10:
                    repeat_dec = False

        return l_ori
                #list_polyline.append([x_1,y_1,x_2,y_2,x_3,y_3,x_4,y_4,m])

    def area_oblique_rectangle(self,l):
        base = math.sqrt((l[0] - l[2])**2 + (l[1] - l[3])**2)
        altezza = math.sqrt((l[0]-l[6])**2 + (l[1]-l[7])**2)
        return base*altezza

    def find_oblique_rectangle(self,x,y,m,q):
        list_polyline=[]
        point_acc=[]
        list_rette=[]
        x1 = int(x)
        y1 = int(y)
        stop = True
        # dal punto x,y seguendo la retta definita da m,q cerco il primo pixel con un valore <=1 per trovare gli estremi dell rettangolo
        i = self.image_dt_2
        while(stop):
            if y1 < self.height and x1 < self.width:
                val = i[y1,x1]
                if val<=1:
                    stop = False
                else:
                    x1 = int(x1 + val)
                    y1 = int(round(m*x1 + q))
            else:
                stop = False
        x2 = x
        y2 = y
        stop = True
        while(stop):
            if y2 < self.height and x2 < self.width:
                val = i[y2,x2]
                if val<=1:
                    stop = False
                else:
                    x2 = int(x2 - val)
                    y2 = int(round(m*x2 + q))
            else:
                stop = False
        list_rette.append([x1+self.x_min,y1+self.y_min,x2+self.x_min,y2+self.y_min])

        # vado a trovare le due rette che passsano per i margini del muro (o del lato del rettangolo )
        # prima trovo i punti di confine e poi le rette

        dist= math.sqrt((x1-x2)**2 + (y1-y2)**2)
        if dist>20:
            point_acc.append([x,y])
            # if (np.abs(m)<=1):
            x3 = x
            y3 = y
            x4 = x
            y4 = y
            stop3 = True
            stop4 = True
            while(stop3 or stop4):
                val3 = i[y3,x3]
                val4 = i[y4,x4]
                if (val3 > 1 and stop3):
                    y3 = y3 + (val3-1)
                else:
                    stop3 = False

                if (val4 > 1 and stop4):
                    y4 = y4 - (val4-1)
                else:
                    stop4 = False
            m3 = m
            q3 = y3 - (m3*x3)
            m4 = m
            q4 = y4 - (m4*x4)
            # vado a individuare i punti di intersezione tra le rette r3 e r4 con le rette passanti per i punti x1,y1 e x2,y2 e perpendicolari a r3 e r4
            stop5 = True
            check5 = True
            while(stop5):
                m5 = -(1/m)
                q5 =  y1 - float(m5*x1)
                # calcolo i punti di intersezione tra la retta 5 e le retta r3 e r4
                x_1 = int(round((q5 - q3) / (m3 - m5)))
                y_1 = int(round((m3 * x_1) + q3))
                x_2 = int(round((q5 - q4) / (m4 - m5)))
                y_2 = int(round((m4 * x_2) + q4))
                if m>0:
                    if (x1 >=x and y1>=y ):
                        if (y_1<self.height-1 and y_2<self.height-1 and x_1<self.width-1 and x_2<self.width-1 ):
                            if (i[y_1,x_1]>=1 and i[y_2,x_2]>=1):
                                stop5= False
                            else:
                                x1 = x1 - 1
                                y1 = int(round(m*x1 + q))
                        else:
                            if(self.height-1<=y_1 or self.width-1<=x_2 ):
                                x1=x1-1
                                y1 = int(round(m*x1 + q))
                            else:
                                stop5 = False
                                check5 = False
                    else:
                        stop5 = False
                        check5 = False
                else:
                    if (x1 >=x and y1<=y):
                        if(y_1<self.height-1 and y_2<self.height-1 and x_1<self.width-1 and x_2<self.width-1 and y_2>0):
                            if (i[y_1,x_1]>=1 and i[y_2,x_2]>=1):
                                stop5= False
                            else:
                                x1 = x1 - 1
                                y1 = int(round(m*x1 + q))
                        else:
                            if(y_2<=0 or self.width<=x_1):
                                x1=x1-1
                                y1 = int(round(m*x1 + q))
                            else:
                                stop5 = False
                                check5 = False
                    else:
                        stop5 = False
                        check5 = False

            stop6 = True
            check6 = True
            while(stop6):
                # c6=c6+1
                m6 = -(1/m)
                q6 =  y2 - (m6*x2)
                # calcolo i punti di intersezione tra la retta 5 e le retta r3 e r4
                x_3 = int(round((q6 - q3) / (m3 - m6)))
                y_3 = int(round(m3 * x_3 + q3))
                x_4 = int(round((q6 - q4) / (m4 - m6)))
                y_4 = int(round(m4 * x_4 + q4))
                # print 'x_3   y_3   x_4   y_4 '
                # print [x_3+self.x_min,y_3+self.y_min,x_4+self.x_min,y_4+self.y_min]
                if m>0:
                    #if (x_3<self.width and x_4 <self.width and y_3<self.height and y_4<self.height):
                    if (x2<=x and y2<=y):
                        if(y_4>0 and y_3>0 and  y_3<self.height-1 and y_4<self.height-1 and x_3<self.width-1 and x_4<self.width-1 ):
                            if (i[y_3,x_3]>=1 and i[y_4,x_4]>=1 ):
                                stop6= False
                            else:
                                #x2 = x2 + val
                                x2 = x2 + 1
                                y2 = int(round(m*x2 + q))
                        else:
                            if(x_3<=0 or y_4<=0):
                                x2 = x2 + 1
                                y2 = int(round(m*x2 + q))
                            else:
                                stop6 = False
                                check6 = False
                    else:
                        stop6 = False
                        check6 = False
                else:
                    if (x2<=x and y2>=y):
                        if( y_3<self.height-1 and y_4<self.height-1 and x_4<self.width-1 and x_3<self.width-1 ):
                            if (i[y_3,x_3]>=1 and i[y_4,x_4]>=1 ):
                                stop6= False
                            else:
                                #x2 = x2 + val
                                x2 = x2 + 1
                                y2 = int(round(m*x2 + q))
                        else:
                            if(y_3>=self.height or y_4<=0):
                                x2 = x2 + 1
                                y2 = int(round(m*x2 + q))
                            else:
                                stop6 = False
                                check6 = False
                    else:
                        stop6 = False
                        check6 = False
            if check5 and check6:
                return [x_1,y_1,x_2,y_2,x_3,y_3,x_4,y_4,m]

        return [-1,-1,-1,-1,-1,-1,-1,-1,-1]

    def find_horizontal_and_vertical_rectangle(self,x,y,max_dt,index_inc,id):

        y1_h = y + max_dt - 1
        y2_h = y - max_dt + 1
        x1_h = x + max_dt - 1
        x2_h = x - max_dt+1

        #find horizontale rectangle
        val_1 = self.image_dt_2[y,x1_h]
        while( val_1 > 1 ):
            x1_h = x1_h + val_1 - 1
            val_1 = self.image_dt_2[y,x1_h]

        val_2 = self.image_dt_2[y,x2_h]
        while( val_2 > 1 ):
            x2_h = x2_h - val_2 + 1
            val_2 = self.image_dt_2[y,x2_h]

        x1_h = x1_h + 1
        x2_h = x2_h - 1

        if(self.image_dt_2[y1_h , x]==2):
            y1_h += 1

        if(self.image_dt_2[y2_h , x]==2):
            y2_h -= 1

        #find vertical rectangle
        y1_v = y + max_dt - 1
        y2_v = y - max_dt + 1
        x1_v = x + max_dt - 1
        x2_v = x - max_dt + 1

        val_1 = self.image_dt_2[y1_v,x]
        while(val_1 > 1):
            y1_v = y1_v + val_1 - 1
            val_1 = self.image_dt_2[y1_v,x]

        val_2 = self.image_dt_2[y2_v,x]
        while(val_2 > 1):
            y2_v = y2_v - val_2 + 1
            val_2 = self.image_dt_2[y2_v,x]

        y1_v = y1_v + self.image_dt_2[y1_v , x] - 1
        y2_v = y2_v - self.image_dt_2[y2_v , x] + 1

        if(self.image_dt_2[y , x1_v]==2):
            x1_v += 1

        if(self.image_dt_2[y , x2_v]==2):
            x2_v -= 1

        rectangle_h=self.image[y2_h:y1_h+1, x2_h:x1_h+1]
        rectangle_v=self.image[y2_v:y1_v+1 , x2_v:x1_v+1]
        # adesso devo risolvere il problema dei effetto pettine che fi forma in alcune immagini
        #
        #   1111111111111111111111111111111111111111111111111111111
        #       1  1 11  1  1 111 11   111  1 1 1 1    1   1  111 1
        #
        # vado verificare se sui quattro lati( o solo su quelli piu lunghi) del rettango appena trovato ho una percentuale di pixel  di valore 255  compresa tra il 20% e 80% se cos ela includo

        rec=Rectangle(id+index_inc)
        id = id+1
        #select horizontal rectanlge based on area size. Get the rectangle with maximum size
        if(rectangle_h.sum() >= rectangle_v.sum()):
            if y1_h+1<self.height:
                score= (np.sum(self.image[y1_h+1, x2_h:x1_h+1])/255)/(x1_h+1- x2_h)
                if (score>0.2 and score<0.8):
                    y1_h = y1_h + 1
            if y2_h-1>=0:
                score= (np.sum(self.image[y2_h-1, x2_h:x1_h+1])/255)/(x1_h+1- x2_h)
                if (score>0.2 and score<0.8):
                    y2_h = y2_h - 1

            rec.x1 = x2_h + self.x_min
            rec.y1 = y2_h + self.y_min

            rec.x2 = x1_h + self.x_min
            rec.y2 = y2_h + self.y_min

            rec.x3 = x1_h + self.x_min
            rec.y3 = y1_h + self.y_min

            rec.x4 = x2_h + self.x_min
            rec.y4 = y1_h + self.y_min
            self.image_dt_1[y2_h:y1_h+1 , x2_h:x1_h+1] = 0
                # add those rectangles to the list
                # app = np.argwhere(i[y2_h:y1_h+1 , x2_h:x1_h+1] !=0)
                # rec.list_point = app.tolist()
                # emove identified rectangle
                # ran=random.randint(50, 75)
                # image[y2_h:y1_h+1 , x2_h:x1_h+1]=ran
                # plt.imshow(image, cmap = 'gray', interpolation = 'bicubic')
                # plt.xticks([]), plt.yticks([])  # to hide tick values on X and Y axis
                # plt.show()

                # select vertical rectangle based on area size. Get the rectangle with maximum size
        else:
            if x1_v+1<=self.width:
                score= (np.sum(self.image[y2_v:y1_v+1 , x1_v+1])/255)/(x1_h+1- x2_h)
                if (score>0.2 and score<0.8):
                    x1_v=x1_v+1
            if x2_v-1>=0:
                score= (np.sum(self.image[y2_v-1:y1_v, x2_v-1])/255)/(x1_h+1- x2_h)
                if (score>0.2 and score<0.8):
                    x2_v=x2_v-1

            rec.x1 = x2_v + self.x_min
            rec.y1 = y2_v + self.y_min

            rec.x2 = x1_v + self.x_min
            rec.y2 = y2_v + self.y_min

            rec.x3 = x1_v + self.x_min
            rec.y3 = y1_v + self.y_min

            rec.x4 = x2_v + self.x_min
            rec.y4 = y1_v + self.y_min
            self.image_dt_1[y2_v:y1_v+1 , x2_v:x1_v+1] = 0
                # ran=random.randint(50, 175)
                # image[y2_v:y1_v+1 , x2_v:x1_v+1]=ran
                # plt.imshow(image, cmap = 'gray', interpolation = 'bicubic')
                # plt.xticks([]), plt.yticks([])  # to hide tick values on X and Y axis
                # plt.show()
                # app= np.argwhere( i[y2_v:y1_v+1 , x2_v:x1_v+1] !=0)
                # rec.list_point=app.tolist()
                #remove identified rectangle
                # id=id+1
                # rec.id=id+index_inc
        #rec.set_sides()
        rec.set_skeleton()
        self.list_rect.append(rec)
        self.image_dt_1 = ndimage.morphology.distance_transform_cdt(self.image_dt_1)
        return id

    def extract_rectangle_in_cc_2(self, index_inc):
         #  for cc in self.list_cc:
        id=0
        #perform distance transform
        i=ndimage.morphology.distance_transform_cdt(self.image)
        #get maximum value of the distances (eg: 2)
        max_dt=i.max()
        # print max_dt
        # go throug loop
        while(max_dt>1):

            #put all cordinates of max_dt value to vet_max_point list
            vet_max_point=np.argwhere(i == max_dt)
            l=vet_max_point.tolist()
            #take a random cordinate value from list
            r=random.choice(l)
            l.pop(l.index(r))
            #get x and y cordinates separated from the selected random cordinate
            y= float(r[0])
            x= float(r[1])
            # print i[y,x]
            oblique,m,q = self.find_orientation_rectangle(i,max_dt,x,y)
            #find horizontale rectangle
            if oblique:
                self.list_obl.append([x,y,m,q])

            y1_h=y+max_dt-1
            y2_h=y-max_dt+1
            x1_h=x+max_dt-1
            x2_h=x-max_dt+1

            while([y,x1_h] in l):
                x1_h=x1_h+max_dt-1

            while([y,x2_h] in l):
                x2_h=x2_h-max_dt+1

            x1_h=x1_h+i[y , x1_h]-1
            x2_h=x2_h-i[y , x2_h]+1

            if(i[y1_h , x]==2):
                y1_h+=1
                # while(i[y1_h , x]==1):
                #     y1_h+=1
            if(i[y2_h , x]==2):
                y2_h-=1
                # while(i[y2_h , x]==1):
                #     y2_h-=1

            #find vertical rectangle
            y1_v=y+max_dt-1
            y2_v=y-max_dt+1
            x1_v=x+max_dt-1
            x2_v=x-max_dt+1


            while([y1_v,x] in l):
                y1_v=y1_v+max_dt-1

            while([y2_v,x] in l):
                y2_v=y2_v-max_dt+1

            y1_v=y1_v+i[y1_v , x]-1
            y2_v=y2_v-i[y2_v , x]+1


            if(i[y , x1_v]==2):
                x1_v+=1
                # while(i[y , x1_v]==1):
                #    x1_v+=1
            if(i[y , x2_v]==2):
                x2_v-=1
                # while(i[y , x2_v]==1):
                #    x2_v-=1

            rectangle_h=self.image[y2_h:y1_h+1, x2_h:x1_h+1]
            rectangle_v=self.image[y2_v:y1_v+1 , x2_v:x1_v+1]
            # adesso devo risolvere il problema dei effetto pettine che fi forma in alcune immagini
            #
            #   1111111111111111111111111111111111111111111111111111111
            #       1  1 11  1  1 111 11   111  1 1 1 1    1   1  111 1
            #
            # vado verificare se sui quattro lati( o solo su quelli piu lunghi) del rettango appena trovato ho una percentuale di pixel  di valore 255  compresa tra il 20% e 80% se cos ela includo
            id=id+1
            rec=Rectangle(id+index_inc)

            #select horizontal rectanlge based on area size. Get the rectangle with maximum size
            if(rectangle_h.sum()>=rectangle_v.sum()):
                if y1_h+1<=self.height:
                    score= (np.sum(self.image[y1_h+2, x2_h:x1_h+1])/255)/(x1_h+1- x2_h)
                    if (score>0.2 and score<0.8):
                        y1_h=y1_h+1
                if y2_h-1>=0:
                    score= (np.sum(self.image[y2_h-1, x2_h:x1_h+1])/255)/(x1_h+1- x2_h)
                    if (score>0.2 and score<0.8):
                        y2_h=y2_h-1

                rec.x1=x2_h + self.x_min
                rec.x2=x1_h + self.x_min
                rec.y1=y2_h + self.y_min
                rec.y2=y2_h + self.y_min

                rec.x3=x1_h + self.x_min
                rec.x4=x2_h + self.x_min
                rec.y3=y1_h + self.y_min
                rec.y4=y1_h + self.y_min

                #add those rectangles to the list
                app=np.argwhere(i[y2_h:y1_h+1 , x2_h:x1_h+1] !=0)
                rec.list_point=app.tolist()
                #remove identified rectangle
                i[y2_h:y1_h+1 , x2_h:x1_h+1]=0

            # select vertical rectangle based on area size. Get the rectangle with maximum size
            else:
                if x1_v+1<=self.width:
                    score= (np.sum(self.image[y2_v:y1_v+1 , x1_v+1])/255)/(x1_h+1- x2_h)
                    if (score>0.2 and score<0.8):
                        x1_v=x1_v+1
                if x2_v-1>=0:
                    score= (np.sum(self.image[y2_v-1:y1_v, x2_v-1])/255)/(x1_h+1- x2_h)
                    if (score>0.2 and score<0.8):
                        x2_v=x2_v-1
                rec.x1=x2_v+self.x_min
                rec.x2=x1_v+self.x_min
                rec.y1=y2_v+self.y_min
                rec.y2=y2_v+self.y_min

                rec.x3=x1_v+self.x_min
                rec.x4=x2_v+self.x_min
                rec.y3=y1_v+self.y_min
                rec.y4=y1_v+self.y_min


                app= np.argwhere( i[y2_v:y1_v+1 , x2_v:x1_v+1] !=0)
                rec.list_point=app.tolist()
                #remove identified rectangle
                i[y2_v:y1_v+1 , x2_v:x1_v+1]=0
            # id=id+1
            # rec.id=id+index_inc
            rec.set_sides()
            self.list_rect.append(rec)
            i=ndimage.morphology.distance_transform_cdt(i)
            max_dt=i.max()

            # plt.imshow(i, cmap = 'gray', interpolation = 'bicubic')
            # plt.xticks([]), plt.yticks([])  # to hide tick values on X and Y axis
            # plt.show()


        #when the max_dt is 1 use this method, otherwise use earlier method
        c=np.argwhere(i == 1)
        l=c.tolist()

        while (len(l)!=0):

            rect=[]
            px=l.pop(0)
            rect.append(px)
            x=px[1]
            y=px[0]
            x_max=x
            y_max=y
            x_min=x
            y_min=y
            ver=True
            x1=x
            y1=y+1

            if([y+1,x] in l):
                rect.append(l.pop(l.index([y+1,x])))
                y_max=y+1
            if([y+1,x-1] in l):
                rect.append(l.pop(l.index([y+1,x-1])))
                y_min=y-1
                x_min=x-1

            while([y,x1+1] in l or [y+1,x1+1] in l):
                x1=x1+1
                if([y,x1] in l):
                    rect.append(l.pop(l.index([y,x1])))
                    x_max=x1
                if([y+1,x1] in l):
                    rect.append(l.pop(l.index([y+1,x1])))
                    y_max=y+1

            if((x1-x)>2):
                #x1=x-1
                ver=False


            if(ver):
                rect=[]
                rect.append(px)
                while([y1+1,x] in l or [y1+1,x-1] in l or [y1+1,x+1] in l):
                    y1=y1+1
                    y_max=y1
                    if([y1,x] in l):
                        rect.append(l.pop(l.index([y1,x])))

                    if([y1,x+1] in l):
                        rect.append(l.pop(l.index([y1,x+1])))
                        x_max=x+1

                    if([y1,x-1] in l):
                        rect.append(l.pop(l.index([y1,x-1])))
                        x_min=x-1
            id=id+1

            rec=Rectangle(id+index_inc)


            rec.x1=x_min+self.x_min
            rec.x2=x_max+self.x_min
            rec.y1=y_min+self.y_min
            rec.y2=y_min+self.y_min

            rec.x3=x_max+self.x_min
            rec.x4=x_min+self.x_min
            rec.y3=y_max+self.y_min
            rec.y4=y_max+self.y_min


            # id=id+1
            # rec.id=id+index_inc
            rec.list_point=rect
            self.list_rect.append(rec)
            rec.set_sides()

        return self.list_rect[:]

    def find_orientation_rectangle(self,i,max_dt,x1,y1):
        list_point = [[x1,y1]]
        list_final_point_x= [x1]
        list_final_point_y= [y1]

        stop = True
        if max_dt>10:
            count = 20
        else:
            count = 10

        # plt.imshow(self.image, cmap = 'gray', interpolation = 'bicubic')
        # plt.xticks([]), plt.yticks([])  # to hide tick values on X and Y axis
        # plt.show()
        while(len(list_point)>0 and stop):
            count=count-1
            if count==0:
                stop=False
            p = list_point.pop()
            x = p[0]
            y = p[1]
            h,w=i.shape
            if max_dt<10:
                if ((y>1 and y<(h-1)) and (x>1 and x<(w-1))):
                    if (i[y-1,x-1]==max_dt):
                        list_point.append([x-1,y-1])
                        list_final_point_x.append(x-1)
                        list_final_point_y.append(y-1)
                    if (i[y-1,x]==max_dt):
                        list_point.append([x,y-1])
                        list_final_point_x.append(x)
                        list_final_point_y.append(y-1)
                    if (i[y-1,x+1]==max_dt):
                        list_point.append([x+1,y-1])
                        list_final_point_x.append(x+1)
                        list_final_point_y.append(y-1)
                    if (i[y,x-1]==max_dt):
                        list_point.append([x-1,y])
                        list_final_point_x.append(x-1)
                        list_final_point_y.append(y)
                    if (i[y,x+1]==max_dt):
                        list_point.append([x+1,y])
                        list_final_point_x.append(x+1)
                        list_final_point_y.append(y)
                    if (i[y+1,x-1]==max_dt):
                        list_point.append([x-1,y+1])
                        list_final_point_x.append(x-1)
                        list_final_point_y.append(y+1)
                    if (i[y+1,x]==max_dt):
                        list_point.append([x,y+1])
                        list_final_point_x.append(x)
                        list_final_point_y.append(y+1)
                    if (i[y+1,x+1]==max_dt):
                        list_point.append([x+1,y+1])
                        list_final_point_x.append(x+1)
                        list_final_point_y.append(y+1)
            else:
                if ((y>1 and y<(h-1)) and (x>1 and x<(w-1))):
                    if (i[y-1,x-1]>=max_dt-1):
                        list_point.append([x-1,y-1])
                        list_final_point_x.append(x-1)
                        list_final_point_y.append(y-1)
                    if (i[y-1,x]>=max_dt-1):
                        list_point.append([x,y-1])
                        list_final_point_x.append(x)
                        list_final_point_y.append(y-1)
                    if (i[y-1,x+1]>=max_dt-1):
                        list_point.append([x+1,y-1])
                        list_final_point_x.append(x+1)
                        list_final_point_y.append(y-1)
                    if (i[y,x-1]>=max_dt-1):
                        list_point.append([x-1,y])
                        list_final_point_x.append(x-1)
                        list_final_point_y.append(y)
                    if (i[y,x+1]>=max_dt-1):
                        list_point.append([x+1,y])
                        list_final_point_x.append(x+1)
                        list_final_point_y.append(y)
                    if (i[y+1,x-1]>=max_dt-1):
                        list_point.append([x-1,y+1])
                        list_final_point_x.append(x-1)
                        list_final_point_y.append(y+1)
                    if (i[y+1,x]>=max_dt-1):
                        list_point.append([x,y+1])
                        list_final_point_x.append(x)
                        list_final_point_y.append(y+1)
                    if (i[y+1,x+1]>=max_dt-1):
                        list_point.append([x+1,y+1])
                        list_final_point_x.append(x+1)
                        list_final_point_y.append(y+1)

        if len(list_final_point_x)>1:
            if list_final_point_x.count(max(list_final_point_x)) != len(list_final_point_x):
                m,q,r,p,s = stats.linregress(np.asarray(list_final_point_x),np.asarray(list_final_point_y))
                if m:
                    if np.abs(r)>=0.9:
                        if np.abs(m)<1000 and np.abs(m)>0.1 :
                            return True,m,q
                        else:
                            return False,m,q
                    else:
                        return False,m,q
                else:
                    return False,m,q
            else:
                return False,0,0
        return False,0,0

    def extract_oblique_rect_in_CC(self,image_point,image_rette):
        list_polyline=[]
        point_acc=[]
        list_rette=[]
       # print '#############################'
       # print 'h: '+str(self.height)+'    w: '+str(self.width)
        for x,y,m,q in self.list_obl:
            # q = y - (m*x)
            cv2.circle( image_point, (int(x+self.x_min),int(y+self.y_min)), 2, (0,0,255), 2)
            i = ndimage.morphology.distance_transform_cdt(self.image)
            # print '########################################'
            # print [x+self.x_min,y+self.y_min,m,i[y,x]]
            x1 = int(x)
            y1 = int(y)
            stop = True
            # dal punto x,y seguendo la retta definita da m,q cerco il primo pixel con un valore <=1 per trovare gli estremi dell rettangolo
            while(stop):
                if y1 < self.height and x1 < self.width:
                    val = i[y1,x1]
                    if val<=1:
                        stop = False
                    else:
                        x1 = int(x1 + val)
                        y1 = int(round(m*x1 + q))
                else:
                    stop = False
            x2 = x
            y2 = y
            stop = True
            while(stop):
                if y2 < self.height and x2 < self.width:
                    val = i[y2,x2]
                    if val<=1:
                        stop = False
                    else:
                        x2 = int(x2 - val)
                        y2 = int(round(m*x2 + q))
                else:
                    stop = False
            list_rette.append([x1+self.x_min,y1+self.y_min,x2+self.x_min,y2+self.y_min])

            # vado a trovare le due rette che passsano per i margini del muro (o del lato del rettangolo )
            # prima trovo i punti di confine e poi le rette

            dist= math.sqrt((x1-x2)**2 + (y1-y2)**2)
            if dist>20:
                point_acc.append([x,y])
                # if (np.abs(m)<=1):
                x3 = x
                y3 = y
                x4 = x
                y4 = y
                stop3 = True
                stop4 = True
                while(stop3 or stop4):
                    val3 = i[y3,x3]
                    val4 = i[y4,x4]
                    if (val3 > 1 and stop3):
                        y3 = y3 + (val3-1)
                    else:
                        stop3 = False

                    if (val4 > 1 and stop4):
                        y4 = y4 - (val4-1)
                    else:
                        stop4 = False
                m3 = m
                q3 = y3 - (m3*x3)
                m4 = m
                q4 = y4 - (m4*x4)
                # vado a individuare i punti di intersezione tra le rette r3 e r4 con le rette passanti per i punti x1,y1 e x2,y2 e perpendicolari a r3 e r4
                stop5 = True
                check5 = True
                # print '@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ p1 p2'

                while(stop5):
                    #c5=c5+1
                    m5 = -(1/m)
                    q5 =  y1 - float(m5*x1)
                    # calcolo i punti di intersezione tra la retta 5 e le retta r3 e r4
                    x_1 = int(round((q5 - q3) / (m3 - m5)))
                    y_1 = int(round((m3 * x_1) + q3))
                    x_2 = int(round((q5 - q4) / (m4 - m5)))
                    y_2 = int(round((m4 * x_2) + q4))
                    if m>0:
                        if (x1 >=x and y1>=y ):
                            if (y_1<self.height-1 and y_2<self.height-1 and x_1<self.width-1 and x_2<self.width-1 ):
                                if (i[y_1,x_1]>=1 and i[y_2,x_2]>=1):
                                    stop5= False
                                else:
                                    x1 = x1 - 1
                                    y1 = int(round(m*x1 + q))
                            else:
                                if(self.height-1<=y_1 or self.width-1<=x_2 ):
                                    x1=x1-1
                                    y1 = int(round(m*x1 + q))
                                else:
                                    stop5 = False
                                    check5 = False
                        else:
                            stop5 = False
                            check5 = False
                    else:
                        if (x1 >=x and y1<=y):
                            if(y_1<self.height-1 and y_2<self.height-1 and x_1<self.width-1 and x_2<self.width-1 and y_2>0):
                                if (i[y_1,x_1]>=1 and i[y_2,x_2]>=1):
                                    stop5= False
                                else:
                                    x1 = x1 - 1
                                    y1 = int(round(m*x1 + q))
                            else:
                                if(y_2<=0 or self.width<=x_1):
                                    x1=x1-1
                                    y1 = int(round(m*x1 + q))
                                else:
                                    stop5 = False
                                    check5 = False
                        else:
                            stop5 = False
                            check5 = False
                        # print [x_1,y_1,x_2,y_2]

                stop6 = True
                check6 = True
                #print '@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ p3 p4'

                while(stop6):
                   # c6=c6+1
                    m6 = -(1/m)
                    q6 =  y2 - (m6*x2)
                    # calcolo i punti di intersezione tra la retta 5 e le retta r3 e r4
                    x_3 = int(round((q6 - q3) / (m3 - m6)))
                    y_3 = int(round(m3 * x_3 + q3))
                    x_4 = int(round((q6 - q4) / (m4 - m6)))
                    y_4 = int(round(m4 * x_4 + q4))
                    # print 'x_3   y_3   x_4   y_4 '
                    # print [x_3+self.x_min,y_3+self.y_min,x_4+self.x_min,y_4+self.y_min]
                    if m>0:
                    #if (x_3<self.width and x_4 <self.width and y_3<self.height and y_4<self.height):
                        if (x2<=x and y2<=y):
                            if(y_4>0 and y_3>0 and  y_3<self.height-1 and y_4<self.height-1 and x_3<self.width-1 and x_4<self.width-1 ):
                                if (i[y_3,x_3]>=1 and i[y_4,x_4]>=1 ):
                                    stop6= False
                                else:
                                    #x2 = x2 + val
                                    x2 = x2 + 1
                                    y2 = int(round(m*x2 + q))
                            else:
                                if(x_3<=0 or y_4<=0):
                                    x2 = x2 + 1
                                    y2 = int(round(m*x2 + q))
                                else:
                                    stop6 = False
                                    check6 = False
                        else:
                            stop6 = False
                            check6 = False
                    else:
                        if (x2<=x and y2>=y):
                            if( y_3<self.height-1 and y_4<self.height-1 and x_4<self.width-1 and x_3<self.width-1 ):
                                if (i[y_3,x_3]>=1 and i[y_4,x_4]>=1 ):
                                    stop6= False
                                else:
                                    #x2 = x2 + val
                                    x2 = x2 + 1
                                    y2 = int(round(m*x2 + q))
                            else:
                                if(y_3>=self.height or y_4<=0):
                                    x2 = x2 + 1
                                    y2 = int(round(m*x2 + q))
                                else:
                                    stop6 = False
                                    check6 = False
                        else:
                            stop6 = False
                            check6 = False

                        # print [x_3,y_3,x_4,y_4]
                # print 'P_1: '+str(x_1+self.x_min)+' '+str(y_1+self.y_min)
                # print 'P_2: '+str(x_2+self.x_min)+' '+str(y_2+self.y_min)
                # print 'P_3: '+str(x_3+self.x_min)+' '+str(y_3+self.y_min)
                # print 'P_4: '+str(x_4+self.x_min)+' '+str(y_4+self.y_min)

                # if check5==False and  check6==True:
                #     print 'false&true: m: '+str(m)+' h:'+str(self.height)+' w:'+str(self.width)+' x:'+str(x)+' y:'+str(y)+' x1:'+str(x1)+' y1:'+str(y1)+' ['+str(x_1)+' '+str(y_1)+' '+str(x_2)+' '+str(y_2)+']'+' c5:'+str(c5)
                if check5 and check6:

                    list_polyline.append([x_1,y_1,x_2,y_2,x_3,y_3,x_4,y_4,m])


                # else:
                #     x3 = x
                #     y3 = y
                #     x4 = x
                #     y4 = y
                #     stop3 = True
                #     stop4 = True
                #     while(stop3 or stop4):
                #
                #         val3 = i[y3,x3]
                #         val4 = i[y4,x4]
                #
                #         if (val3 > 1 and stop3):
                #             x3 = x3 + (val3-1)
                #         else:
                #             stop3 = False
                #
                #         if (val4 > 1 and stop4):
                #             x4 = x4 - (val4-1)
                #         else:
                #             stop4 = False
                #
                # #individuo le rette su cui sanno i lati dei rettangoli
                #
                #     m3 = m
                #     q3 = y3 - (m3*x3)
                #     m4 = m
                #     q4 = y4 - (m4*x4)
                #
                #     # self.list_print_obl.append([x1,y1,x2,y2,x3,y3,x4,y4,x5,y5,x6,y6])
                #     # print 'P3  e  P4 '
                #     # print [x3+self.x_min,y3+self.y_min,x4+self.x_min,y4+self.y_min]
                #     val = i[y,x]
                #     # vado a individuare i punti di intersezione tra le rette r3 e r4 con le rette passanti per i punti x1,y1 e x2,y2
                #     stop5 = True
                #     check5 = True
                #    # print '@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ p1 p2'
                #     while(stop5):
                #         m5 = -(1/m)
                #         q5 =  y1 - float(m5*x1)
                #         # calcolo i punti di intersezione tra la retta 5 e le retta r3 e r4
                #         x_1 = int(round((q5 - q3) / (m3 - m5)))
                #         y_1 = int(round((m3 * x_1) + q3))
                #         x_2 = int(round((q5 - q4) / (m4 - m5)))
                #         y_2 = int(round((m4 * x_2) + q4))
                #
                #        # stop5=False
                #         #if (   x_1<self.width and x_2 <self.width and y_1<self.height and y_2<self.height):
                #         if (x1 >=x and y1>=y  and y_1<self.height and y_2<self.height and x_1<self.width and x_2<self.width ):
                #             if (i[y_1,x_1]>=1 and i[y_2,x_2]>=1):
                #                 stop5= False
                #             else:
                #                 #x1 = x1 - val
                #                 x1 = x1 - 1
                #
                #                 y1 = int(round(m*x1 + q))
                #         else:
                #             stop5 = False
                #             check5 = False
                #        # print [x_1,y_1,x_2,y_2]
                #
                #     stop6 = True
                #     check6 = True
                #     #print '@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ p3 p4'
                #     while(stop6):
                #         m6 = -(1/m)
                #         q6 =  y2 - (m6*x2)
                #         # calcolo i punti di intersezione tra la retta 5 e le retta r3 e r4
                #         x_3 = int(round((q6 - q3) / (m3 - m6)))
                #         y_3 = int(round(m3 * x_3 + q3))
                #         x_4 = int(round((q6 - q4) / (m4 - m6)))
                #         y_4 = int(round(m4 * x_4 + q4))
                #         # print 'x_3   y_3   x_4   y_4 '
                #         # print [x_3+self.x_min,y_3+self.y_min,x_4+self.x_min,y_4+self.y_min]
                #
                #         #if (x_3<self.width and x_4 <self.width and y_3<self.height and y_4<self.height):
                #         if (x2<x and y2<=y and y_3<self.height-1 and y_4<self.height-1  ):
                #             if (i[y_3,x_3]>=1 and i[y_4,x_4]>=1 ):
                #                 stop6= False
                #             else:
                #                 #x2 = x2 + val
                #                 x2 = x2 + 1
                #                 y2 = int(round(m*x2 + q))
                #         else:
                #             stop6 = False
                #             check6 = False
                #         if check5 and check6:
                #
                #             list_polyline.append([x_1,y_1,x_2,y_2,x_3,y_3,x_4,y_4,m])
                    # print 'P_1: '+str(x_1+self.x_min)+' '+str(y_1+self.y_min)
                    # print 'P_2: '+str(x_2+self.x_min)+' '+str(y_2+self.y_min)
                    # print 'P_3: '+str(x_3+self.x_min)+' '+str(y_3+self.y_min)
                    # print 'P_4: '+str(x_4+self.x_min)+' '+str(y_4+self.y_min)
                    #list_polyline.append([x_2,y_2,x_1,y_1,x_4,y_4,x_3,y_3,m])

            #return  list_polyline


        for x,y in point_acc:
            cv2.circle( image_point, (int(x+self.x_min),int(y+self.y_min)), 2, (0,255,0), 2)

        for x1,y1,x2,y2 in list_rette:
            cv2.line(image_rette,(int(x1),int(y1)),(int(x2),int(y2)),(255,0,0),3)

        return list_polyline

    def extract_oblique_rect_in_CC2(self,image_point,image_rette):
        list_polyline=[]
        point_acc=[]
        list_rette=[]
        img2 = (np.array(self.image)-255)*(-1)

        for x,y,m,q in self.list_obl:
            cv2.circle( image_point, (int(x+self.x_min),int(y+self.y_min)), 2, (0,0,255), 2)
            i = ndimage.morphology.distance_transform_cdt(self.image)
            # print '########################################'
            # print [x+self.x_min,y+self.y_min,m,i[y,x]]
            x1 = int(x)
            y1 = int(y)
            stop = True

            # 0,0174533 radianti di un grado
            max_count=[0,0,0,0,0,0,0,0,0,0]
            for k in range(-20,21):
                m_rad = math.atan(m)+(k*0.0174533)
                m_item = math.tan(m_rad)
                q = y - m_item * x

            # dal punto x,y seguendo la retta definita da m,q cerco il primo pixel con un valore <=1 per trovare gli estremi dell rettangolo
                while(stop):
                    if y1 < self.height and y1>0 and x1>0 and x1 < self.width:
                        val = i[y1,x1]
                        if val<=1:
                            stop = False
                        else:
                            x1 = int(x1 + val)
                            y1 = int(round(m_item*x1 + q))
                    else:
                        stop = False
                x2 = x
                y2 = y
                stop = True
                while(stop):
                    if y2 < self.height and y2>0 and x2>0 and x2 < self.width:
                        val = i[y2,x2]
                        if val<=1:
                            stop = False
                        else:
                            x2 = int(x2 - val)
                            y2 = int(round(m_item*x2 + q))
                    else:
                        stop = False
                list_rette.append([x1+self.x_min,y1+self.y_min,x2+self.x_min,y2+self.y_min])
             # self.list_print_obl.append([x1,y1,x2,y2])
                dist= math.sqrt((x1-x2)**2 + (y1-y2)**2)
                if dist>30:
                    point_acc.append([x,y])
                    if (np.abs(m_item)<=1):
                        x3 = x
                        y3 = y
                        x4 = x
                        y4 = y
                        stop3 = True
                        stop4 = True
                        while(stop3 or stop4):
                            val3 = i[y3,x3]
                            val4 = i[y4,x4]
                            if (val3 > 1 and stop3):
                                y3 = y3 + (val3-1)
                            else:
                                stop3 = False

                            if (val4 > 1 and stop4):
                                y4 = y4 - (val4-1)
                            else:
                                stop4 = False
                        m1 = (-1)/m_item
                        q1 =  y1 - (m1*x1)
                        m2 = (-1)/m_item
                        q2 =  y2 - (m2*x2)
                        m3 = m_item
                        q3 = y3 - (m3*x3)
                        m4 = m_item
                        q4 = y4 - (m4*x4)
                        x_1 = int(round((q1 - q3) / (m3 - m1)))
                        y_1 = int(round((m3 * x_1) + q3))
                        x_2 = int(round((q1 - q4) / (m4 - m1)))
                        y_2 = int(round((m4 * x_2) + q4))
                        x_3 = int(round((q2 - q3) / (m3 - m2)))
                        y_3 = int(round(m3 * x_3 + q3))
                        x_4 = int(round((q2 - q4) / (m4 - m2)))
                        y_4 = int(round(m4 * x_4 + q4))

                        img = np.zeros((self.height+2,self.width+2))
                        pts = np.array([[x_1,y_1],[x_2,y_2],[x_4,y_4],[x_3,y_3]], np.int32)
                        pts = pts.reshape((-1,1,2))
                        cv2.fillPoly(img,[pts],True,1)
                        logic_and = img*img2
                        c = np.sum(logic_and)/255
                        if(c>max_count[9]):
                            max_count[0]= x_1
                            max_count[1]= y_1
                            max_count[2]= x_2
                            max_count[3]= y_2
                            max_count[4]= x_3
                            max_count[5]= y_3
                            max_count[6]= x_4
                            max_count[7]= y_4
                            max_count[8]= m_item
                            max_count[9]= c
                    else:
                        x3 = x
                        y3 = y
                        x4 = x
                        y4 = y
                        stop3 = True
                        stop4 = True
                        while(stop3 or stop4):
                            val3 = i[y3,x3]
                            val4 = i[y4,x4]
                            if (val3 > 1 and stop3):
                                x3 = x3 + (val3-1)
                            else:
                                stop3 = False

                            if (val4 > 1 and stop4):
                                x4 = x4 - (val4-1)
                            else:
                                stop4 = False
                        m1 = (-1)/m_item
                        q1 =  y1 - (m1*x1)
                        m2 = (-1)/m_item
                        q2 =  y2 - (m2*x2)
                        m3 = m_item
                        q3 = y3 - (m3*x3)
                        m4 = m_item
                        q4 = y4 - (m4*x4)
                        x_1 = int(round((q1 - q3) / (m3 - m1)))
                        y_1 = int(round((m3 * x_1) + q3))
                        x_2 = int(round((q1 - q4) / (m4 - m1)))
                        y_2 = int(round((m4 * x_2) + q4))
                        x_3 = int(round((q2 - q3) / (m3 - m2)))
                        y_3 = int(round(m3 * x_3 + q3))
                        x_4 = int(round((q2 - q4) / (m4 - m2)))
                        y_4 = int(round(m4 * x_4 + q4))

                        img = np.zeros((self.height+2,self.width+2))
                        pts = np.array([[x_1,y_1],[x_2,y_2],[x_4,y_4],[x_3,y_3]], np.int32)
                        pts = pts.reshape((-1,1,2))
                        cv2.fillPoly(img,[pts],True,1)
                        logic_and = img*img2
                        c = np.sum(logic_and)/255
                        if(c>max_count[9]):
                            max_count[0]= x_1
                            max_count[1]= y_1
                            max_count[2]= x_2
                            max_count[3]= y_2
                            max_count[4]= x_3
                            max_count[5]= y_3
                            max_count[6]= x_4
                            max_count[7]= y_4
                            max_count[8]= m_item
                            max_count[9]= c
                        # calcolo i punti di intersezione tra la retta r3 e r4 con le retter r1 e r2

            list_polyline.append(max_count)



        for x,y in point_acc:
            cv2.circle( image_point, (int(x+self.x_min),int(y+self.y_min)), 2, (0,255,0), 2)

        for x1,y1,x2,y2 in list_rette:
            cv2.line(image_rette,(int(x1),int(y1)),(int(x2),int(y2)),(255,0,0),3)

        return list_polyline
    #getting individual rectangles in  one connected component
    def extract_rectangle_in_cc(self,index_inc):
        #  for cc in self.list_cc:
        id=0
        #perform distance transform
        i=ndimage.morphology.distance_transform_cdt(self.image)
        #get maximum value of the distances (eg: 2)
        max_dt=i.max()

        # go throug loop
        while(max_dt>1):

            #put all cordinates of max_dt value to vet_max_point list
            vet_max_point=np.argwhere(i == max_dt)
            l=vet_max_point.tolist()
            #take a random cordinate value from list
            r=random.choice(l)
            l.pop(l.index(r))
            #get x and y cordinates seperated from the selected random cordinate
            y= r[0]
            x= r[1]
            y1_h=y+max_dt-1
            y2_h=y-max_dt+1
            x1_h=x+max_dt-1
            x2_h=x-max_dt+1

            while([y,x1_h] in l):
                x1_h=x1_h+max_dt-1

            while([y,x2_h] in l):
                x2_h=x2_h-max_dt+1

            x1_h=x1_h+i[y , x1_h]-1
            x2_h=x2_h-i[y , x2_h]+1

            if(i[y1_h , x]==2):
                y1_h+=1
                # while(i[y1_h , x]==1):
                #     y1_h+=1
            if(i[y2_h , x]==2):
                y2_h-=1
                # while(i[y2_h , x]==1):
                #     y2_h-=1

            #find vertical rectangle
            y1_v=y+max_dt-1
            y2_v=y-max_dt+1
            x1_v=x+max_dt-1
            x2_v=x-max_dt+1


            while([y1_v,x] in l):
                y1_v=y1_v+max_dt-1

            while([y2_v,x] in l):
                y2_v=y2_v-max_dt+1

            y1_v=y1_v+i[y1_v , x]-1
            y2_v=y2_v-i[y2_v , x]+1


            if(i[y , x1_v]==2):
                x1_v+=1
                # while(i[y , x1_v]==1):
                #    x1_v+=1
            if(i[y , x2_v]==2):
                x2_v-=1
                # while(i[y , x2_v]==1):
                #    x2_v-=1

            rectangle_h=self.image[y2_h:y1_h+1, x2_h:x1_h+1]
            rectangle_v=self.image[y2_v:y1_v+1 , x2_v:x1_v+1]
            # adesso devo risolvere il problema dei effetto pettine che fi forma in alcune immagini
            #
            #   1111111111111111111111111111111111111111111111111111111
            #       1  1 11  1  1 111 11   111  1 1 1 1    1   1  111 1
            #
            # vado verificare se sui quattro lati( o solo su quelli piu lunghi) del rettango appena trovato ho una percentuale di pixel  di valore 255  compresa tra il 20% e 80% se cos ela includo
            id=id+1
            rec=Rectangle(id+index_inc)

            #select horizontal rectanlge based on area size. Get the rectangle with maximum size
            if(rectangle_h.sum()>=rectangle_v.sum()):
                if y1_h+1<=self.height:
                    score= (np.sum(self.image[y1_h+2, x2_h:x1_h+1])/255)/(x1_h+1- x2_h)
                    if (score>0.2 and score<0.8):
                        y1_h=y1_h+1
                if y2_h-1>=0:
                    score= (np.sum(self.image[y2_h-1, x2_h:x1_h+1])/255)/(x1_h+1- x2_h)
                    if (score>0.2 and score<0.8):
                        y2_h=y2_h-1

                rec.x1=x2_h + self.x_min
                rec.x2=x1_h + self.x_min
                rec.y1=y2_h + self.y_min
                rec.y2=y2_h + self.y_min

                rec.x3=x1_h + self.x_min
                rec.x4=x2_h + self.x_min
                rec.y3=y1_h + self.y_min
                rec.y4=y1_h + self.y_min

                #add those rectangles to the list
                app=np.argwhere(i[y2_h:y1_h+1 , x2_h:x1_h+1] !=0)
                rec.list_point=app.tolist()
                #remove identified rectangle
                i[y2_h:y1_h+1 , x2_h:x1_h+1]=0

            # select vertical rectangle based on area size. Get the rectangle with maximum size
            else:
                if x1_v+1<=self.width:
                    score= (np.sum(self.image[y2_v:y1_v+1 , x1_v+1])/255)/(x1_h+1- x2_h)
                    if (score>0.2 and score<0.8):
                        x1_v=x1_v+1
                if x2_v-1>=0:
                    score= (np.sum(self.image[y2_v-1:y1_v, x2_v-1])/255)/(x1_h+1- x2_h)
                    if (score>0.2 and score<0.8):
                        x2_v=x2_v-1




                rec.x1=x2_v+self.x_min
                rec.x2=x1_v+self.x_min
                rec.y1=y2_v+self.y_min
                rec.y2=y2_v+self.y_min

                rec.x3=x1_v+self.x_min
                rec.x4=x2_v+self.x_min
                rec.y3=y1_v+self.y_min
                rec.y4=y1_v+self.y_min


                app= np.argwhere( i[y2_v:y1_v+1 , x2_v:x1_v+1] !=0)
                rec.list_point=app.tolist()
                #remove identified rectangle
                i[y2_v:y1_v+1 , x2_v:x1_v+1]=0
            # id=id+1
            # rec.id=id+index_inc
            rec.set_sides()
            self.list_rect.append(rec)
            i=ndimage.morphology.distance_transform_cdt(i)
            max_dt=i.max()

            # plt.imshow(i, cmap = 'gray', interpolation = 'bicubic')
            # plt.xticks([]), plt.yticks([])  # to hide tick values on X and Y axis
            # plt.show()


        #when the max_dt is 1 use this method, otherwise use earlier method
        c=np.argwhere(i == 1)
        l=c.tolist()

        while (len(l)!=0):

            rect=[]
            px=l.pop(0)
            rect.append(px)
            x=px[1]
            y=px[0]
            x_max=x
            y_max=y
            x_min=x
            y_min=y
            ver=True
            x1=x
            y1=y+1

            if([y+1,x] in l):
                rect.append(l.pop(l.index([y+1,x])))
                y_max=y+1
            if([y+1,x-1] in l):
                rect.append(l.pop(l.index([y+1,x-1])))
                y_min=y-1
                x_min=x-1

            while([y,x1+1] in l or [y+1,x1+1] in l):
                x1=x1+1
                if([y,x1] in l):
                    rect.append(l.pop(l.index([y,x1])))
                    x_max=x1
                if([y+1,x1] in l):
                    rect.append(l.pop(l.index([y+1,x1])))
                    y_max=y+1

            if((x1-x)>2):
                #x1=x-1
                ver=False


            if(ver):
                rect=[]
                rect.append(px)
                while([y1+1,x] in l or [y1+1,x-1] in l or [y1+1,x+1] in l):
                    y1=y1+1
                    y_max=y1
                    if([y1,x] in l):
                        rect.append(l.pop(l.index([y1,x])))

                    if([y1,x+1] in l):
                        rect.append(l.pop(l.index([y1,x+1])))
                        x_max=x+1

                    if([y1,x-1] in l):
                        rect.append(l.pop(l.index([y1,x-1])))
                        x_min=x-1
            id=id+1

            rec=Rectangle(id+index_inc)


            rec.x1=x_min+self.x_min
            rec.x2=x_max+self.x_min
            rec.y1=y_min+self.y_min
            rec.y2=y_min+self.y_min

            rec.x3=x_max+self.x_min
            rec.x4=x_min+self.x_min
            rec.y3=y_max+self.y_min
            rec.y4=y_max+self.y_min


            # id=id+1
            # rec.id=id+index_inc
            rec.list_point=rect
            self.list_rect.append(rec)
            rec.set_sides()

        return self.list_rect[:]

    def find_adjacent_rectangles(self):
        list_of_serch=self.list_rect[:]# create new list


        while(len(list_of_serch)>0):

            rect_1=list_of_serch.pop()
            merge=False
            # and rect_1.get_area()>10):  levo il controlo sull'area
            # for rect_2 in list_of_serch:
            for rect_2 in self.list_rect:
                # adjacent l1,l3
                # if rect_2.get_area>10:
                # adjacent l1,l3
                if rect_1 is not rect_2:
                # qui potrei mettere il primo membro |y2-y1|<3 per includere anche quelle vicini ma non attaccati anche se probabilmente fanno parte di un altra cc
                    if ((rect_2.list_sides[2].y2==rect_1.list_sides[0].y1-1) and (rect_2.list_sides[2].x2<=rect_1.list_sides[0].x2) and (rect_2.list_sides[2].x1>=rect_1.list_sides[0].x1)) :

                        percent_adjacency=min(float(rect_1.x2-rect_1.x1),float(rect_2.x2-rect_2.x1))/(max(float(rect_1.x2-rect_1.x1),float(rect_2.x2-rect_2.x1))+1)
                        thinckness_r1=min( (rect_1.x2-rect_1.x1) , (rect_1.y4-rect_1.y1) )+1
                        thinckness_r2=min( (rect_2.x3-rect_2.x4) , (rect_2.y4-rect_2.y1) )+1
                        ratio_of_thicknesses= float(min(thinckness_r1,thinckness_r2))/float(max(thinckness_r1,thinckness_r2))

                        if(percent_adjacency<0.7):# questo mi basta per capire se i due rettangoli sono allineati
                            # puo bastare fare solo il secondo if?
                            #     if( (min( (rect_1.x2-rect_1.x1) , (rect_1.y4-rect_1.y1) ) == (rect_1.x2-rect_1.x1)) and ( min( (rect_2.x3-rect_2.x4) , (rect_2.y4-rect_2.y1) ) == (rect_2.x3-rect_2.x4)) ):
                            #
                            #         rect_1.list_adjacent_rect.append(['TANGENT',rect_2.id, 1, 3])
                            #         rect_2.list_adjacent_rect.append(['TANGENT',rect_1.id, 3, 1])

                            if (ratio_of_thicknesses<0.5):
                                if ['TANGENT',rect_2, 1, 3] not in rect_1.list_adjacent_rect:
                                    rect_1.list_adjacent_rect.append(['TANGENT',rect_2, 1, 3])
                                    rect_2.list_adjacent_rect.append(['TANGENT',rect_1, 3, 1])

                            else:
                                if ['ADIACENT',rect_2, 1, 3] not in rect_1.list_adjacent_rect:
                                    rect_1.list_adjacent_rect.append(['ADIACENT',rect_2, 1, 3])
                                    rect_2.list_adjacent_rect.append(['ADIACENT',rect_1, 3, 1])

                        else:

                            if np.abs((rect_1.x2-rect_2.x2))<=1 and  np.abs(rect_1.x1-rect_2.x1)<=1:
                                r=Rectangle()
                                r.x2=max(rect_1.x2,rect_2.x2)
                                r.x1=min(rect_1.x1,rect_2.x1)
                                r.y1=rect_2.y1
                                r.y2=rect_2.y2
                                r.x3=max(rect_1.x2,rect_2.x2)
                                r.x4=min(rect_1.x1,rect_2.x1)
                                r.y3=rect_1.y3
                                r.y4=rect_1.y4
                                r.set_sides()
                                if rect_2 in list_of_serch:
                                    list_of_serch.pop(list_of_serch.index(rect_2))
                                self.list_rect.pop(self.list_rect.index(rect_2))
                                self.list_rect.pop(self.list_rect.index(rect_1))
                                r.id=rect_1.id

                                if len(rect_2.list_adjacent_rect)==0: #and len(rect_1.list_adjacent_rect)>0:

                                    if len(rect_1.list_adjacent_rect)>0:
                                        for a in rect_1.list_adjacent_rect:
                                            a[1].list_adjacent_rect.pop(a[1].list_adjacent_rect.index([a[0],rect_1,a[3],a[2]]))# elimino adiacenza nel rettangolo che prima era adiacente


                                elif len(rect_1.list_adjacent_rect)==0: #and len(rect_2.list_adjacent_rect)>0:

                                    if len(rect_2.list_adjacent_rect)>0:
                                        for a in rect_2.list_adjacent_rect:
                                            a[1].list_adjacent_rect.pop(a[1].list_adjacent_rect.index([a[0],rect_2,a[3],a[2]]))

                                elif len(rect_1.list_adjacent_rect)>0: # and len(rect_2.list_adjacent_rect)>0:

                                    for a in rect_1.list_adjacent_rect:
                                        a[1].list_adjacent_rect.pop(a[1].list_adjacent_rect.index([a[0],rect_1,a[3],a[2]]))

                                    for a in rect_2.list_adjacent_rect:
                                        a[1].list_adjacent_rect.pop(a[1].list_adjacent_rect.index([a[0],rect_2,a[3],a[2]]))


                                self.list_rect.append(r)
                                list_of_serch.append(r)
                                #print 'bingo'+' '+str(rect_1.id)+' '+str(rect_2.id)
                                merge=True
                            else:
                                if ['ALIGNED',rect_2, 1, 3] not in rect_1.list_adjacent_rect:
                                    rect_1.list_adjacent_rect.append(['ALIGNED',rect_2, 1, 3])
                                    rect_2.list_adjacent_rect.append(['ALIGNED',rect_1, 3, 1])
                                    # It is a list composed: [type adjacency, id rectangle ad. , side of rectangle, % side]

                                    #list_new.append(rect_1)

                    # if merge:
                    #     break

                    # adjacent l3,l1
                    if((rect_2.list_sides[0].y2==rect_1.list_sides[2].y1+1) and (rect_2.list_sides[0].x1<=rect_1.list_sides[2].x1) and (rect_2.list_sides[0].x2>=rect_1.list_sides[2].x2) ):

                        percent_adjacency=min(float(rect_1.x2-rect_1.x1),float(rect_2.x2-rect_2.x1))/(max(float(rect_1.x2-rect_1.x1),float(rect_2.x2-rect_2.x1))+1)
                        thinckness_r1=min( (rect_1.x2-rect_1.x1) , (rect_1.y4-rect_1.y1) )+1
                        thinckness_r2=min( (rect_2.x3-rect_2.x4) , (rect_2.y4-rect_2.y1) )+1
                        ratio_of_thicknesses= float(min(thinckness_r1,thinckness_r2))/float(max(thinckness_r1,thinckness_r2))


                        if(percent_adjacency<0.7):
                            # if(rect_1.x2-rect_1.x1>rect_2.x2-rect_1.x2):
                            #  if( (min( (rect_2.x2-rect_2.x1) , (rect_2.y4-rect_2.y1) ) == (rect_2.x2-rect_2.x1)) and ( min( (rect_1.x3-rect_1.x4) , (rect_1.y4-rect_1.y1) ) == (rect_1.x3-rect_1.x4)) ):
                            #      rect_1.list_adjacent_rect.append(['TANGENT',rect_2.id, 3, 1])
                            #      rect_2.list_adjacent_rect.append(['TANGENT',rect_1.id, 1, 3])

                            if (ratio_of_thicknesses<0.5):
                                if ['TANGENT',rect_2, 3, 1] not in rect_1.list_adjacent_rect:
                                    rect_1.list_adjacent_rect.append(['TANGENT',rect_2, 3, 1])
                                    rect_2.list_adjacent_rect.append(['TANGENT',rect_1, 1, 3])

                            else:
                                if ['ADIACENT',rect_2, 3, 1] not in rect_1.list_adjacent_rect:
                                    rect_1.list_adjacent_rect.append(['ADIACENT',rect_2, 3, 1])
                                    rect_2.list_adjacent_rect.append(['ADIACENT',rect_1, 1, 3])

                        else:
                            if np.abs((rect_1.x2-rect_2.x2))<=1 and  np.abs(rect_1.x1-rect_2.x1)<=1:
                                r=Rectangle()
                                r.x2=max(rect_1.x2,rect_2.x2)
                                r.x1=min(rect_1.x1,rect_2.x1)
                                r.y1=rect_1.y1
                                r.y2=rect_1.y2
                                r.x3=max(rect_1.x2,rect_2.x2)
                                r.x4=min(rect_1.x1,rect_2.x1)
                                r.y3=rect_2.y3
                                r.y4=rect_2.y4

                                r.set_sides()
                                if rect_2 in list_of_serch:
                                    list_of_serch.pop(list_of_serch.index(rect_2))
                                self.list_rect.pop(self.list_rect.index(rect_2))
                                self.list_rect.pop(self.list_rect.index(rect_1))
                                r.id=rect_1.id

                                if len(rect_2.list_adjacent_rect)==0:

                                    if len(rect_1.list_adjacent_rect)>0:
                                        for a in rect_1.list_adjacent_rect:
                                            a[1].list_adjacent_rect.pop(a[1].list_adjacent_rect.index([a[0],rect_1,a[3],a[2]]))# elimino adiacenza nel rettangolo che prima era adiacente


                                elif len(rect_1.list_adjacent_rect)==0:

                                    if len(rect_2.list_adjacent_rect)>0:
                                        for a in rect_2.list_adjacent_rect:
                                            a[1].list_adjacent_rect.pop(a[1].list_adjacent_rect.index([a[0],rect_2,a[3],a[2]]))

                                else:

                                    for a in rect_1.list_adjacent_rect:
                                        a[1].list_adjacent_rect.pop(a[1].list_adjacent_rect.index([a[0],rect_1,a[3],a[2]]))

                                    for a in rect_2.list_adjacent_rect:
                                        a[1].list_adjacent_rect.pop(a[1].list_adjacent_rect.index([a[0],rect_2,a[3],a[2]]))

                                self.list_rect.append(r)
                                list_of_serch.append(r)
                                #print 'bingo'+' '+str(rect_1.id)+' '+str(rect_2.id)
                                merge=True
                            else:
                                if ['ALIGNED',rect_2, 3, 1] not in rect_1.list_adjacent_rect:
                                    rect_1.list_adjacent_rect.append(['ALIGNED',rect_2, 3, 1])
                                    rect_2.list_adjacent_rect.append(['ALIGNED',rect_1, 1, 3])
                                    # It is a list composed: [type adjacency, id rectangle ad. , side of rectangle, % side]

                                    # It is a list composed: [type adjacency, id rectangle ad. , side of rectangle, % side]

                                    #list_new.append(rect_1)
                    # if merge:
                    #     break

                    # adjacent l2,l4
                    if((rect_2.list_sides[3].x1==rect_1.list_sides[1].x2+1) and (rect_2.list_sides[3].y1>=rect_1.list_sides[1].y1) and (rect_2.list_sides[3].y2<=rect_1.list_sides[1].y2) ):

                        percent_adjacency=min(float(rect_1.y3-rect_1.y2),float(rect_2.y3-rect_2.y2))/(max(float(rect_1.y3-rect_1.y2),float(rect_2.y3-rect_2.y2))+1)
                        thinckness_r1=min( (rect_1.x2-rect_1.x1) , (rect_1.y4-rect_1.y1) )+1
                        thinckness_r2=min( (rect_2.x3-rect_2.x4) , (rect_2.y4-rect_2.y1) )+1
                        ratio_of_thicknesses= float(min(thinckness_r1,thinckness_r2))/float(max(thinckness_r1,thinckness_r2))

                        if(percent_adjacency<0.7):

                            # if( (min( (rect_1.x2-rect_1.x1) , (rect_1.y4-rect_1.y1) ) == (rect_1.y4-rect_1.y1)) and ( min( (rect_2.x3-rect_2.x4) , (rect_2.y4-rect_2.y1) ) == (rect_2.y4-rect_2.y1)) ):
                            #     rect_1.list_adjacent_rect.append(['TANGENT',rect_2.id, 2, 4])
                            #     rect_2.list_adjacent_rect.append(['TANGENT',rect_1.id, 4, 2])

                            if (ratio_of_thicknesses<0.5):
                                if ['TANGENT',rect_2, 2, 4] not in rect_1.list_adjacent_rect:
                                    rect_1.list_adjacent_rect.append(['TANGENT',rect_2, 2, 4])
                                    rect_2.list_adjacent_rect.append(['TANGENT',rect_1, 4, 2])

                            else:
                                if ['ADIACENT', rect_2, 2, 4] not in rect_1.list_adjacent_rect:
                                    rect_1.list_adjacent_rect.append(['ADIACENT', rect_2, 2, 4])
                                    rect_2.list_adjacent_rect.append(['ADIACENT', rect_1, 4, 2])
                        else:
                            if np.abs((rect_1.y1-rect_2.y1))<=1 and  np.abs(rect_1.y4-rect_2.y4)<=1:
                                r=Rectangle()
                                r.y4=max(rect_1.y4,rect_2.y4)
                                r.y1=min(rect_1.y1,rect_2.y1)
                                r.x1=rect_1.x1
                                r.x4=rect_1.x4
                                r.y3=max(rect_1.y4,rect_2.y4)
                                r.y2=min(rect_1.y1,rect_2.y1)
                                r.x3=rect_2.x3
                                r.x2=rect_2.x2
                                r.set_sides()
                                if rect_2 in list_of_serch:
                                    list_of_serch.pop(list_of_serch.index(rect_2))
                                self.list_rect.pop(self.list_rect.index(rect_2))
                                self.list_rect.pop(self.list_rect.index(rect_1))
                                r.id=rect_1.id

                                if len(rect_2.list_adjacent_rect)==0:

                                    if len(rect_1.list_adjacent_rect)>0:
                                        for a in rect_1.list_adjacent_rect:
                                            a[1].list_adjacent_rect.pop(a[1].list_adjacent_rect.index([a[0],rect_1,a[3],a[2]]))# elimino adiacenza nel rettangolo che prima era adiacente


                                elif len(rect_1.list_adjacent_rect)==0:

                                    if len(rect_2.list_adjacent_rect)>0:
                                        for a in rect_2.list_adjacent_rect:
                                            a[1].list_adjacent_rect.pop(a[1].list_adjacent_rect.index([a[0],rect_2,a[3],a[2]]))

                                else:

                                    for a in rect_1.list_adjacent_rect:
                                        a[1].list_adjacent_rect.pop(a[1].list_adjacent_rect.index([a[0],rect_1,a[3],a[2]]))

                                    for a in rect_2.list_adjacent_rect:
                                        a[1].list_adjacent_rect.pop(a[1].list_adjacent_rect.index([a[0],rect_2,a[3],a[2]]))

                                self.list_rect.append(r)
                                list_of_serch.append(r)
                                merge=True
                                # print 'bingo'+' '+str(rect_1.id)+' '+str(rect_2.id)

                            else:
                                if ['ALIGNED',rect_2, 2, 4] not in rect_1.list_adjacent_rect:
                                    rect_1.list_adjacent_rect.append(['ALIGNED',rect_2, 2, 4])
                                    rect_2.list_adjacent_rect.append(['ALIGNED',rect_1, 4, 2])
                                    # It is a list composed: [type adjacency, id rectangle ad. , side of rectangle, % side]
                                    #list_new.append(rect_1)
                    # if merge:
                    #     break
                    # adjacent l4,l2
                    if((rect_2.list_sides[1].x1==rect_1.list_sides[3].x2-1) and (rect_2.list_sides[1].y1<=rect_1.list_sides[3].y1) and (rect_2.list_sides[1].y2>=rect_1.list_sides[3].y2) ):

                        percent_adjacency=min(float(rect_1.y3-rect_1.y2),float(rect_2.y3-rect_2.y2))/(max(float(rect_1.y3-rect_1.y2),float(rect_2.y3-rect_2.y3))+1)
                        thinckness_r1=min( (rect_1.x2-rect_1.x1) , (rect_1.y4-rect_1.y1) )+1
                        thinckness_r2=min( (rect_2.x3-rect_2.x4) , (rect_2.y4-rect_2.y1) )+1
                        ratio_of_thicknesses= float(min(thinckness_r1,thinckness_r2))/float(max(thinckness_r1,thinckness_r2))

                        if(percent_adjacency<0.7):
                            # if( (min( (rect_2.x2-rect_2.x1) , (rect_2.y4-rect_2.y1) ) == (rect_2.y4-rect_2.y1)) and ( min( (rect_1.x3-rect_1.x4) , (rect_1.y4-rect_1.y1) ) == (rect_1.y4-rect_1.y1)) ):
                            #     rect_1.list_adjacent_rect.append(['TANGENT',rect_2.id, 4, 2])
                            #     rect_2.list_adjacent_rect.append(['TANGENT',rect_1.id, 2, 4])

                            if (ratio_of_thicknesses<0.5):
                                if ['TANGENT',rect_2, 4, 2] not in rect_1.list_adjacent_rect:
                                    rect_1.list_adjacent_rect.append(['TANGENT',rect_2, 4, 2])
                                    rect_2.list_adjacent_rect.append(['TANGENT',rect_1, 2, 4])

                            else:
                                if ['ADIACENT', rect_2, 4, 2] not in rect_1.list_adjacent_rect:
                                    rect_1.list_adjacent_rect.append(['ADIACENT', rect_2, 4, 2])
                                    rect_2.list_adjacent_rect.append(['ADIACENT', rect_1, 2, 4])


                        else:
                            if np.abs((rect_1.y1-rect_2.y1))<=1 and  np.abs(rect_1.y4-rect_2.y4)<=1:
                                r=Rectangle()
                                r.y4=max(rect_1.y4,rect_2.y4)
                                r.y1=min(rect_1.y1,rect_2.y1)
                                r.x1=rect_2.x1
                                r.x4=rect_2.x4
                                r.y3=max(rect_1.y4,rect_2.y4)
                                r.y2=min(rect_1.y1,rect_2.y1)
                                r.x3=rect_1.x3
                                r.x2=rect_1.x2
                                r.set_sides()
                                if rect_2 in list_of_serch:
                                    list_of_serch.pop(list_of_serch.index(rect_2))
                                self.list_rect.pop(self.list_rect.index(rect_2))
                                self.list_rect.pop(self.list_rect.index(rect_1))
                                r.id=rect_1.id

                                if len(rect_2.list_adjacent_rect)==0:

                                    if len(rect_1.list_adjacent_rect)>0:
                                        for a in rect_1.list_adjacent_rect:
                                            a[1].list_adjacent_rect.pop(a[1].list_adjacent_rect.index([a[0],rect_1,a[3],a[2]]))# elimino adiacenza nel rettangolo che prima era adiacente


                                elif len(rect_1.list_adjacent_rect)==0:

                                    if len(rect_2.list_adjacent_rect)>0:
                                        for a in rect_2.list_adjacent_rect:
                                            a[1].list_adjacent_rect.pop(a[1].list_adjacent_rect.index([a[0],rect_2,a[3],a[2]]))

                                else:

                                    for a in rect_1.list_adjacent_rect:
                                        a[1].list_adjacent_rect.pop(a[1].list_adjacent_rect.index([a[0],rect_1,a[3],a[2]]))

                                    for a in rect_2.list_adjacent_rect:
                                        a[1].list_adjacent_rect.pop(a[1].list_adjacent_rect.index([a[0],rect_2,a[3],a[2]]))

                                self.list_rect.append(r)
                                list_of_serch.append(r)
                                #  print 'bingo'+' '+str(rect_1.id)+' '+str(rect_2.id)
                                merge=True
                            else:
                                if ['ALIGNED',rect_2, 4, 2] not in rect_1.list_adjacent_rect:
                                    rect_1.list_adjacent_rect.append(['ALIGNED',rect_2, 4, 2])
                                    rect_2.list_adjacent_rect.append(['ALIGNED',rect_1, 2, 4])
                                    # It is a list composed: [type adjacency, id rectangle ad. , side of rectangle, % side]
                                    #list_new.append(rect_1)
                                    # else:
                                    #     rect_2.num_set_of_rectangle=0

                    # if merge:
                    #     break            # if len(list_of_serch)==0 and len(rect_1.list_adjacent_rect)>0:
                #     self.list_new.append(rect_1)
                if merge:
                        break

    def intersection_skeletons_adiacent(self,r,r2):
         # per fondere dei rettangoli adiacenti e meglio ambiare le coordinate dello scheletro
         x=0
         y=0

         if r.skeleton[0]!=r.skeleton[2]:
            m1=float(r.skeleton[1]-r.skeleton[3]) / float(r.skeleton[0]-r.skeleton[2])
            q1=m1*(-r.skeleton[0])+r.skeleton[1]

         else:
             m1=np.inf
             q1=r.skeleton[0]

         if r2.skeleton[0]!=r2.skeleton[2]:
            m2=float(r2.skeleton[1]-r2.skeleton[3]) / float(r2.skeleton[0]-r2.skeleton[2])
            q2=m2*(-r2.skeleton[0])+r2.skeleton[1]
         else:
             m2=np.inf
             q2=r2.skeleton[0]


         if m1==0:
            if m2==np.inf :
                x=q2
                y=q1
                self.merge_line_skeleton(r,r2,int(x),int(y))
            elif m2!=0:
                y=q1
                x=(y-q2)/m2
                self.merge_line_skeleton(r,r2,int(x),int(y))

         elif m1==np.inf :
             if m2==0:
                x=q1
                y=q2
                self.merge_line_skeleton(r,r2,int(x),int(y))
             elif m2!=np.inf:
                x=q1
                y=(x*m2)+q2
                self.merge_line_skeleton(r,r2,int(x),int(y))

         else:
             if m2==0:
                y=q2
                x=(y-q1)/m1
                self.merge_line_skeleton(r,r2,int(x),int(y))
             elif m2==np.inf:
                x=q2
                y=(x*m1)+q1
                self.merge_line_skeleton(r,r2,int(x),int(y))
             elif m1!=m2:
                x=(q2-q1)/(m1-m2)
                y=(x*m1)+q1
                self.merge_line_skeleton(r,r2,int(x),int(y))
         return int(x), int(y)
         # adesso devo andare a verificare se m1=0 m2=inf e viceversa, in questo modo il punto di intersezione sono q2,q1 e viceversa
         # se uno dei due m ha valore 0 allora conosco gia la coordinata y
         # se uno dei due m ha valore inf allora conoscco gia la  coordinata x
         # e altra la posso trovare con un semplice calcolo

         # se entrambe hanno m con un valore allora utilizzo la formula x=q2-q1/m1-m2

    def intersection_skeletons_aligned(self,r,r2,s1,s2):
       # nel caso in cui ho dei rettangoli allineati invece vado a modificare immagine delle scheletro

        if s1==1:
            r.skeleton[1]=r2.skeleton[3]

            y=r.skeleton[1]
            x=int((max(r.skeleton[0],r2.skeleton[0])-min(r.skeleton[0],r2.skeleton[0]))/2)+min(r.skeleton[0],r2.skeleton[0])
        elif s1==3:
            r.skeleton[3]=r2.skeleton[1]

            y=r.skeleton[3]
            x=int((max(r.skeleton[0],r2.skeleton[0])-min(r.skeleton[0],r2.skeleton[0]))/2)+min(r.skeleton[0],r2.skeleton[0])
        elif s1==2:
            r.skeleton[2]=r2.skeleton[0]

            x=r.skeleton[2]
            y=int((max(r.skeleton[1],r2.skeleton[1])-min(r.skeleton[1],r2.skeleton[1]))/2)+min(r.skeleton[1],r2.skeleton[1])
        else:
            r.skeleton[0]=r2.skeleton[2]

            x=r.skeleton[0]
            y=int((max(r.skeleton[1],r2.skeleton[1])-min(r.skeleton[1],r2.skeleton[1]))/2)+min(r.skeleton[1],r2.skeleton[1])
        return int(x),int(y)

    def print_adiacenze(self):
        for r in self.list_rect:
            print r.id
            for a in r.list_adjacent_rect:
                print str(a[0])+' '+str(a[1].id)+' '+str(a[2])+' '+str(a[3])




    ###Pierpaolo's
    def find_contour(self):
        image = np.array(self.image,dtype=np.uint8)
        self.contourNone, hierarchy = cv2.findContours(image, cv2.RETR_TREE,cv2.CHAIN_APPROX_NONE)
        self.contourSimple, hierarchy = cv2.findContours(image, cv2.RETR_TREE,cv2.CHAIN_APPROX_SIMPLE)
        self.remove_contours()
        # self.contourNone.append(contourNone[0])
        # contourNone.remove(contourNone[0])
        # self.contourSimple.append(contourSimple[0])
        # contourSimple.remove(contourSimple[0])
        # for cnt in contourNone:
        #     area = cv2.contourArea(cnt)
        #     if area > 30:
        #         self.contourNone.append(cnt)
        # for cnt in contourSimple:
        #     area = cv2.contourArea(cnt)
        #     if area > 30:
        #         self.contourSimple.append(cnt)
        for i in range(0, len(self.contourSimple)):
            M = cv2.moments(self.contourNone[i])
            if M['m00']==0:
                M['m00'] = 0.01
            self.cx.append(int(M['m10']/M['m00']))
            self.cy.append(int(M['m01']/M['m00']))

        self.area = cv2.contourArea(self.contourSimple[0])
        return self.contourNone, self.contourSimple

    def remove_contours(self):

        contourNone = []
        contourSimple = []
        contourNone.append(self.contourNone[0])
        self.contourNone.remove(self.contourNone[0])
        contourSimple.append(self.contourSimple[0])
        self.contourSimple.remove(self.contourSimple[0])
        for cnt in self.contourNone:
            area = cv2.contourArea(cnt)
            if area > 30:
                contourNone.append(cnt)
        for cnt in self.contourSimple:
            area = cv2.contourArea(cnt)
            if area > 30:
                contourSimple.append(cnt)
        self.contourNone = contourNone
        self.contourSimple = contourSimple

    def get_extremes_from_center(self):
        xmin = 999999
        xmax = 0
        ymin = 999999
        ymax = 0
        for c in self.contourNone[0]:
            if c[0][0] == self.cx[0]:
                if c[0][1] < ymin:
                    ymin = c[0][1]
                if c[0][1] > ymax:
                    ymax = c[0][1]
            if c[0][1] == self.cy[0]:
                if c[0][0] < xmin:
                    xmin = c[0][0]
                if c[0][0] > xmax:
                    xmax = c[0][0]
        return xmin+self.x_min, xmax+self.x_min, ymin+self.y_min, ymax+self.y_min

    def is_linked(self, id):
        for u in self._indices:
            if u.id == id:
                return True
            else:
                return False

    def add_link(self, cc):
        if cc not in self._indices:
            self._list_linked.append(cc)
            self._indices[cc] = len(self._list_linked) - 1
        return self._list_linked

    def get_linked(self):
        return self._indices

    def get_linked_node(self,u):
        if u in self._indices:
            return self._list_linked[self._indices[u]]
        else:
            print 'linked node self! ' +str(self)
            return self

    def add_notlink(self, id):
        #da controllare se funziona all'inizializzazione, ma dovrebbe!
        if id not in self._notindices:
            self._list_notlinked.append(id)
            self._notindices[id] = len(self._list_notlinked) - 1
        return self._list_notlinked

    def islinked(self,id):
        if id in self._indices:
            return True
        else:
            return False

    def isnotlinked(self,id):
        if id in self._notindices:
            return True
        else:
            return False

    def calculateListAngles(self):
        angleList = []
        if (len(self._list_linked)-1) == 0:
            #print 'nessuna componente adiacente'
            return angleList
        elif (len(self._list_linked)-1) == 1:
            angleList.append(1)
            #print 'una sola componente adiacente'
        elif (len(self._list_linked)-1) == 2:
            #print 'due sole componenti adiacenti'
            go = True
            for cc1 in self._indices:
                if cc1.id != self.id and go == True:
                    for cc2 in self._indices:
                        if cc2.id != self.id and cc2.id != cc1.id:
                            angle = mm.carnot(cc1.center_x, cc1.center_y, cc2.center_x, cc2.center_y, self.center_x, self.center_y, rad = False)
                            angleList.append(angle/360)
                            angleList.append(1 - angle)
                            go = False
        else:
            #print 'abbiamo '+str(len(self._list_linked)-1)+' componenti adiacenti'
            order, angleList = self.order_linked()
            for i in range(0, len(order)):
                #print 'order = ' +str(order)
                cc1 = self.get_linked_node_from_id(order[i])
                cc2 = self.get_linked_node_from_id(order[(i+1)%len(order)])

                crossProd = mm.crossProduct(cc1.center_y, cc2.center_y, self.center_y, cc1.center_x, cc2.center_x, self.center_x)
                if crossProd == 0:
                    #print 'linee parallele!'
                    angle = 0.5
                elif crossProd > 0:
                    angle = mm.carnot(cc1.center_x, cc1.center_y, cc2.center_x, cc2.center_y, self.center_x, self.center_y, rad = False)
                else:
                    angle = 360 - mm.carnot(cc1.center_x, cc1.center_y, cc2.center_x, cc2.center_y, self.center_x, self.center_y, rad = False)
                angleList.append(angle/360)
                #check number of angles!
        if len(angleList) != len(self._list_linked)-1:

            print 'Molto male: avemu '+str(len(angleList)) +' angoli e ' +str(len(self._list_linked)-1) +' cc adiacenti!'
            print 'angleList = '+str(angleList)
        return angleList

        #     for cc1 in self._indices:
        #         if cc1.id != self.id:
        #             min1 = 400
        #             min2 = 400
        #             for cc2 in self._indices:
        #                 if cc2.id != self.id and cc2.id != cc1.id:
        #                     corner = mm.carnot(cc1.center_x, cc1.center_y, cc2.center_x, cc2.center_y, self.center_x, self.center_y, rad = False)
        #                     if corner < min1 and min1 ==400:
        #                         id1 = cc2.id
        #                         min1 = corner
        #                     elif corner < min1 and min1 < 400:
        #                         id2 = id1
        #                         id1 = cc2.id
        #                         min2 = min1
        #                         min1 = corner
        #                     elif corner < min2:
        #                         id2 = cc2.id
        #                         min2 = corner
        #             angleList.add_angle(Angle(cc1.id, id1, min1))
        #             angleList.add_angle(Angle(cc1.id, id2, min2))
        #     angles = angleList.get_order_angles()
        #     #check number of angles!
        #     if len(angles) == len(self._list_linked):
        #         print 'OK: abbiamo tanti angoli tante cc adiacenti'
        #     else:
        #         print 'Molto male: avemu '+str(len(angles)) +' angoli e ' +str(len(self._list_linked)) +' cc adiacenti!'
        # return angles

    def order_linked(self):
        angleList = []
        tmpRight = []
        tmpLeft = []
        for cc in self._indices:
            if cc.id != self.id:
                if cc.center_x == self.center_x and cc.center_y  == self.center_y:
                    #print 'trovata cc concentrica '
                    angleList.append(Angle(cc.id, self.id, 0))
                else:
                    diff = mm.differenceQuozient(cc.center_x, self.center_x, cc.center_y, self.center_y)
                    if cc.center_y > self.center_y or (cc.center_y == self.center_y and cc.center_x > self.center_x):
                        tmpRight.append([diff, cc.id])
                    else:
                        tmpLeft.append([diff, cc.id])
        order = []
        if len(tmpRight) > 0:
            tmpRight = np.array(tmpRight)
            tmpRight = tmpRight[np.argsort(tmpRight[:,0])]
            for i in range(0,len(tmpRight)):
                order.append(int(tmpRight[i][1]))
        if len(tmpLeft) > 0:
            tmpLeft = np.array(tmpLeft)
            tmpLeft = tmpLeft[np.argsort(tmpLeft[:,0])]
            for i in range(0,len(tmpLeft)):
                order.append(int(tmpLeft[i][1]))
        #print 'ci sono '+str(len(self._list_linked)-1) +' cc adiacenti con i seguenti id: '+str(order)
        return order, angleList

    def check_shape(self):
        self.shape = ''
        self.find_rectangle()
        if len(self.shape) == 0:
            self.find_circle()
        if len(self.shape) == 0:
            self.find_ellipse()
        if len(self.shape) == 0:
            self.shape = 'unknown'
        # print 'cc '+str(self.id)+' is a '+str(self.shape)
        return self.shape

    def find_circle(self):
        if len(self.contourNone) == 0:
            self.find_contour()
        cx = self.cx[0]
        cy = self.cy[0]
        self.get_max_distance()
        self.get_min_distance()
        if self.max_distance!=0:
            ratio = self.min_distance/self.max_distance
            if eps_circle <= ratio <= 1:
                self.shape = 'circle'
        return self.shape

    def find_rectangle(self):
        if len(self.contourNone) == 0:
            self.find_contour()
        self.get_max_distance()
        self.get_min_distance()
        area = (self.height-2)*(self.width-2)
        # if self.id == 77:
        #     print 'area rect = '+str(area)
        #     print 'area cnt = '+str(self.area)
        if eps_rect <= float(self.area)/float(area) <=1:
            self.shape = 'rectangle'
            self.rotated=0
            return self.shape
        cx = self.cx[0]
        cy = self.cy[0]
        a = self.max_distance
        b = self.min_distance
        angle1 = mm.carnot(self.min_point[0], cy ,self.min_point[0], self.min_point[1],cx,cy,rad=False)
        img2 = self.add_pad(a,a,a,a)
        h,w = img2.shape
        cx = (w+1)/2
        cy = (h+1)/2
        M = cv2.getRotationMatrix2D((cx,cy),360-angle1,1)
        dst = cv2.warpAffine(img2,M,(w,h))
        dst = np.array(dst,dtype=np.uint8)
        ret,thresh = cv2.threshold(dst,127,255,0)
        point= np.argwhere(thresh>0)
        y_min=point[:,0].min()
        x_min=point[:,1].min()
        y_max=point[:,0].max()
        x_max=point[:,1].max()
        image=np.where(thresh[y_min:y_max+1,x_min:x_max+1] >0, 255, 0)
        image = self.add_pad(1,1,1,1)
        img = np.array(image,dtype=np.uint8)
        contours,hierarchy = cv2.findContours(img, cv2.RETR_TREE,cv2.CHAIN_APPROX_SIMPLE)
        cnt = contours[0]
        area_rect = (x_max-x_min)*(y_max-y_min)
        area = cv2.contourArea(cnt)
        # print 'area rettangolo = '+str(area_rect)
        # print 'area cc ='+str(area)
        if area_rect!=0:
            ratio = float(area)/float(area_rect)
            # print 'ratio = '+str(ratio)
            if eps_rect<=ratio<=1:
                self.rotated=angle1
                self.shape = 'rectangle'
        return self.shape

    def find_ellipse(self):
        if len(self.contourNone) == 0:
            self.find_contour()
        cx = self.cx[0]
        cy = self.cy[0]
        self.get_max_distance()
        self.get_min_distance()
        point, a = mm.find_perpendicular(self.contourNone[0], cx, cy, self.min_point[0], self.min_point[1])
        ratio = a/self.max_distance
        # print 'ratio = '+str(ratio)
        if self.min_distance > 3 and ratio > 0.9:
                # print 'min = '+str(self.min_distance)
                # print 'max = '+str(self.max_distance)
                # print 'min point ='+str(self.min_point)
                # print 'max point ='+str(self.max_point)
                # print 'centro = '+str((cx, cy))
                # angle1 = mm.carnot(point[0], point[1] ,self.min_point[0], self.min_point[1],cx,cy,rad=False)
                # print 'angolo assi trovati = '+str(angle1)
                b = self.get_min_distance()
                angle2 = mm.pitagora(cx, cy, point[0], point[1], rad=False)
                # print 'asse a (orizzontale) inclinato di '+str(angle2)+' gradi'
                if angle2 != 0:
                    img2 = self.add_pad(1,1,a-b,a-b)
                    h,w = img2.shape
                    M = cv2.getRotationMatrix2D((w/2,h/2),360-angle2,1)
                    dst = cv2.warpAffine(img2,M,(w,h))
                    dst = np.array(dst,dtype=np.uint8)
                    ret,thresh = cv2.threshold(dst,127,255,0)
                    contours,hierarchy = cv2.findContours(thresh, cv2.RETR_TREE,cv2.CHAIN_APPROX_NONE)
                    none = contours[0]
                    contours,hierarchy = cv2.findContours(thresh, cv2.RETR_TREE,cv2.CHAIN_APPROX_SIMPLE)
                    simple = contours[0]
                    M = cv2.moments(simple)
                    if M['m00']==0:
                        M['m00'] = 0.01
                    cx = (int(M['m10']/M['m00']))
                    cy = (int(M['m01']/M['m00']))
                    max_distance, max_point = mm.calculate_max_distance(simple,cx,cy)
                    min_distance, min_point = mm.calculate_min_distance(none,cx,cy)
                    a = max_distance
                    b = min_distance
                    # print 'max_distance = '+str(max_distance)
                    # print 'min_distance = '+str(min_distance)
                    # print 'centro = '+str((cx, cy))
                    # print 'max = '+str(max_point)
                    # print 'min = '+str(min_point)
                    # angle1 = mm.carnot(max_point[0], max_point[1] ,min_point[0], min_point[1],cx,cy,rad=False)
                    # print 'angolo assi trovati = '+str(angle1)
                else:
                    none = self.contourNone[0]
                    a = self.get_max_distance()
                max1 = 0
                min1 = 999999
                for xA in none:
                    cost1 = float((cx-xA[0][0])**2)/(a**2) + float((cy-xA[0][1])**2)/(b**2)
                    if cost1 > max1:
                        max1 = cost1
                    if cost1 < min1:
                        min1 = cost1
                # print 'max1 = '+str(max1)
                # print 'min1 = '+str(min1)
                if 1 <= max1 <= 1+eps_ellipse and 1-0.5*eps_ellipse < (max1+min1)/2 < 1+0.5*eps_ellipse:
                    # print 'trovato un ellisse!'
                    self.shape = 'ellipse'
                    self.rotated = angle2
                    return self.shape
        self.shape = ''
        return self.shape



    def add_pad(self, padup,paddown,padleft,padright):
        s = self.image.shape
        h = s[0]
        w = s[1]
        image = deepcopy(self.image)
        if padup > 0:
            z1=np.zeros((padup,w))
            # print 'z1 =  '+str(z1.shape)
            # print 'image = '+str(cc.image.shape)
            image=np.concatenate((z1,image),axis=0) #aggiungo padding sopra
        if paddown > 0:
            z2=np.zeros((paddown,w))
            image=np.concatenate((image,z2),axis=0) #aggiungo padding sotto
        s = image.shape
        h = s[0]
        w = s[1]
        if padleft > 0:
            z3=np.zeros((h,padleft))
            image=np.concatenate((z3,image),axis=1)  #aggiungo padding a sinistra
        if padright > 0:
            z4=np.zeros((h, padright))
            image=np.concatenate((image,z4),axis=1) #aggiungo padding a destra
        # h1,w1=image.shape
        return image


    def create_histogram (self, bins1, bins2):
        del self.histogram[:]
        del self.histogram2[:]
        histogram = np.zeros((bins1,1))
        histogram2 = np.zeros((bins2,1))
        self.remove_contours()
        i = 0
        if self.max_distance == 0:
            self.max_distance, self.max_point = mm.calculate_max_distance(self.contourSimple[0],self.cx[0],self.cy[0])
        for i in range(0, len(self.contourSimple)):
            if i != 0:
                max_distance, max_point = mm.calculate_max_distance(self.contourSimple[i],self.cx[i],self.cy[i])
            else:
                max_distance = self.max_distance
            for xA in self.contourNone[i]:
                dist = mm.distance2D(self.cx[i],self.cy[i],xA[0][0],xA[0][1])
                if max_distance!=0:
                    dist = dist/max_distance
                else:
                    dist = 0
                b = 0
                continueSearch = True

                while b <= bins1 and continueSearch == True:
                    if dist >= b/float(bins1):
                        b = b+1
                    else:
                        histogram[b-1] += 1
                        continueSearch = False
                angle = mm.pitagora(self.cx[i],self.cy[i],xA[0][0],xA[0][1], rad=True)
                b = 0
                continueSearch = True
                while b <= bins2 and continueSearch == True:
                    if angle >= b/float(bins2):
                        b = b+1
                    else:
                        histogram2[b-1] += 1
                        continueSearch = False
            self.histogram.append(histogram)
            self.histogram2.append(histogram2)


        for j in range(0, len(self.histogram)):
            elements = float(len(self.contourNone[j]))
            for i in range (0, bins1):
                self.histogram[j][i][0] = float(self.histogram[j][i][0]) /elements
        for j in range(0, len(self.histogram)):
            elements = float(len(self.contourNone[j]))
            for i in range (0, bins2):
                self.histogram2[j][i][0] = float(self.histogram2[j][i][0]) /elements
        self.calculate_ratio()

    def calculate_ratio(self):
        self.image_dist_tran = ndimage.morphology.distance_transform_cdt(self.image)
        self.avg_width = ndimage.measurements.mean(self.image_dist_tran,self.image)
        if self.max_distance == 0:
            self.max_distance, self.max_point = mm.calculate_max_distance(self.contourSimple[0],self.cx[0],self.cy[0])
        self.ratio = self.avg_width/self.max_distance

        #print 'ecco lo istogramma della componente '+str(self.id) +': ' +str(self.histogram)

    def get_second_histogram(self):
        max = 0
        k = 0
        for i in range(1, len(self.contourSimple)):
            area = cv2.contourArea(self.contourSimple[i])
            if area > max:
                max = area
                k = i
        return self.histogram[k], self.histogram2[k]

    def get_linked_node_from_id(self,id):
        found = False
        for i in self._indices:
            node = self.get_linked_node(i)
            if id == node.id:
                return node
        if found == False:
            # print 'node with id = '+str(id)+' not found'
            sys.exit()

    def clean(self):
        self._indices.clear()
        self._notindices.clear()
        del self._list_linked[:]
        del self._list_notlinked[:]
        self.add_link(self)

    def remove_linked(self, cc):
        if cc in self._indices:
            self._list_linked.remove(cc)
            for n in self._list_linked[self._indices[cc]:len(self._list_linked)]:
                self._indices[n] = self._indices[n] - 1
            del self._indices[cc]

    def set_coordinates(self):
        s = self.image.shape
        self.height = s[0]
        self.width = s[1]
        self.x_max = self.x_min+self.width-2
        self.y_max = self.y_min+self.height-2
        self.center_x = self.x_min+(self.width/2)
        self.center_y = self.y_min+(self.height/2)

    def contours_number(self):
        return len(self.contourSimple)

    def get_min_distance(self):
        if self.min_distance > 99999 or len(self.min_point) == 0:
            self.min_distance, self.min_point = mm.calculate_min_distance(self.contourNone[0], self.cx[0], self.cy[0])
        return self.min_distance

    def get_max_distance(self):
        if self.max_distance == 0 or len(self.max_point) == 0:
            self.max_distance, self.max_point = mm.calculate_max_distance(self.contourSimple[0], self.cx[0], self.cy[0])
        return self.max_distance

    def set_distances(self):
        self.get_max_distance()
        self.get_min_distance()

    def check_point_inside(self, cx, cy):
        if len(self.contourSimple) == 0:
            self.find_contour()
        if len(self.polygon) == 0:
            poly = []
            for xA in self.contourSimple[0]:
                poly.append((xA[0][0]+self.x_min, xA[0][1]+self.y_min))

            self.polygon = mplPath.Path(np.array(poly))
        # print 'contour = '+str(self.contourSimple[0])
        # print 'poly = '+str(poly)
        # print 'polygon = '+str(bbPath)
        # print 'centroide = '+str(self.cx[0])+', '+str(self.cy[0])
        inside = self.polygon.contains_point((cx, cy))
        # print 'centroide inside? '+str(inside)
        return inside

    def check_point_inside_rectangle(self, cx, cy):
        if len(self.polygon_rec) == 0:
            poly = []
            poly.append((self.x_min, self.y_min))
            poly.append((self.x_min, self.y_max))
            poly.append((self.x_max, self.y_max))
            poly.append((self.x_max, self.y_min))
            self.polygon_rec = mplPath.Path(np.array(poly))
        # print 'contour = '+str(self.contourSimple[0])
        # print 'poly = '+str(poly)
        # print 'polygon = '+str(bbPath)
        # print 'centroide = '+str(self.cx[0])+', '+str(self.cy[0])
        inside = self.polygon_rec.contains_point((cx, cy))
        # print 'centroide inside? '+str(inside)
        return inside

    def check_cc_inside_rectangle(self, cc):
        if len(self.polygon_rec) == 0:
            poly = []
            poly.append((self.x_min, self.y_min))
            poly.append((self.x_min, self.y_max))
            poly.append((self.x_max, self.y_max))
            poly.append((self.x_max, self.y_min))
            self.polygon_rec = mplPath.Path(np.array(poly))
        cnt = cc.contourSimple[0]
        go = True
        for xA in cnt:
            if go:
                cx = xA[0][0]+cc.x_min
                cy = xA[0][1]+cc.y_min
                inside = self.polygon_rec.contains_point((cx, cy))
                if inside == False:
                    go = False
        if go:
            # print 'cc '+str(cc.id)+' dentro la cc '+str(self.id)
            return True
        return False

    def get_extremes(self):
        return self.x_min, self.y_min, self.x_max, self.y_max