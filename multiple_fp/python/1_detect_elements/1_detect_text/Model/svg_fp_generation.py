import re, cv2, numpy as np,PIL.ImageOps
from PIL import Image

from iterative_functions import iterative_functions_class

class svg_fp_generation_class():
    def __init__(self,current_directory,file_name,path_gravvitas,path_gravvitas_svg,
                 orginal_img_height, orginal_img_width):
        self.current_directory = current_directory
        self.file_name = file_name
        self.path_gravvitas = path_gravvitas
        self.path_gravvitas_svg = path_gravvitas_svg
        self.orginal_img_height, self.orginal_img_width = orginal_img_height, orginal_img_width
        self.iterative_obj = iterative_functions_class()

    def generate_svg_floorplan(self,new_img_height,new_img_width, all_fp_comps, fp_text,
                               modified_room_cordinates):
        self.draw_whole_fp_svg(self.file_name, self.path_gravvitas_svg, new_img_height,
                               new_img_width, all_fp_comps, fp_text)
        self.draw_rooms_svg(modified_room_cordinates)




    # ----- all methods related to svg generation
    def draw_whole_fp_svg(self, file_name, svg_path, img_for_svg_height, img_for_svg_width,
                 all_floorplan_components,fp_text):
        # test path
        # svg_path = '/home/ub/Documents/floorplan_recognition/tesimagistrale/input_fps/object_test/gravitas_test/results/'
        # input_path = '/home/ub/Documents/floorplan_recognition/tesimagistrale/input_fps/object_test/output_test/fp_10/Stefano_output.png'
        # # door_imageo = cv2.imread(input_path,0)
        # # orginal_img_height,orginal_img_width = door_imageo.shape
        # # door_image = ~(np.zeros((orginal_img_height,orginal_img_width,3), np.uint8))
        #--added 2 dots for tactitle processing. Need to remove that
        fp_text_stp_1 = fp_text.split('..')
        fp_text = '.'.join(fp_text_stp_1)

        only_image_name = file_name[0:-4]

        # --extract floorplan_data
        rooms_point_list = all_floorplan_components[0]
        object_point_list = all_floorplan_components[1]
        stairs_point_list = all_floorplan_components[2]
        windows_point_list = all_floorplan_components[3]
        doors_rect_list = all_floorplan_components[4]
        wall_point_list = all_floorplan_components[7]

        l2_space, l3_space = '  ', '    '
        svg_file = open(svg_path + str(only_image_name) + '.svg', 'w')
        svg_file.write(
            '<svg id="svg-graphic" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="' + str(
                img_for_svg_width) + '" height="' + str(img_for_svg_height) + '" viewBox="0 0 ' + str(
                img_for_svg_width + 300) + ' ' + str(
                img_for_svg_height + 100) + '" preserveAspectRatio="xMinYMin meet">' + '\n')
        svg_file.write(l2_space + '<title>'+only_image_name+'</title>' + '\n')
        svg_file.write(l2_space + '<g xmlns="http://www.w3.org/2000/svg" id="svg-graphic-components">' + '\n')

        # --place to store gravvitas meta data
        gravvitas_meta_data = ''

        # print text_path+only_image_name+'_7_wall_details.txt'
        wall_ID_int = 200
        wall_color_int, wall_color_hex = self.get_color('Wl', 'F')
        r, g, b = wall_color_int
        wall_color = str(r) + ',' + str(g) + ',' + str(b)
        wall_text = 'Wall'
        for wall_num, wall_row in enumerate(wall_point_list):
            contour_points_string = ''
            for cordinate in wall_row:
                x, y = cordinate
                contour_points_string = contour_points_string + str(x) + ',' + str(y) + ' '
            wall_ID_int = wall_ID_int + wall_num
            wall_ID = 'fp' + str(wall_ID_int)
            svg_file.write(l3_space + '<polygon points="'
                           + contour_points_string + '" id="'
                           + str(wall_ID) + '" class="" transform="matrix(1,0,0,1,0,0)" style="stroke-width: 2; stroke: rgb('
                           + wall_color + '); fill: rgb('
                           + wall_color + '); cursor: default;">')
            svg_file.write('<title>' + wall_text + '</title>' + '\n')
            svg_file.write('</polygon>' + '\n')
            current_element_data = self.meta_data_tag_creation(l3_space, wall_ID,
                                                               wall_color_hex, wall_text)
            gravvitas_meta_data = gravvitas_meta_data + current_element_data

        room_number = 100
        for room_row in rooms_point_list:
            if room_row[1] != 'OP':
                room_text = room_row[0]
                room_contour = room_row[-1]
                room_number = room_number + 1
                room_ID = 'fp' + str(room_number)
                color_int, color_hex = self.get_color('R', room_text)
                if len(color_int)== 3:
                    r, g, b = color_int
                else:
                    print 'SVG-The room name '+room_text+' doesnt exist'
                room_color = str(r) + ',' + str(g) + ',' + str(b)


                contour_points_string = ''
                for cordinate in room_contour:
                    x, y = cordinate
                    contour_points_string = contour_points_string + str(x) + ',' + str(y) + ' '

                svg_file.write(l3_space + '<polygon points="'
                               + contour_points_string + '" id="'
                               + str(room_ID) + '" class="" transform="matrix(1,0,0,1,0,0)" style="stroke-width: 2; stroke: rgb(0,0,0); fill: rgb('
                               + room_color + '); cursor: default;">')
                svg_file.write('<title>' + room_text + '</title>' + '\n')
                svg_file.write('</polygon>' + '\n')
                current_element_data = self.meta_data_tag_creation(l3_space, room_ID,
                                                                   color_hex, room_text)
                gravvitas_meta_data = gravvitas_meta_data + current_element_data



        object_number = 300
        # --set color
        object_color_int, object_color_hex = self.get_color('O', 'F')
        r, g, b = object_color_int
        object_color = str(r) + ',' + str(g) + ',' + str(b)
        for each_object_row in object_point_list:
            object_number = object_number + 1
            object_ID = 'fp' + str(object_number)
            object_text = str(each_object_row[0])
            # # --getting color
            # # g = (int(math.floor(object_number/10)% 10))*100
            # # b = (object_number% 10)*25
            # # r = (b+g)% 255
            # object_color = str(147) + ',' + str(21) + ',' + str(185)

            for each_object in each_object_row[1:]:
                contour_points_string = ''
                for cordinate in each_object:
                    x, y = cordinate
                    contour_points_string = contour_points_string + str(x) + ',' + str(y) + ' '
                svg_file.write(l3_space + '<polygon points="'
                               + contour_points_string + '" id="'
                               + str(object_ID) + '" class="" transform="matrix(1,0,0,1,0,0)" style="stroke-width: 2; stroke: rgb('
                               + object_color + '); fill: rgb('
                               + object_color + '); cursor: default;">')
                svg_file.write('<title>' + object_text + '</title>' + '\n')
                svg_file.write('</polygon>' + '\n')
                current_element_data = self.meta_data_tag_creation(l3_space, object_ID,
                                                                   object_color_hex,
                                                                   object_text)
                gravvitas_meta_data = gravvitas_meta_data + current_element_data

        stair_ID_int = 400
        # --set color
        stair_color_int, stair_color_hex = self.get_color('S', 'F')
        r, g, b = stair_color_int
        stair_color = str(r) + ',' + str(g) + ',' + str(b)
        stair_text = 'Stairs'
        for stair_num,stair_row in enumerate(stairs_point_list):
            stair_ID_int = stair_ID_int + stair_num
            stair_ID = 'fp' + str(stair_ID_int)
            contour_points_string = ''
            for cordinate in stair_row:
                x, y = cordinate
                contour_points_string = contour_points_string + str(x) + ',' + str(y) + ' '

            svg_file.write(l3_space + '<polygon points="'
                           + contour_points_string + '" id="'
                           + str(stair_ID) + '" class="" transform="matrix(1,0,0,1,0,0)" style="stroke-width: 1px; stroke: rgb('
                           + stair_color + '); fill: rgb('
                           + stair_color + '); cursor: default;">')
            svg_file.write('<title>' + stair_text + '</title>' + '\n')
            svg_file.write('</polygon>' + '\n')
            current_element_data = self.meta_data_tag_creation(l3_space, stair_ID,
                                                               stair_color_hex,
                                                               stair_text)
            gravvitas_meta_data = gravvitas_meta_data + current_element_data

        door_ID_int = 500
        # --set color
        door_color_int, door_color_hex = self.get_color('D', 'F')
        r, g, b = door_color_int
        door_color = str(r) + ',' + str(g) + ',' + str(b)
        door_text = 'Door'
        for door_num, door_row in enumerate(doors_rect_list):
            door_ID_int = door_ID_int + door_num
            door_ID = 'fp' + str(door_ID_int)
            contour_points_string = ''
            for cordinate in door_row:
                x, y = cordinate
                contour_points_string = contour_points_string + str(x) + ',' + str(y) + ' '
            svg_file.write(l3_space + '<polygon points="'
                           + contour_points_string + '" id="'
                           + str(door_ID) + '" class="" transform="matrix(1,0,0,1,0,0)" style="stroke-width: 1px; stroke: rgb('
                           + door_color + '); fill: rgb('
                           + door_color + '); cursor: default;">')
            # x1, y1 = door_row[0]
            # x2, y2 = door_row[1]
            # width = x2 - x1
            # height = y2 - y1
            # svg_file.write(l3_space + '<rect x="'
            #                + str(x1) + '" y="'
            #                + str(y1) + '" width="'
            #                + str(width) + '" height="'
            #                + str(height) + '" class="" id="'
            #                + str(door_ID) + '" transform="matrix(1,0,0,1,0,0)" style="stroke-width: 2; stroke: rgb('
            #                + door_color + '); fill: rgb('
            #                + door_color + '); cursor: default;">')
            svg_file.write('<title>' + door_text + '</title>' + '\n')
            svg_file.write('</polygon>' + '\n')
            current_element_data = self.meta_data_tag_creation(l3_space, door_ID,
                                                               door_color_hex, door_text)
            gravvitas_meta_data = gravvitas_meta_data + current_element_data

        window_ID_int = 600
        # --set color
        window_color_int, window_color_hex = self.get_color('Win', 'F')
        r, g, b = window_color_int
        window_color = str(r) + ',' + str(g) + ',' + str(b)
        window_text = 'Window'
        for win_num, window_row in enumerate(windows_point_list):
            window_ID_int = window_ID_int + win_num
            window_ID = 'fp' + str(window_ID_int)
            contour_points_string = ''
            for cordinate in window_row:
                x, y = cordinate
                contour_points_string = contour_points_string + str(x) + ',' + str(y) + ' '

            svg_file.write(l3_space + '<polygon points="'
                           + contour_points_string + '" id="'
                           + str(window_ID) + '" class="" transform="matrix(1,0,0,1,0,0)" style="stroke-width: 1px; stroke: rgb('
                           + window_color + '); fill: rgb('
                           + window_color + '); cursor: default;">')
            svg_file.write('<title>' + window_text + '</title>' + '\n')
            svg_file.write('</polygon>' + '\n')
            current_element_data = self.meta_data_tag_creation(l3_space, window_ID,
                                                               window_color_hex,
                                                               window_text)
            gravvitas_meta_data = gravvitas_meta_data + current_element_data

        svg_file.write(l2_space + '</g>')

        svg_file.write(
            l2_space + '<metadata id="rp-928bd05a-4b51-432c-8ff4-2f4c82a3c5b2" rpid="rp-928bd05a-4b51-432c-8ff4-2f4c82a3c5b2" title="Moscone Center Second Floor WWDC 2016" description="Floor Plan of the Moscone Centre for WWDC 2016" category="map" group="" subgroup="" keywords="" collections="" orientation="Landscape" targetdevicename="iPad all">' + '\n')
        svg_file.write(
            l3_space + '<summary>'+fp_text+'</summary>' + '\n')
        svg_file.write(gravvitas_meta_data)
        svg_file.write(l2_space + '</metadata>' + '\n')

        svg_file.write('</svg>')
        svg_file.close()

    def meta_data_tag_creation(self,l3_space,component_ID,color_ID,text):
        id_tag = l3_space+'<gravvitas><id>'+str(component_ID)+'</id>'
        color_tag = '<interiorcolor>'+str(color_ID)+'</interiorcolor><bordercolor/><cornercolor/>'
        audio_tag = '<audio>26</audio>'
        text_tag = '<text>'+str(text)+'</text>'
        rest_of_tag = '<vibration/><annotation/></gravvitas>'+'\n'
        meta_tag = id_tag+color_tag+audio_tag+text_tag+rest_of_tag

        return meta_tag

    def get_color(self,element_type,text):
        color_int, color_hex = (), ''


        if element_type=='R':
            text_list = text.split()
            fp_text = text_list[0]
            room_color_file_path = self.current_directory+'/Model/floor_plan_text/room_types.txt'
            room_color_file = open(room_color_file_path,'r')

            for room_color_data in room_color_file:
                #--structure - '[1 : [0,0,0] :[FFFFF]: [bedroom]'
                line_seperate = room_color_data.split(':')
                break_list = False
                if len(line_seperate) > 2:
                    room_text_list = line_seperate[3].split(',')
                    # room_text_list = re.findall(r"[\w']+", line_seperate[element_number])
                    for each_room in room_text_list:
                        pattern = re.compile("[^\w ]")
                        room_name = pattern.sub('', each_room)
                        # print len(text),len(room_name.strip())
                        # print fp_text,room_name.strip()
                        if fp_text == room_name.strip():
                            room_color_string_list = re.findall(r"[\w']+", line_seperate[1])
                            color_int = tuple([int(x) for x in room_color_string_list])
                            color_hex = re.findall(r"[\w']+", line_seperate[2])[0]
                            break_list = True
                            break
                    if break_list:
                        break
            if text == '0':
                color_int, color_hex = (1,158,151), '979E01'
            # print text, color_int,color_hex


        elif element_type=='O':
            color_int, color_hex = (147, 21, 185), '9315b9'
        elif element_type=='S':
            color_int, color_hex = (0, 0, 255), '0000FF'
        elif element_type=='Win':
            color_int, color_hex = (0, 255,0), 'FFFF00'
        elif element_type=='D':
            color_int, color_hex = (255, 0, 0), '00FF00'
        elif element_type=='Wl':
            color_int, color_hex = (0, 0, 0), '000000'
        else:
            color_int, color_hex = (255,255,255), 'FFFFFF'


        return color_int, color_hex

    def check_validity(self,y,x,labeled_list,last_element,direction):
        orginal_y, orginal_x = y,x
        passed= False
        if direction=='BL':
            if labeled_list[y+1][x-1] != 0:
                iteration_count = 1
            else:
                iteration_count = 20
            y,x = y+iteration_count,x-iteration_count
        elif direction=='LL':
            if labeled_list[y][x-1] != 0:
                iteration_count = 1
            else:
                iteration_count = 20
            y,x = y,x-iteration_count
        elif direction=='TL':
            if labeled_list[y-1][x-1] != 0:
                iteration_count = 1
            else:
                iteration_count = 20
            y,x = y-iteration_count,x-iteration_count
        elif direction=='TT':
            if labeled_list[y-1][x] == 0:
                iteration_count = 20
            else:
                iteration_count = 1
            y,x = y-iteration_count,x
        elif direction=='TR':
            if labeled_list[y-1][x+1] == 0:
                iteration_count = 20
            else:
                iteration_count = 1
            y,x = y-iteration_count,x+iteration_count
        elif direction=='RR':
            if labeled_list[y][x+1] == 0:
                iteration_count = 20
            else:
                iteration_count = 1
            y,x = y,x+iteration_count
        elif direction=='BR':
            if labeled_list[y+1][x+1] == 0:
                iteration_count = 20
            else:
                iteration_count = 1
            y,x = y+iteration_count,x+iteration_count
        elif direction=='BB':
            if labeled_list[y+1][x] == 0:
                iteration_count = 20
            else:
                iteration_count = 1
            y,x = y+iteration_count,x

        if labeled_list[y][x] != 0 and labeled_list[y][x]!= 1 and labeled_list[y][x]!= -1:
            last_element = labeled_list[y][x]
            labeled_list[orginal_y][orginal_x] = -1
            passed = True

        if passed==False:
            y,x = orginal_y,orginal_x
        # else:
        #     print orginal_x,orginal_y,'--',direction,last_element,passed,'--',x,y

        return last_element, labeled_list,y,x,passed

    def draw_rooms_svg(self,room_data):
        only_image_name=self.file_name[0:-4]

        #--place to store gravvitas meta data
        # gravvitas_meta_data = ''
        room_number = 100
        meta_data_room_color = 'FFFF00'
        room_color = str(255)+','+str(255)+','+str(0)
        for room_row in room_data:
            gravvitas_meta_data = ''
            if room_row[0][0] != 'OP':
                current_room = room_row[0]
                object_data = room_row[1]
                window_data = room_row[2]
                door_data = room_row[3]
                wall_data = room_row[4]


                room_name = str(current_room[1])
                room_points = current_room[2]
                img_width, img_height = room_row[5]
                # room_number = room_number+1
                # room_ID = 'fp'+str(only_image_name)+'Room No: '+str(room_number)
                room_number = room_number + 1
                room_ID = 'fp' + str(room_number)
                l2_space,l3_space ='  ','    '
                svg_file = open(self.path_gravvitas_svg+only_image_name+'_'+room_name+'_plan.svg', 'w')
                svg_file.write('<svg id="svg-graphic" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="'
                               +str(img_width)+'" height="'+str(img_height)+'" viewBox="0 0 '
                               +str(img_width)+' '+str(img_height)+'" preserveAspectRatio="xMinYMin meet">'+'\n')
                svg_file.write(l2_space+'<title>'+room_name+' Floorplan</title>'+'\n')
                svg_file.write(l2_space+'<g xmlns="http://www.w3.org/2000/svg" id="svg-graphic-components">'+'\n')

                # #--to check if objects and others are inside room
                # room_contour = self.iterative_obj.convert_points_to_contour(room_points)

                contour_points_string = ''
                for cordinate in room_points:
                    x,y = cordinate
                    contour_points_string = contour_points_string + str(x) + ',' + str(y) + ' '

                svg_file.write(l3_space+'<polygon points="'+contour_points_string+'" id="'+str(room_ID)+'" class="" transform="matrix(1,0,0,1,0,0)" style="stroke-width: 2; stroke: rgb('+room_color+'); fill: rgb('+room_color+'); cursor: default;">')
                svg_file.write('<title>'+room_name+'</title>'+'\n')
                svg_file.write('</polygon>'+'\n')
                current_element_data = self.meta_data_tag_creation(l3_space, room_ID, meta_data_room_color,room_name)
                gravvitas_meta_data = gravvitas_meta_data+current_element_data




                #--extract wall data
                wall_ID_int = 200
                wall_ID = 'fp' + str(wall_ID_int)
                wall_color_int, wall_color_hex = self.get_color('Wl', 'F')
                r, g, b = wall_color_int
                wall_color = str(r) + ',' + str(g) + ',' + str(b)
                wall_text = 'Wall'
                outside_contour,reveresed_inside_contour = wall_data
                contour_string = 'M '
                for i in xrange(len(outside_contour)):
                    x, y = outside_contour[i][0]
                    contour_string = contour_string+str(x) + ',' + str(y) + ' '
                contour_string = contour_string +'z M '

                for i in xrange(len(reveresed_inside_contour)):
                    x, y = reveresed_inside_contour[i][0]
                    contour_string = contour_string +str(x) + ',' + str(y) + ' '
                contour_string = contour_string + 'z'
                svg_file.write(l3_space + '<path d="'
                               + contour_string + '" id="'
                               + str(wall_ID) + '" class="" transform="matrix(1,0,0,1,0,0)" style="stroke-width: 2; stroke: rgb('
                               + wall_color + '); fill: rgb('
                               + wall_color + '); fill-rule:evenodd; cursor: default;">')

                svg_file.write('<title>' + wall_text + '</title>' + '\n')
                svg_file.write('</path>' + '\n')
                current_element_data = self.meta_data_tag_creation(l3_space, wall_ID,
                                                                   wall_color_hex, wall_text)
                gravvitas_meta_data = gravvitas_meta_data + current_element_data



                object_number = 300
                # --set color
                object_color_int, object_color_hex = self.get_color('O', 'F')
                r, g, b = object_color_int
                object_color = str(r) + ',' + str(g) + ',' + str(b)
                for objrow_num, each_object_row in enumerate(object_data):
                    object_number = object_number + objrow_num
                    object_ID = 'fp' + str(object_number)
                    object_text = str(each_object_row[0])
                    object_points = each_object_row[1]

                    contour_points_string = ''
                    for cordinate in object_points:
                        x, y = cordinate
                        contour_points_string = contour_points_string + str(x) + ',' + str(y) + ' '
                    svg_file.write(l3_space + '<polygon points="'
                                   + contour_points_string + '" id="'
                                   + str(
                        object_ID) + '" class="" transform="matrix(1,0,0,1,0,0)" style="stroke-width: 2; stroke: rgb('
                                   + object_color + '); fill: rgb('
                                   + object_color + '); cursor: default;">')
                    svg_file.write('<title>' + object_text + '</title>' + '\n')
                    svg_file.write('</polygon>' + '\n')
                    current_element_data = self.meta_data_tag_creation(l3_space, object_ID,
                                                                       object_color_hex,
                                                                       object_text)
                    gravvitas_meta_data = gravvitas_meta_data + current_element_data

                door_ID_int = 500
                # --set color
                door_color_int, door_color_hex = self.get_color('D', 'F')
                r, g, b = door_color_int
                door_color = str(r) + ',' + str(g) + ',' + str(b)
                door_text = 'Door'
                for door_num, door_contour in enumerate(door_data):
                    door_ID_int = door_ID_int + door_num
                    door_ID = 'fp' + str(door_ID_int)
                    # --get door corner points
                    contour_points_string = ''
                    for cordinate in door_contour:
                        x, y = cordinate
                        contour_points_string = contour_points_string + str(x) + ',' + str(y) + ' '
                    svg_file.write(l3_space + '<polygon points="'
                                   + contour_points_string + '" id="'
                                   + str(door_ID) + '" class="" transform="matrix(1,0,0,1,0,0)" style="stroke-width: 1px; stroke: rgb('
                                   + door_color + '); fill: rgb('
                                   + door_color + '); cursor: default;">')
                    # x1, y1 = door_contour[0]
                    # x2, y2 = door_contour[1]
                    # width = x2 - x1
                    # height = y2 - y1
                    # svg_file.write(l3_space + '<rect x="'
                    #                + str(x1) + '" y="'
                    #                + str(y1) + '" width="'
                    #                + str(width) + '" height="'
                    #                + str(height) + '" class="" id="'
                    #                + str(door_ID) + '" transform="matrix(1,0,0,1,0,0)" style="stroke-width: 2; stroke: rgb('
                    #                + door_color + '); fill: rgb('
                    #                + door_color + '); cursor: default;">')
                    svg_file.write('<title>' + door_text + '</title>' + '\n')
                    svg_file.write('</polygon>' + '\n')
                    current_element_data = self.meta_data_tag_creation(l3_space, door_ID,
                                                                       door_color_hex, door_text)
                    gravvitas_meta_data = gravvitas_meta_data + current_element_data

                window_ID_int = 600
                window_color_int, window_color_hex = self.get_color('Win', 'F')
                r, g, b = window_color_int
                window_color = str(r) + ',' + str(g) + ',' + str(b)
                window_text = 'Window'
                for win_num, window_row in enumerate(window_data):
                    window_ID_int = window_ID_int + win_num
                    window_ID = 'fp' + str(window_ID_int)
                    contour_points_string = ''
                    for cordinate in window_row:
                        x, y = cordinate
                        contour_points_string = contour_points_string + str(x) + ',' + str(y) + ' '

                    svg_file.write(l3_space + '<polygon points="'
                                   + contour_points_string + '" id="'
                                   + str(window_ID) + '" class="" transform="matrix(1,0,0,1,0,0)" style="stroke-width: 1px; stroke: rgb('
                                   + window_color + '); fill: rgb('
                                   + window_color + '); cursor: default;">')
                    svg_file.write('<title>' + window_text + '</title>' + '\n')
                    svg_file.write('</polygon>' + '\n')
                    current_element_data = self.meta_data_tag_creation(l3_space, window_ID,
                                                                       window_color_hex,
                                                                       window_text)
                    gravvitas_meta_data = gravvitas_meta_data + current_element_data




                svg_file.write(l2_space + '</g>')

                svg_file.write(
                    l2_space + '<metadata id="rp-928bd05a-4b51-432c-8ff4-2f4c82a3c5b2" rpid="rp-928bd05a-4b51-432c-8ff4-2f4c82a3c5b2" title="Moscone Center Second Floor WWDC 2016" description="Floor Plan of the Moscone Centre for WWDC 2016" category="map" group="" subgroup="" keywords="" collections="" orientation="Landscape" targetdevicename="iPad all">' + '\n')
                svg_file.write(
                    l3_space + '<summary>This floor plan is showing '+room_name+'</summary>' + '\n')
                svg_file.write(gravvitas_meta_data)
                svg_file.write(l2_space + '</metadata>' + '\n')

                svg_file.write('</svg>')
                svg_file.close()






