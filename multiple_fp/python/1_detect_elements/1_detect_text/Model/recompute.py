__author__ = 'anu'

import cv2,copy
import os
import math
import itertools
import numpy as np
import PIL.ImageOps

from PIL import Image
from operator import itemgetter

from color_check import color_check_class
from open_plan_line import line
from open_plan import planClass
from penalties_and_scores import penalties_scores_class
from iterative_functions import iterative_functions_class
from line_optimization_functions import line_optimization_function_class
from generate_other_lines import generate_other_lines_class

class recompute:

    def __init__(self):
        self.penalties_scores_obj = penalties_scores_class()


    def find_open_plan_area(self,current_directory,orginal_image_name,avg_door_width,
                            weight_list,input_image,final_output_path,new_output_path,
                            orginal_open_plan_text_cordinate,original_text_words,
                            open_plan_level,text_bounding_boxes_original,debug_mode):
        self.line_optimization_function_obj = line_optimization_function_class()

        gray = input_image
        gray_height,gray_width = gray.shape
        open_plan_path =new_output_path+'plans/'
        os.mkdir(open_plan_path)
        region_grow_path =new_output_path+'region_grow/'
        os.mkdir(region_grow_path)
        voronoi_lines_path =new_output_path+'voronoi_lines/'
        os.mkdir(voronoi_lines_path)
        edge_extensions_path =new_output_path+'edge_extensions/'
        os.mkdir(edge_extensions_path)
        shortest_path_path =new_output_path+'shortest_path/'
        os.mkdir(shortest_path_path)
        line_optimizaiton_path =new_output_path+'line_optimizaiton/'
        os.mkdir(line_optimizaiton_path)
        line_scoring_path =new_output_path+'line_scoring/'
        os.mkdir(line_scoring_path)

        self.line_optimization_function_obj = line_optimization_function_class()

        iteration_obj = iterative_functions_class()
        all_detected_contour_lines = iteration_obj.find_open_plans(current_directory,input_image,new_output_path,open_plan_path,orginal_open_plan_text_cordinate,original_text_words,text_bounding_boxes_original,avg_door_width)


        if len(all_detected_contour_lines)==0:
            if debug_mode:
                print 'No Open Plan'
            # return len(all_detected_contour_lines),0,orginal_open_plan_text_cordinate,original_text_words,text_bounding_boxes_original
            return len(all_detected_contour_lines),0


        major_voronoi_data =[]
        major_all_contour_lines=[]
        major_edge_extensions=[]
        major_shortest_path_data=[]
        major_max_lines=[]
        major_edge_extensions_between_coordinates=[]
        open_area_list = os.listdir(open_plan_path)
        open_area_list.sort()

        # major_open_plan_text_cordinate,major_text_words,major_text_bounding_boxes = [],[],[]
        for common_room_number,room in enumerate(open_area_list):
            open_plan_text_cordinate,text_words,text_bounding_boxes = [],[],[]
            image_name = str(room)
            name = image_name[0:-4]
            room_number = image_name[:-4]

            current_open_plan_contour_data = []
            for contour_detail_row in all_detected_contour_lines:
                if contour_detail_row[0]==int(room_number):
                    current_open_plan_contour_data = contour_detail_row
                    break
            contour_check = current_open_plan_contour_data[2]
            # print 'current_open_plan_contour_data',current_open_plan_contour_data

            for r,row in enumerate(current_open_plan_contour_data[3:]):
                open_plan_text_cordinate.append(row[1])
                text_words.append(row[0])
                text_bounding_boxes.append(row[2])

            if debug_mode:
                print 'room name', room
                for text_w in text_words:
                    print text_w









# -------start region grow
            outer_cont = ~(np.zeros((gray_height,gray_width,3), np.uint8))
            cv2.drawContours(outer_cont,[contour_check],0,(0,0,0),1)
            cv2.imwrite(region_grow_path+name+'_STEP_01.png',outer_cont)
            # print 'open_plan_text_cordinate',open_plan_text_cordinate
            # print 'Room_number',room_number
            # print 'Text_words',text_words
            # print 'Open_plan_text_cordinate'
            # for row in open_plan_text_cordinate:
            #     print row
            image = Image.open(region_grow_path+name+'_STEP_01.png')
            width, height = image.size
            pixels = image.load()

            #---- recheck if can be combined
            threshold_array_row_length = len(open_plan_text_cordinate)
            top_threshold_pixel_array = [[0 for b in range(threshold_array_row_length)] for c in range(width)]
            bottom_threshold_pixel_array = [[height for b in range(threshold_array_row_length)] for c in range(width)]
            left_threshold_pixel_array = [[0 for b in range(threshold_array_row_length)] for c in range(height)]
            right_threshold_pixel_array = [[width for b in range(threshold_array_row_length)] for c in range(height)]

            #-----create array to store all direction and pixel data and initialize it
            pixel_data = [[-1 for column in range(width)] for row in range(height)]

            # max_size = (max(width,height))/2
            color_val_obj = color_check_class(pixels, top_threshold_pixel_array,bottom_threshold_pixel_array,left_threshold_pixel_array, right_threshold_pixel_array)
            temp_array_copy = []
            temp_ring_array = []
            flag_list = [0 for x in range(len(open_plan_text_cordinate))]
            for ring_count in range(1500):
                for current_txt_label in range(len(open_plan_text_cordinate)):
                    if flag_list[current_txt_label]==0:
                        each_cordinate = open_plan_text_cordinate[current_txt_label]
                        if ring_count == 0:
                            #---- update pixel_data array with txt_lable cordinate
                            column,row = each_cordinate
                            pixel_data[row][column]= current_txt_label
                            #-----add txt_label cordinate to temp_array
                            ta_length = len(temp_array_copy)
                            temp_array_copy.append([])
                            temp_array_copy[ta_length].append(each_cordinate)
                            temp_array_copy[ta_length].append(current_txt_label)
                        flag_count = 0
                        text_exists_flag = -1
                        for row in temp_array_copy:
                            if row[1]==current_txt_label:
                                text_exists_flag=1
                                col_coord, row_coord = row[0]
                                top_pixel = col_coord, row_coord-1
                                left_pixel = col_coord-1, row_coord
                                bottom_pixel = col_coord, row_coord+1
                                right_pixel = col_coord+1, row_coord

                                top_col,top_row = top_pixel
                                if cv2.pointPolygonTest(contour_check,(top_col,top_row),False)==1:
                                    if pixel_data[top_row][top_col] ==-1:
                                        temp_ring_array, pixel_data = color_val_obj.color_match(top_pixel,pixel_data,temp_ring_array,'T',current_txt_label)
                                        flag_count= flag_count+1

                                left_col,left_row = left_pixel
                                if cv2.pointPolygonTest(contour_check,(left_col,left_row),False)==1:
                                    if pixel_data[left_row][left_col] ==-1:
                                        temp_ring_array, pixel_data = color_val_obj.color_match(left_pixel,pixel_data,temp_ring_array,'L',current_txt_label)
                                        flag_count= flag_count+1

                                bottom_col,bottom_row = bottom_pixel
                                if cv2.pointPolygonTest(contour_check,(bottom_col,bottom_row),False)==1:
                                    if pixel_data[bottom_row][bottom_col] ==-1:
                                        temp_ring_array, pixel_data = color_val_obj.color_match(bottom_pixel,pixel_data,temp_ring_array,'B',current_txt_label)
                                        flag_count= flag_count+1

                                right_col,right_row = right_pixel
                                if cv2.pointPolygonTest(contour_check,(right_col,right_row),False)==1:
                                    if pixel_data[right_row][right_col] ==-1:
                                        temp_ring_array, pixel_data = color_val_obj.color_match(right_pixel,pixel_data,temp_ring_array,'R',current_txt_label)
                                        flag_count= flag_count+1
                            else:
                                text_exists_flag=0

                        if flag_count==0 and text_exists_flag==1:
                            flag_list[current_txt_label]= 1
                            break
                temp_array_copy=temp_ring_array
                temp_ring_array = []

                if sum(flag_list)==len(open_plan_text_cordinate):
                    # if debug_mode:
                    #     print ring_count
                    break
            for img_row in range(height-1):
                for img_column in range(width-1):
                    # print img_row,img_column
                    if pixel_data[img_row][img_column]==0:
                        pixels[img_column,img_row] = (255, 0, 0)
                    if pixel_data[img_row][img_column]==1:
                        pixels[img_column,img_row] = (0, 255, 0)
                    if pixel_data[img_row][img_column]==2:
                        pixels[img_column,img_row] = (0, 0, 255)
                    if pixel_data[img_row][img_column]==3:
                        pixels[img_column,img_row] = (255, 255, 0)
                    if pixel_data[img_row][img_column]==4:
                        pixels[img_column,img_row] = (102, 51, 0)
                    if pixel_data[img_row][img_column]==5:
                        pixels[img_column,img_row] = (255, 0, 255)
                    if pixel_data[img_row][img_column]==6:
                        pixels[img_column,img_row] = (0, 255, 255)
                    if pixel_data[img_row][img_column]==7:
                        pixels[img_column,img_row] = (255, 0, 125)
                    if pixel_data[img_row][img_column]==8:
                        pixels[img_column,img_row] = (255, 128, 0)
                    if pixel_data[img_row][img_column]==9:
                        pixels[img_column,img_row] = (125, 0, 255)
                    if pixel_data[img_row][img_column]==10:
                        pixels[img_column,img_row] = (102, 0, 0)
                    if pixel_data[img_row][img_column]==11:
                        pixels[img_column,img_row] = (0, 150, 75)
                    if pixel_data[img_row][img_column]==12:
                        pixels[img_column,img_row] = (102, 0, 102)
                    if pixel_data[img_row][img_column]==13:
                        pixels[img_column,img_row] = (150, 150, 0)
                    if pixel_data[img_row][img_column]==14:
                        pixels[img_column,img_row] = (200, 255, 200)


            # output_image.save(self.output_directory+name+'_STEP_02-1.png')
            image.save(region_grow_path+name+'_STEP_02.png')

#------------------------getting partitions out of region grow
            if debug_mode:
                image = cv2.imread(region_grow_path+name+'_STEP_02.png',cv2.IMREAD_COLOR)
            new_partitions_path= region_grow_path+name+'/'
            os.mkdir(new_partitions_path)
            pil_image = Image.open(region_grow_path+name+'_STEP_02.png')
            clrs = pil_image.getcolors()
            existing_colors =[]
            for row in clrs:
                color = row[1]
                exists = False
                if (color[0]==0 and color[1]==0 and color[2]==0) or (color[0]==255 and color[1]==255 and color[2]==255):
                    exists=True
                if row[0]>5 and exists==False:
                    existing_colors.append(row[1])

            all_contours=[]
            redraw_contours= []
            all_contour_lines=[]
            count = 0
            width, height = pil_image.size
            pixels_2 = pil_image.load()
            for c, (r,g,b) in enumerate(existing_colors):
                empty_new_image = ~(np.zeros((gray_height,gray_width,3), np.uint8))
                cv2.imwrite(region_grow_path+name+'_INTERMEDIATE.png', empty_new_image)
                image_3 = Image.open(region_grow_path+name+'_INTERMEDIATE.png')
                pixels_3 = image_3.load()
                for img_row in range(height-1):
                    for img_column in range(width-1):
                        color = pixels_2[img_column,img_row]
                        if color[0]==r and color[1]==g and color[2]==b:
                            pixels_3[img_column,img_row]= (0,0,0)
                image_3.save(new_partitions_path+'BLACK'+str(c)+'_Conour.png')
                os.remove(region_grow_path+name+'_INTERMEDIATE.png')

                gray_room = cv2.imread(new_partitions_path+'BLACK'+str(c)+'_Conour.png',cv2.IMREAD_GRAYSCALE)
                ret,thresh = cv2.threshold(gray_room,0,255,1)
                contours,hierachy = cv2.findContours(thresh,1,2)
                if debug_mode:
                    redraw_contours.append(contours)
                empty_new_image = ~(np.zeros((gray_height,gray_width,3), np.uint8))
                cv2.drawContours(empty_new_image, contours, -1, (0,0,0), 1,cv2.cv.CV_AA)
                image_contour = []
                image_contour_lines=[]
                first_element =0
                current_cont = 0
                text_cordinate = 0
                #--need to go in to several levels since findcontours give arrays inside arrays
                if len(contours) > 0:
                    for cnt in contours:
                        current_cont = cnt
                    for l, level1 in enumerate(current_cont):
                        for level2 in level1:
                            x,y = level2
                            #----- append all contour cordinates to new array based on contour
                            image_contour.append([x,y])
                            #-----based on cordinates get contour lines and append to image_contour_lines
                            if l==0:
                                image_contour_lines.append([])
                                image_contour_lines[l].append([x,y])
                                first_element=[x,y]
                            elif l==len(current_cont)-1:
                                image_contour_lines.append([])
                                image_contour_lines[l].append([x,y])
                                image_contour_lines[l-1].append([x,y])
                                image_contour_lines[l].append(first_element)
                            else:
                                image_contour_lines.append([])
                                image_contour_lines[l].append([x,y])
                                image_contour_lines[l-1].append([x,y])
                for text_row, text_cord in enumerate(open_plan_text_cordinate):
                    if cv2.pointPolygonTest(current_cont,(tuple(text_cord)),False)==1:
                        text_cordinate = text_row
                        break

                #write each contour in black to a white image
                if debug_mode:
                    cv2.imwrite(new_partitions_path+'ONLY CONT'+str(c)+"_contour.png", empty_new_image)
                #------ add all room contours to one array
                all_contours.append(image_contour)
                if len(image_contour_lines)>0:
                    all_contour_lines.append([])
                    all_contour_lines[count].append(image_contour_lines)
                    all_contour_lines[count].append(text_cordinate)
                count = count+1

            # #-------write all room contours to a new image
            if debug_mode:
                empty_new_image2 = ~(np.zeros((gray_height,gray_width,3), np.uint8))
                for each in redraw_contours:
                    cv2.drawContours(empty_new_image2, each, -1, (0,0,0), 1,cv2.cv.CV_AA)
                cv2.imwrite(region_grow_path+name+'_STEP_03.png', empty_new_image2)

            # all_contour_lines = [
            #     [[[[887, 1377], [887, 1690]], [[887, 1690], [1127, 1690]], [[1127, 1690], [1132, 1685]],
            #       [[1132, 1685], [1372, 1685]], [[1372, 1685], [1372, 1684]], [[1372, 1684], [1373, 1683]],
            #       [[1373, 1683], [1373, 1670]], [[1373, 1670], [1374, 1669]], [[1374, 1669], [1374, 1657]],
            #       [[1374, 1657], [1375, 1656]], [[1375, 1656], [1375, 1644]], [[1375, 1644], [1376, 1643]],
            #       [[1376, 1643], [1376, 1630]], [[1376, 1630], [1377, 1629]], [[1377, 1629], [1377, 1617]],
            #       [[1377, 1617], [1378, 1616]], [[1378, 1616], [1378, 1604]], [[1378, 1604], [1379, 1603]],
            #       [[1379, 1603], [1379, 1590]], [[1379, 1590], [1380, 1589]], [[1380, 1589], [1380, 1577]],
            #       [[1380, 1577], [1381, 1576]], [[1381, 1576], [1381, 1564]], [[1381, 1564], [1382, 1563]],
            #       [[1382, 1563], [1382, 1550]], [[1382, 1550], [1383, 1549]], [[1383, 1549], [1383, 1537]],
            #       [[1383, 1537], [1384, 1536]], [[1384, 1536], [1384, 1524]], [[1384, 1524], [1385, 1523]],
            #       [[1385, 1523], [1385, 1501]], [[1385, 1501], [1381, 1501]], [[1381, 1501], [1381, 1504]],
            #       [[1381, 1504], [1380, 1505]], [[1380, 1505], [1366, 1505]], [[1366, 1505], [1365, 1504]],
            #       [[1365, 1504], [1365, 1377]], [[1365, 1377], [887, 1377]]], 1],
            #     [[[[771, 1040], [771, 1386]], [[771, 1386], [779, 1394]], [[779, 1394], [779, 1395]],
            #       [[779, 1395], [787, 1403]], [[787, 1403], [787, 1404]], [[787, 1404], [795, 1412]],
            #       [[795, 1412], [795, 1413]], [[795, 1413], [803, 1421]], [[803, 1421], [803, 1422]],
            #       [[803, 1422], [812, 1431]], [[812, 1431], [812, 1432]], [[812, 1432], [820, 1440]],
            #       [[820, 1440], [820, 1441]], [[820, 1441], [828, 1449]], [[828, 1449], [828, 1450]],
            #       [[828, 1450], [836, 1458]], [[836, 1458], [836, 1459]], [[836, 1459], [844, 1467]],
            #       [[844, 1467], [844, 1468]], [[844, 1468], [853, 1477]], [[853, 1477], [852, 1478]],
            #       [[852, 1478], [770, 1478]], [[770, 1478], [770, 1512]], [[770, 1512], [769, 1513]],
            #       [[769, 1513], [740, 1513]], [[740, 1513], [740, 1588]], [[740, 1588], [776, 1588]],
            #       [[776, 1588], [777, 1589]], [[777, 1589], [777, 1617]], [[777, 1617], [776, 1618]],
            #       [[776, 1618], [716, 1618]], [[716, 1618], [726, 1618]], [[726, 1618], [727, 1619]],
            #       [[727, 1619], [737, 1619]], [[737, 1619], [738, 1620]], [[738, 1620], [748, 1620]],
            #       [[748, 1620], [749, 1621]], [[749, 1621], [759, 1621]], [[759, 1621], [760, 1622]],
            #       [[760, 1622], [770, 1622]], [[770, 1622], [771, 1623]], [[771, 1623], [781, 1623]],
            #       [[781, 1623], [782, 1624]], [[782, 1624], [862, 1624]], [[862, 1624], [877, 1609]],
            #       [[877, 1609], [877, 1145]], [[877, 1145], [878, 1144]], [[878, 1144], [1181, 1144]],
            #       [[1181, 1144], [1181, 1040]], [[1181, 1040], [771, 1040]]], 0],
            #     [[[[559, 1914], [559, 2197]], [[559, 2197], [558, 2198]], [[558, 2198], [469, 2198]],
            #       [[469, 2198], [469, 2539]], [[469, 2539], [782, 2539]], [[782, 2539], [783, 2540]],
            #       [[783, 2540], [783, 2585]], [[783, 2585], [782, 2586]], [[782, 2586], [469, 2586]],
            #       [[469, 2586], [469, 2934]], [[469, 2934], [1254, 2934]], [[1254, 2934], [1254, 2296]],
            #       [[1254, 2296], [1132, 2296]], [[1132, 2296], [750, 1914]], [[750, 1914], [559, 1914]]], 3],
            #     [[[[877, 1610], [862, 1625]], [[862, 1625], [793, 1625]], [[793, 1625], [804, 1625]],
            #       [[804, 1625], [805, 1626]], [[805, 1626], [805, 1627]], [[805, 1627], [807, 1629]],
            #       [[807, 1629], [807, 1630]], [[807, 1630], [810, 1633]], [[810, 1633], [810, 1634]],
            #       [[810, 1634], [812, 1636]], [[812, 1636], [812, 1637]], [[812, 1637], [815, 1640]],
            #       [[815, 1640], [815, 1641]], [[815, 1641], [817, 1643]], [[817, 1643], [817, 1644]],
            #       [[817, 1644], [820, 1647]], [[820, 1647], [820, 1648]], [[820, 1648], [823, 1651]],
            #       [[823, 1651], [823, 1652]], [[823, 1652], [825, 1654]], [[825, 1654], [825, 1655]],
            #       [[825, 1655], [828, 1658]], [[828, 1658], [828, 1659]], [[828, 1659], [830, 1661]],
            #       [[830, 1661], [830, 1662]], [[830, 1662], [833, 1665]], [[833, 1665], [833, 1666]],
            #       [[833, 1666], [835, 1668]], [[835, 1668], [835, 1669]], [[835, 1669], [838, 1672]],
            #       [[838, 1672], [838, 1673]], [[838, 1673], [841, 1676]], [[841, 1676], [841, 1677]],
            #       [[841, 1677], [843, 1679]], [[843, 1679], [843, 1680]], [[843, 1680], [846, 1683]],
            #       [[846, 1683], [846, 1684]], [[846, 1684], [848, 1686]], [[848, 1686], [848, 1687]],
            #       [[848, 1687], [851, 1690]], [[851, 1690], [851, 1691]], [[851, 1691], [853, 1693]],
            #       [[853, 1693], [853, 1694]], [[853, 1694], [856, 1697]], [[856, 1697], [856, 1698]],
            #       [[856, 1698], [859, 1701]], [[859, 1701], [859, 1706]], [[859, 1706], [858, 1707]],
            #       [[858, 1707], [832, 1707]], [[832, 1707], [831, 1708]], [[831, 1708], [805, 1708]],
            #       [[805, 1708], [804, 1709]], [[804, 1709], [778, 1709]], [[778, 1709], [777, 1710]],
            #       [[777, 1710], [751, 1710]], [[751, 1710], [750, 1711]], [[750, 1711], [724, 1711]],
            #       [[724, 1711], [723, 1712]], [[723, 1712], [697, 1712]], [[697, 1712], [776, 1712]],
            #       [[776, 1712], [777, 1713]], [[777, 1713], [777, 1741]], [[777, 1741], [776, 1742]],
            #       [[776, 1742], [559, 1742]], [[559, 1742], [559, 1913]], [[559, 1913], [750, 1913]],
            #       [[750, 1913], [1132, 2295]], [[1132, 2295], [1254, 2295]], [[1254, 2295], [1254, 2292]],
            #       [[1254, 2292], [1255, 2291]], [[1255, 2291], [1803, 2291]], [[1803, 2291], [1803, 2043]],
            #       [[1803, 2043], [1802, 2042]], [[1802, 2042], [1801, 2042]], [[1801, 2042], [1799, 2040]],
            #       [[1799, 2040], [1798, 2040]], [[1798, 2040], [1796, 2038]], [[1796, 2038], [1795, 2038]],
            #       [[1795, 2038], [1793, 2036]], [[1793, 2036], [1792, 2036]], [[1792, 2036], [1790, 2034]],
            #       [[1790, 2034], [1789, 2034]], [[1789, 2034], [1787, 2032]], [[1787, 2032], [1786, 2032]],
            #       [[1786, 2032], [1784, 2030]], [[1784, 2030], [1783, 2030]], [[1783, 2030], [1781, 2028]],
            #       [[1781, 2028], [1780, 2028]], [[1780, 2028], [1778, 2026]], [[1778, 2026], [1777, 2026]],
            #       [[1777, 2026], [1775, 2024]], [[1775, 2024], [1774, 2024]], [[1774, 2024], [1772, 2022]],
            #       [[1772, 2022], [1771, 2022]], [[1771, 2022], [1769, 2020]], [[1769, 2020], [1768, 2020]],
            #       [[1768, 2020], [1766, 2018]], [[1766, 2018], [1765, 2018]], [[1765, 2018], [1763, 2016]],
            #       [[1763, 2016], [1762, 2016]], [[1762, 2016], [1760, 2014]], [[1760, 2014], [1759, 2014]],
            #       [[1759, 2014], [1757, 2012]], [[1757, 2012], [1756, 2012]], [[1756, 2012], [1754, 2010]],
            #       [[1754, 2010], [1753, 2010]], [[1753, 2010], [1751, 2008]], [[1751, 2008], [1750, 2008]],
            #       [[1750, 2008], [1748, 2006]], [[1748, 2006], [1747, 2006]], [[1747, 2006], [1745, 2004]],
            #       [[1745, 2004], [1744, 2004]], [[1744, 2004], [1742, 2002]], [[1742, 2002], [1741, 2002]],
            #       [[1741, 2002], [1739, 2000]], [[1739, 2000], [1738, 2000]], [[1738, 2000], [1736, 1998]],
            #       [[1736, 1998], [1735, 1998]], [[1735, 1998], [1733, 1996]], [[1733, 1996], [1732, 1996]],
            #       [[1732, 1996], [1730, 1994]], [[1730, 1994], [1729, 1994]], [[1729, 1994], [1727, 1992]],
            #       [[1727, 1992], [1726, 1992]], [[1726, 1992], [1724, 1990]], [[1724, 1990], [1723, 1990]],
            #       [[1723, 1990], [1721, 1988]], [[1721, 1988], [1720, 1988]], [[1720, 1988], [1718, 1986]],
            #       [[1718, 1986], [1717, 1986]], [[1717, 1986], [1715, 1984]], [[1715, 1984], [1714, 1984]],
            #       [[1714, 1984], [1712, 1982]], [[1712, 1982], [1711, 1982]], [[1711, 1982], [1709, 1980]],
            #       [[1709, 1980], [1708, 1980]], [[1708, 1980], [1706, 1978]], [[1706, 1978], [1705, 1978]],
            #       [[1705, 1978], [1703, 1976]], [[1703, 1976], [1702, 1976]], [[1702, 1976], [1700, 1974]],
            #       [[1700, 1974], [1699, 1974]], [[1699, 1974], [1697, 1972]], [[1697, 1972], [1696, 1972]],
            #       [[1696, 1972], [1694, 1970]], [[1694, 1970], [1693, 1970]], [[1693, 1970], [1691, 1968]],
            #       [[1691, 1968], [1690, 1968]], [[1690, 1968], [1688, 1966]], [[1688, 1966], [1687, 1966]],
            #       [[1687, 1966], [1685, 1964]], [[1685, 1964], [1684, 1964]], [[1684, 1964], [1682, 1962]],
            #       [[1682, 1962], [1681, 1962]], [[1681, 1962], [1679, 1960]], [[1679, 1960], [1678, 1960]],
            #       [[1678, 1960], [1676, 1958]], [[1676, 1958], [1675, 1958]], [[1675, 1958], [1673, 1956]],
            #       [[1673, 1956], [1672, 1956]], [[1672, 1956], [1670, 1954]], [[1670, 1954], [1669, 1954]],
            #       [[1669, 1954], [1667, 1952]], [[1667, 1952], [1666, 1952]], [[1666, 1952], [1664, 1950]],
            #       [[1664, 1950], [1663, 1950]], [[1663, 1950], [1661, 1948]], [[1661, 1948], [1660, 1948]],
            #       [[1660, 1948], [1658, 1946]], [[1658, 1946], [1657, 1946]], [[1657, 1946], [1655, 1944]],
            #       [[1655, 1944], [1654, 1944]], [[1654, 1944], [1652, 1942]], [[1652, 1942], [1651, 1942]],
            #       [[1651, 1942], [1649, 1940]], [[1649, 1940], [1648, 1940]], [[1648, 1940], [1646, 1938]],
            #       [[1646, 1938], [1645, 1938]], [[1645, 1938], [1643, 1936]], [[1643, 1936], [1642, 1936]],
            #       [[1642, 1936], [1640, 1934]], [[1640, 1934], [1639, 1934]], [[1639, 1934], [1637, 1932]],
            #       [[1637, 1932], [1636, 1932]], [[1636, 1932], [1634, 1930]], [[1634, 1930], [1633, 1930]],
            #       [[1633, 1930], [1631, 1928]], [[1631, 1928], [1630, 1928]], [[1630, 1928], [1628, 1926]],
            #       [[1628, 1926], [1627, 1926]], [[1627, 1926], [1625, 1924]], [[1625, 1924], [1624, 1924]],
            #       [[1624, 1924], [1622, 1922]], [[1622, 1922], [1621, 1922]], [[1621, 1922], [1619, 1920]],
            #       [[1619, 1920], [1618, 1920]], [[1618, 1920], [1616, 1918]], [[1616, 1918], [1615, 1918]],
            #       [[1615, 1918], [1613, 1916]], [[1613, 1916], [1612, 1916]], [[1612, 1916], [1610, 1914]],
            #       [[1610, 1914], [1609, 1914]], [[1609, 1914], [1607, 1912]], [[1607, 1912], [1606, 1912]],
            #       [[1606, 1912], [1604, 1910]], [[1604, 1910], [1603, 1910]], [[1603, 1910], [1601, 1908]],
            #       [[1601, 1908], [1600, 1908]], [[1600, 1908], [1598, 1906]], [[1598, 1906], [1597, 1906]],
            #       [[1597, 1906], [1595, 1904]], [[1595, 1904], [1594, 1904]], [[1594, 1904], [1592, 1902]],
            #       [[1592, 1902], [1591, 1902]], [[1591, 1902], [1589, 1900]], [[1589, 1900], [1588, 1900]],
            #       [[1588, 1900], [1586, 1898]], [[1586, 1898], [1585, 1898]], [[1585, 1898], [1583, 1896]],
            #       [[1583, 1896], [1582, 1896]], [[1582, 1896], [1580, 1894]], [[1580, 1894], [1579, 1894]],
            #       [[1579, 1894], [1577, 1892]], [[1577, 1892], [1576, 1892]], [[1576, 1892], [1574, 1890]],
            #       [[1574, 1890], [1573, 1890]], [[1573, 1890], [1571, 1888]], [[1571, 1888], [1570, 1888]],
            #       [[1570, 1888], [1568, 1886]], [[1568, 1886], [1567, 1886]], [[1567, 1886], [1565, 1884]],
            #       [[1565, 1884], [1564, 1884]], [[1564, 1884], [1562, 1882]], [[1562, 1882], [1561, 1882]],
            #       [[1561, 1882], [1559, 1880]], [[1559, 1880], [1558, 1880]], [[1558, 1880], [1556, 1878]],
            #       [[1556, 1878], [1555, 1878]], [[1555, 1878], [1553, 1876]], [[1553, 1876], [1552, 1876]],
            #       [[1552, 1876], [1550, 1874]], [[1550, 1874], [1549, 1874]], [[1549, 1874], [1547, 1872]],
            #       [[1547, 1872], [1546, 1872]], [[1546, 1872], [1544, 1870]], [[1544, 1870], [1543, 1870]],
            #       [[1543, 1870], [1541, 1868]], [[1541, 1868], [1540, 1868]], [[1540, 1868], [1538, 1866]],
            #       [[1538, 1866], [1537, 1866]], [[1537, 1866], [1536, 1865]], [[1536, 1865], [1536, 1864]],
            #       [[1536, 1864], [1535, 1863]], [[1535, 1863], [1534, 1863]], [[1534, 1863], [1532, 1861]],
            #       [[1532, 1861], [1531, 1861]], [[1531, 1861], [1529, 1859]], [[1529, 1859], [1528, 1859]],
            #       [[1528, 1859], [1526, 1857]], [[1526, 1857], [1525, 1857]], [[1525, 1857], [1524, 1856]],
            #       [[1524, 1856], [1523, 1856]], [[1523, 1856], [1521, 1854]], [[1521, 1854], [1520, 1854]],
            #       [[1520, 1854], [1518, 1852]], [[1518, 1852], [1517, 1852]], [[1517, 1852], [1515, 1850]],
            #       [[1515, 1850], [1514, 1850]], [[1514, 1850], [1512, 1848]], [[1512, 1848], [1511, 1848]],
            #       [[1511, 1848], [1510, 1847]], [[1510, 1847], [1509, 1847]], [[1509, 1847], [1507, 1845]],
            #       [[1507, 1845], [1506, 1845]], [[1506, 1845], [1504, 1843]], [[1504, 1843], [1503, 1843]],
            #       [[1503, 1843], [1501, 1841]], [[1501, 1841], [1500, 1841]], [[1500, 1841], [1498, 1839]],
            #       [[1498, 1839], [1497, 1839]], [[1497, 1839], [1496, 1838]], [[1496, 1838], [1495, 1838]],
            #       [[1495, 1838], [1493, 1836]], [[1493, 1836], [1492, 1836]], [[1492, 1836], [1490, 1834]],
            #       [[1490, 1834], [1489, 1834]], [[1489, 1834], [1487, 1832]], [[1487, 1832], [1486, 1832]],
            #       [[1486, 1832], [1484, 1830]], [[1484, 1830], [1483, 1830]], [[1483, 1830], [1482, 1829]],
            #       [[1482, 1829], [1481, 1829]], [[1481, 1829], [1480, 1828]], [[1480, 1828], [1480, 1827]],
            #       [[1480, 1827], [1479, 1826]], [[1479, 1826], [1478, 1826]], [[1478, 1826], [1476, 1824]],
            #       [[1476, 1824], [1475, 1824]], [[1475, 1824], [1473, 1822]], [[1473, 1822], [1472, 1822]],
            #       [[1472, 1822], [1470, 1820]], [[1470, 1820], [1469, 1820]], [[1469, 1820], [1468, 1819]],
            #       [[1468, 1819], [1467, 1819]], [[1467, 1819], [1465, 1817]], [[1465, 1817], [1464, 1817]],
            #       [[1464, 1817], [1462, 1815]], [[1462, 1815], [1461, 1815]], [[1461, 1815], [1459, 1813]],
            #       [[1459, 1813], [1458, 1813]], [[1458, 1813], [1456, 1811]], [[1456, 1811], [1455, 1811]],
            #       [[1455, 1811], [1454, 1810]], [[1454, 1810], [1453, 1810]], [[1453, 1810], [1451, 1808]],
            #       [[1451, 1808], [1450, 1808]], [[1450, 1808], [1448, 1806]], [[1448, 1806], [1447, 1806]],
            #       [[1447, 1806], [1445, 1804]], [[1445, 1804], [1444, 1804]], [[1444, 1804], [1442, 1802]],
            #       [[1442, 1802], [1441, 1802]], [[1441, 1802], [1440, 1801]], [[1440, 1801], [1439, 1801]],
            #       [[1439, 1801], [1437, 1799]], [[1437, 1799], [1436, 1799]], [[1436, 1799], [1434, 1797]],
            #       [[1434, 1797], [1433, 1797]], [[1433, 1797], [1431, 1795]], [[1431, 1795], [1430, 1795]],
            #       [[1430, 1795], [1429, 1794]], [[1429, 1794], [1428, 1794]], [[1428, 1794], [1427, 1793]],
            #       [[1427, 1793], [1427, 1792]], [[1427, 1792], [1426, 1791]], [[1426, 1791], [1425, 1791]],
            #       [[1425, 1791], [1423, 1789]], [[1423, 1789], [1422, 1789]], [[1422, 1789], [1420, 1787]],
            #       [[1420, 1787], [1419, 1787]], [[1419, 1787], [1417, 1785]], [[1417, 1785], [1416, 1785]],
            #       [[1416, 1785], [1415, 1784]], [[1415, 1784], [1414, 1784]], [[1414, 1784], [1412, 1782]],
            #       [[1412, 1782], [1411, 1782]], [[1411, 1782], [1409, 1780]], [[1409, 1780], [1408, 1780]],
            #       [[1408, 1780], [1406, 1778]], [[1406, 1778], [1405, 1778]], [[1405, 1778], [1403, 1776]],
            #       [[1403, 1776], [1402, 1776]], [[1402, 1776], [1401, 1775]], [[1401, 1775], [1400, 1775]],
            #       [[1400, 1775], [1398, 1773]], [[1398, 1773], [1397, 1773]], [[1397, 1773], [1395, 1771]],
            #       [[1395, 1771], [1394, 1771]], [[1394, 1771], [1392, 1769]], [[1392, 1769], [1391, 1769]],
            #       [[1391, 1769], [1389, 1767]], [[1389, 1767], [1388, 1767]], [[1388, 1767], [1387, 1766]],
            #       [[1387, 1766], [1386, 1766]], [[1386, 1766], [1384, 1764]], [[1384, 1764], [1383, 1764]],
            #       [[1383, 1764], [1381, 1762]], [[1381, 1762], [1380, 1762]], [[1380, 1762], [1378, 1760]],
            #       [[1378, 1760], [1377, 1760]], [[1377, 1760], [1375, 1758]], [[1375, 1758], [1374, 1758]],
            #       [[1374, 1758], [1373, 1757]], [[1373, 1757], [1372, 1757]], [[1372, 1757], [1371, 1756]],
            #       [[1371, 1756], [1371, 1754]], [[1371, 1754], [1368, 1754]], [[1368, 1754], [1367, 1753]],
            #       [[1367, 1753], [1367, 1750]], [[1367, 1750], [1368, 1749]], [[1368, 1749], [1368, 1737]],
            #       [[1368, 1737], [1369, 1736]], [[1369, 1736], [1369, 1724]], [[1369, 1724], [1370, 1723]],
            #       [[1370, 1723], [1370, 1710]], [[1370, 1710], [1371, 1709]], [[1371, 1709], [1371, 1697]],
            #       [[1371, 1697], [1372, 1696]], [[1372, 1696], [1372, 1686]], [[1372, 1686], [1132, 1686]],
            #       [[1132, 1686], [1127, 1691]], [[1127, 1691], [887, 1691]], [[887, 1691], [887, 1740]],
            #       [[887, 1740], [886, 1741]], [[886, 1741], [878, 1741]], [[878, 1741], [877, 1740]],
            #       [[877, 1740], [877, 1610]]], 2]
            # ]

            voronoi_data = []
            common_contour_line = []
            count = 0
            # outer_contour=[]
            common_outer_contour_line = []
            outer_count = 0
            iterative_obj = iterative_functions_class()
            for cont, contour in enumerate(all_contour_lines):
                for line1 in contour[0]:
                    x1, y1 = line1[0]
                    x2, y2 = line1[1]
                    # --get gradeint and centre point of line1
                    m1, c1 = iterative_obj.find_mc_of_line(x1, y1, x2, y2)
                    # center_x, center_y = iterative_obj.find_centre_of_line(line1)
                    for cont2, contour2 in enumerate(all_contour_lines):
                        if cont2 != cont:
                            for line2 in contour2[0]:
                                x3, y3 = line2[0]
                                x4, y4 = line2[1]
                                # --measure distance from centre_point in line to line2
                                # distance_from_point, intersect_x, intersect_y = iterative_obj.calculate_distance_from_point_to_line([center_x, center_y],line2)
                                # ---get gradient of line2
                                m2, c2 = iterative_obj.find_mc_of_line(x3, y3, x4, y4)
                                voronoi_line = False
                                if (m1 == 'a' and m2 == 'a') and (abs(x1 - x3) <= 2):
                                    voronoi_line = iterative_obj.find_line_subsets(line1, line2)
                                elif (m1 != 'a' and m2 != 'a') and abs(m1 - m2) < 0.02 and (abs(c1 - c2) <= 2):
                                    voronoi_line = iterative_obj.find_line_subsets(line1, line2)

                                # ----in line & line2 if gradient is appprox similar, close to each other: add as voronoi
                                if voronoi_line:
                                    voronoi_exists = False
                                    for row in common_contour_line:
                                        t1,t2,line_c = row
                                        if t1==contour[1] and t2==contour2[1] and iterative_obj.find_similarity_two_lines_ordered(line_c,line1):
                                            voronoi_exists = True
                                    if voronoi_exists==False:
                                        common_contour_line.append([])
                                        common_contour_line[count].append(contour[1])
                                        common_contour_line[count].append(contour2[1])
                                        common_contour_line[count].append(line1)
                                        count = count + 1
                                else:
                                    common_outer_contour_line.append([])
                                    common_outer_contour_line[outer_count].append(contour[1])
                                    common_outer_contour_line[outer_count].append(contour2[1])
                                    common_outer_contour_line[outer_count].append(line1)
                                    outer_count = outer_count + 1
            temp_voronoi_data = []
            for r,row in enumerate(common_contour_line):
                if r==0:
                    temp_voronoi_data.append(row)
                else:
                    round1_cord1 = row[0]
                    round1_cord2 = row[1]
                    exists = False
                    for v, vor_row in enumerate(temp_voronoi_data):
                        round2_cord1 = vor_row[0]
                        round2_cord2 = vor_row[1]
                        if round1_cord1==round2_cord1 and round1_cord2==round2_cord2:
                            exists = True
                            count = v
                            break
                    if exists is True:
                        temp_voronoi_data[count].append(row[2])
                    else:
                        temp_voronoi_data.append(row)

            #--check if [0,1 [line...]] and [1,0[line2.....] exists
            for r1,row1 in enumerate(temp_voronoi_data):
                if r1 == 0:
                    voronoi_data.append(row1)
                else:
                    exists = False
                    for r2, row2 in enumerate(voronoi_data):
                        if row2[0]==row1[1] and row2[1]==row1[0]:
                            exists = True
                            break
                    if exists == False:
                        voronoi_data.append(row1)



            if debug_mode:
                contour_image = cv2.imread(region_grow_path+name+'_STEP_01.png',cv2.IMREAD_COLOR)
                for r, row in enumerate(voronoi_data):
                    contour_image_copy = contour_image.copy()
                    for v_line in row[2:]:
                        cv2.line(contour_image_copy,(tuple(v_line[0])),(tuple(v_line[1])),(0,0,255),2,cv2.cv.CV_AA)
                    one_text, second_text = False, False
                    for row_num, text in enumerate(open_plan_text_cordinate):
                        if row_num==row[0]:
                            voronoi_text_cordinate = text
                            cv2.circle(contour_image_copy, (tuple(voronoi_text_cordinate)), 5, (255, 0, 0), -1)
                            one_text = True
                        if row_num==row[1]:
                            voronoi_text_cordinate = text
                            cv2.circle(contour_image_copy, (tuple(voronoi_text_cordinate)), 5, (255, 0, 0), -1)
                            second_text = True
                        if one_text and second_text:
                            break
                    # cv2.circle(contour_image_copy, (tuple(row[0])), 5, (255, 0, 0), -1)
                    # cv2.circle(contour_image_copy, (tuple(row[1])), 5, (255, 0, 0), -1)
                    cv2.imwrite(region_grow_path+name+' : '+str(r)+'_STEP_04.png', contour_image_copy)
                cv2.imwrite(region_grow_path+name+'_STEP_04.png', contour_image)

                outer_cont_image = ~(np.zeros((gray_height,gray_width,3), np.uint8))
                for row in common_outer_contour_line:
                    for line_c in row[2:]:
                        cv2.line(outer_cont_image,(tuple(line_c[0])),(tuple(line_c[1])),(0,0,0),1,cv2.cv.CV_AA)
                cv2.imwrite(region_grow_path+name+'_STEP_05.png', outer_cont_image)

                cv2.imwrite(open_plan_path+image_name, contour_image)

                voronoi_image = cv2.imread(region_grow_path+name+'_STEP_01.png',cv2.IMREAD_COLOR)
                # for each in redraw_contours:
                #     cv2.drawContours(voronoi_image, each, -1, (0,0,0), 1,cv2.cv.CV_AA)
                for row in voronoi_data:
                    for v_line in row[2:]:
                        cv2.line(voronoi_image,(tuple(v_line[0])),(tuple(v_line[1])),(255,0,0),2,cv2.cv.CV_AA)
                for cordinate in open_plan_text_cordinate:
                    cv2.circle(voronoi_image, (tuple(cordinate)), 5, (0, 0, 255), -1)
                cv2.imwrite(region_grow_path+name+'_STEP_06.png', voronoi_image)

            major_voronoi_data.append(voronoi_data)
            major_all_contour_lines.append(all_contour_lines)

            # print '-----voronoi_data----'
            # for row in voronoi_data:
            #     print row
            # print '-----all_contour_lines----'
            # for row in all_contour_lines:
            #     print row
















#-----------------------------------------------------------------extending edges
            # debug_mode= True
            common_image_name = image_name
            common_name = name
            image_edge_extensions_path=edge_extensions_path+common_name+'/'
            os.mkdir(image_edge_extensions_path)
            image_path = open_plan_path+common_image_name
            open_plan_image = cv2.imread(image_path,cv2.IMREAD_GRAYSCALE)
            openplan_height,openplan_width = open_plan_image.shape

            image_contour_lines = current_open_plan_contour_data[1]
            contour_to_check = current_open_plan_contour_data[2]
            # ret,thresh = cv2.threshold(open_plan_image,0,255,1)
            # contours_to_be_reused,hierachy = cv2.findContours(thresh,1,2)
            #
            # cordinates =[]
            # for contour, cnt in enumerate(contours_to_be_reused):
            #     #-------since for 4 cornered shape it caused 8 nd 27 contours, i chose just first contour
            #     if contour==1:
            #         cordinates=cnt
            #         break

            original_edges_and_extensions = []
            empty_new_image = ~(np.zeros((openplan_height,openplan_width,3), np.uint8))
            #----changed line thickness to 2 since when its 1 findcontours is not finding it
            cv2.drawContours(empty_new_image, [contour_to_check], -1, (0,0,0), 2,cv2.cv.CV_AA)
            # if debug_mode:
            cv2.imwrite(image_edge_extensions_path+'_STEP_01.png',empty_new_image)
            os.remove(image_path)
            cv2.imwrite(image_path,empty_new_image)


            count = 0
            # radius = avg_door_width/4
            #----set minimum line length based on level since with each level small line segments length increase
            plan_level= int(open_plan_level)
            # print '-------------------------------',plan_level,'-----------------------'
            if plan_level<3:
                current_min_line_length = avg_door_width/40
            elif plan_level==3:
                current_min_line_length = avg_door_width/30
            else:
                current_min_line_length = avg_door_width/20

            #line extraction and finding intersections
            for l1, level1 in enumerate(image_contour_lines):
                x1,y1 = level1[0]
                x2,y2 = level1[1]
                #--check if length of currnet line is not too small
                line_length = math.hypot(x2 - x1, y2 - y1)
                if line_length>current_min_line_length:
                    #----find if two points beside line are inside contour
                    line_obj1 = line()
                    point_inside_line = line_obj1.find_points_next_to_end_points_in_line(x1,y1,x2,y2,line_length,contour_to_check)
                    if point_inside_line:
                        # print 'loop 3', l1,level1
                        #--find line numbers to find before and after lines
                        line_number_before, line_number_after = 0,0
                        if l1==0:
                            line_number_before = len(image_contour_lines)-1
                        elif l1== len(image_contour_lines)-1:
                            line_number_after = 0
                        else:
                            line_number_before = l1-1
                            line_number_after = l1+1
                        #--find length of line before current line
                        line_before = image_contour_lines[line_number_before]
                        before_x1,before_y1 = line_before[0]
                        before_x2,before_y2 = line_before[1]
                        length_line_before = math.hypot(before_x2 - before_x1, before_y2 - before_y1)
                        #--find length of line after current line
                        line_after = image_contour_lines[line_number_after]
                        after_x1,after_y1 = line_after[0]
                        after_x2,after_y2 = line_after[1]
                        length_line_after = math.hypot(after_x2 - after_x1, after_y2 - after_y1)
                        #--if line before OR after are more than door width: extend that line
                        if length_line_before>int(avg_door_width/10) or length_line_after>int(avg_door_width/10) or line_length>current_min_line_length:
                            # print 'loop 4', l1,level1
                            for l2, level2 in enumerate(image_contour_lines):
                                if l1==0:
                                    lower = len(image_contour_lines)-1
                                else:
                                    lower = l1-1
                                if l1==len(image_contour_lines)-1:
                                    upper = 0
                                else:
                                    upper = l1+1
                                check_value = False
                                if l2==l1 or l2==upper or l2==lower:
                                    check_value = True
                                if check_value == False:
                                    # if l2>l1:
                                    x3,y3 = level2[0]
                                    x4,y4 = level2[1]
                                    max_x1 = max(x3,x4)
                                    min_x1 = min(x3,x4)
                                    max_y1 = max(y3,y4)
                                    min_y1 = min(y3,y4)
                                    line_obj = line()
                                    intersect_x,intersect_y = line_obj.find_contour_intersections(x1,y1,x2,y2,x3,y3,x4,y4)
                                    if (intersect_x>=min_x1 and intersect_x<=max_x1 and intersect_y>=min_y1 and intersect_y<=max_y1):
                                        # print 'loop 5', l1,level1
                                        distance_to_first_cordinate_line1 = math.hypot(intersect_x - x1, intersect_y - y1)
                                        distance_to_second_cordinate_line1 = math.hypot(intersect_x - x2, intersect_y - y2)
                                        closest_point_to_intersection = min(distance_to_first_cordinate_line1,distance_to_second_cordinate_line1)
                                        extended_edge_p1_x,extended_edge_p1_y,extended_edge_p2_x,extended_edge_p2_y = 0,0,0,0
                                        if closest_point_to_intersection==distance_to_first_cordinate_line1:
                                            extended_edge_p1_x,extended_edge_p1_y,extended_edge_p2_x,extended_edge_p2_y = x1,y1,intersect_x,intersect_y
                                        if closest_point_to_intersection==distance_to_second_cordinate_line1:
                                            extended_edge_p1_x,extended_edge_p1_y,extended_edge_p2_x,extended_edge_p2_y = x2,y2,intersect_x,intersect_y

                                        max_e_x = max(extended_edge_p1_x,extended_edge_p2_x)
                                        min_e_x = min(extended_edge_p1_x,extended_edge_p2_x)
                                        max_e_y = max(extended_edge_p1_y,extended_edge_p2_y)
                                        min_e_y = min(extended_edge_p1_y,extended_edge_p2_y)
                                        text_intersects_flag=False
                                        line_obj=line()
                                        # -----check if improved_line intersects with the text boundary box
                                        for t,text_data in enumerate(current_open_plan_contour_data[3:]):
                                            for bb_line in text_data[3]:
                                                # print bb_line
                                                bb_line_x1,bb_line_y1 = bb_line[0]
                                                bb_line_x2,bb_line_y2 = bb_line[1]
                                                bb_line_max_x = max(bb_line_x1,bb_line_x2)
                                                bb_line_min_x = min(bb_line_x1,bb_line_x2)
                                                bb_line_max_y = max(bb_line_y1,bb_line_y2)
                                                bb_line_min_y = min(bb_line_y1,bb_line_y2)
                                                intersect_x1, intersect_y1 = line_obj.find_contour_intersections(x1,y1,x2,y2,bb_line_x1,bb_line_y1,bb_line_x2,bb_line_y2)
                                                if (intersect_x1>=min_e_x and intersect_x1<=max_e_x and intersect_y1>=min_e_y and intersect_y1<=max_e_y) and (intersect_x1>=bb_line_min_x and intersect_x1<=bb_line_max_x and intersect_y1>=bb_line_min_y and intersect_y1<=bb_line_max_y):
                                                    text_intersects_flag= True
                                                    break
                                            if text_intersects_flag:
                                                break

                                        if not text_intersects_flag:
                                            num_of_points_outside_contour = line_obj.check_line_inside_contour_special(extended_edge_p1_x,extended_edge_p1_y,extended_edge_p2_x,extended_edge_p2_y,contour_to_check)
                                            if num_of_points_outside_contour<10:
                                                ee_x1, ee_y1 = int(extended_edge_p1_x), int(extended_edge_p1_y)
                                                ee_x2, ee_y2 = int(extended_edge_p2_x), int(extended_edge_p2_y)

                                                original_edges_and_extensions.append([[[x1, y1], [x2, y2]],
                                                                                      [[ee_x1, ee_y1], [ee_x2, ee_y2]]])

                                                cv2.circle(empty_new_image,(intersect_x,intersect_y),5, (0, 255, 0), -1)
                                                cv2.circle(empty_new_image,(extended_edge_p1_x,extended_edge_p1_y),5, (0, 0, 0), -1)
                                                count = count+1
            # cv2.imwrite(image_edge_extensions_path+'_TESTTTTTTTTTTTT.png',extended_line_sections_image)
            if debug_mode:
                cv2.imwrite(image_edge_extensions_path+'_STEP_03.png',empty_new_image)
                extended_line_sections_image = ~(np.zeros((openplan_height,openplan_width,3), np.uint8))
                cv2.drawContours(extended_line_sections_image, [contour_to_check], -1, (0, 0, 0), 2,cv2.CV_AA)
                for line_row in original_edges_and_extensions:
                    point = line_row[1]
                    x1,y1 = point[0]
                    x2,y2 = point[1]
                    cv2.line(extended_line_sections_image,(x1,y1),(x2,y2),(0,255,0),2,cv2.CV_AA)
                cv2.imwrite(image_edge_extensions_path+'_STEP_04.png',extended_line_sections_image)



            generate_other_lines_obj = generate_other_lines_class()
            original_edges_and_extensions, interesting_lines,contour_angle_data = generate_other_lines_obj.generate_lines_from_interesting_points(
                name, image_edge_extensions_path, original_edges_and_extensions,
                image_contour_lines,text_bounding_boxes, open_plan_text_cordinate,
                avg_door_width,openplan_height,openplan_width)

            major_edge_extensions.append(original_edges_and_extensions)



            #-----------------------find shortest path
            image_shortest_path_path=shortest_path_path+common_name+'/'
            os.mkdir(image_shortest_path_path)

            contour_check = contour_to_check
            outer_cont = ~(np.zeros((openplan_height,openplan_width,3), np.uint8))
            cv2.drawContours(outer_cont,[contour_check],0,(0,0,0),1)
            cv2.imwrite(image_shortest_path_path+'_STEP_01.png',outer_cont)

            #-------start process of rings
            image = Image.open(image_shortest_path_path+'_STEP_01.png')
            width, height = image.size
            pixels = image.load()
            outer_cont_copy1 = outer_cont.copy()

            shortest_path_data = []
            vornoi_data= major_voronoi_data[common_room_number]
            for r_count, v_row in enumerate(vornoi_data):
                top_threshold_pixel_array = [0 for c in range(width)]
                bottom_threshold_pixel_array = [height for c in range(width)]
                left_threshold_pixel_array = [0 for c in range(height)]
                right_threshold_pixel_array = [width for c in range(height)]
                one_text, second_text = False, False
                starting_cordinate, end_cordinate, starting_row_ID, end_row_ID = 0,0,0,0
                for row_num, text_cord in enumerate(open_plan_text_cordinate):
                    if row_num==v_row[0]:
                        starting_cordinate = text_cord
                        starting_row_ID = row_num
                        one_text = True
                    if row_num==v_row[1]:
                        end_cordinate = text_cord
                        end_row_ID = row_num
                        second_text = True
                    if one_text and second_text:
                        break
                # if r_count==0:
                pixel_data = [[0 for column in range(width)] for row in range(height)]
                shortest_path_points = []
                color_val_obj = color_check_class(pixels, top_threshold_pixel_array,bottom_threshold_pixel_array,left_threshold_pixel_array, right_threshold_pixel_array)
                temp_array_copy = []
                temp_ring_array = []
                #------start of single txt co-ordinate
                end_ring = False
                for ring_count in range(5000):
                    if ring_count == 0:
                        column,row = starting_cordinate
                        pixel_data[row][column]= 'S'
                        column1,row1 = end_cordinate
                        pixel_data[row1][column1]= 'F'
                        temp_array_copy.append(starting_cordinate)

                    for row in temp_array_copy:
                        col_coord, row_coord = row
                        top_pixel = col_coord, row_coord-1
                        left_pixel = col_coord-1, row_coord
                        bottom_pixel = col_coord, row_coord+1
                        right_pixel = col_coord+1, row_coord


                        top_col,top_row = top_pixel
                        if pixel_data[top_row][top_col] is 'F':
                            shortest_path_points.append([top_col,top_row])
                            shortest_path_points = color_val_obj.get_shortest_path(row,pixel_data,shortest_path_points)
                            end_ring = True
                            break
                        else:
                            if (pixel_data[top_row][top_col] ==0) and (cv2.pointPolygonTest(contour_check,(top_col,top_row ),False)==1):
                                temp_ring_array, pixel_data = color_val_obj.shortest_path_color_match(top_pixel,pixel_data,temp_ring_array,'T')

                        left_col,left_row = left_pixel
                        if pixel_data[left_row][left_col] is 'F':
                            shortest_path_points.append([left_col,left_row])
                            shortest_path_points = color_val_obj.get_shortest_path(row,pixel_data,shortest_path_points)
                            end_ring = True
                            break
                        else:
                            if (pixel_data[left_row][left_col] ==0)and (cv2.pointPolygonTest(contour_check,(left_col,left_row),False)==1):
                                temp_ring_array, pixel_data = color_val_obj.shortest_path_color_match(left_pixel,pixel_data,temp_ring_array,'L')

                        bottom_col,bottom_row = bottom_pixel
                        if pixel_data[bottom_row][bottom_col] is 'F':
                            shortest_path_points.append([bottom_col,bottom_row])
                            shortest_path_points = color_val_obj.get_shortest_path(row,pixel_data,shortest_path_points)
                            end_ring = True
                            break
                        else:
                            if (pixel_data[bottom_row][bottom_col] ==0)and (cv2.pointPolygonTest(contour_check,(bottom_col,bottom_row),False)==1):
                                temp_ring_array, pixel_data = color_val_obj.shortest_path_color_match(bottom_pixel,pixel_data,temp_ring_array,'B')

                        right_col,right_row = right_pixel
                        if pixel_data[right_row][right_col] is 'F':
                            shortest_path_points.append([right_col,right_row])
                            shortest_path_points = color_val_obj.get_shortest_path(row,pixel_data,shortest_path_points)
                            end_ring = True
                            break
                        else:
                            if (pixel_data[right_row][right_col] ==0)and (cv2.pointPolygonTest(contour_check,(right_col,right_row),False)==1):
                                temp_ring_array, pixel_data = color_val_obj.shortest_path_color_match(right_pixel,pixel_data,temp_ring_array,'R')

                    temp_array_copy = temp_ring_array
                    temp_ring_array = []
                    if end_ring:
                        break

                shortest_path =[]
                shortest_path_data.append([])
                shortest_path_data[r_count].append(end_row_ID)
                shortest_path_data[r_count].append(starting_row_ID)

                for p,point in enumerate(shortest_path_points):
                    x,y = point
                    if p==0:
                        shortest_path.append([])
                        shortest_path[p].append([x,y])
                    elif p==len(shortest_path_points)-1:
                        shortest_path[p-1].append([x,y])
                    else:
                        shortest_path.append([])
                        shortest_path[p].append([x,y])
                        shortest_path[p-1].append([x,y])


                outer_cont_copy2 = outer_cont.copy()
                for each_line in shortest_path:
                    shortest_path_data[r_count].append(each_line)
                    if debug_mode:
                        cv2.line(outer_cont_copy2,(tuple(each_line[0])),(tuple(each_line[1])),(0,0,255),2,cv2.cv.CV_AA)
                        cv2.line(outer_cont_copy1,(tuple(each_line[0])),(tuple(each_line[1])),(0,0,255),2,cv2.cv.CV_AA)
                if debug_mode:
                    cv2.imwrite(image_shortest_path_path+'_STEP_02_'+str(r_count)+'.png',outer_cont_copy2)
            if debug_mode:
                cv2.imwrite(image_shortest_path_path+'_STEP_03.png',outer_cont_copy1)
            major_shortest_path_data.append(shortest_path_data)

            # print '---shortest path data--'
            # for row in shortest_path_data:
            #     print row
            # print '---open_plan_text_cordinate--'
            # for row in open_plan_text_cordinate:
            #     print row

















#----------------------scoring
        # open_area_list = os.listdir(open_plan_path)
        # open_area_list.sort()

        #------go through each open plan (this was found earlier by checking the number of text labels in each room)
        # for room_num,room in enumerate(open_area_list):
            room_image_org = cv2.imread(open_plan_path+room,cv2.IMREAD_GRAYSCALE)
            img_height,img_width = room_image_org.shape
            # image_name = str(room)
            # name = image_name[0:-4]
            #--- set path to store all outputs
            c_line_scoring_path=line_scoring_path+common_name+'/'
            os.mkdir(c_line_scoring_path)

            #----------create a copy of floor plan to be used later
            floor_plan_copy = room_image_org.copy()

            #-----------------put all voronoi lines and edge extensions in to the same image
            all_contour_lines=major_all_contour_lines[common_room_number]
            voronoi_data= major_voronoi_data[common_room_number]
            # edge_extensions_between_coordinates=major_edge_extensions_between_coordinates[common_room_number]
            # if debug_mode:
            #     for row in voronoi_data:
            #         for v_line in row[2:]:
            #             cv2.line(room_image_org,(tuple(v_line[0])),(tuple(v_line[1])),(0,0,0),2,cv2.cv.CV_AA)
            #     for row2 in edge_extensions_between_coordinates:
            #         for e_line1 in row2[2:]:
            #             e_line = e_line1[0]
            #             cv2.line(room_image_org,(tuple(e_line[0])),(tuple(e_line[1])),(0,0,255),2,cv2.cv.CV_AA)
            #     cv2.imwrite(c_line_scoring_path+"_STEP_07__ALL LINES.png", room_image_org)

            # print 'edge_extensions_between_coordinates'
            # for row in edge_extensions_between_coordinates:
            #     print row


            #-------sort contours to order them by text_cordinate_ID
            partition_area_lines = sorted(all_contour_lines,key=itemgetter(1))


            only_contour_lines =[]
            for cont, partiton_row in enumerate(partition_area_lines):
                # print partiton_row[1]
                contour_text_ID = partiton_row[1]
                only_contour_lines.append([])
                only_contour_lines[cont].append(contour_text_ID)
                for contour_line in partiton_row[0]:
                    x1,y1 = contour_line[0]
                    x2,y2 = contour_line[1]
                    contour_line_is_voronoi_line= False
                    for voronoi_row in voronoi_data:
                        exists = False
                        # if voronoi_row[0]==contour_text_ID:
                        for v_line in voronoi_row[2:]:
                            x3,y3 = v_line[0]
                            x4,y4 = v_line[1]
                            if (abs(x1-x3)<5 and abs(y1-y3)<5 and abs(x2-x4)<5 and abs(y2-y4)<5) or (abs(x1-x4)<5 and abs(y1-y4)<5 and abs(x2-x3)<5 and abs(y2-y3)<5):
                                contour_line_is_voronoi_line = True
                                exists= True
                                break
                        if exists:
                            break
                    if contour_line_is_voronoi_line==False:
                        only_contour_lines[cont].append(contour_line)

            if debug_mode:
                for cont, contour in enumerate(only_contour_lines):
                    test_image =cv2.imread(open_plan_path+room,cv2.IMREAD_COLOR)
                    for l1,each_line in enumerate(contour[1:]):
                        cv2.line(test_image,(tuple(each_line[0])),(tuple(each_line[1])),(0,0,255),2,cv2.cv.CV_AA)
                    cv2.imwrite(c_line_scoring_path+str(cont)+"_TEST_IMAGE.png", test_image)



            # -sort contours to order them by text_cordinate_ID
            partition_area_lines = sorted(all_contour_lines, key=itemgetter(1))

            # print '-----voronoi_data111----'
            # for row in voronoi_data:
            #     print row
            # print '----partition_area_lines111-----'
            # for row in partition_area_lines:
            #     print row

            # -get ordered voronoi lines


            voronoi_lines_in_contour_line_order = []
            count = 0
            iterative_obj = iterative_functions_class()
            # voronoi_data = [
            #     [1, 2, [[1237, 1063], [1531, 1063]]],
            #     [1, 0, [[2176, 650], [2242, 650]], [[2242, 650], [2072, 650]], [[2072, 650], [2068, 646]]],
            #     [3, 2, [[1499, 1861], [1242, 1604]], [[1242, 1604], [1242, 1476]]]
            # ]
            # partition_area_lines = [
            #     [[[[1957, 254], [1957, 422]], [[1957, 422], [2067, 422]], [[2067, 422], [2068, 423]],
            #       [[2068, 423], [2068, 645]], [[2068, 645], [2072, 649]], [[2072, 649], [2242, 649]],
            #       [[2242, 649], [2242, 258]], [[2242, 258], [2148, 258]], [[2148, 258], [2147, 257]],
            #       [[2147, 257], [2147, 254]], [[2147, 254], [1957, 254]]], 0],
            #     [[[[2068, 646], [2068, 663]], [[2068, 663], [2067, 664]], [[2067, 664], [2056, 664]],
            #       [[2056, 664], [2055, 663]], [[2055, 663], [2055, 656]], [[2055, 656], [1944, 656]],
            #       [[1944, 656], [1944, 665]], [[1944, 665], [1943, 666]], [[1943, 666], [1647, 666]],
            #       [[1647, 666], [1647, 908]], [[1647, 908], [1646, 909]], [[1646, 909], [1323, 909]],
            #       [[1323, 909], [1323, 919]], [[1323, 919], [1322, 920]], [[1322, 920], [1311, 920]],
            #       [[1311, 920], [1311, 1026]], [[1311, 1026], [1310, 1027]], [[1310, 1027], [1237, 1027]],
            #       [[1237, 1027], [1237, 1063]], [[1237, 1063], [1531, 1063]], [[1531, 1063], [1531, 1012]],
            #       [[1531, 1012], [1532, 1011]], [[1532, 1011], [1755, 1011]], [[1755, 1011], [1755, 769]],
            #       [[1755, 769], [1756, 768]], [[1756, 768], [2358, 768]], [[2358, 768], [2358, 663]],
            #       [[2358, 663], [2176, 663]], [[2176, 663], [2175, 662]], [[2175, 662], [2175, 651]],
            #       [[2175, 651], [2176, 650]], [[2176, 650], [2242, 650]], [[2242, 650], [2072, 650]],
            #       [[2072, 650], [2068, 646]]], 1],
            #     [[[[1237, 1064], [1237, 1134]], [[1237, 1134], [1310, 1134]], [[1310, 1134], [1311, 1135]],
            #       [[1311, 1135], [1311, 1165]], [[1311, 1165], [1310, 1166]], [[1310, 1166], [1238, 1166]],
            #       [[1238, 1166], [1238, 1462]], [[1238, 1462], [1310, 1462]], [[1310, 1462], [1311, 1463]],
            #       [[1311, 1463], [1311, 1475]], [[1311, 1475], [1310, 1476]], [[1310, 1476], [1243, 1476]],
            #       [[1243, 1476], [1243, 1604]], [[1243, 1604], [1500, 1861]], [[1500, 1861], [1796, 1861]],
            #       [[1796, 1861], [1796, 1726]], [[1796, 1726], [1797, 1725]], [[1797, 1725], [1804, 1725]],
            #       [[1804, 1725], [1804, 1438]], [[1804, 1438], [1532, 1438]], [[1532, 1438], [1531, 1437]],
            #       [[1531, 1437], [1531, 1064]], [[1531, 1064], [1237, 1064]]], 2],
            #     [[[[1027, 1476], [1027, 2095]], [[1027, 2095], [1419, 2095]], [[1419, 2095], [1419, 1862]],
            #       [[1419, 1862], [1420, 1861]], [[1420, 1861], [1499, 1861]], [[1499, 1861], [1242, 1604]],
            #       [[1242, 1604], [1242, 1476]], [[1242, 1476], [1027, 1476]]], 3]
            #
            # ]



            # --------order the voronoi lines in order of contour lines so that later on voronoi lines can be drawn easily

            for cont, contour in enumerate(partition_area_lines):
                # --take contourID as contour_text_ID
                contour_text_ID = contour[-1]
                voronoi_lines_in_contour_line_order.append([])
                # --add contourID to array
                voronoi_lines_in_contour_line_order[count].append(contour_text_ID)
                # -----iterate through contour[0] because all line details are appended as a single element (elelment 0) in to line
                num_of_lines = len(contour[0])
                # --take contour lines 1 by 1
                for l1, line1 in enumerate(contour[0]):
                    x1, y1 = line1[0]
                    x2, y2 = line1[1]
                    m1, c1 = iterative_obj.find_mc_of_line(x1, y1, x2, y2)
                    # --per contour line, compare with voronoi data
                    for cont2, contour2 in enumerate(voronoi_data):
                        # --take 2 textIDs in voronoi row
                        text1 = contour2[0]
                        text2 = contour2[1]
                        # --if current contour(contour_text_ID)==text1: we are comparing same contour and its related voronoi lines
                        if contour_text_ID == text1 or contour_text_ID == text2:
                            # --take voronoi line data
                            for line2 in contour2[2:]:
                                x3, y3 = line2[0]
                                x4, y4 = line2[1]
                                m2, c2 = iterative_obj.find_mc_of_line(x3, y3, x4, y4)
                                # --check if current_contour line and current_voronoi is paralell and very close
                                voronoi_line = False
                                if (m1 == 'a' and m2 == 'a') and (abs(x1 - x3) <= 2):
                                    voronoi_line = iterative_obj.find_line_subsets(line1, line2)
                                elif (m1 != 'a' and m2 != 'a') and abs(m1 - m2) < 0.02 and (abs(c1 - c2) <= 2):
                                    voronoi_line = iterative_obj.find_line_subsets(line1, line2)
                                # ----in line & line2 if gradient is appprox similar, close to each other: add as voronoi
                                if voronoi_line:
                                    # --add voronoi line, textID related to it, line number for later use
                                    if contour_text_ID == text1:
                                        text_num = text2
                                    elif contour_text_ID == text2:
                                        text_num = text1
                                    else:
                                        continue

                                    #--check if this line already exists
                                    vor_data_exists_flag = False
                                    if len(voronoi_lines_in_contour_line_order[count])>1:
                                        cur_line_data = voronoi_lines_in_contour_line_order[count][1:]
                                        for existing_line in cur_line_data:
                                            line_cord,t2,l_num = existing_line
                                            if t2==text_num and l_num==l1:
                                                vor_data_exists_flag = True
                                                break
                                    if vor_data_exists_flag==False:
                                        voronoi_lines_in_contour_line_order[count].append([line1, text_num, l1])
                                    # elif contour_text_ID == text2:
                                    #     voronoi_lines_in_contour_line_order[count].append([line1, text1, l1])
                voronoi_lines_in_contour_line_order[count].append(num_of_lines)
                count = count + 1

            # ------- STEP 02 : Find Boundaries
            # Explanation:
            # Using 'voronoi_lines_in_contour_line_order' find boundaries
            # At the same time order voronoi lines according to boundaries
            # -- how to order voronoi lines:
            # -- in the 'voronoi_lines_in_contour_line_order[]' we had [txt1,[line1,txt2,line_num],[line2,txt3,line_num],[line3,txt4,line_num]]
            # -- so for each row: add line to 'seperated_voronoi_lines' & add textID to 'boundaries'
            # -- order all lines by line_num in voronoi_lines_in_contour_line_order[row1] each boundary
            # -- sort boundaries in each row by textID2s
            # --refer book

            # print '----voronoi_lines_in_contour_line_order---'
            # for row in voronoi_lines_in_contour_line_order:
            #     print row

            seperated_voronoi_lines = []
            boundaries = []
            for r, row in enumerate(voronoi_lines_in_contour_line_order):
                # print 'row',row
                text1 = row[0]
                line_data = row[1:-1]
                temp_array_vor = []
                temp_boundaries = []
                last_line_number, last_textID = -1, -1
                x1, y1, x2, y2, x3, y3, x4, y4 = 0, 0, 0, 0, 0, 0, 0, 0
                # ---boundaries list creation
                boundaries.append([])
                boundaries[r].append(text1)
                # ---seperated_voronoi_lines list creation
                seperated_voronoi_lines.append([])
                seperated_voronoi_lines[r].append(text1)
                for e, element in enumerate(line_data):
                    # print 'element',element
                    # --take cords of voronoi line
                    current_line = element[0]
                    x1, y1 = current_line[0]
                    x2, y2 = current_line[1]
                    # --take textID current voronoi line is connected to
                    text_ID = element[1]
                    # --line number of voronoi line (according to contour line order)
                    line_number = element[2]
                    # --if first element: just add everything to 2 arrays
                    if e == 0:
                        temp_array_vor.append([current_line])
                        temp_boundaries.append([text_ID])
                        last_line_number = line_number
                        x3, y3, x4, y4 = x1, y1, x2, y2

                    else:
                        # ---check if last line in array and is sharing a cordinate with very first line (e==0)
                        if e == len(row[1:-1]) - 1 and (
                            (x1 == x3 and y1 == y3 or x1 == x4 and y1 == y4) or (
                                    x2 == x4 and y2 == y4 or x2 == x3 and y2 == y3)):

                            if len(row[1:-1]) == 2:
                                if (line_number - last_line_number) == 1:
                                    temp_array_vor[-1].append(current_line)
                                    exists_flag = False
                                    for t_row1 in temp_boundaries[-1]:
                                        if t_row1 == text_ID:
                                            exists_flag = True
                                            break
                                    if exists_flag == False:
                                        temp_boundaries[-1].append(text_ID)
                                else:
                                    temp_array_vor[0].insert(0, (current_line))
                                    exists_flag = False
                                    for t_row1 in temp_boundaries[0]:
                                        if t_row1 == text_ID:
                                            exists_flag = True
                                            break
                                    if exists_flag == False:
                                        temp_boundaries[0].insert(0, text_ID)
                            # -----check is this last line is also connected to line before it
                            elif (line_number - last_line_number) <= 1:
                                # -----insert current line to last element in temp_array_vor
                                if (line_number - last_line_number) == 1:
                                    temp_array_vor[-1].append(current_line)
                                # ---reverse alst element in temp_array_vor, & add each line to temp_array_vor[0]
                                for each_el in list(reversed(temp_array_vor[-1])):
                                    temp_array_vor[0].insert(0, each_el)
                                # -----delete last element
                                del temp_array_vor[-1]

                                # -----------------textID array change----------
                                # -------take all textIDs from last element and put them in first one too
                                # -------do the same for text_cordinates array
                                all_last_boundary_texts = temp_boundaries[-1]
                                texts = []
                                for text_num, last_boundary_text in enumerate(
                                        list(reversed(all_last_boundary_texts))):
                                    exists_flag = False
                                    for t_row1 in temp_boundaries[0]:
                                        if t_row1 == last_boundary_text:
                                            exists_flag = True
                                    if exists_flag == False:
                                        temp_boundaries[0].insert(0, last_boundary_text)
                                        texts.append(last_boundary_text)
                                if len(list(set(all_last_boundary_texts) - set(texts))) == 0:
                                    del temp_boundaries[-1]
                                else:
                                    temp_boundaries[-1] == list(set(all_last_boundary_texts) - set(texts))
                            # -------if last line is connected to first line but is not connected to line taken in array immediately before
                            else:
                                # ---add current line to temp_array_vor[0]
                                temp_array_vor[0].insert(0, current_line)
                                exists_flag = False
                                for t_row1 in temp_boundaries[0]:
                                    if t_row1 == text_ID:
                                        exists_flag = True
                                        break
                                if exists_flag == False:
                                    temp_boundaries[0].insert(0, text_ID)
                            # -----common to both if/else
                            last_line_number = line_number
                        # --------if NOT last line but last entered line and current line is connected
                        elif (line_number - last_line_number) <= 1:
                            if (line_number - last_line_number) == 1:
                                # if ~((x1==x3 and y1==y3 or x1==x4 and y1==y4) or (x2==x4 and y2==y4  or x2==x3 and y2==y3)):
                                temp_array_vor[-1].append(current_line)
                            exists_flag = False
                            for t_row1 in temp_boundaries[-1]:
                                if t_row1 == text_ID:
                                    exists_flag = True
                                    break
                            if exists_flag == False:
                                temp_boundaries[-1].append(text_ID)
                            last_line_number = line_number
                        # ---- if NOT last line but is creating a new/seperate boundary
                        else:
                            temp_array_vor.append([current_line])
                            temp_boundaries.append([text_ID])
                            last_line_number = line_number
                seperated_voronoi_lines[r].append(temp_array_vor)
                boundaries[r].append(temp_boundaries)

            # print '----seperated_voronoi_lines---'
            # for row in seperated_voronoi_lines:
            #     print row
            # print '----boundaries---'
            # for row in boundaries:
            #     print row

            #-------------write contour of each partition in to new image
            output_path = c_line_scoring_path+'sequential_output/'
            os.mkdir(output_path)
            open_plan_partition_path = c_line_scoring_path+'open_plan_partitions/'
            os.mkdir(open_plan_partition_path)

            for r,row in enumerate(partition_area_lines):
                contour_image = ~(np.zeros((img_height,img_width,3), np.uint8))
                for each_line in row[0]:
                    cv2.line(contour_image,(tuple(each_line[0])),(tuple(each_line[1])),(0,0,0),2,cv2.cv.CV_AA)
                cv2.imwrite(open_plan_partition_path+str(row[1])+"_contour.png", contour_image)

            # contour_outer_image = cv2.imread(shortest_path_path+common_name+'/_STEP_01.png',cv2.IMREAD_GRAYSCALE)
            # open_plan_c = planClass('test', 'test', contour_outer_image,'test',False)
            # improved_cont_lines = open_plan_c.improve_outer_contour(contour_outer_image,avg_door_width)
            image_contour_lines = current_open_plan_contour_data[1]
            # image_contour_lines = line_obj.get_contour_lines(contour_outer_image)

            if debug_mode:
                contour_image_test = ~(np.zeros((img_height,img_width,3), np.uint8))
                for each_line in image_contour_lines:
                    cv2.line(contour_image_test,(tuple(each_line[0])),(tuple(each_line[1])),(0,0,0),2,cv2.cv.CV_AA)
                cv2.imwrite(output_path+'TEMPORARY.png', contour_image_test)

            #----2017/03/26
            #------special case for bedroom,corridor and wc,etc
            corridor_list, room_list, wc_list,bath_list,furniture_list = [],[],[],[],[]
            for row_num,row in enumerate(text_words):
                #--furniture list
                furniture_file = open(current_directory+'/Model/floor_plan_text/furniture_list.txt','r+')
                furniture_string = furniture_file.read()
                furniture_words_list = furniture_string.split()
                #--non open plan list
                non_open_plan_rooms_file = open(current_directory+'/Model/floor_plan_text/non_open_plan_rooms_list.txt','r+')
                non_open_plan_rooms_string = non_open_plan_rooms_file.read()
                non_open_plan_rooms_words_list = non_open_plan_rooms_string.split()
                #--wc_list
                wc_file = open(current_directory+'/Model/floor_plan_text/wc_list.txt','r+')
                wc_string = wc_file.read()
                wc_words_list = wc_string.split()
                #--bath_list
                bath_file = open(current_directory+'/Model/floor_plan_text/bath_list.txt','r+')
                bath_string = bath_file.read()
                bath_words_list = bath_string.split()
                #--corridor list
                corridor_file = open(current_directory+'/Model/floor_plan_text/corridor_list.txt','r+')
                corridor_string = corridor_file.read()
                corridor_words_list = corridor_string.split()

                for furniture in furniture_words_list:
                    if row[0]==furniture:
                        furniture_list.append(row_num)
                        break
                for non_open_plan_room in non_open_plan_rooms_words_list:
                    if row[0]==non_open_plan_room:
                        room_list.append(row_num)
                        break
                for wc in wc_words_list:
                    if row[0]==wc:
                        wc_list.append(row_num)
                        break
                for bath in bath_words_list:
                    if row[0]==bath:
                        bath_list.append(row_num)
                        break
                for corridor in corridor_words_list:
                    if row[0]==corridor:
                        corridor_list.append(row_num)
                        break

            # print furniture_list
            # print room_list
            # print wc_list
            # print bath_list
            # print corridor_list


            seperated_voronoi_lines_copy = seperated_voronoi_lines
            seperated_voronoi_lines =[]
            boundaries_copy = boundaries
            boundaries =[]
            line_obj = line()
            room_type = 0
            if len(furniture_list)>0:
                for row in furniture_list:
                    seperated_voronoi_lines,boundaries= line_obj.new_list_creation2(seperated_voronoi_lines_copy,boundaries_copy,seperated_voronoi_lines,boundaries,row)
                    room_type = 'furniture'
            elif len(room_list)>0:
                for row in room_list:
                    seperated_voronoi_lines,boundaries= line_obj.new_list_creation2(seperated_voronoi_lines_copy,boundaries_copy,seperated_voronoi_lines,boundaries,row)
                    room_type = 'NOPRoom'
            elif len(wc_list)>0:
                for row in wc_list:
                    seperated_voronoi_lines,boundaries= line_obj.new_list_creation2(seperated_voronoi_lines_copy,boundaries_copy,seperated_voronoi_lines,boundaries,row)
                    room_type = 'wc'
            elif len(bath_list)>0:
                for row in bath_list:
                    seperated_voronoi_lines,boundaries= line_obj.new_list_creation2(seperated_voronoi_lines_copy,boundaries_copy,seperated_voronoi_lines,boundaries,row)
                    room_type = 'bath'
            elif len(corridor_list)>0:
                for row in corridor_list:
                    seperated_voronoi_lines,boundaries= line_obj.new_list_creation2(seperated_voronoi_lines_copy,boundaries_copy,seperated_voronoi_lines,boundaries,row)
                    room_type = 'corridor'
            else:
                seperated_voronoi_lines = seperated_voronoi_lines_copy
                boundaries = boundaries_copy
                room_type = 'OP'
            # print 'Room Type is---- ',room_type
            # for row in boundaries:
            #     print row
            # for row in open_plan_text_cordinate:
            #     print row
            # for row in text_words:
            #     print row
            #------END OF special case for bedroom,corridor and wc,etc
            # room_type = 'OP'
            # ----2017/03/26

            # print '----seperated_voronoi_lines---'
            # for row in seperated_voronoi_lines:
            #     print row



            # ----------------------------------------------------------------------------------------------------------------
            # ------- STEP 03 : Separate Edge Extensions based on Boundaries
            # --seperate edge extensions and its data (original edge, similar to ipl or not
            edge_extensions = []
            edge_extension_data = []
            for eel in original_edges_and_extensions:
                edge_extension_data.append([eel[0], eel[2]])
                edge_extensions.append(eel[1])

            # ---assign eel to boundaries
            seperated_edge_lines = []
            for r_num, boundary_row in enumerate(boundaries):
                # --[1,[[2,3],[4]]]
                text1 = boundary_row[0]
                seperated_edge_lines.append([])
                seperated_edge_lines[r_num].append(text1)
                other_text_lines = []
                for text2_set in boundary_row[1]:
                    eel_for_text2_set = []
                    single_lines = []
                    sp_intersect_eel_combined = []
                    for text2 in text2_set:
                        shortest_path = self.line_optimization_function_obj.find_relevant_shortest_path(text1, text2,
                                                                                                        shortest_path_data)
                        sp_intersect_eel = self.line_optimization_function_obj.find_lines_intersect_with_sp(
                            shortest_path, edge_extensions)
                        sp_intersect_eel_combined = self.line_optimization_function_obj.check_if_line_repeats(
                            sp_intersect_eel_combined, sp_intersect_eel)
                    # --find if EEL is partitioning text as it should be
                    eel_partitioning_text = self.line_optimization_function_obj.find_lines_partitioning_relevant_text(
                        text1, text2_set, sp_intersect_eel_combined, image_contour_lines,
                        open_plan_text_cordinate, img_height, img_width, avg_door_width)
                    eel_for_text2_set.append(eel_partitioning_text)
                    for line_set in eel_for_text2_set:
                        for each_line in line_set:
                            single_lines.append(each_line)
                    other_text_lines.append(single_lines)
                seperated_edge_lines[r_num].append(other_text_lines)

            # ---assign ipl to boundaries
            seperated_ipl_lines = []
            for r_num, boundary_row in enumerate(boundaries):
                # --[1,[[2,3],[4]]]
                text1 = boundary_row[0]
                seperated_ipl_lines.append([])
                seperated_ipl_lines[r_num].append(text1)
                other_text_lines = []
                for text2_set in boundary_row[1]:
                    ipl_for_text2_set = []
                    single_lines = []
                    sp_intersect_ipl_combined = []
                    for text2 in text2_set:
                        shortest_path = self.line_optimization_function_obj.find_relevant_shortest_path(text1, text2,
                                                                                                        shortest_path_data)
                        sp_intersect_ipl = self.line_optimization_function_obj.find_lines_intersect_with_sp(
                            shortest_path, interesting_lines)
                        sp_intersect_ipl_combined = self.line_optimization_function_obj.check_if_line_repeats(
                            sp_intersect_ipl_combined, sp_intersect_ipl)

                    ipl_partitioning_text = self.line_optimization_function_obj.find_lines_partitioning_relevant_text(
                        text1, text2_set, sp_intersect_ipl_combined, image_contour_lines,
                        open_plan_text_cordinate, img_height, img_width, avg_door_width)
                    ipl_for_text2_set.append(ipl_partitioning_text)
                    for line_set in ipl_for_text2_set:
                        for each_line in line_set:
                            single_lines.append(each_line)
                    other_text_lines.append(single_lines)
                seperated_ipl_lines[r_num].append(other_text_lines)


            # ----- special case for placard
            bw_image = floor_plan_copy.copy()
            max_line_floor_plan = cv2.cvtColor(bw_image, cv2.COLOR_GRAY2RGB)
            max_line = []

            # ---2017/03/26
            # ---if only 2 txts+seperated_edge_lines is 0+one word is placard : execute special case
            row_num, furniture_exists = -1, False
            if len(furniture_list) > 0:
                furniture_exists = True
                row_num = furniture_list[0]

            # --check if at least one row in seperated_edge_lines has content
            edge_list_empty = False
            for row, edge_row in enumerate(seperated_edge_lines):
                if edge_row[0] == row_num:
                    if len(edge_row[1]) == 1:
                        if len(edge_row[1][0]) == 0:
                            edge_list_empty = True
                            break
                    else:
                        # --for each boundary check if @least one boundary(connected to PL) has EE. If yes (edge_list_empty = False) break
                        for each_boundary in edge_row[1]:
                            if len(each_boundary) == 0:
                                edge_list_empty = True
                            else:
                                edge_list_empty = False
                                break
                        break
            # ---find if texts are too close to each other
            if edge_list_empty and furniture_exists:
                average_distance = line_obj.calculate_distance_between_text(boundaries, open_plan_text_cordinate,
                                                                            row_num)
            else:
                average_distance = 0
            # print 'average_distance',average_distance, avg_door_width
            # if edge_list_empty and furniture_exists and average_distance>avg_door_width:
            #     max_line_floor_plan = floor_plan_copy.copy()
            #     max_line,max_line_floor_plan = line_obj.special_case_cupboard(image_contour_lines,text_bounding_boxes,text_words,row_num,output_path, max_line_floor_plan,boundaries)
            # #     print 'PL case'
            # else:
            # #     print 'Else'
            # # ---2017/03/26

            line_drawable = False
            # --new change on 2017/05/04 to have special case if furniture exista at any place
            if furniture_exists:
                line_drawable, max_line, max_line_floor_plan = self.line_optimization_function_obj.partition_cupboard(
                    image_contour_lines, text_bounding_boxes,
                    text_words, row_num, output_path,
                    max_line_floor_plan, avg_door_width)

            if line_drawable == False or furniture_exists == False:
            # print 'Else'
            # --2017/03/26







                #------- STEP 04 : Separate Improved Voronoi Lines based on Boundaries
                # Explanation:
                    # 1. Pre-processing of voronoi lines: Remove line segments<door width and connect other lines
                    # 2. Find cordinates of optimized_voronoi_lines
                    # 3. Get possible lines from cordinates
                    # 4. If these lines don't intersect with contour lines/text cordinates/is in between 2 text cordinates take them as 'improved_voronoi_lines'

                #1. Pre-processing of voronoi lines: Remove line segments<door width and connect other lines
                optimized_voronoi_lines = []
                min_line_length = avg_door_width/10
                first_line_check,first_line = 0,0
                short_length_flag = False
                for r,voronoi_row1 in enumerate(seperated_voronoi_lines):
                    boundary_line =[]
                    optimized_voronoi_lines.append([])
                    optimized_voronoi_lines[r].append(voronoi_row1[0])
                    for boundary in voronoi_row1[1]:
                        line_connection =[]
                        for l,each_line in enumerate(boundary):
                            x1,y1 = each_line[0]
                            x2,y2 = each_line[1]
                            line_length = math.hypot(x2 - x1, y2 - y1)
                            if l ==0 and line_length<min_line_length:
                                first_line_check = True
                                first_line = each_line
                                short_length_flag = False
                            elif l== len(boundary)-1 and line_length<min_line_length:
                                if first_line_check:
                                    line_connection.append([first_line[0],each_line[1]])
                                    first_line_check = False
                                else:
                                    last_element = line_connection[-1]
                                    del line_connection[-1]
                                    line_connection.append([last_element[1],each_line[1]])
                                short_length_flag = False
                            else:
                                if line_length>min_line_length:
                                    if first_line_check:
                                        line_connection.append([first_line[0],each_line[1]])
                                        first_line_check = False
                                    elif short_length_flag and len(line_connection)>0:
                                        last_element = line_connection[-1]
                                        line_connection.append([last_element[1],each_line[1]])
                                    else:
                                        line_connection.append(each_line)
                                    short_length_flag = False
                                else:
                                    short_length_flag= True
                        boundary_line.append(line_connection)
                    optimized_voronoi_lines[r].append(boundary_line)



                #----------get cordinates out of boundary lines
                boundary_cordinates = []
                for r,voronoi_row1 in enumerate(optimized_voronoi_lines):
                    boundary_line =[]
                    boundary_cordinates.append([])
                    boundary_cordinates[r].append(voronoi_row1[0])#-----------******************
                    voronoi_row=voronoi_row1[1]#-----------******************
                    for boundary in voronoi_row:
                        line_connection =[]
                        for l,each_line in enumerate(boundary):
                            if l==0:
                                line_connection.append(each_line[0])
                                line_connection.append(each_line[1])
                            else:
                                line_connection.append(each_line[1])
                        boundary_line.append(line_connection)
                    boundary_cordinates[r].append(boundary_line)#-----------******************



                improved_voronoi_cordinates=[]
                for b_cord_r,row1 in enumerate(boundary_cordinates):#-----------******************
                    improved_voronoi_cordinates.append([])#-----------******************
                    improved_voronoi_cordinates[b_cord_r].append(row1[0])#-----------******************
                    row=row1[1]#-----------******************
                    boundary_line=[]
                    for boundary in row:
                        line_connection =[]
                        boundary_length = len(boundary)
                        if boundary_length>2:
                            #--------create cordinate_lookup list with details about number of cordinates to look at in each boundary
                            test_list = []
                            for row in range(2,boundary_length):
                                test_list.append(row)
                            cordinate_lookup =[]
                            for L in range(1,len(test_list)+1):
                                row=[]
                                for subset in itertools.combinations(test_list,L):
                                    row.append(list(subset))
                                cordinate_lookup.append(row)
                            for r_count,row in enumerate(cordinate_lookup):
                               for element in row:
                                   temp_line=[]
                                   for r, each_line in enumerate(boundary):
                                       if r+1 not in element:
                                           temp_line.append(each_line)
                                   line_connection.append(temp_line)
                        boundary_line.append(line_connection)
                    improved_voronoi_cordinates[b_cord_r].append(boundary_line)#-----------******************


                improved_voronoi_lines_temp=[]
                for r,row1 in enumerate(improved_voronoi_cordinates):#-----------******************
                    improved_voronoi_lines_temp.append([])#-----------******************
                    improved_voronoi_lines_temp[r].append(row1[0])#-----------******************
                    row=row1[1]#-----------******************
                    boundary_line=[]
                    for boundary in row:
                        line_connection =[]
                        for cordinate_set in boundary:
                            cordinate_list =[]
                            for c,cords in enumerate(cordinate_set):
                                # if len(cordinate_set)>2:
                                if c==0:
                                    cordinate_list.append([])
                                    cordinate_list[c].append(cords)
                                elif c==len(cordinate_set)-1:
                                    cordinate_list[c-1].append(cords)
                                else:
                                    cordinate_list.append([])
                                    cordinate_list[c].append(cords)
                                    cordinate_list[c-1].append(cords)
                            line_connection.append(cordinate_list)
                        boundary_line.append(line_connection)
                    improved_voronoi_lines_temp[r].append(boundary_line)#-----------******************

                # 4. If these lines don't intersect with contour lines/text cordinates/is in between 2 text cordinates take them as 'improved_voronoi_lines'
                # 4.2 Find lines that don't intersect with contour lines and text cordinates
                contour_non_intersect_improved_voronoi_lines = self.line_optimization_function_obj.find_lines_not_intersecting_with_contour(
                    improved_voronoi_lines_temp, boundaries, open_plan_text_cordinate, image_contour_lines,
                    text_bounding_boxes)

                # 4.3 Find lines that are in between 2 text cords (not putting all text labels in one side)
                improved_voronoi_lines_temp2 = self.line_optimization_function_obj.find_lines_seperating_text(
                    contour_non_intersect_improved_voronoi_lines, boundaries, open_plan_text_cordinate,
                    floor_plan_copy, avg_door_width, output_path)

                # 4.4 if SMPL has > 1 line per boundary: select line with min(segments) as SMPL
                # 4.4 if gradient(SMPL) is ~= 90/0 create another candidate with 90/0 gradient
                # test_image = floor_plan_copy.copy()
                # colored_test_image = cv2.cvtColor(test_image, cv2.COLOR_GRAY2RGB)
                iterative_obj = iterative_functions_class()
                for r_num, row in enumerate(improved_voronoi_lines_temp2):
                    # --take each boundary one by one
                    for b_num, boundary in enumerate(row[1]):
                        # --sort sublists in boundary (SMPL_LineSets) based on number of lines in each SMPL_LineSet
                        boundary.sort(key=len)
                        if len(boundary) > 0:
                            # --select SMPL_LineSet with min(segments) from sorted array
                            new_boundary_lines = [boundary[0]]
                            first_element_length = len(boundary[0])
                            # --if SMPLineSet with  min(segments) is 1, then there's going to be only one such line
                            # --so we check if min(segments) > 1
                            if first_element_length > 1:
                                for line_set_num, improved_line_set in enumerate(boundary):
                                    if line_set_num > 0:
                                        # --in sorted list check if 2,3... elements also have the same number of elements as 1
                                        if len(improved_line_set) == first_element_length:
                                            new_boundary_lines.append(improved_line_set)
                                        # --since list is sorted, we can break
                                        else:
                                            break

                            # --finding approx 0/90 line per SMPL
                            for num, smpl_set in enumerate(new_boundary_lines):
                                if len(smpl_set) == 1:
                                    current_line = smpl_set[0]
                                    x1, y1 = current_line[0]
                                    x2, y2 = current_line[1]
                                    line_gradient, line_c = iterative_obj.find_mc_of_line(x1, y1, x2, y2)
                                    if line_gradient == 0 or line_gradient == 'a' or (
                                            line_gradient < 0.4 and line_gradient > -0.4) or (
                                            line_gradient > 3 and line_gradient < -3):
                                        new_line = self.line_optimization_function_obj.find_approx_straight_line(
                                            current_line, line_gradient, image_contour_lines)
                                        new_boundary_lines.append([new_line])
                                        break

                            improved_voronoi_lines_temp2[r_num][1][b_num] = new_boundary_lines

                contour_non_intersect_2 = self.line_optimization_function_obj.find_lines_not_intersecting_with_contour(
                    improved_voronoi_lines_temp2, boundaries, open_plan_text_cordinate, image_contour_lines,
                    text_bounding_boxes)
                # 4.3 Find lines that are in between 2 text cords (not putting all text labels in one side)
                improved_voronoi_lines = self.line_optimization_function_obj.find_lines_seperating_text(
                    contour_non_intersect_2, boundaries, open_plan_text_cordinate, floor_plan_copy,
                    avg_door_width, output_path)

                if debug_mode:
                    image_test = floor_plan_copy.copy()
                    for r1,row in enumerate(improved_voronoi_lines):
                        for b1,boundary in enumerate(row[1]):
                            for i,improved_line_set in enumerate(boundary):
                                # print r1,b1,i,improved_line_set
                                # image_test = floor_plan_copy.copy()
                                for improved_line in improved_line_set:
                                    cv2.line(image_test,(tuple(improved_line[0])),(tuple(improved_line[1])),(255,0,0),2,cv2.cv.CV_AA)
                    cv2.imwrite(c_line_scoring_path+name+"IMPORVED LINES.png", image_test)


                    #----------for display purposes draw all lines in to an image
                    black_all_lines_image = floor_plan_copy.copy()
                    all_lines_image = cv2.cvtColor(black_all_lines_image,cv2.COLOR_GRAY2RGB)
                    for r,row in enumerate(optimized_voronoi_lines):
                        for b,boundary in enumerate(row[1]):
                            for v_line in boundary:
                                cv2.line(all_lines_image,(tuple(v_line[0])),(tuple(v_line[1])),(240,30,230),3,cv2.cv.CV_AA)
                    for row in voronoi_data:
                        for v_line in row[2:]:
                            cv2.line(all_lines_image,(tuple(v_line[0])),(tuple(v_line[1])),(0,150,255),3,cv2.cv.CV_AA)
                    for row in seperated_edge_lines:
                        for boundary in row[1]:
                            for e_line1 in boundary:
                                e_line = e_line1[0]
                                cv2.line(all_lines_image,(tuple(e_line[0])),(tuple(e_line[1])),(0,255,0),2,cv2.cv.CV_AA)
                    for row in improved_voronoi_lines:
                        for boundary in row[1]:
                            for improved_line_set in boundary:
                                for improved_line in improved_line_set:
                                    cv2.line(all_lines_image,(tuple(improved_line[0])),(tuple(improved_line[1])),(255,0,0),2,cv2.cv.CV_AA)
                    for row in seperated_ipl_lines:
                        for boundary in row[1]:
                            for ipl_line1 in boundary:
                                ipl_line = ipl_line1[0]
                                cv2.line(all_lines_image, (tuple(ipl_line[0])), (tuple(ipl_line[1])), (255, 255, 0), 2,
                                         cv2.cv.CV_AA)

                    for row_number, cordinate in enumerate(open_plan_text_cordinate):
                        for boundary_row in boundaries:
                            if boundary_row[0]== row_number:
                                cv2.circle(all_lines_image, (tuple(cordinate)), 5, (0, 0, 255), -1)
                    cv2.imwrite(c_line_scoring_path+"_STEP_06_COMPLETE LINE SET.png", all_lines_image)


                # print '-------improved_voronoi_lines------'
                # for row in improved_voronoi_lines:
                #     print row


                non_voronoi_use =[]
                for row_count,row in enumerate(boundaries):
                    # contour_image = ~(np.zeros((img_height,img_width,3), np.uint8))
                    # print row
                    text1 = row[0]
                    # print 'text1',text1
                    non_voronoi_use.append([])
                    non_voronoi_use[row_count].append(text1)
                    boundary_lines = []
                    for boundary_num,boundary in enumerate(row[1]):
                        boundary_row = []
                        for only_contour in only_contour_lines:
                            if only_contour[0]==text1:
                                boundary_row.append(only_contour[1:])
                                # print only_contour[1:]
                                # for non_vornoi_line in only_contour[1:]:
                                #     cv2.line(contour_image,(tuple(non_vornoi_line[0])),(tuple(non_vornoi_line[1])),(255,0,0),2,cv2.cv.CV_AA)
                                break
                        all_texts = [text1]+boundary
                        # print 'all_texts',all_texts
                        for vor_row in voronoi_data:
                            if vor_row[0]==text1 or vor_row[1]==text1:
                                boundary_row.append(vor_row[2:])
                        boundary_lines.append(boundary_row)
                        # cv2.imwrite(c_line_scoring_path+str(row_count)+str(boundary_num)+"_CHECK_LINES.png", contour_image)

                    non_voronoi_use[row_count].append(boundary_lines)


                # debug_mode = True
                if debug_mode:
                    # for row in boundaries:
                    #     print row
                    # for row in open_plan_text_cordinate:
                    #     print row
                    # for row in text_words:
                    #     print row
                    for r,row in enumerate(non_voronoi_use):
                        for b,boundary in enumerate(row[1]):
                            contour_image = ~(np.zeros((img_height,img_width,3), np.uint8))
                            for non_vornoi_line_set in boundary:
                                for non_vornoi_line in non_vornoi_line_set:
                                    cv2.line(contour_image,(tuple(non_vornoi_line[0])),(tuple(non_vornoi_line[1])),(255,0,0),2,cv2.cv.CV_AA)
                            cv2.imwrite(c_line_scoring_path+str(r)+str(b)+"_Z_NEWWWWWW_LINES.png", contour_image)



                os.mkdir(c_line_scoring_path+'Scores and Penalties/')
                score_path = c_line_scoring_path+'Scores and Penalties/'
                os.mkdir(c_line_scoring_path+'Test Outputs/')
                Test_path = c_line_scoring_path+'Test Outputs/'

                voronoi_penalties, edge_extension_penalties, improved_voronoi_penalties, ipl_penalties = self.penalties_scores_obj.calculate_penalties(
                    room_type, avg_door_width,
                    img_height, img_width, weight_list,
                    shortest_path_data, edge_extension_data,
                    open_plan_text_cordinate, boundaries,
                    seperated_voronoi_lines, seperated_edge_lines,
                    improved_voronoi_lines, seperated_ipl_lines,
                    non_voronoi_use, floor_plan_copy,
                    score_path, Test_path, debug_mode)

                # print '-----boundaries-----'
                # for row in boundaries:
                #     print row
                # print '-----seperated_voronoi_lines-----'
                # for row in seperated_voronoi_lines:
                #     print row
                # print '-----seperated_edge_lines-----'
                # for row in seperated_edge_lines:
                #     print row
                # print '-----improved_voronoi_lines-----'
                # for row in improved_voronoi_lines:
                #     print row
                # print '-----seperated_ipl_lines-----'
                # for row in seperated_ipl_lines:
                #     print row
                #
                #
                #
                #
                # print '-----voronoi_penalties-----'
                # for row in voronoi_penalties:
                #     print row
                # print '-----edge_extension_penalties-----'
                # for row in edge_extension_penalties:
                #     print row
                # print '-----improved_voronoi_penalties-----'
                # for row in improved_voronoi_penalties:
                #     print row
                # print '-----ipl_penalties-----'
                # for row in ipl_penalties:
                #     print row

                # ------- STEP 07 : Calculate Scores
                voronoi_scores, edge_extension_scores, improved_voronoi_scores, ipl_scores = self.penalties_scores_obj.calculate_scores(
                    voronoi_penalties, edge_extension_penalties,
                    improved_voronoi_penalties, ipl_penalties,
                    seperated_voronoi_lines, seperated_edge_lines,
                    improved_voronoi_lines, seperated_ipl_lines,
                    open_plan_text_cordinate, boundaries,
                    score_path, debug_mode)

                # #---
                # voronoi_penalties = copy.copy(voronoi_scores)
                # # ------- STEP 07 : Calculate scores
                # # Explanation:
                # # 1. Calculate voronoi score = 0 for all
                # # 2. Calculate edge_extension score = voronoi penalty - edge penalty
                # # 3. Calculate improved_voronoi score = voronoi penalty - improved voronoi penalty
                # # 1. Calculate voronoi score = 0 for all
                # voronoi_scores = self.line_optimization_function_obj.calculate_voronoi_score(
                #     voronoi_penalties)
                # # 2. Calculate edge_extension score = voronoi penalty - edge penalty
                # edge_extension_scores = self.line_optimization_function_obj.calculate_eel_score(
                #     voronoi_penalties, edge_extension_scores)
                # # 3.Calculate improved_voronoi score = voronoi penalty - improved voronoi penalty
                # improved_voronoi_scores = self.line_optimization_function_obj.calculate_ivl_score(
                #     voronoi_penalties, improved_voronoi_scores)
                # # 4. Calculate ipl score = voronoi penalty - ipl penalty
                # ipl_scores = self.line_optimization_function_obj.calculate_ipl_score(
                #     voronoi_penalties, ipl_penalties)
                #
                if debug_mode:
                    self.line_optimization_function_obj.print_all_scores(
                        boundaries, voronoi_scores, edge_extension_scores,
                        improved_voronoi_scores, seperated_voronoi_lines,
                        seperated_edge_lines, improved_voronoi_lines,
                        seperated_ipl_lines, ipl_scores,
                        open_plan_text_cordinate, score_path)

                # print '----edge_extension_scores--'
                # for row in edge_extension_scores:
                #     print row
                # print '----improved_voronoi_scores--'
                # for row in improved_voronoi_scores:
                #     print row
                # print '----voronoi_penalties--'
                # for row in voronoi_penalties:
                #     print row

                # #------- STEP 08 : Get Best line from all 3 line types
                bw_image = floor_plan_copy.copy()
                max_line_floor_plan = cv2.cvtColor(bw_image,cv2.COLOR_GRAY2RGB)
                line_obj= line()
                max_line,max_line_floor_plan = line_obj.find_maximum_score(
                    edge_extension_scores,improved_voronoi_scores,
                    ipl_scores, voronoi_penalties,
                    seperated_voronoi_lines, improved_voronoi_lines,
                    seperated_edge_lines, seperated_ipl_lines,
                    max_line, max_line_floor_plan
                )


            if debug_mode:
                cv2.imwrite(c_line_scoring_path+'_STEP_08_MAX_LINE.png',max_line_floor_plan)
            cv2.imwrite(final_output_path+str(orginal_image_name)+'-'+str(room_number)+'.png',max_line_floor_plan)
            major_max_lines.append(max_line)

        return len(all_detected_contour_lines),major_max_lines