import sys,os,shutil,traceback,re,math
from natsort import natsorted
import xml.etree.ElementTree as ET
from lxml import etree

class MainFormLogic:
    def __init__(self):
        current_directory, images_path, python_image_folder_path, output_path,html_input_path = self.getJSargs()
        # self.current_directory = '/home/anu/Documents/Bit_Bucket/SVG_Detection/multiple_fp/js_code/../python/0_create_folders/'
        # images_path = '/home/anu/Documents/Bit_Bucket/SVG_Detection/multiple_fp/files/uploaded_input/serious_test/'
        # python_image_folder_path = '/home/anu/Documents/Bit_Bucket/SVG_Detection/multiple_fp/js_code/../files/outputs/'
        # output_path = '/home/anu/Documents/Bit_Bucket/SVG_Detection/multiple_fp/js_code/../files/uploaded_input/pre_processed/'
        # html_input_path = '/home/anu/Documents/Bit_Bucket/SVG_Detection/multiple_fp/js_code/../files/html_input/'
        self.clean_output_folder(output_path)
        self.preprocess_images(images_path,output_path)
        names_list = self.get_images_names_list(images_path)
        self.create_folders(names_list,python_image_folder_path)
        self.copy_files(output_path,html_input_path)



    def getJSargs(self):
        js_message = sys.stdin.readlines()
        current_directory,images_path,python_folder_path,output_path,html_input_path = '','', '','',''
        for row_num, each in enumerate(js_message):
            if row_num == 0:
                current_directory = each.rstrip()
            elif row_num == 1:
                images_path = each.rstrip()
            elif row_num == 2:
                python_folder_path = each.rstrip()
            elif row_num == 3:
                output_path = each.rstrip()
            else:
                html_input_path = each.rstrip()
        return current_directory, images_path,python_folder_path,output_path,html_input_path

    def clean_output_folder(self,output_path):
        # --delete sub files inside self.output_path
        for html_file in os.listdir(output_path):
            if html_file != '0_start.png':
                os.remove(output_path + html_file)

    def get_images_names_list(self,images_path):
        #--copy all image files to html_input path and get images name list
        names_list = []
        if not(os.path.exists(images_path)):
            print 'Directory Not Found.'
            sys.exit()
        else:
            original_fp_path_unsorted = os.listdir(images_path)
            original_fp_path = natsorted(original_fp_path_unsorted)

            for each_image in original_fp_path:
                names_list.append(str(each_image))
                # shutil.copy(images_path + str(each_image),
                #             output_path)
        return names_list

    def create_folders(self,names_list,python_image_folder_path):
        #--empty contents inside pyton path
        for content in os.listdir(python_image_folder_path):
            content_path = os.path.join(python_image_folder_path,content)
            try:
                shutil.rmtree(content_path)
            except:
                traceback.print_exc()

        #--create one folder to put all results
        os.mkdir(python_image_folder_path+'results_collected/')
        os.mkdir(python_image_folder_path + 'results_collected/final_lines/')
        #--create folders by image name
        # sub_folder_names = ['1_text','2_stairs','3_doors','4_walls','5_windows',
        #                     '6_rooms','7_objects','8_JS_results']
        sub_folder_names = ['1_text', '2_roc', '8_JS_results']
        sub_sub_folders = ['1_images','2_state_files',
                           '3_detection_files','4_svg_process_files']
        for image_name in names_list:
            #--ignore default display image for html page
            if image_name != 'Start.png':
                image_folder_path = python_image_folder_path+image_name[:-4]+'/'
                os.mkdir(image_folder_path)
                for each_sub in sub_folder_names:
                    os.mkdir(image_folder_path+each_sub+'/')
                    if each_sub != '8_JS_results':
                        for each_sub2 in sub_sub_folders:
                            os.mkdir(image_folder_path + each_sub + '/'+each_sub2+'/')
                            if each_sub2=='1_images':
                                os.mkdir(image_folder_path+each_sub+'/'+each_sub2+'/input/')
                                os.mkdir(image_folder_path + each_sub +'/'+each_sub2+ '/output/')

    def copy_files(self,output_path,html_input_path):
        for each_svg in natsorted(os.listdir(output_path)):
            if '_visible.svg' in str(each_svg):
                write_html_input_path = html_input_path+str(each_svg)[:-12]+'.svg'

                svg_file = open(output_path + str(each_svg))
                new_svg = open(write_html_input_path, 'w')

                for t_row, text_line in enumerate(svg_file):
                    if t_row == 0:
                        svg_tag_part = text_line[:4]
                        svg_rest_of_line = text_line[4:]
                        new_first_line = svg_tag_part + ' xmlns="http://www.w3.org/2000/svg"' + svg_rest_of_line
                        new_svg.write(new_first_line)
                    else:
                        new_svg.write(text_line)


    def preprocess_images(self,images_path,output_path):
        restricted_color_combi_list, background_colors = self.pre_define_structures()
        for each_svg in natsorted(os.listdir(images_path)):
            write_output_path = output_path + str(each_svg)[:-4]
            # --create and open text file to store svg changed element data
            svg_text_file = open(write_output_path + '_svg_text_data.txt', 'w+')

            #--read file and add IDs
            input_tree = ET.parse(images_path + each_svg)
            id_root = self.assign_ids(input_tree)

            #--test js
            # visibility_root = id_root
            # cleaned_root = id_root

            #--get id tree and remove hidden elements
            visibility_root, cleaned_root = 0, 0
            visibility_root = self.preprocess_tag_visibility(id_root,visibility_root,
                              restricted_color_combi_list,background_colors)

            # -- get visibility_root and remove same color items, convert path-> lines
            cleaned_root, svg_text_file = self.clean_svg(visibility_root,
                                                         cleaned_root, svg_text_file)

            #--- write results to file
            self.write_tree_to_file(visibility_root, write_output_path, '_visible.svg')
            self.write_tree_to_file(cleaned_root, write_output_path, '_cleaned.svg')

            svg_text_file.close()


    def pre_define_structures(self):
        # --restricted_color_combi_list has all the possibilities of invalid combinations
        # --color can have in a element eg: row1->[1, 0, 1, 0] means
        # -- if style_fill, style_stroke was true, then we don't want it
        # --restricted_color_combi_list[style_fill, fill, style_stroke, stroke]
        restricted_color_combi_list = [
            [1, 0, 1, 0],
            [0, 1, 0, 1],
            [1, 0, 0, 1],
            [0, 1, 1, 0],
            [1, -1, -1, -1],
            [-1, 1, -1, -1],
            [-1, -1, 1, -1],
            [-1, -1, -1, 1]
        ]
        background_colors = ['#white', '#FFFFFF', 'none']
        return restricted_color_combi_list, background_colors

    def assign_ids(self,input_tree):
        root = input_tree.getroot()
        id_count = 0
        for element in root.iter():
            if 'id' not in element.attrib:
                element.attrib['id'] = 'n_id_' + str(id_count)
                id_count = id_count + 1
        # namespace = root.tag.split('}')[0][1:]
        # # --- use same namespace in new svg : otherwise python adds a 'ns0' namesapce
        # ET.register_namespace("", namespace)

        return root
        # tree.write(self.images_path + each_svg, 'utf-8')



    def preprocess_tag_visibility(self, svg_element, output_root,
                                  restricted_color_combi_list,background_colors):
        tag_name = svg_element.tag.split('}', 1)[1]
        if tag_name == 'svg':
            dict_test = {}
            for row in svg_element.attrib:
                dict_test[row] = svg_element.attrib[row]
            output_root = ET.Element(tag_name, dict_test)
        else:
            # if svg_element.attrib['id']=='n_id_706':
            #     print svg_element.attrib['id']
            # -- if svg_element DOESN'T have a invalid color (ex: fill & stroke=white)
            # -- then add to tree
            if not self.is_restricted_color_combination(svg_element,
                                                        restricted_color_combi_list, background_colors):

                dict_test = {}
                for row in svg_element.attrib:
                    dict_test[row] = svg_element.attrib[row]
                current_tree = ET.SubElement(output_root, tag_name, dict_test)
                # --- if has text, insert it too
                if svg_element.text is not None:
                # if tag_name == 'text' or tag_name == 'style':
                    current_tree.text = svg_element.text
                output_root = current_tree

        children = len(list(svg_element))
        # -- if tag has children take each one and loop through it
        if children > 0:
            for child_no, child in enumerate(list(svg_element)):
                # -- if tag is set to be hidden skip it and its children
                hidden_condition = 'style' in child.attrib.keys() and (
                        'display:none' in child.attrib['style'] or 'visibility:hidden' in 'style' in child.attrib[
                    'style'])

                if hidden_condition:
                    continue
                else:
                    current_tree = self.preprocess_tag_visibility(child, output_root,
                                                                  restricted_color_combi_list, background_colors)
        return output_root

    def clean_svg(self, svg_element, cleaned_root, svg_text_file):
        tag_name = svg_element.tag

        if tag_name == 'svg':
            dict_test = {}
            for row in svg_element.attrib:
                dict_test[row] = svg_element.attrib[row]
            cleaned_root = ET.Element(tag_name, dict_test)
        else:
            # # --- find if SVG element is repeated
            # element_similarity_flag = self.find_element_repeat(visibility_root, svg_element,
            #                                                    tag_name, element_no)
            # if element_similarity_flag == False:

            new_svg_element_data = self.extract_attributes(tag_name, svg_element)
            # -- if svg_element was changes: then only there will be
            # -- values for 'new_svg_element_data'
            if len(new_svg_element_data) > 0:
                current_tree = self.create_new_svg(svg_element, cleaned_root,
                                                   new_svg_element_data, svg_text_file)
                cleaned_root = current_tree
            else:
                dict_test = {}
                for row in svg_element.attrib:
                    dict_test[row] = svg_element.attrib[row]
                current_tree = ET.SubElement(cleaned_root, tag_name, dict_test)
                if svg_element.text is not None:
                    current_tree.text = svg_element.text
                cleaned_root = current_tree

        children = len(list(svg_element))
        # -- if tag has children take each one and loop through it
        if children > 0:
            for child_no, child in enumerate(list(svg_element)):
                # -- if the child is a clippath, we dont want to process it or it's children,
                # -- so we just add whole tree of child as it is
                if child.tag == 'clipPath':
                    cleaned_root.append(child)
                else:
                    current_root, svg_text_file = self.clean_svg(child, cleaned_root, svg_text_file)

        return cleaned_root, svg_text_file

    def write_tree_to_file(self, output_root, output_path, output_file_name):
        # -- get new results tree (with path-> lines)
        tree2 = ET.ElementTree(output_root)
        output_image_path = output_path + output_file_name
        tree2.write(output_image_path)
        # -- prettify the xml
        parser = etree.XMLParser(remove_blank_text=True)
        tree = etree.parse(output_image_path, parser)
        tree.write(output_image_path, pretty_print=True)

    def is_restricted_color_combination(self, element, restricted_color_combi_list, background_colors):
        style_fill, fill, style_stroke, stroke = -1, -1, -1, -1
        if 'style' in element.attrib.keys():
            # ---style_fill
            if 'fill:' in element.attrib['style']:
                fill_color = (element.attrib['style'].split('fill:'))[1].split(';')[0]
                if fill_color in background_colors:
                    style_fill = 1
                else:
                    style_fill = 0

            # --- style_stroke
            if 'stroke:' in element.attrib['style']:
                stroke_color = (element.attrib['style'].split('stroke:'))[1].split(';')[0]
                if stroke_color in background_colors:
                    style_stroke = 1
                else:
                    style_stroke = 0

        if style_fill == -1 and 'fill' in element.attrib:
            if element.attrib['fill'] in background_colors:
                fill = 1
            else:
                fill = 0

        if style_stroke == -1 and 'stroke' in element.attrib:
            if element.attrib['stroke'] in background_colors:
                stroke = 1
            else:
                stroke = 0

        color_data = [style_fill, fill, style_stroke, stroke]

        # --if color_data is in the restricted_color_list
        if color_data in restricted_color_combi_list:
            return True
        else:
            return False

    def find_element_repeat(self, svg_root, svg_element, tag_name, element_no):
        element_similarity_flag = False
        different_attributes = ['id', 'fill', 'stroke', 'style']
        for element_no2, svg_element2 in enumerate(svg_root.iter()):
            tag_name2 = svg_element2.tag
            if element_no2 > element_no:
                if tag_name2 == tag_name:
                    if len(svg_element.attrib) == len(svg_element2.attrib):
                        # print svg_element.attrib['id'],svg_element2.attrib['id']
                        attrib_similarity_flag = False
                        for attribute in svg_element.attrib:
                            if attribute in svg_element2.attrib.keys() and attribute not in different_attributes:
                                data1 = svg_element.attrib[attribute]
                                data2 = svg_element2.attrib[attribute]
                                if data1 == data2:
                                    attrib_similarity_flag = True
                                else:
                                    attrib_similarity_flag = False
                                    break
                        if attrib_similarity_flag:
                            element_similarity_flag = True
                            break
        return element_similarity_flag

    def extract_attributes(self, tag_name, svg_element):
        svg_element_dict = []
        if 'id' in svg_element.attrib.keys():
            id_str = svg_element.attrib['id']
        else:
            id_str = 'test'

        if tag_name == 'rect':
            rect_width = float(svg_element.attrib['width'])
            rect_height = float(svg_element.attrib['height'])
            # -- found 61.svg/86.svg that has rect with only x/only y. Then missing
            # -- x/y means it's 0. So addressed that issue
            if 'x' in svg_element.attrib.keys():
                rect_x = float(svg_element.attrib['x'])
            else:
                rect_x = 0.0
            if 'y' in svg_element.attrib.keys():
                rect_y = float(svg_element.attrib['y'])
            else:
                rect_y = 0.0
            current_data_dict = {'EType': tag_name, 'id': id_str,
                                 'rect_width': rect_width, 'rect_height': rect_height,
                                 'rect_x': rect_x, 'rect_y': rect_y}
            # --- get lines
            lines_list = self.lines_from_rects(current_data_dict)
            # -- add lines to dictionary
            current_data_dict.update({'lines_list': lines_list})
            svg_element_dict.append(current_data_dict)
            return svg_element_dict

        if tag_name == 'polyline':
            points = svg_element.attrib['points'].strip()
            current_data_dict = {'EType': tag_name, 'id': id_str, 'points': points}
            # --- get lines
            lines_list = self.lines_from_polylines(current_data_dict)
            # -- add lines to dictionary
            current_data_dict.update({'lines_list': lines_list})
            svg_element_dict.append(current_data_dict)
            return svg_element_dict

        elif tag_name == 'polygon':
            points = svg_element.attrib['points'].strip()
            current_data_dict = {'EType': tag_name, 'id': id_str, 'points': points}
            # --- get lines
            lines_list = self.lines_from_polygons(current_data_dict)
            # -- add lines to dictionary
            current_data_dict.update({'lines_list': lines_list})
            svg_element_dict.append(current_data_dict)
            return svg_element_dict

        elif tag_name == 'path':
            data_points = svg_element.attrib['d'].strip()
            # --- get lines
            path_lines_list, path_curves_list = self.lines_from_paths(data_points, id_str)
            # -- add lines to dictionary == 'LlHhVvMm' commands are broken to lines
            for lines_list in path_lines_list:
                current_data_dict = {'EType': tag_name, 'id': id_str,
                                     'data_points': data_points,
                                     'lines_list': lines_list}
                svg_element_dict.append(current_data_dict)
            # -- add paths to dictionary == 'CcQqSsTtAa' commands are broken to sub paths
            for paths_row in path_curves_list:
                current_data_dict = {'EType': tag_name, 'id': id_str,
                                     'data_points': data_points,
                                     'paths_list': paths_row}
                svg_element_dict.append(current_data_dict)

            return svg_element_dict
        else:
            return svg_element_dict

    def lines_from_rects(self, current_data_dict):
        rect_width = current_data_dict['rect_width']
        rect_height = current_data_dict['rect_height']
        rect_x = current_data_dict['rect_x']
        rect_y = current_data_dict['rect_y']

        x1, y1 = rect_x, rect_y
        x2, y2 = rect_x + rect_width, rect_y
        x3, y3 = rect_x + rect_width, rect_y + rect_height
        x4, y4 = rect_x, rect_y + rect_height

        line_1 = [[x1, y1], [x2, y2]]
        line_2 = [[x2, y2], [x3, y3]]
        line_3 = [[x3, y3], [x4, y4]]
        line_4 = [[x4, y4], [x1, y1]]

        lines_list = [line_1, line_2, line_3, line_4]

        return lines_list

    def lines_from_polylines(self, current_data_dict):
        all_points = re.split('[,\s]', current_data_dict['points'])
        final_point_set = [float(x) for x in all_points if len(x) > 0]
        number_list_as_cords = [final_point_set[x:x + 2] for x in xrange(0, len(final_point_set), 2)]
        lines_list = self.points_to_numbers(number_list_as_cords, connect_to_first_point=False)
        return lines_list

    def lines_from_polygons(self, current_data_dict):
        all_points = re.split('[,\s]', current_data_dict['points'])
        final_point_set = [float(x) for x in all_points if len(x) > 0]
        number_list_as_cords = [final_point_set[x:x + 2] for x in xrange(0, len(final_point_set), 2)]
        lines_list = self.points_to_numbers(number_list_as_cords, connect_to_first_point=True)
        return lines_list

    def lines_from_paths(self, data_points, id_str):
        path_lines_list = []
        path_curves_list = []

        # ---break to sub paths
        # ---result doesn't have M in it since .split(M) removes M and splits from that point
        sub_path_list_split = re.split('([Mm])', data_points)
        # print sub_path_list_split
        sub_path_list = [item for item in sub_path_list_split if len(item) > 0]
        # print sub_path_list
        abs_sub_path_list = []
        for row_num, sub_path in enumerate(sub_path_list):
            if 'm' in sub_path or 'M' in sub_path:
                # ---issue detected on 2018-03-06
                m_command = sub_path
                data = sub_path_list[row_num + 1]
                # --- get each sub path and break in to list with [[C,[points]],[C,[points]]]
                sub_path_cmd_data_list = self.get_command_data_list(data)
                # --- convert data string in sub_path_cmd_data_list to list of points
                sub_path_data, curve_path_data = self.convert_data_string_to_points(sub_path_cmd_data_list, id_str)
                # --- get absolute cords and special processing for 'h','v','z'
                abs_sub_path_data, path_list = self.convert_relative_to_abs(sub_path_data,
                                                                            abs_sub_path_list, m_command,
                                                                            curve_path_data, id_str)
                # --- store coz 'M' can be 'm' too and then need to get abs for start of 'm'
                abs_sub_path_list.append(abs_sub_path_data)
                # --- getting the line_list from sub paths
                lines_list = self.get_lines_from_abs_points(abs_sub_path_data)

                for sub_lines_list in lines_list:
                    if len(sub_lines_list) > 0:
                        path_lines_list.append(sub_lines_list)
                if len(path_list) > 0:
                    path_curves_list.append(path_list)

        return path_lines_list, path_curves_list

    def get_command_data_list(self, sub_path):
        # -- in regex '(char)' means split by char but keep that delimiter too
        # -- in regex '[]' allows to give multiple delimiters
        svg_cmds = 'LlHhVvZzCcSsQqTtAa'
        path_data = re.split('([' + svg_cmds + '])', sub_path)
        execute_flag = True
        sub_path_cmd_data_list = []
        for row_num, path_data_row in enumerate(path_data):
            if len(path_data_row) > 0:
                if execute_flag:
                    if path_data_row.strip() in svg_cmds:
                        command = path_data_row.strip()
                        path_data_points = path_data[row_num + 1].strip()
                        execute_flag = False
                        # ---special case for 'z'
                        if command in 'Zz':
                            sub_path_cmd_data_list.append([command, ''])
                            continue
                        else:
                            sub_path_cmd_data_list.append([command, path_data_points])
                    else:
                        command = 'NC'
                        path_data_points = path_data_row.strip()
                        execute_flag = True
                        sub_path_cmd_data_list.append([command, path_data_points])
                else:
                    execute_flag = True
                    continue
        return sub_path_cmd_data_list

    def convert_data_string_to_points(self, sub_path_cmd_data_list, id_str):
        sub_path_data = []
        curve_path_data = []
        # print id_str
        for current_path_seg_no, each_path_data in enumerate(sub_path_cmd_data_list):
            command = each_path_data[0]
            path_data_points = each_path_data[1]
            # print command, path_data_points

            # ---special case for 'z'
            if command in 'Zz':
                number_list_as_cords = sub_path_data[-1][1][-1]
                sub_path_data.append([command, number_list_as_cords])
                continue
            else:
                # ---Add , infront of -
                # ---check if (-) is in each_path_data if so add (,) to infront of it
                positions = [m.start() for m in re.finditer('-', path_data_points)]
                e_count = 0
                for place, p in enumerate(positions):
                    if p > 0 and path_data_points[:p + place - e_count][-1] == 'e':
                        e_count = e_count + 1
                        continue
                    path_data_points = path_data_points[:p + place - e_count] + ',' + path_data_points[
                                                                                      p + place - e_count:]

                # --if there was a -point as the first element
                # -- we want  -0.5,1,5 not ,-0.5,1,5
                if path_data_points[0] == ',':
                    path_data_points = path_data_points[1:]

                # -- split path_data by ',' and ' '-> to get all numbers seperated
                # -- all x and y s are mixed up now -> x,y,x1,y1,x2,y2
                all_points = re.split('[,\s]', path_data_points)
                # if id_str == 'path286':
                # print all_points
                final_point_set = [float(x) for x in all_points if len(x) > 0]

                # -- convert all mixed up x and ys to x,y
                number_list_as_cords = [final_point_set[x:x + 2] for x in xrange(0, len(final_point_set), 2)]
                if command in 'Aa':
                    # print command, path_data_points
                    # print final_point_set
                    # print final_point_set[-2:]
                    # print number_list_as_cords
                    curve_path_data.append([command, final_point_set[-2:], current_path_seg_no])
                # --- append to list with relevant command
                sub_path_data.append([command, number_list_as_cords])
        return sub_path_data, curve_path_data

    def convert_relative_to_abs(self, sub_path_data, abs_sub_path_list, m_command,
                                curve_path_data, id_str):
        abs_sub_path_data = []
        path_list = []
        # print id_str
        for data_row_num, data_row in enumerate(sub_path_data):
            command = data_row[0]
            current_cordinates = data_row[1]
            # print data_row
            # --first cordinate won't have a command other than 'M'
            # --so we need to process commands only if row>0 && if there is a command
            if data_row_num > 0 and type(data_row[0]) is not 'NC':
                # --Get last cordinate to use for absolute point caluclation
                earlier_row = abs_sub_path_data[data_row_num - 1]
                earlier_cordinates = earlier_row[1][-1]

                abs_cordinate = []
                # --process based on commands
                if command in 'Zz':
                    p1 = earlier_cordinates
                    p2 = abs_sub_path_data[0][1][0]
                    abs_cordinate = [p1, p2]
                else:
                    # --convert to get absolute
                    if command.islower():
                        # --- for non line-ish elements we only want last element
                        if command in 'csqta':
                            # --- for 'Aa' there are 7 point in d=''. it makes it hard
                            # --- to bind points 2 by 2.So for 'Aa' data are stored seperately
                            if command in 'a':
                                for curve in curve_path_data:
                                    if curve[-1] == data_row_num:
                                        current_cord = curve[1]
                            else:
                                current_cord = current_cordinates[-1]
                            abs_x = round((current_cord[0] + earlier_cordinates[0]), 2)
                            abs_y = round((current_cord[1] + earlier_cordinates[1]), 2)
                            abs_cordinate = [[abs_x, abs_y]]
                            # --- add data as it is to path_list
                            str_path = self.generate_curve_path_string(command,
                                                                       current_cordinates,
                                                                       earlier_cordinates)
                            path_list.append(str_path)
                        else:
                            for cor_num, cord in enumerate(current_cordinates):
                                if cor_num == 0:
                                    if command == 'h':
                                        abs_x = round((cord[0] + earlier_cordinates[0]), 2)
                                        abs_y = earlier_cordinates[1]
                                        new_cord = [abs_x, abs_y]
                                    elif command == 'v':
                                        abs_y = round((cord[0] + earlier_cordinates[1]), 2)
                                        abs_x = earlier_cordinates[0]
                                        new_cord = [abs_x, abs_y]
                                    elif command == 'l':
                                        new_cord = [round((x + y), 2) for x, y in zip(earlier_cordinates, cord)]
                                    else:
                                        print '-New command'
                                        new_cord = [0, 0]

                                else:
                                    if command == 'h':
                                        abs_x = round((cord[0] + abs_cordinate[cor_num - 1][0]), 2)
                                        abs_y = abs_cordinate[cor_num - 1][1]
                                        new_cord = [abs_x, abs_y]
                                    elif command == 'v':
                                        abs_y = round((cord[0] + abs_cordinate[cor_num - 1][1]), 2)
                                        abs_x = abs_cordinate[cor_num - 1][0]
                                        new_cord = [abs_x, abs_y]
                                    elif command == 'l':
                                        new_cord = [round((x + y), 2) for x, y in
                                                    zip(abs_cordinate[cor_num - 1], cord)]
                                    else:
                                        print '-New command'
                                        new_cord = [0, 0]
                                abs_cordinate.append(new_cord)
                    else:
                        # --- if abs cord, append to list
                        for cor_num, cord in enumerate(current_cordinates):
                            if command == 'H':
                                new_cord = [cord[0], earlier_cordinates[1]]
                                abs_cordinate.append(new_cord)
                            elif command == 'V':
                                new_cord = [earlier_cordinates[0], cord[0]]
                                abs_cordinate.append(new_cord)
                            elif command == 'L':
                                abs_cordinate = data_row[1]
                            # --- for 'CcSsQqTtAa' only last point is needed to get lines
                            else:
                                abs_cordinate = [data_row[1][-1]]
                        # -- if curve path add to paths list
                        if command in 'CSQTA':
                            str_path = self.generate_curve_path_string(command, current_cordinates,
                                                                       earlier_cordinates)
                            path_list.append(str_path)
                            # --- for 'Aa' there are 7 point in d=''. it makes it hard
                            # --- to bind points 2 by 2.So for 'Aa' data are stored seperately
                            if command in 'A':
                                for curve in curve_path_data:
                                    if curve[-1] == data_row_num:
                                        current_cord = curve[1]
                            else:
                                current_cord = current_cordinates[-1]
                            abs_cordinate = [current_cord]

                # --- add the newly create abs_cordinates and command to list
                abs_sub_path_data.append([command, abs_cordinate])

            else:
                # -- if 'm' is the subpath command, then all points after it, until
                # -- a 'VvHhLlCcQqSsTtAA' cmd is met, has to be relative
                abs_cordinates = []
                # --- for 'm/M' data_row_num is always going to be 0,
                # --- coz each sub path will always start from a M/m
                # --- coz we created sub paths by datapoints.split('M/m')
                if data_row_num == 0 and m_command == 'm':
                    # -- if current 'm' is the M immediately after d
                    # -- (eg: <path d='m__ __.....'/>)
                    # -- start point is absolute, rest of points are
                    # -- relative to start point
                    if len(abs_sub_path_list) == 0:
                        for cord, current_cordinate in enumerate(current_cordinates):
                            if cord == 0:
                                new_cord = current_cordinate
                            else:
                                new_cord = [round((x + y), 2) for x, y in
                                            zip(abs_cordinates[-1], current_cordinate)]
                            abs_cordinates.append(new_cord)

                    else:
                        # -- if current 'm' is an M in the middle/end of set of data points
                        # -- (eg: <path d='___ __.... m__ __....'/>)
                        # -- get start point from earlier sub path in current path data
                        start_point = abs_sub_path_list[-1][-1][1][-1]
                        for cord, current_cordinate in enumerate(current_cordinates):
                            if cord == 0:
                                new_cord = [round((x + y), 2) for x, y in
                                            zip(start_point, current_cordinate)]
                            else:
                                new_cord = [round((x + y), 2) for x, y in
                                            zip(abs_cordinates[-1], current_cordinate)]
                            abs_cordinates.append(new_cord)
                else:
                    abs_cordinates = current_cordinates

                abs_sub_path_data.append([command, abs_cordinates])

        return abs_sub_path_data, path_list

    def generate_curve_path_string(self, command, current_cordinates,
                                   earlier_cordinates):
        path = 'M' + str(earlier_cordinates[0]) + ',' + str(earlier_cordinates[1]) + ' '
        # --- special case executed in 'A' commands
        # ---for 'A' theres going to be 5 points. So when points are paired, A is going to
        # ---have last point as len()==1. So we make special case for it
        if len((current_cordinates)[-1]) == 1:
            # --- eg: A50,25 0 1,0 50,25
            for cord_num, cord in enumerate(current_cordinates):
                # --- A50,25
                if cord_num == 0:
                    path = path + command + str(cord[0]) + ',' + str(cord[1]) + ' '
                # --- A50,25 0 1,0 50,25 -> '25'
                elif cord_num == len(current_cordinates) - 1:
                    path = path + str(cord)
                # --- "A50,25 0 1,"-> "0 1," && A50,25 0 1,0 50, -> "0 50,"
                else:
                    path = path + str(cord[0]) + ' ' + str(cord[1]) + ','
        else:
            for cord_num, cord in enumerate(current_cordinates):
                if cord_num == 0:
                    path = path + command + str(cord[0]) + ',' + str(cord[1])
                else:
                    str_cords = str(cord[0]) + ',' + str(cord[1])
                    path = path + ' ' + str_cords
        return path

    def get_lines_from_abs_points(self, abs_sub_path_data):
        final_lines_list = []
        lines_list = []
        last_point = 0
        for data_row_num, data_row in enumerate(abs_sub_path_data):
            # print data_row
            # --- extract command and point data
            command = data_row[0]
            data = data_row[1]
            # --- add last point from 'C etc' to list: since path is continous
            # --- until you meet a 'M'
            if command not in 'CcQqSsAaTt' and last_point != 0:
                lines_list.append([last_point])
                last_point = 0

            # -- if no command(NC)
            if command is 'NC' or command in 'HhVvLl':
                lines_list = self.get_lines_from_point_list(data, lines_list)

            elif command in 'Zz':
                # --- since 'Zz' is already in line format [[p1],[p2]] dont need to
                # --- dont need to generate new lines
                if len(lines_list) > 0 and len(lines_list[-1]) == 1:
                    # --- if there is a single [p1] in lines_list added
                    # --- by an earlier step then remove it
                    del lines_list[-1]
                # --- Next insert z line
                lines_list.append(data)
            # --- if command is 'CcQqSsAaTt'
            else:
                # --- store last point for later use and add current path to path_list
                last_point = data[0]
                # --- add fist point in 'C' as last element of line_list && reset line_list
                if len(lines_list) > 0 and len(lines_list[-1]) == 1:
                    del lines_list[-1]
                if len(lines_list) > 0:
                    # --- add lines created so far to 'final_lines_list[]', reset lines_list
                    # --- coz prog will come here if met by 'CcQqSsAaTt',
                    # --- since we keep 'CcQqSsAaTt' as paths, we need to re set lines_list
                    final_lines_list.append(lines_list)
                    lines_list = []

        # ------- post processing results
        # --- if lines_list has last row with 1 element: delete it
        # --- coz we need a lines list, not points && it's already there in earlier row
        if len(lines_list) > 0 and len(lines_list[-1]) == 1:
            del lines_list[-1]
        # --- check if last row has same point repeated
        # --- [ [[5,6],[1,2]]
        # ---   [[1,2],[1,2]] ] <-- this row needs to be removed
        if len(lines_list) > 0:
            # print lines_list[-1][0]
            x, y = lines_list[-1][0]
            x1, y1 = lines_list[-1][1]
            if math.hypot(x1 - x, y1 - y) == 0:
                del lines_list[-1]
        # --- coz after all 'del' might hv lost all elements
        if len(lines_list) > 0:
            # for row in lines_list:
            #     print row
            final_lines_list.append(lines_list)

        return final_lines_list

    def get_lines_from_point_list(self, point_list, lines_list):
        for p_num, point in enumerate(point_list):
            if len(lines_list) > 0 and len(lines_list[-1]) == 1:
                lines_list[-1].append(point)
            lines_list.append([point])
        return lines_list

    def points_to_numbers(self, points_list, connect_to_first_point):
        lines_list = []
        for row, each_point in enumerate(points_list):
            if row == 0:
                continue
            elif connect_to_first_point and row == len(points_list) - 1:
                lines_list.append([points_list[row - 1], each_point])
                lines_list.append([each_point, points_list[0]])
            else:
                lines_list.append([points_list[row - 1], each_point])
        return lines_list

    def create_new_svg(self, svg_element, root, svg_element_dict, svg_text_file):
        if 'id' in svg_element.attrib.keys():
            element_id = svg_element.attrib['id']
        # else:
        #     element_id = 'test'
        new_text_line = self.create_original_element_text_line(svg_element)
        # --explanation comments--------------------------------
        # --- svg_element_dict[] can have >1 elements: but they all belong to same svg_elem
        # --- if there was >1 lines, curve paths found in line finding, each was added
        # --- to svg_element_dict as [
        # [etype:path_1,attributes:attrib,subpath1_lineslist[]]
        # [etype:path_1,attributes:attrib,subpath2_lineslist[]]   ]
        # --- so all first few columns per element in svg_element_dict is similar to each other
        # --- only lineslist[] will have new values
        if svg_element_dict[0]['EType'] == 'path':
            # print svg_element_dict[0]['id']
            org_dict_attrib = {}
            for each_attrib in svg_element.attrib:
                if each_attrib not in ['id', 'd']:
                    org_dict_attrib[each_attrib] = svg_element.attrib[each_attrib]
            # --- if path has not stroke color, add black as color,
            # --- coz even if <path> with no color gets black as default, <line> needs a color
            if 'stroke' not in svg_element.attrib.keys():
                if 'style' not in svg_element.attrib.keys():
                    org_dict_attrib['stroke'] = '#000000'
                elif 'stroke:' not in svg_element.attrib['style']:
                    org_dict_attrib['stroke'] = '#000000'
            lines_count, paths_count = 0, 0
            for rownum, sub_path in enumerate(svg_element_dict):
                dict_attrib = org_dict_attrib.copy()
                if 'lines_list' in sub_path.keys():
                    for each_line in sub_path['lines_list']:
                        new_id = element_id + '_line_' + str(lines_count)
                        new_text_line = self.write_id_to_text_line(new_text_line,
                                                                   new_id)
                        new_text_line = new_text_line
                        dict_attrib['id'] = new_id
                        x1_cord, y1_cord = each_line[0]
                        x2_cord, y2_cord = each_line[1]
                        dict_attrib['x1'] = str(x1_cord)
                        dict_attrib['y1'] = str(y1_cord)
                        dict_attrib['x2'] = str(x2_cord)
                        dict_attrib['y2'] = str(y2_cord)
                        ET.SubElement(root, 'line', dict_attrib)
                        lines_count = lines_count + 1

                elif 'paths_list' in sub_path.keys():
                    if 'fill' in svg_element.attrib.keys():
                        dict_attrib['fill'] = 'none'
                    if 'style' in svg_element.attrib.keys():
                        if 'fill:' in svg_element.attrib['style']:
                            (svg_element.attrib['style'].split('fill:'))[1].split(';')[0] = 'none'
                        dict_attrib['style'] = svg_element.attrib['style']

                    for each_curve in sub_path['paths_list']:
                        new_id = element_id + '_line_' + str(paths_count)
                        new_text_line = self.write_id_to_text_line(new_text_line,
                                                                   new_id)
                        dict_attrib['id'] = new_id
                        dict_attrib['d'] = each_curve
                        ET.SubElement(root, 'path', dict_attrib)
                        paths_count = paths_count + 1
                else:
                    'Something wrong in paths'

        else:
            new_text_line = self.process_non_path_elements(svg_element, svg_element_dict, root,
                                                           element_id, new_text_line)
        svg_text_file.write(new_text_line[:-1] + '\n' + '\n')
        return root

    def create_original_element_text_line(self, svg_element):
        tag_name = svg_element.tag
        new_text_line = tag_name + ':::'
        for att_no, attribute in enumerate(svg_element.attrib):
            if att_no == 0:
                new_text_line = new_text_line + attribute + '::' + svg_element.attrib[attribute]
            else:
                new_text_line = new_text_line + ':::' + attribute + '::' + svg_element.attrib[attribute]
        new_text_line = new_text_line + '::::'
        return new_text_line

    def write_id_to_text_line(self, new_text_line, new_id):
        new_text_line = new_text_line + new_id + ','
        return new_text_line

    def process_non_path_elements(self, svg_element, svg_element_dict, root,
                                  element_id, new_text_line):
        current_element = svg_element_dict[0]
        current_element_type = current_element['EType']
        dict_attrib = {}
        # -- get non-structural attributes based on element ytpe
        if current_element_type == 'rect':
            for each_attrib in svg_element.attrib:
                if each_attrib not in ['id', 'x', 'y', 'width', 'height']:
                    dict_attrib[each_attrib] = svg_element.attrib[each_attrib]
        elif current_element_type == 'polyline' or current_element_type == 'polygon':
            for each_attrib in svg_element.attrib:
                if each_attrib not in ['id', 'points']:
                    dict_attrib[each_attrib] = svg_element.attrib[each_attrib]
        else:
            print current_element_type, ' error'

        # --- if path has not stroke color, add black as color,
        # --- coz even if <path> with no color gets black as default, <line> needs a color
        if 'stroke' not in svg_element.attrib.keys():
            if 'style' not in svg_element.attrib.keys():
                dict_attrib['stroke'] = '#000000'
            elif 'stroke:' not in svg_element.attrib['style']:
                dict_attrib['stroke'] = '#000000'
        lines_count = 0
        # --- get all new lines and add each line to new svg
        for rownum, sub_path in enumerate(svg_element_dict):
            for each_line in sub_path['lines_list']:
                new_id = element_id + '_line_' + str(lines_count)
                new_text_line = self.write_id_to_text_line(new_text_line,
                                                           new_id)
                dict_attrib['id'] = new_id
                x1_cord, y1_cord = each_line[0]
                x2_cord, y2_cord = each_line[1]
                dict_attrib['x1'] = str(x1_cord)
                dict_attrib['y1'] = str(y1_cord)
                dict_attrib['x2'] = str(x2_cord)
                dict_attrib['y2'] = str(y2_cord)
                ET.SubElement(root, 'line', dict_attrib)
                lines_count = lines_count + 1

        return new_text_line

if __name__ == '__main__':
    hwl1 = MainFormLogic()