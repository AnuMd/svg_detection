var fs = require('fs');

elementData = [ [ 'text',
[ 'id-t3',
  'transform-matrix(1.0236 0 0 1 431.4062 99.0947)',
  'font-family-\'CenturyGothic-Bold\'',
  'font-size-12.3217' ],
[ 85.46665954589844,
  429.73333740234375,
  495.01666259765625,
  103.33332824707031 ] ],
[ 'text',
[ 'id-t2',
  'transform-matrix(1.0236 0 0 1 307.7148 150.6938)',
  'font-family-\'CenturyGothic-Bold\'',
  'font-size-12.3217' ],
[ 136.9499969482422,
  306.3500061035156,
  361.01666259765625,
  154.8000030517578 ] ],
[ 'text',
[ 'id-t1',
  'transform-matrix(1.0236 0 0 1 51.1411 94.6411)',
  'font-family-\'CenturyGothic-Bold\'',
  'font-size-12.3217' ],
[ 81.03334045410156,
  50.41667175292969,
  187.03334045410156,
  98.88333129882812 ] ],
[ 'svg',
[ 'version-1.1',
  'id-Layer_1',
  'x-0px',
  'y-0px',
  'width-550px',
  'height-200px',
  'viewBox-0 0 550 200',
  'enable-background-new 0 0 550 200' ],
[ 0, 0, 550.5, 199.5 ] ] ];

writeBBsAttribsToFile(elementData);


function writeBBsAttribsToFile(elementData){
    var file = fs.createWriteStream('textData.txt');
    for (var i = 0; i< elementData.length; ++i) {
        tagName = elementData[i][0];
        attribData = elementData[i][1];
        bbData = elementData[i][2];
        listforFile = new Array();
        attribStr = '';
        for (var j = 0; j< attribData.length; ++j) {
            // listforFile.push(attribData[j]);
            attribStr += attribData[j]+',';
        }
        file.write(tagName +' : '+ attribStr +' : BBData-'+ bbData+'\n');
    }
    file.end(); 
}