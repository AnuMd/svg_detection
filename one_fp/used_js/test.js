// var express = require('express');  
// var app = express();  
// var server = require('http').createServer(app);  
// var io = require('socket.io')(server);

// app.use(express.static(__dirname + '/bower_components'));  
// app.get('/', function(req, res,next) {  
//     res.sendFile(__dirname + '/index.html');
// });

// server.listen(4200);  


// server functions
const fs_extra = require('fs-extra')
var express = require('express')
var app = express();
var server = require('http').createServer(app);
var io = require('socket.io')(server);
var path = require('path')

var fs = require('fs')
  , exec = require('child_process').exec
  , util = require('util')
  , Files = {};
var PythonShell = require('python-shell');

server.listen(8080);

app.use( express.static( "output" ) );
//path to store image uploaded
InputImagePath = __dirname+'/uploads/'
//output image name
OutputImageName = 'final_floorplan.svg'
//path to store the SVG created by python program
OutputImagePath = __dirname+'/output/'
// OutputImagePath = __dirname+'/output/'+OutputImageName
//path to current directory passed over to python progam
CurrentDirectoryPath = String(__dirname)+'/python/floor_plan_code'
//test_directory
// CurrentDirectoryPath = String(__dirname)+'/python'



//when user types only port number: 
app.get('/', function (req, res) {
  //redirected through this to 'index.html'
  res.sendFile(path.join(__dirname+'/index.html'));
});

// app.route('/*').get(function(req, res) { 
//     return res.sendFile(path.join(__dirname+'/index.html')); 
// });



io.on('connection', function (socket) {
  	socket.on('Start', function (data) { //data contains the variables that we passed through in the html file
		fs_extra.emptyDir(InputImagePath)
		.then(() => {
		  console.log('success!')
		})
		.catch(err => {
		  console.error(err)
		})
		

		console.log('Start Executed')
		var Name = data['Name'];
		Files[Name] = {  //Create a new Entry in The Files Variable
			FileSize : data['Size'],
			Data	 : "",
			Downloaded : 0
		}
		var Place = 0;
		try{
			var Stat = fs.statSync('uploads/' +  Name);
			if(Stat.isFile())
			{
				Files[Name]['Downloaded'] = Stat.size;
				Place = Stat.size / 524288;
			}
		}
  		catch(er){} //It's a New File

		fs.open("uploads/" + Name, 'a', 0755, function(err, fd){
			if(err)
			{
				console.log(err);
			}
			else
			{
				Files[Name]['Handler'] = fd; //We store the file handler so we can write to it later
				socket.emit('MoreData', { 'Place' : Place, Percent : 0 });
				
			}
		});
	});
	
	socket.on('Upload', function (data){
		var Name = data['Name'];
		Files[Name]['Data'] += data['Data'];
		console.log('Upload Executed')
		fs.write(Files[Name]['Handler'], Files[Name]['Data'], null, 'Binary', function(err, Writen){
			console.log('started executing python with '+Name);

			var pyshell = new PythonShell('python/1_detect_text/Model/MainLogic.py');
			//create output image name
			var Name_list = Name.split('.') 
			OnlyImageName = Name_list[0] 
			OutputImageName = OnlyImageName+'.svg'

			pyshell.send(CurrentDirectoryPath);
			pyshell.send(InputImagePath);
			pyshell.send(OutputImagePath+OutputImageName);

			 

			pyshell.on('message', function (message) {
			  // received a message sent from the Python script (a simple "print" statement)
			  console.log(message);
			});

			// end the input stream and allow the process to exit
			pyshell.end(function (err) {
			  	if (err) 
			  	{
			  		console.log('Error in Image');
			  		socket.emit('Done','Oops.jpg');
			  		// throw err;
			  	}
			  	else
			  	{
				  	console.log('finished');
					//show output when program has finished running
					socket.emit('Done',OutputImageName);
				}
			});
			//show still loading message while program is running
			socket.emit('Done','loading.gif');
		});
	});


});

