// server functions
var express = require('express')
var app = express();
var server = require('http').createServer(app);
var io = require('socket.io')(server);
var path = require('path')
var formidable = require('formidable');
var fs = require('fs');
var PythonShell = require('python-shell');
var ncp = require('ncp').ncp;

server.listen(8080);


app.use( express.static( "files/1_text/1_images/input/" ) );


// when user types only port number: 
app.get('/', function (req, res) {
    res.writeHead(200, {'Content-Type': 'text/html'});
    res.write('<form action="/upload" method="post" enctype="multipart/form-data">');
    res.write('<input type="text" name="title">');
    res.write('<input type="file" name="filetoupload">');
    res.write('<input type="submit" value="Upload">');
    res.write('</form>');
    return res.end();
});


//when url has '/upload' which is given by form action
app.post('/upload', function(req, res) {
    var form = new formidable.IncomingForm();
    form.parse(req, function (err, fields, files) {
        var oldpath = files.filetoupload.path;
        var newpath = __dirname+ '/files/1_text/1_images/input/image.svg';
        fs.rename(oldpath, newpath, function (err) {
        if (err) throw err;
        return res.sendFile(path.join(__dirname+'/image_display.html'));
        res.end();
        });
    });
});


//connect to socket
io.on('connection', function (socket) {
    // if 'GetRectData' is triggered
    socket.on('GetRectData', function (data) {
        // textDetection(data);
        // stairsDetection();
        // doorDetection();
        // wallDetection();
        // windowDetection();
        // roomDetection();
        // objectDetection();    
    });
});



function textDetection(data){
    // get bb rect data passed as 'Rects' in data object
    var rectDataArray = data['Rects'];
    // P1- if there are text tags
    if (rectDataArray.length > 0){
        console.log('TExt Tags Exist');
        // 1. extract data about rects and put in to an array
        var elementList = extractRectData(rectDataArray);
        // 2. write to file
        writeTextToFile(elementList);
        // 3. call python program to erase selected elements
        // 3.1 extract id data
        var idArray = new Array();
        for (var i = 0; i< elementList.length; ++i) {
            var id = elementList[i]['ID'];
            idArray.push(id);
        }
        // 3.2 call python program to erase detected elements
        executePythonProgram('8_erase_elements','1_text','1_text','Erased.svg', idArray);
        // copyResultsToNextPythonSystem('1_text','2_stairs',['images','detections']);
    }
    // P2- if there are no text tags
    else{
        console.log('No Text Tags');
        var copy_file_paras = ['1_text','2_stairs',['images','detections']];
        executePythonProgram('1_detect_text','1_text','1_text','','',copy_file_paras);
        // if (completion_flag == 'Completed'){
        // //     // readPythonDetectionsFromFile();
        // //     // findElementsForPythonDetections();
        // //     // findPatterns();
        // //     // writeNewDetectionsToFile();
        // //     // //erase detections and generate new svg
        // //     // executePythonProgram('8_erase_elements','1_text','1_text','text_erased.svg',idArray);
        //     copyResultsToNextPythonSystem('1_text','2_stairs',['images','detections']);
        // }
        
    }
}

function stairsDetection(){
    var copy_file_paras = ['2_stairs','3_doors',['images','detections']];
    executePythonProgram('2_detect_stairs','2_stairs','2_stairs','','',copy_file_paras);
    // readPythonDetectionsFromFile();
    // findElementsForPythonDetections();
    // findPatterns();
    // writeNewDetectionsToFile();
    // //erase detections and generate new svg
    // executePythonProgram('8_erase_elements','2_stairs','2_stairs','Erased.svg',idArray);
    // copyResultsToNextPythonSystem('2_stairs','3_doors');
}


function doorDetection(){
    var copy_file_paras = ['3_doors','4_walls',['images','detections']];
    executePythonProgram('3_detect_doors','3_doors','3_doors','','',copy_file_paras);
    // readPythonDetectionsFromFile();
    // findElementsForPythonDetections();
    // findPatterns();
    // writeNewDetectionsToFile();
    // //erase detections and generate new svg
    // executePythonProgram('8_erase_elements','3_doors','3_doors','Erased.svg',idArray);
    // copyResultsToNextPythonSystem('1_text','4_walls');
}

function wallDetection(){
    executePythonProgram('4_detect_walls','4_walls','4_walls','','');
    // //erase detections and generate new svg
    // executePythonProgram('8_erase_elements','4_walls','4_walls','Erased.svg',idArray);
    copyResultsToNextPythonSystem('4_walls','5_windows');
 }

function windowDetection(){
    executePythonProgram('5_detect_windows','5_windows','5_windows','','');
    // //erase detections and generate new svg
    // executePythonProgram('8_erase_elements','5_windows','5_windows','Erased.svg',idArray);
    copyResultsToNextPythonSystem('5_windows','6_rooms');
 }

function roomDetection(){
    executePythonProgram('6_detect_rooms','6_rooms','6_rooms','','');
    // //erase detections and generate new svg
    // executePythonProgram('8_erase_elements','6_rooms','6_rooms','Erased.svg',idArray);
    copyResultsToNextPythonSystem('6_rooms','7_objects');
 }

function objectDetection(){
    executePythonProgram('7_detect_objects','7_objects','7_objects','','');
    // //erase detections and generate new svg
    // executePythonProgram('8_erase_elements','7_objects','7_objects','Erased.svg',idArray);
    // copyResultsToNextPythonSystem('7_objects','2_stairs');
 }

 


function executePythonProgram(programFolderName,inputFolder,outputFolder,
    OutputImageName,idArray,copy_file_paras){
    console.log('Executing '+programFolderName);
    var check_value;
    // program path
    var pyshell = new PythonShell('python/'+programFolderName+'/Model/MainLogic.py');
    //path to current directory passed over to python progam
    var MainProgramPath = String(__dirname)+'/python/'+programFolderName+'/';
    //path to store image uploaded
    var SystemInputImagePath = __dirname+'/files/'+inputFolder+'/';
    //path to store the SVG created by python program
    var OutputImagePath = __dirname+'/files/'+outputFolder+'/';
    
    //pass paths to python program
    pyshell.send(MainProgramPath);
    pyshell.send(SystemInputImagePath);
    pyshell.send(OutputImagePath);
    // only for '8_erase_elements' program needs erased.svg image name && id list of elements to delete
    if (OutputImageName.length != 0){
        pyshell.send(OutputImageName);
        pyshell.send(idArray);
    }
    
        
    //executing python program
    pyshell.on('message', function (message) {
        // received a message sent from the Python script (a simple "print" statement)
        console.log(message);
    });

    // end the input stream and allow the process to exit
    pyshell.end(function (err) {
        if (err) {
            throw err;
        }
        else{
            console.log('Executed');
            // copyResultsToNextPythonSystem(copy_file_paras);
            check_value = true;
            return check_value;
        }
        
    });

    return check_value;
}

function copyResultsToNextPythonSystem(allParameters){
    console.log(allParameters);
    sourceFolder = allParameters[0];
    destination_folder = allParameters[1];
    subfolderToCopy = allParameters[2];
    console.log(sourceFolder,destination_folder,subfolderToCopy);
    for (var i = 0; i< subfolderToCopy.length; ++i){
        if (i == 0){
            //temporarily copy /1_images/output/ since we haven't got to deleting elements yet
            source =  __dirname+'/files/'+sourceFolder+'/1_images/input/';
            destination =  __dirname+'/files/'+destination_folder+'/1_images/input/';
        }
        else if (i==1) {
            source =  __dirname+'/files/'+sourceFolder+'/3_detection_files/';
            destination =  __dirname+'/files/'+destination_folder+'/3_detection_files/';
        }
        else if (i==2) {
            source =  __dirname+'/files/'+sourceFolder+'/2_state_files/';
            destination =  __dirname+'/files/'+destination_folder+'/2_state_files/';
        }

        ncp.limit = 16;
        ncp(source, destination, function (err) {
            if (err) {
                return console.error(err);
            }
            else{
                console.log('Copied!'+destination);
            }
        });
    }  
}


function extractRectData(rectDataArray){
    var elementList = new Array();
    //get each element in rectDataArray
    for (var i = 0; i< rectDataArray.length; ++i) {
        // elements in rectDataArray are seperated by ','
        var elementDataList = rectDataArray[i].split(',');
        var id = elementDataList[0];
        var textName = elementDataList[1];
        // bb elements position data = 'top:60'
        var top = elementDataList[2].split(':')
        var left = elementDataList[3].split(':')
        var right = elementDataList[4].split(':')
        var bottom = elementDataList[5].split(':')
        // add bb data as an object
        var bbDataObjects = {
            'ID':id,
            'Name':textName,
            'Left_T':[parseInt(left[1]),parseInt(top[1])],
            'Right_B': [parseInt(right[1]),parseInt(bottom[1])],
        }
        //add bbbox data to array
        elementList.push(bbDataObjects);    
    }
    return elementList
}



function writeTextToFile(elementList){
    var file = fs.createWriteStream('textData.txt');
    for (var i = 0; i< elementList.length; ++i) {
        var name = elementList[i]['Name'].toLowerCase();
        var leftT_list = elementList[i]['Left_T'];
        var rightB_list = elementList[i]['Right_B'];
        // console.log(leftT_list);
        var cx = parseInt((leftT_list[0] + rightB_list[0]) / 2)
        var cy = parseInt((leftT_list[1] + rightB_list[1]) / 2)
        var name_string = String(name)+' : '
        var centrepointdata = '['+String(cx)+','+String(cy)+'] :';
        var bb_data = '['+leftT_list[0]+','+leftT_list[1]+','+rightB_list[0]+','+rightB_list[1]+'] :';
        var confidence = '['+ String(100)+'] : match';

        file.write(name_string+centrepointdata+bb_data+confidence+'\n');
    } 
    file.end(); 
}



