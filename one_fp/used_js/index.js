// server functions
const fs_extra = require('fs-extra')
var express = require('express')
var app = express();
var server = require('http').createServer(app);
var io = require('socket.io')(server);
var path = require('path')

var fs = require('fs')
  , exec = require('child_process').exec
  , util = require('util')
  , Files = {};
var PythonShell = require('python-shell');
var DOMParser = require('xmldom').DOMParser;
// var getBoundingBox = require('svg-path-bounding-box');

server.listen(8080);

app.use( express.static( "output" ) );
current_directory = __dirname
MainInputImagePath = current_directory+'/uploads/'


//when user types only port number: 
app.get('/', function (req, res) {
  //redirected through this to 'index.html'
  res.sendFile(path.join(__dirname+'/index.html'));
});

// app.route('/*').get(function(req, res) { 
//     return res.sendFile(path.join(__dirname+'/index.html')); 
// });



io.on('connection', function (socket) {
  	socket.on('Start', function (data) { //data contains the variables that we passed through in the html file
		fs_extra.emptyDir(MainInputImagePath)
		.then(() => {
		  console.log('success!')
		})
		.catch(err => {
		  console.error(err)
		})
		

		console.log('Start Executed')
		var Name = data['Name'];
		Files[Name] = {  //Create a new Entry in The Files Variable
			FileSize : data['Size'],
			Data	 : "",
			Downloaded : 0
		}
		var Place = 0;
		try{
			var Stat = fs.statSync('uploads/' +  Name);
			if(Stat.isFile())
			{
				Files[Name]['Downloaded'] = Stat.size;
				Place = Stat.size / 524288;
			}
		}
  		catch(er){} //It's a New File

		fs.open("uploads/" + Name, 'a', 0755, function(err, fd){
			if(err)
			{
				console.log(err);
			}
			else
			{
				Files[Name]['Handler'] = fd; //We store the file handler so we can write to it later
				socket.emit('MoreData', { 'Place' : Place, Percent : 0 });
				
			}
		});
	});
	
	socket.on('Upload', function (data){
		var Name = data['Name'];
		var test_data = data['file_Data'];
		Files[Name]['Data'] += data['Data'];
		console.log('Upload Executed')
		svg_file = Files[Name]['Data']
		fs.write(Files[Name]['Handler'], Files[Name]['Data'], null, 'Binary', function(err, Writen){
			
			// 1.write code for text----------------------
			console.log('Running Text Detection for '+Name);

			// check if svg_file has text as text tags
			

			// var oParser = new DOMParser();
			// var oDOM = oParser.parseFromString(test_data, "image/svg+xml");
			// var svgDoc = oDOM.documentElement;
			// var nodeList = svgDoc.getElementsByTagName("text");
			// console.log(nodeList[0].getBoundingClientRect());
			
			// console.log(test_data);







			// // get the inner element by id
			// var nodeList = svgDoc.getElementsByTagName("text");
			// console.log(nodeList[0].getBoundingClientRect());
			// // for (var i = 0; i < nodeList.length; ++i) {
			// // 	console.log(nodeList[i].getBoundingClientRect());
			// // }





			// // var svgDoc = oDOM.documentElement;
			// var nodeList = svgDoc.getElementsByTagName("text");
            // for (var i = 0; i < nodeList.length; ++i) {
			// 	// console.log(nodeList[i].textContent);
			// 	console.log(nodeList[i].getBoundingClientRect());
			//     // console.log(nodeList[i].getBoundingClientRect());
			// }




			// program path
			var pyshell = new PythonShell('python/1_detect_text/Model/MainLogic.py');
			//path to current directory passed over to python progam
			MainProgramPath = String(current_directory)+'/python/1_detect_text'
			//path to store image uploaded
			SystemInputImagePath = current_directory+'/uploads/'
			//path to store the SVG created by python program
			OutputImagePath = current_directory+'/output/'
			//create output image name
			var Name_list = Name.split('.') 
			OnlyImageName = Name_list[0] 
			OutputImageName = OnlyImageName+'_no_text.png'
			
			//pass paths to python program
			pyshell.send(MainProgramPath);
			pyshell.send(SystemInputImagePath);
			pyshell.send(OutputImagePath+OutputImageName);
			 
			//executing python program
			pyshell.on('message', function (message) {
			  // received a message sent from the Python script (a simple "print" statement)
			  console.log(message);
			});

			// end the input stream and allow the process to exit
			pyshell.end(function (err) {
			  	if (err) 
			  	{
			  		console.log('Error in Image');
			  		socket.emit('Done','Oops.jpg');
			  		// throw err;
			  	}
			  	else
			  	{
				  	console.log('finished');
					//show output when program has finished running
					socket.emit('Done',OutputImageName);
					console.log(OutputImageName)
				}
			});
			// show still loading message while program is running
			socket.emit('Done','loading.gif');
			// console.log(OutputImageName)
			// socket.emit('Done',OutputImageName);
		});
	});


});

// get uploaded .svg
// convert to png
// for the .PNG-------------
// 	Call text identification 
// 	read text-detection.txt
// 	get each texts' cordinate
// 	for each cordinate: 
// 			find corresponding element in svg
// 			save to text_list = []
// 	find a pattern in text_list
// 	find similar elements(to pattern) in svg : add them to text_list[]
// 	erase all text_list[] elements from .svg
// 	re-generate .PNG
// for the .PNG-------------
// 	Call wall identification 
// for the .PNG-------------
// 	Call door identification 
// for the .PNG-------------
// 	Call window identification 
// for the .PNG-------------
// 	Call room identification 
// for the .PNG-------------
// 	Call object identification 