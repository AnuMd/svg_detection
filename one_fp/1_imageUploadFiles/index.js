// server functions
var express = require('express')
var app = express();
var server = require('http').createServer(app);
var io = require('socket.io')(server);
var path = require('path')
var formidable = require('formidable');
var fs = require('fs');
var PythonShell = require('python-shell');
var ncp = require('ncp').ncp;

server.listen(8080);

var parent_dir = __dirname + '/..'
app.use( express.static( parent_dir+"/files/1_text/1_images/input/" ) );



// when user types only port number: 
app.get('/', function (req, res) {
    res.writeHead(200, {'Content-Type': 'text/html'});
    res.write('<form action="/upload" method="post" enctype="multipart/form-data">');
    res.write('<input type="text" name="title">');
    res.write('<input type="file" name="filetoupload">');
    res.write('<input type="submit" value="Upload">');
    res.write('</form>');
    return res.end();
});


//when url has '/upload' which is given by form action
app.post('/upload', function(req, res) {
    var form = new formidable.IncomingForm();
    form.parse(req, function (err, fields, files) {
        var oldpath = files.filetoupload.path;
        var newpath = parent_dir+ '/files/1_text/1_images/input/image.svg';
        fs.rename(oldpath, newpath, function (err) {
        if (err) throw err;
        return res.sendFile(path.join(__dirname+'/image_display.html'));
        res.end();
        });
    });
});


//connect to socket
io.on('connection', function (socket) {
    // if 'GetRectData' is triggered
    socket.on('DetectText', function (data) {
        // console.log('No Text Tags');
        // findElementsPython('T',socket);

        //uncomment later from here till end
        // get bb rect data passed as 'Rects' in data object
        var TextTagDataArray = data['TextTags'];
        writeBBsAttribsToFile(data['AllElements']);
        // P1- if there are text tags
        if (TextTagDataArray.length > 0){
            console.log('TExt Tags Exist');
            // 1. extract data about rects and put in to an array
            var elementList = extractRectData(TextTagDataArray);
            // 2. write to file
            writeTextToFile(elementList);
            // 3. call python program to erase selected elements
            // 3.1 extract id data
            var idArray = new Array();
            for (var i = 0; i< elementList.length; ++i) {
                var id = elementList[i]['ID'];
                idArray.push(id);
            }
            // 3.2 call python program to erase detected elements
            // Later make pyton read id array from elementList written to text file
            patternFindEraseElements('T',socket);
        }
        // P2- if there are no text tags
        else{
            console.log('No Text Tags');
            // 1------Detecting Text with python ----------
            findElementsPython('T',socket);
        }
    });

    socket.on('FindTextPatterns', function (data) {
        console.log('Came to text patterns');
        writeBBsAttribsToFile(data['elementData']);
        // 2------Find Text patterns and erase with python ----------
        patternFindEraseElements('T',socket);
    });

    socket.on('FindStairsPatterns', function (data) {
        // console.log(data);
        // 4------Find Stairs patterns and erase with python ----------
        patternFindEraseElements('S',socket);
    });

    socket.on('FindDoorPatterns', function (data) {
        // console.log(data);
        // 6------Find Door patterns and erase with python ----------
        patternFindEraseElements('D',socket);
    });

    socket.on('FindWallPatterns', function (data) {
        // console.log(data);
        // 8------Find Walls patterns and erase with python ----------
        patternFindEraseElements('Wl',socket);
    });

    socket.on('FindWindowPatterns', function (data) {
        // console.log(data);
        // 10------Find Windows patterns and erase with python ----------
        patternFindEraseElements('Win',socket);
    });

    socket.on('FindRoomPatterns', function (data) {
        // console.log(data);
        // 12------Find Rooms patterns and erase with python ----------
        patternFindEraseElements('R',socket);
    });

    socket.on('FindobjectPatterns', function (data) {
        // console.log(data);
        // 14------Find Objects patterns and erase with python ----------
        patternFindEraseElements('O',socket);
    });

});


function extractRectData(rectDataArray){
    var elementList = new Array();
    //get each element in rectDataArray
    for (var i = 0; i< rectDataArray.length; ++i) {
        // elements in rectDataArray are seperated by ','
        var elementDataList = rectDataArray[i].split(',');
        var id = elementDataList[0];
        var textName = elementDataList[1];
        // bb elements position data = 'top:60'
        var top = elementDataList[2].split(':')
        var left = elementDataList[3].split(':')
        var right = elementDataList[4].split(':')
        var bottom = elementDataList[5].split(':')
        // add bb data as an object
        var bbDataObjects = {
            'ID':id,
            'Name':textName,
            'Left_T':[parseInt(left[1]),parseInt(top[1])],
            'Right_B': [parseInt(right[1]),parseInt(bottom[1])],
        }
        //add bbbox data to array
        elementList.push(bbDataObjects);    
    }
    return elementList
}



function writeTextToFile(elementList){
    var file = fs.createWriteStream('textData.txt');
    for (var i = 0; i< elementList.length; ++i) {
        var name = elementList[i]['Name'].toLowerCase();
        var leftT_list = elementList[i]['Left_T'];
        var rightB_list = elementList[i]['Right_B'];
        // console.log(leftT_list);
        var cx = parseInt((leftT_list[0] + rightB_list[0]) / 2)
        var cy = parseInt((leftT_list[1] + rightB_list[1]) / 2)
        var name_string = String(name)+' : '
        var centrepointdata = '['+String(cx)+','+String(cy)+'] :';
        var bb_data = '['+leftT_list[0]+','+leftT_list[1]+','+rightB_list[0]+','+rightB_list[1]+'] :';
        var confidence = '['+ String(100)+'] : match';

        file.write(name_string+centrepointdata+bb_data+confidence+'\n');
    } 
    file.end(); 
}

function writeBBsAttribsToFile(elementData){
    var file = fs.createWriteStream('ElementData.txt');
    for (var i = 0; i< elementData.length; ++i) {
        tagName = elementData[i][0];
        attribData = elementData[i][1];
        bbData = elementData[i][2];
        listforFile = new Array();
        attribStr = '';
        for (var j = 0; j< attribData.length; ++j) {
            attribStr += attribData[j]+';';
        }
        file.write(tagName +' : '+ attribStr +' : BBData-'+ bbData+'\n');
    }
    file.end();  
}


function patternFindEraseElements(elementType,socket){
    // set paths
    if (elementType == 'T'){
        var programFolderName = 'findPatternsEraseElements/1_findTextpatterns';
            inputFolder = '1_text';
            outputFolder= '2_stairs'
            OutputImageName = '';
    }
    else if (elementType == 'S'){
        var programFolderName = 'findPatternsEraseElements/1_findTextpatterns';
            inputFolder = '2_stairs';
            outputFolder= '3_doors'
            OutputImageName = '';
    } 
    else if (elementType == 'D'){
        var programFolderName = 'findPatternsEraseElements/1_findTextpatterns';
            inputFolder = '3_doors';
            outputFolder= '4_walls'
            OutputImageName = '';
    } 
    else if (elementType == 'Wl'){
        var programFolderName = 'findPatternsEraseElements/1_findTextpatterns';
            inputFolder = '4_walls';
            outputFolder= '5_windows'
            OutputImageName = '';
    } 
    else if (elementType == 'Win'){
        var programFolderName = 'findPatternsEraseElements/1_findTextpatterns';
            inputFolder = '5_windows';
            outputFolder= '6_rooms'
            OutputImageName = '';
    } 
    else if (elementType == 'R'){
        var programFolderName = 'findPatternsEraseElements/1_findTextpatterns';
            inputFolder = '6_rooms';
            outputFolder= '7_objects'
            OutputImageName = '';
    } 
    else if (elementType == 'O'){
        // in finding objects the input out put folder might change later
        var programFolderName = 'findPatternsEraseElements/1_findTextpatterns';
            inputFolder = '6_rooms';
            outputFolder= '7_objects'
            OutputImageName = '';
    }

    //testing
    programFolderName = 'hello';
    

    //execute pattern find python here
    console.log('************Executing '+programFolderName+' for '+inputFolder);
    // program path
    var pyshell = new PythonShell('../python/'+programFolderName+'/Model/MainLogic.py');
    //path to current directory passed over to python progam
    var MainProgramPath = String(parent_dir)+'/python/'+programFolderName+'/';
    //path to store image uploaded
    var SystemInputImagePath = parent_dir+'/files/'+inputFolder+'/';
    //path to store the SVG created by python program
    var OutputImagePath = parent_dir+'/files/'+outputFolder+'/';
    
    //pass paths to python program
    pyshell.send(MainProgramPath);
    pyshell.send(SystemInputImagePath);
    pyshell.send(OutputImagePath);
        
    //executing python program
    pyshell.on('message', function (message) {
        // received a message sent from the Python script (a simple "print" statement)
        console.log(message);
    });

    // end the input stream and allow the process to exit
    pyshell.end(function (err) {
        if (err) {
            throw err;
        }
        else{
            console.log('Executed Pattern Python');
            if (elementType == 'T'){
                findElementsPython('S',socket);
            }
            else if (elementType == 'S'){
                findElementsPython('D',socket);
            }
            else if (elementType == 'D'){
                findElementsPython('Wl',socket);
            }
            else if (elementType == 'Wl'){
                findElementsPython('Win',socket);
            }
            else if (elementType == 'Win'){
                findElementsPython('R',socket);
            }
            else if (elementType == 'R'){
                findElementsPython('O',socket);
            }
            else if (elementType == 'O'){
                console.log('Program Execution Completed');
            }
            else{
                console.log ('An error in Find Pattern program finding');
            }
        }
        
    });
    
}


function findElementsPython(elementType,socket){
    // set paths
    if (elementType == 'T'){
        var programFolderName = '1_detect_text';
            inputFolder = '1_text';
            outputFolder= '1_text'
            OutputImageName = '';
    }
    else if (elementType == 'S'){
        var programFolderName = '2_detect_stairs';
        inputFolder = '2_stairs';
        outputFolder= '2_stairs'
        OutputImageName = '';
    } 
    else if (elementType == 'D'){
        var programFolderName = '3_detect_doors';
            inputFolder = '3_doors';
            outputFolder= '3_doors'
            OutputImageName = '';
    } 
    else if (elementType == 'Wl'){
        var programFolderName = '4_detect_walls';
            inputFolder = '4_walls';
            outputFolder= '4_walls'
            OutputImageName = '';
    } 
    else if (elementType == 'Win'){
        var programFolderName = '5_detect_windows';
            inputFolder = '5_windows';
            outputFolder= '5_windows'
            OutputImageName = '';
    } 
    else if (elementType == 'R'){
        var programFolderName = '6_detect_rooms';
            inputFolder = '6_rooms';
            outputFolder= '6_rooms'
            OutputImageName = '';
    } 
    else if (elementType == 'O'){
        var programFolderName = '7_detect_objects';
            inputFolder = '7_objects';
            outputFolder= '7_objects'
            OutputImageName = '';
    }

    //testing
    var programFolderName = 'hello';

    //execute element detection
    console.log('----------------------------Executing '+programFolderName+' for '+inputFolder);
    // program path
    var pyshell = new PythonShell('../python/'+programFolderName+'/Model/MainLogic.py');
    //path to current directory passed over to python progam
    var MainProgramPath = String(parent_dir)+'/python/'+programFolderName+'/';
    //path to store image uploaded
    var SystemInputImagePath = parent_dir+'/files/'+inputFolder+'/';
    //path to store the SVG created by python program
    var OutputImagePath = parent_dir+'/files/'+outputFolder+'/';
    
    //pass paths to python program
    pyshell.send(MainProgramPath);
    pyshell.send(SystemInputImagePath);
    pyshell.send(OutputImagePath);
        
    //executing python program
    pyshell.on('message', function (message) {
        // received a message sent from the Python script (a simple "print" statement)
        console.log(message);
    });

    // end the input stream and allow the process to exit
    pyshell.end(function (err) {
        if (err) {
            throw err;
        }
        else{
            console.log('Executed Python');
            if (elementType == 'T'){
                patternFindEraseElements('T',socket);
                // socket.emit('detectedTexts','image.svg');
            }
            else if (elementType == 'S'){
                socket.emit('detectedStairs','image.svg');
            } 
            else if (elementType == 'D'){
                socket.emit('detectedDoors','image.svg');
            } 
            else if (elementType == 'Wl'){
                socket.emit('detectedWalls','image.svg');
            } 
            else if (elementType == 'Win'){
                socket.emit('detectedWindows','image.svg');
            } 
            else if (elementType == 'R'){
                socket.emit('detectedRooms','image.svg');
            } 
            else if (elementType == 'O'){
                socket.emit('detectedObjects','image.svg');
            } 
            else{
                console.log ('An error in detect element program finding');
            }
        }
        
    });
}