import os, shutil

class mainLogic:
    def __init__(self):
        main_path = '/home/anu/Documents/Bit_Bucket/SVG_FPID/python/'
        python_program_names = ['1_detect_text/','2_detect_stairs/','3_detect_doors/',
                                '4_detect_walls/','5_detect_windows/','6_detect_rooms/',
                                '7_detect_objects/']
        sub_folder_names = ['GraVVITAS/','OUTPUT/','OUTPUT_DEBUG/','Component_Data/',
                            'OBJECTS/']

        for each_py_prog in python_program_names:
            for each_sub in sub_folder_names:
                system_path = main_path + each_py_prog + each_sub
                if os.path.exists(system_path):
                    for each_dir in os.listdir(system_path):
                        if os.path.isfile(system_path+each_dir):
                            os.remove(system_path+each_dir)
                        else:
                            shutil.rmtree(system_path+each_dir)


if __name__ == '__main__':
    mainLogic()