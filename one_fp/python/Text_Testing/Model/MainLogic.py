import os,shutil,re,cv2
from natsort import natsorted

#---program files
from bb_elements import bb_elements
from bb_image import bb_image


class MainFormLogic():
    def __init__(self):


        self.current_directory = os.path.abspath(os.path.join(os.getcwd(), '..'))
        text_output_path = self.current_directory+ '/output/text/'
        image_output_path = self.current_directory+ '/output/images/'
        bb_image_path  = image_output_path + 'BB_images/'
        corpus_path = self.current_directory + '/input/Corpus/'
        input_im_path = self.current_directory + '/input/input_images/'
        convert_path = self.current_directory + '/input/convert/'

        bb_elements_obj = bb_elements()
        bb_image_obj = bb_image(input_im_path, image_output_path,bb_image_path,self.current_directory)

        # #---1...
        self.reset_folders(text_output_path)
        self.get_text_files(text_output_path)

        # # ---2...
        self.reset_folders(input_im_path)
        self.reset_folders(convert_path)
        self.find_relevant_images_copy(text_output_path,corpus_path,input_im_path)
        self.convert_svg_png(input_im_path,convert_path)

        # ---3...
        self.reset_folders(image_output_path)
        bb_element_data,ocr_data = bb_elements_obj.find_BB_elements(convert_path,text_output_path,
                              image_output_path)
        # bb_element_data,ocr_data = [],[]
        #
        # # ---4....
        self.reset_folders(bb_image_path)
        bb_image_obj.extract_bb_elements(bb_element_data,ocr_data)
        bb_image_obj.perform_ocr()


    def reset_folders(self,path):
        if (os.path.exists(path)):
            if os.path.isfile(path):
                os.remove(path)
            else:
                shutil.rmtree(path)
        os.mkdir(path)

    def get_text_files(self,text_output_path):
        input_path = '/home/anu/Documents/Bit_Bucket/SVG_FPID/files/outputs/'

        for image_folder_name in os.listdir(input_path):
            python_text_path = input_path+image_folder_name+'/1_text/3_detection_files/'
            js_text_path = input_path+image_folder_name+'/8_JS_results/'
            paths = [python_text_path,js_text_path]

            for each_path in paths:
                for each_text_file in os.listdir(each_path):
                    if 'cupboard' not in each_text_file:
                        shutil.copy(each_path + str(each_text_file),
                            text_output_path+image_folder_name+'_'+each_text_file[-16:])
                        # print self.text_output_path+image_folder_name+'_'+each_text_file[-16:]

    def find_relevant_images_copy(self,text_path,corpus_path,input_im_path):
        images_list = []
        for each_text_file in natsorted(os.listdir(text_path)):
            name = each_text_file.split('_')
            if name[0] not in images_list:
                images_list.append(name[0])

        for each_image in images_list:
            if os.path.exists(corpus_path+each_image+'.svg'):
                source = corpus_path+each_image+'.svg'
            else:
                source = corpus_path + each_image + '.SVG'
            destination = input_im_path+each_image+'.svg'
            shutil.copy(source,destination)


    def convert_svg_png(self,input_im_path,convert_path):
        for image_name in os.listdir(input_im_path):
            only_image_name = image_name[:-4]
            os.system('convert ' + input_im_path + str(
                image_name) + ' ' + convert_path + only_image_name + '.png')




if __name__ == '__main__':
    hwl1 = MainFormLogic()




    # def get_image_conversion_values(self,image_name,input_im_path,convert_path):
    #     original_image = cv2.imread(convert_path + image_name,cv2.IMREAD_GRAYSCALE)
    #     original_width, original_height  = original_image.shape
    #
    #     current_svg_image_path = input_im_path+image_name[:-4]+'.svg'
    #     new_image_path = convert_path + '250_'+image_name[:-4] + '.png'
    #     os.system('convert -density 250 ' + current_svg_image_path + ' -set density 250 ' + new_image_path)
    #     new_image = cv2.imread(new_image_path, cv2.IMREAD_GRAYSCALE)
    #     new_width, new_height = new_image.shape
    #     x_ratio = float(original_width) / new_width
    #     y_ratio = float(original_height)/new_height
    #     os.remove(new_image_path)
    #     print x_ratio,y_ratio
    #     return [x_ratio,y_ratio]