
import os,cv2,re
from natsort import natsorted

from math_functions import math_functions

class bb_elements:
    def __init__(self):
        self.math_functions_obj = math_functions()

    def find_BB_elements(self,convert_path,text_output_path,
                         image_output_path):
        bb_element_data = []
        ocr_data = []
        for image_name in natsorted(os.listdir(convert_path)):
            only_image_name = image_name[:-4]
            im_height,im_width = cv2.imread((convert_path+image_name),cv2.IMREAD_GRAYSCALE).shape
            # --- get ocr data
            # image_dimensions = self.get_image_conversion_values(image_name,input_im_path,convert_path)
            #-- since we are using 250 density  its always 0.288 for both x and y
            #-- use above method if we change density some day
            image_dimensions = [0.288,0.288]
            text_details_list = self.get_OCR_data(text_output_path,
                                                  only_image_name,image_dimensions)

            #--if text is not as text tags:
            if len(text_details_list) != 0:
                # --- read data written to file from js
                js_data_list = self.process_js_data(text_output_path, only_image_name)
                # --- get image
                image_color = cv2.imread(convert_path + image_name, cv2.IMREAD_COLOR)
                # --- match BBxes
                bb_elements = self.find_matchingBB(image_output_path,js_data_list,
                                     text_details_list,image_color, only_image_name)
                bb_element_data.append([image_name,[im_height,im_width],bb_elements])
                ocr_data.append([image_name,text_details_list])
            else:
                print image_name+' has Text Tags'

        return bb_element_data,ocr_data

    def process_js_data(self,output_path,only_image_name):
        js_data_list = []
        #-- read text file
        js_data = open(output_path+only_image_name+'_lElementData.txt','r')
        for element_data in js_data:
            #--get tagname ::: attribute data ::: BBdata
            element_data_list = element_data.split(':::')
            tagName = element_data_list[0].strip()
            position = element_data_list[1].strip()
            bbdata = element_data_list[2].strip()
            #---BB procesiing
            bb_data_list = bbdata.split(',')
            #--keep only numbers and '.'s
            bb_data_list[0] = bb_data_list[0][7:]
            num_bb_data_list =  [float(x) for x in bb_data_list]

            #--- attributes processing
            attributes = position.split('::')

            #--- get centre point of bbxes
            x1,y1,x2,y2 = num_bb_data_list
            center_x, center_y = (x1 + x2) / 2, (y1 + y2) / 2

            #-- add al to data list
            js_data_list.append([tagName,attributes,num_bb_data_list,[center_x, center_y]])

        js_data_list.reverse()

        return js_data_list

    def go_up_python_path(self,path, levels):
        final_path = path
        while levels >0:
            final_path = os.path.abspath(os.path.join(final_path, '..'))
            levels = levels - 1
        return final_path



    def get_OCR_data(self,output_path,only_image_name,image_dimensions):
        x_ratio, y_ratio = image_dimensions
        ocr_text_file_path = output_path+only_image_name+'_text_details.txt'
        if os.path.exists(ocr_text_file_path):
            # ---text details extraction from text file
            text_details_file = open(ocr_text_file_path, 'r')
            text_details_list = []
            for text_line in text_details_file:
                text_line_to_analyze = []
                detail_seperate = text_line.split(':')
                # ---since we find all text, it can have words like ':U' or 'KK:' or 'K:K',
                # ---need to add ':' to the detected text in that instance
                if len(detail_seperate) == 6:
                    text_line_to_analyze = detail_seperate
                elif len(detail_seperate) == 7:
                    word = []
                    if len(detail_seperate[0]) == 0:
                        word.append(':' + detail_seperate[1])
                    elif len(detail_seperate[1]) == 0:
                        word.append(detail_seperate[0] + ':')
                    else:
                        word.append(detail_seperate[0] + ':' + detail_seperate[1])

                    text_line_to_analyze = [word[0], detail_seperate[2], detail_seperate[3],
                                            detail_seperate[4], detail_seperate[5]]

                if len(text_line_to_analyze) > 0:
                    # --seperate and convert text cordinate to int
                    cordinates_string_2 = re.findall(r"[\w']+", text_line_to_analyze[1])
                    int_cordinates = [int(x) for x in cordinates_string_2]
                    cordintes_chunks = [int_cordinates[x:x + 2] for x in xrange(0, len(int_cordinates), 2)]
                    cordinate_to_append = 0
                    for cord in cordintes_chunks:
                        cordinate_to_append = cord

                    # --seperate and convert BB-box to int
                    bb_cordinates_string_2 = text_line_to_analyze[2].split(',')
                    bb_cordinates_string_2[0] = bb_cordinates_string_2[0][2:]
                    bb_cordinates_string_2[-1] = bb_cordinates_string_2[-1][:-2]
                    int_bb_cordinates = [float(x) for x in bb_cordinates_string_2]
                    int_bb_cordinates2 = [int(x) for x in int_bb_cordinates]
                    bb_cordintes_chunks = [int_bb_cordinates2[x:x + 4] for x in xrange(0, len(int_bb_cordinates2), 4)]
                    bb_cordinate_to_append = 0
                    for bb_cord in bb_cordintes_chunks:
                        bb_cordinate_to_append = bb_cord
                    confidence_string = re.findall(r"[\w']+", text_line_to_analyze[3])
                    confidence = int(confidence_string[0])

                    match_data = re.findall(r"[\w']+", text_line_to_analyze[-1])

                    text_details_list.append([text_line_to_analyze[0].strip(),
                                              cordinate_to_append, bb_cordinate_to_append,
                                              confidence, str(match_data[0])])
            text_details_file.close()

            # -- change cords
            for r,row in enumerate(text_details_list):
                centre_x,centre_y = row[1]
                bb_x1, bb_y1, bb_x2, bb_y2 = row[2]
                centre_x, centre_y = int(centre_x*x_ratio),int(centre_y*y_ratio)
                bb_x1, bb_y1, bb_x2, bb_y2 = int(bb_x1*x_ratio), int(bb_y1*y_ratio),int(bb_x2*x_ratio), int(bb_y2*y_ratio)
                text_details_list[r][1] = [centre_x,centre_y]
                text_details_list[r][2] = [bb_x1, bb_y1, bb_x2, bb_y2]
        else:
            text_details_list=[]

        return text_details_list

    def find_matchingBB(self,image_output_path,js_data_list,text_details_list,
                        image_color,only_image_name):
        bb_elements = []
        # print 'only_image_name----',only_image_name
        for ocr_data_row in text_details_list:
            #--get ocr BB
            ocr_x1,ocr_y1,ocr_x2,ocr_y2 = ocr_data_row[2]
            text = ocr_data_row[0]
            element_id_list = []
            cv2.rectangle(image_color, (int(ocr_x1), int(ocr_y1)),
                                              (int(ocr_x2), int(ocr_y2)), (0, 255, 0), 1)
            # if only_image_name == '11':
            #     print text,'--ocr',ocr_x1,ocr_y1,ocr_x2,ocr_y2

            # if text == 'ensuite' and only_image_name=='15':

            #--get each svg element BB
            for r, js_row in enumerate(js_data_list):
                    attributes = js_row[1]
                    bb_data = js_row[2]
                    bb_x1, bb_y1, bb_x2, bb_y2 = bb_data
                    bb_cx1, bb_cy1 = js_row[3]
                    # for row in js_row[1]:
                    #     if 'id' in row:
                    #         print row
                    #--check if svg_bb centre point is inside ocr_bb
                    inside_flag = self.math_functions_obj.check_if_point_inside_rect(bb_cx1,bb_cy1,ocr_x1,ocr_y1,ocr_x2,ocr_y2)
                    if inside_flag:
                        intersect_x1, intersect_y1, intersect_x2, intersect_y2 = self.math_functions_obj.find_intersection_of_two_rectangles(ocr_x1,ocr_y1,ocr_x2,ocr_y2,bb_x1, bb_y1, bb_x2, bb_y2)
                        intersect_width, intersect_height = self.math_functions_obj.get_rect_dimensions(intersect_x1,intersect_y1,intersect_x2,intersect_y2)
                        bb_width, bb_height = self.math_functions_obj.get_rect_dimensions(bb_x1, bb_y1, bb_x2, bb_y2 )

                        svg_bb_area = bb_width * bb_height
                        intersect_area = intersect_width * intersect_height
                        svg_bb_outside_ocr_area = svg_bb_area - intersect_area
                        #--debugging
                        # for row in js_row[1]:
                        #     if 'id' in row:
                        #         print row
                        # print svg_bb_area,intersect_area, svg_bb_outside_ocr_area
                        # print 'ratio: ', svg_bb_outside_ocr_area/svg_bb_area

                        if svg_bb_outside_ocr_area/svg_bb_area < 0.5:
                            ocr_width, ocr_height = self.math_functions_obj.get_rect_dimensions(ocr_x1,ocr_y1,ocr_x2,ocr_y2)
                            ocr_area = ocr_width * ocr_height
                            # print 'bb less outside ocr - True'
                            # for row in js_row[1]:
                            #     if 'id' in row:
                            #         print row
                            # print svg_bb_area,ocr_area
                            # print svg_bb_area/ocr_area
                            if svg_bb_area/ocr_area > 0.005:
                                # print "svg bb signitificant - True"
                                cv2.rectangle(image_color, (int(bb_x1), int(bb_y1)),
                                              (int(bb_x2), int(bb_y2)), (0, 0, 255), 1)
                                element_id = self.get_attribute_id(attributes)
                                element_id_list.append(element_id)

            cv2.putText(image_color, text,
                        (ocr_x1-5,ocr_y1-5), cv2.FONT_HERSHEY_PLAIN, 1,
                        (0, 0, 255), 2)
            bb_elements.append([text,element_id_list])
        cv2.imwrite(image_output_path + only_image_name+'_BB.png', image_color)
        return bb_elements

    def get_attribute_id(self,attributes):
        id = ''
        for single_attrib in attributes:
            if 'id' in single_attrib:
                id = single_attrib.split('=')[1]
                break
        return id