import sys,os,shutil,cv2,re,traceback
from Naked.toolshed.shell import execute_js, muterun_js
import urllib2


class MainFormLogic():
    #-------initialization method
    def __init__(self):
        self.current_directory, self.input_path, self.output_path = self.getJSargs()
        # self.current_directory = '/home/anu/Documents/Bit_Bucket/SVG_FPID/python/findPatternsEraseElements/1_findTextpatterns/'
        # self.results_path = self.current_directory+'/Results/'
        print 'Find Text Patterns and Erase'

        # --- copy input images files
        self.copy_files()


    def getJSargs(self):
        js_message = sys.stdin.readlines()
        input_path, current_directory, output_path,data = '', '', '',''
        for row_num, each in enumerate(js_message):
            if row_num == 0:
                current_directory = each.rstrip()
            elif row_num == 1:
                input_path = each.rstrip()
            else:
                output_path = each.rstrip()
        return current_directory, input_path, output_path

    def copy_files(self):
        # folders = ['/1_images/input/','/2_state_files/','/3_detection_files/',
        #            '/4_svg_process_files/']
        folders = ['/1_images/input/', '/2_state_files/', '/3_detection_files/']

        for each_folder in folders:
            source_path = os.path.join(self.input_path+each_folder)
            destination_path = os.path.join(self.output_path+each_folder)
            source_files = os.listdir(source_path)
            for source_file in source_files:
                if os.path.isfile(source_path + str(source_file)):
                    shutil.copy(source_path + str(source_file),
                                destination_path)

                if each_folder=='/1_images/input/':
                    detection_type = ((self.output_path.split('/'))[-2])[2:]
                    parent_dir = self.go_up_python_path(self.output_path,3)
                    destination_path = parent_dir + '/html_input/'+source_file[0:-4]+'_'+detection_type+'.svg'
                    shutil.copy(source_path + str(source_file),
                                destination_path)


    def go_up_python_path(self,path, levels):
        final_path = path
        while levels >0:
            final_path = os.path.abspath(os.path.join(final_path, '..'))
            levels = levels - 1
        return final_path














    #--- not used any more methods
    def check_BBrect_on_image(self):
        detected_data = [
            ['master bedroom', [118, 89],[50, 81, 187, 98],[100]],
            ['dining', [333, 145],[306, 136, 361, 154],[100]],
            ['kitchen', [462, 94],[429, 85, 495, 103],[100]]
        ]

        imagestore_path = '/home/anu/Documents/Bit_Bucket/SVG_FPID/'
        # svg_image_path = '/home/anu/Documents/Bit_Bucket/SVG_FPID/test.svg'
        os.system('convert ' + imagestore_path+'test.svg ' +imagestore_path+ 'test.png')

        imagestore_path = '/home/anu/Documents/Bit_Bucket/SVG_FPID/'
        image_color = cv2.imread(imagestore_path + 'test.png', cv2.IMREAD_COLOR)
        for text_row in detected_data:
            x1,y1,x2,y2 = text_row[2]
            cv2.rectangle(image_color, (x1, y1), (x2, y2), (0, 0, 255), 2)

        cv2.imwrite(imagestore_path + 'test-colored.png', image_color)


if __name__ == '__main__':
    hwl1 = MainFormLogic()