import sys,os,shutil,traceback
from natsort import natsorted
import xml.etree.ElementTree as ET
from lxml import etree

class MainFormLogic:
    def __init__(self):
        self.current_directory, self.images_path, self.python_folder_path, self.html_input_path = self.getJSargs()
        self.assign_ids()
        names_list = self.get_images_names_list()
        self.create_folders(names_list)


        # self.images_path = '/home/anu/Documents/Bit_Bucket/SVG_FPID/2orMoreimageUploadFiles/input_files/test/'
        # self.assign_ids()

    def getJSargs(self):
        js_message = sys.stdin.readlines()
        current_directory,images_path,python_folder_path,html_input_path = '', '','',''
        for row_num, each in enumerate(js_message):
            if row_num == 0:
                current_directory = each.rstrip()
            elif row_num == 1:
                images_path = each.rstrip()
            elif row_num == 2:
                python_folder_path = each.rstrip()
            else:
                html_input_path = each.rstrip()
        return current_directory, images_path,python_folder_path,html_input_path

    def get_images_names_list(self):
        #--delete sub files inside self.html_input_path
        for html_file in os.listdir(self.html_input_path):
            if html_file != '0_start.png':
                os.remove(self.html_input_path+html_file)

        #--copy all image files to html_input path and get images name list
        names_list = []
        if not(os.path.exists(self.images_path)):
            print 'Directory Not Found.'
            sys.exit()
        else:
            original_fp_path_unsorted = os.listdir(self.images_path)
            original_fp_path = natsorted(original_fp_path_unsorted)

            for each_image in original_fp_path:
                names_list.append(str(each_image))
                shutil.copy(self.images_path + str(each_image),
                            self.html_input_path)
        return names_list

    def create_folders(self,names_list):
        #--empty contents inside pyton path
        for content in os.listdir(self.python_folder_path):
            content_path = os.path.join(self.python_folder_path,content)
            try:
                shutil.rmtree(content_path)
            except:
                traceback.print_exc()

        #--create folders by image name
        sub_folder_names = ['1_text','2_stairs','3_doors','4_walls','5_windows',
                            '6_rooms','7_objects','8_JS_results']
        sub_sub_folders = ['1_images','2_state_files',
                           '3_detection_files','4_svg_process_files']
        for image_name in names_list:
            #--ignore default display image for html page
            if image_name != 'Start.png':
                image_folder_path = self.python_folder_path+image_name[:-4]+'/'
                os.mkdir(image_folder_path)
                for each_sub in sub_folder_names:
                    os.mkdir(image_folder_path+each_sub+'/')
                    if each_sub != '8_JS_results':
                        for each_sub2 in sub_sub_folders:
                            os.mkdir(image_folder_path + each_sub + '/'+each_sub2+'/')
                            if each_sub2=='1_images':
                                os.mkdir(image_folder_path+each_sub+'/'+each_sub2+'/input/')
                                os.mkdir(image_folder_path + each_sub +'/'+each_sub2+ '/output/')

    def assign_ids(self):
        for each_svg in natsorted(os.listdir(self.images_path)):
            file = open(self.images_path + each_svg, "r")
            tree = ET.parse(file)
            root = tree.getroot()
            id_count = 0
            for element in root.iter():
                # tag = element.tag.split("}")[1]
                if 'id' not in element.attrib:
                    element.attrib['id'] = 'n_id_' + str(id_count)
                    id_count = id_count+1
            namespace = root.tag.split('}')[0][1:]
            # # --- use same namespace in new svg : otherwise python adds a 'ns0' namesapce
            ET.register_namespace("", namespace)
            # test_path = '/home/anu/Documents/Bit_Bucket/SVG_FPID/'
            tree.write(self.images_path+ each_svg,'utf-8')



if __name__ == '__main__':
    hwl1 = MainFormLogic()