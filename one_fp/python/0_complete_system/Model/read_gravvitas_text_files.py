
import re

class read_gravvitas_text_files_class:



    def read_text_data(self,path,only_image_name):
        # ---text details extraction from text file
        text_details_file = open(path + only_image_name + '_6_text_details.txt', 'r')
        text_details_list = []
        for text_line in text_details_file:
            text_line_to_analyze = []
            detail_seperate = text_line.split(':')
            #---since we find all text, it can have words like ':U' or 'KK:' or 'K:K',
            #---need to add ':' to the detected text in that instance
            if len(detail_seperate) == 5:
                text_line_to_analyze = detail_seperate
            elif len(detail_seperate) == 6:
                word =[]
                if len(detail_seperate[0]) == 0:
                    word.append(':' + detail_seperate[1])
                elif len(detail_seperate[1]) == 0:
                    word.append(detail_seperate[0] + ':')
                else:
                    word.append(detail_seperate[0]+':'+detail_seperate[1])

                text_line_to_analyze = [word[0],detail_seperate[2],detail_seperate[3],
                                        detail_seperate[4],detail_seperate[5]]


            if len(text_line_to_analyze)>0:
                # --seperate and convert text cordinate to int
                cordinates_string_2 = re.findall(r"[\w']+", text_line_to_analyze[1])
                int_cordinates = [int(x) for x in cordinates_string_2]
                cordintes_chunks = [int_cordinates[x:x + 2] for x in xrange(0, len(int_cordinates), 2)]
                cordinate_to_append = 0
                for cord in cordintes_chunks:
                    cordinate_to_append = cord

                # --seperate and convert BB-box to int
                bb_cordinates_string_2 = re.findall(r"[\w']+", text_line_to_analyze[2])
                int_bb_cordinates = [int(x) for x in bb_cordinates_string_2]
                bb_cordintes_chunks = [int_bb_cordinates[x:x + 4] for x in xrange(0, len(int_bb_cordinates), 4)]
                bb_cordinate_to_append = 0
                for bb_cord in bb_cordintes_chunks:
                    bb_cordinate_to_append = bb_cord

                confidence_string = re.findall(r"[\w']+", text_line_to_analyze[3])
                confidence = int(confidence_string[0])

                match_data  = re.findall(r"[\w']+", text_line_to_analyze[-1])

                text_details_list.append([text_line_to_analyze[0].strip(),
                                          cordinate_to_append, bb_cordinate_to_append,
                                          confidence,str(match_data[0])])
        text_details_file.close()

        return text_details_list

    def find_dictionary_matched_words(self,all_detected_words):
        dictionary_words = []
        for text_row in all_detected_words:
            if text_row[-1] == 'match':
                dictionary_words.append(text_row[0:3])
        return dictionary_words