authour = 'anu'

import cv2, math, numpy as np,os
from PIL import Image
from operator import itemgetter

from iterative_functions import iterative_functions_class

class cupboard_class:

    def __init__(self,path_output_d,only_image_name):
        self.path_output_d = path_output_d
        self.iterative_functions_obj = iterative_functions_class()

    def find_cupboard(self,otsu_path, input_path,only_image_name,current_directory,text_cordinates, debug_mode):
        input_image_path = input_path+only_image_name+'/no_text.png'
        floorplan_img = cv2.imread(input_image_path, cv2.IMREAD_GRAYSCALE)
        img_height, img_width = floorplan_img.shape

        cupboard_data = []

        # --furniture list
        furniture_file = open(current_directory + '/Model/floor_plan_text/furniture_list.txt', 'r+')
        furniture_string = furniture_file.read()
        furniture_words_list = furniture_string.split()

        # --find text data of 'cupboard'
        for text_row in text_cordinates:
            for furniture_names in furniture_words_list:
               if text_row[0] == furniture_names:
                cupboard_data.append(text_row)

        possible_cupboards = []
        #--crop image to include only one cupboard
        for cupboard_num, cupboard_row in enumerate(cupboard_data):
            cupboard_textcordinate = cupboard_row[1]
            #--get bounding box cords
            x1,y1,x2,y2 = cupboard_row[2]
            #resize bb
            extension_length = abs(x2-x1)*5
            x3, y3, x4, y4 = self.iterative_functions_obj.resize_line(x1, y1,x2,y2, extension_length)
            #--extract resozed_BB area and write to new image
            stencil = np.zeros(floorplan_img.shape).astype(floorplan_img.dtype)
            p1, p2, p3, p4 = self.iterative_functions_obj.find_points_rectangle(x3, y3, x4, y4)
            contours = [np.array([[p1], [p2], [p3],[p4]])]

            color = [255, 255, 255]
            cv2.fillPoly(stencil, contours, color)
            extracted_image = cv2.bitwise_and(floorplan_img, stencil)
            # cv2.imwrite(self.cupboard_path+'--'+str(cupboard_num)+'_cropped.jpg', extracted_image)

            # output_path = self.cupboard_path+'-'+str(cupboard_num)
            # new_resolution_image = self.iterative_functions_obj.change_image_density(extracted_image,output_path)

            #--detect new contours
            ret, thresh = cv2.threshold(extracted_image, 0, 255, 1)
            contours, hierachy = cv2.findContours(thresh, 1, 2)
            selected_contours = []
            for count, current_cont in enumerate(contours):
                test_img = ~(np.zeros((img_height, img_width, 3), np.uint8))
                cv2.drawContours(test_img,[current_cont],-1,(0,0,0),3)
                # cv2.imwrite(self.cupboard_path + str(count) + '_contour.png', test_img)

                if cv2.pointPolygonTest(current_cont, (tuple(cupboard_textcordinate)), False) == 1:
                    current_area = cv2.contourArea(current_cont)
                    selected_contours.append([current_cont,current_area])

            #--sort selected contours based on area DESC
            contours_sorted = sorted(selected_contours, key=itemgetter(1))
            #--sort the contours to find smallest conotur-PL
            if len(contours_sorted)>0:
                possible_cupboards.append(contours_sorted[0][0])

        cupboard_contours = []
        for possible_cup in possible_cupboards:
            if len(possible_cup) > 0:
                cupboard_contours.append(possible_cup)

        text_cupboard_contours = []
        for cnt in cupboard_contours:
            temp_image_contour = []
            for l, level1 in enumerate(cnt):
                for level2 in level1:
                    x, y = level2
                    temp_image_contour.append([int(x), int(y)])
            if len(temp_image_contour) > 0:
                text_cupboard_contours.append(temp_image_contour)

        return text_cupboard_contours

    def add_cupboard_to_main_image(self,cupboard_contours,image_path,only_image_name,current_directory):
        #--non testing path
        input_image_path = image_path+only_image_name+'/'+'new_walls_windows_door.png'

        # # ## ------testing section
        # # # --english
        # # # new_walls_path = current_directory+'/input_fps/new_walls/online_walls/'
        # # # --french
        # new_walls_path = current_directory + '/input_fps/new_walls/walls_2/'
        # new_walls_list = os.listdir(new_walls_path)
        # for wall_room_num, wall_room in enumerate(new_walls_list):
        #     wall_image_name = str(wall_room)
        #     # --english
        #     # wall_name = wall_image_name[0:-4]
        #     # --french
        #     wall_name = wall_image_name[0:-11]
        #     if wall_name == only_image_name:
        #         # --english
        #         # self.orginal_image_path = new_walls_path+wall_name+'.png'
        #         # --french
        #         input_image_path = new_walls_path + wall_name + '_output.png'
        #         # print self.orginal_image_path
        #         break
        #--end testing


        main_image = cv2.imread(input_image_path,cv2.IMREAD_COLOR)
        #--draw cupboards
        for each_cupboard in cupboard_contours:
            cv2.drawContours(main_image, [each_cupboard], -1, (0, 0, 255), 5)
        cv2.imwrite(input_image_path,main_image)