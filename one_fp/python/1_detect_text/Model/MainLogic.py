__author__ = 'stefano'


from connectedcomponent import eps_circle, eps_ellipse, eps_rect
import shutil,os,cv2,time, sys, subprocess,getopt,traceback,copy,pyttsx
import numpy as np
from PIL import Image
from distutils.dir_util import copy_tree
from franges import frange
from franges import drange
from scipy import ndimage
from operator import itemgetter
from natsort import natsorted
from shutil import copyfile
from xlwt import Workbook


from image import Image_class
from graphCC import GraphCC
from room_object_match import room_object_match_class
import imageOperations as imop
from copy import deepcopy
from connectedcomponent import CC
from pre_process import pre_process_class
from accessible_fp import accessible_fp_generator
from iterative_functions import iterative_functions_class
from room_functions import room_functions_class
from evaluate import evaluation_class
from cupboard_find import cupboard_class
from svg_generation import svg_generator



class MainFormLogic():
    #-------initialization method
    def __init__(self):
        self.path_floorplan = ''
        self.converted_floorplan_path = ''
        self.path_output = ''
        self.path_output_d = ''
        self.path_gravvitas = ''
        self.current_directory =''
        self.path_component_data = ''
        #--make 'debug_mode' True to see intermediate results-------
        debug_mode = False

        #----get path
        self.current_directory, self.input_path, self.output_path = self.get_path()

        #--create folders
        self.create_output_folders(self.current_directory,debug_mode)


        #--get list of input floorplans
        list_images_floorplan = self.pre_process_images(self.current_directory)


        ##---take each floorplan name from list of images
        for file_name in list_images_floorplan:
            try:
                print 'Image is : ',file_name
                img = cv2.imread(self.path_floorplan + file_name, cv2.IMREAD_GRAYSCALE)
                self.floorplan_component_ID(file_name,img,self.path_floorplan,debug_mode)


            except(KeyboardInterrupt,SystemExit):
                print "User interrupted!"
                try:
                    sys.exit(0)
                except SystemExit:
                    os._exit(0)

            except:
                print "---------------------Error occured with-------------------------", str(file_name)
                traceback.print_exc()


        svg_files = os.listdir(self.path_gravvitas)
        for svg_file in svg_files:
            if os.path.isfile(self.path_gravvitas + str(svg_file)):
                if 'cupboard_details.txt' in str(svg_file):
                    shutil.copyfile((self.path_gravvitas + str(svg_file)),
                                    self.output_path + '/8_cupboard_details.txt')
                elif 'text_details.txt' in str(svg_file):
                    shutil.copyfile((self.path_gravvitas + str(svg_file)),
                                    self.output_path + '/6_text_details.txt')



    def create_output_folders(self,current_directory,debug_mode):
        self.path_output = current_directory + '/OUTPUT/'
        print self.path_output
        if not (os.path.exists(self.path_output)):
            os.mkdir(self.path_output)

        self.path_output_d = current_directory + '/OUTPUT_DEBUG/'
        if not (os.path.exists(self.path_output_d)):
            os.mkdir(self.path_output_d)
        self.path_component_data = current_directory + '/Component_Data/'
        if not (os.path.exists(self.path_component_data)):
            os.mkdir(self.path_component_data)
        #--create place to write text for GraVVITTAS
        self.path_gravvitas=current_directory+'/GraVVITAS/'
        if not(os.path.exists(self.path_gravvitas)):
            os.mkdir(self.path_gravvitas)


        list_dir_output = os.listdir(self.path_output)
        if len(list_dir_output) > 0:
            t = [int(x) for x in list_dir_output]
            t.sort()
            name = str(t[len(t) - 1] + 1)
            self.path_output = self.path_output + name + '/'
            os.mkdir(self.path_output)
        else:
            self.path_output = self.path_output + str(1) + '/'
            name = "1"
            os.mkdir(self.path_output)

        # OUTPUT_DEBUG folder incremental values for folder name
        if not (os.path.exists(self.path_output_d + name + "/")):
            os.mkdir(self.path_output_d + name + "/")
            self.path_output_d = self.path_output_d + name + "/"
        else:
            shutil.rmtree(self.path_output_d + name + "/")
            os.mkdir(self.path_output_d + name + "/")
            self.path_output_d = self.path_output_d + name + "/"


        #GRAVVITAS folder incremental values for folder name
        if not(os.path.exists(self.path_gravvitas+ name +"/")):
            os.mkdir(self.path_gravvitas+ name +"/")
            self.path_gravvitas = self.path_gravvitas+ name +"/"
        else:
            shutil.rmtree(self.path_gravvitas+ name +"/")
            os.mkdir(self.path_gravvitas+ name +"/")
            self.path_gravvitas = self.path_gravvitas+ name +"/"
        self.path_gravvitas=self.path_gravvitas+'/TEXT/'
        if not(os.path.exists(self.path_gravvitas)):
            os.mkdir(self.path_gravvitas)


        #create subfolders inside main OUTPUT and OUTPUT_DEBUG folders
        self.create_path_extract_text()
        self.create_path_preprocessing()
        self.create_path_state_files()

    def pre_process_images_js(self,current_directory):
        input_path_floorplan = self.input_path

        #--check if input folder exists, carry out rest of the program if it does
        if not(os.path.exists(input_path_floorplan)):
            print 'Directory Not Found.'
            sys.exit()
        else:
            # print 'input_path_floorplan',input_path_floorplan
            # original_fp_path_unsorted = os.listdir(input_path_floorplan)
            # original_fp_path = natsorted(original_fp_path_unsorted)



            # for each_image in original_fp_path:


            self.converted_floorplan_path = self.current_directory+'/input_fps/converted_fps/'
            if (os.path.exists(self.converted_floorplan_path)):
                shutil.rmtree(self.converted_floorplan_path)
            os.makedirs(self.converted_floorplan_path)


            if str(self.image_name)=='.DS_Store':
                os.remove(input_path_floorplan+str(self.image_name))
            else:
                if self.image_name[-4:]=='.png':
                    shutil.copy(input_path_floorplan+str(self.image_name),self.converted_floorplan_path+str(self.image_name))
                elif self.image_name[-4:]=='.svg' or self.image_name[-4:]=='.SVG':
                    im_name = str(self.image_name)
                    only_im_name = im_name[:-4]
                    # os.system('convert -density 40 '+input_path_floorplan+str(floor)+' -set density 40 '+self.converted_floorplan_path+only_im_name+'_40.png')
                    # os.system('convert -density 50 '+input_path_floorplan+str(floor)+' -set density 50 '+self.converted_floorplan_path+only_im_name+'_50.png')
                    # os.system('convert -density 60 '+input_path_floorplan+str(floor)+' -set density 60 '+self.converted_floorplan_path+only_im_name+'_60.png')
                    # os.system('convert -density 70 '+input_path_floorplan+str(floor)+' -set density 70 '+self.converted_floorplan_path+only_im_name+'_70.png')
                    # os.system('convert -density 100 '+input_path_floorplan+str(floor)+' -set density 100 '+self.converted_floorplan_path+only_im_name+'_100.png')
                    # os.system('convert -density 150 '+input_path_floorplan+str(floor)+' -set density 150 '+self.converted_floorplan_path+only_im_name+'_150.png')
                    # os.system('convert -density 200 '+input_path_floorplan+str(floor)+' -set density 200 '+self.converted_floorplan_path+only_im_name+'_200.png')
                    os.system('convert -density 250 '+input_path_floorplan+str(self.image_name)+' -set density 250 '+self.converted_floorplan_path+only_im_name+'.png')

                else:
                    im_conv = Image.open(input_path_floorplan+str(self.image_name))
                    im_name = str(self.image_name)
                    only_im_name = im_name[:-4]
                    im_conv.save(self.converted_floorplan_path+only_im_name+'.png')

            self.path_floorplan = self.converted_floorplan_path
            list_images_floorplan_unsorted = os.listdir(self.converted_floorplan_path)
            list_images_floorplan = natsorted(list_images_floorplan_unsorted)

            return list_images_floorplan


    def pre_process_images(self,current_directory):
        input_path_floorplan = self.input_path

        #--check if input folder exists, carry out rest of the program if it does
        if not(os.path.exists(input_path_floorplan)):
            print 'Directory Not Found.'
            sys.exit()
        else:
            # self.path_floorplan = input_path_floorplan
            # print 'input_path_floorplan',input_path_floorplan
            original_fp_path_unsorted = os.listdir(input_path_floorplan)
            original_fp_path = natsorted(original_fp_path_unsorted)
            # for each_image in original_fp_path:


            self.converted_floorplan_path = self.current_directory+'/input_fps/converted_fps/'
            if (os.path.exists(self.converted_floorplan_path)):
                shutil.rmtree(self.converted_floorplan_path)
            os.makedirs(self.converted_floorplan_path)

            for floor in original_fp_path:
                # print 'str(floor)',str(floor)
                if str(floor)=='.DS_Store':
                    os.remove(input_path_floorplan+str(floor))
                else:
                    if floor[-4:]=='.png':
                        shutil.copy(input_path_floorplan+str(floor),self.converted_floorplan_path+str(floor))
                    elif floor[-4:]=='.svg' or floor[-4:]=='.SVG':
                        im_name = str(floor)
                        only_im_name = im_name[:-4]
                        os.system('convert -density 250 ' + input_path_floorplan + str(
                            floor) + ' -set density 250 ' + self.converted_floorplan_path + only_im_name + '.png')

                    else:
                        im_conv = Image.open(input_path_floorplan+str(floor))
                        im_name = str(floor)
                        only_im_name = im_name[:-4]
                        im_conv.save(self.converted_floorplan_path+only_im_name+'.png')

            self.path_floorplan = self.converted_floorplan_path
            list_images_floorplan_unsorted = os.listdir(self.converted_floorplan_path)
            list_images_floorplan = natsorted(list_images_floorplan_unsorted)



            return list_images_floorplan


    def get_image_dimensions(self,input_path_floorplan,only_im_name,floor):
        image_path = self.converted_floorplan_path + 'original_' + only_im_name + '.png'
        os.system('convert ' + input_path_floorplan + str(
            floor) + ' ' + image_path)
        current_image = cv2.imread(image_path,cv2.IMREAD_GRAYSCALE)
        original_height, original_width =  current_image.shape
        os.remove(image_path)

        os.system('convert -density 250 ' + input_path_floorplan + str(
            floor) + ' -set density 250 ' + self.converted_floorplan_path + only_im_name + '.png')
        density_changed_image = cv2.imread(self.converted_floorplan_path + only_im_name + '.png', cv2.IMREAD_GRAYSCALE)
        converted_height, converted_width = density_changed_image.shape

        # --get conversion ratio
        x_ratio = float(original_width) / converted_width
        y_ratio = float(original_height) / converted_height


        return x_ratio, y_ratio


    def floorplan_component_ID(self,file_name,img,orginal_img_path,debug_mode):
        # ---extract only image name without extensions
        only_image_name = file_name[0:-4]
        path = self.path_output_d + only_image_name + '/'
        if not (os.path.exists(path)):
            os.mkdir(path)


        image_color = cv2.imread(orginal_img_path + file_name, cv2.IMREAD_COLOR)

        # ---------Otsu's thresholding to clean image and remove smoothing effects
        ret2, otsu = cv2.threshold(img, 0, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)
        cv2.imwrite(self.path_output_d + 'preprocessing/' + only_image_name + '_otsu.png', otsu)


        image = Image_class(self.current_directory,file_name, self.path_output,
                            self.path_output_d,self.path_component_data, otsu, image_color)


        print 'Finding connected components'
        image.find_connected_componet()

        print 'Text Recognition'
        text_cordinates = image.detect_text(self.path_gravvitas, only_image_name, otsu,
                                            debug_mode)

        print 'Cupboard Recognition'
        cupboard_obj = cupboard_class(self.path_output_d, only_image_name)
        otsu_path = self.path_output_d + 'preprocessing/' + only_image_name + '_otsu.png'
        cupboard_contours = cupboard_obj.find_cupboard(otsu_path, self.path_output,
                                                       only_image_name, self.current_directory, text_cordinates,
                                                       debug_mode)
        self.write_list_rect('contour', cupboard_contours, orginal_img_path + file_name,
                             only_image_name, '_8_cupboard_details.txt', 'Cupboard')

        # print 'Erase Text'
        # shutil.copy(self.path_output+only_image_name+'/'+'no_text.png',
        #             self.path_gravvitas + only_image_name + '_no_text.png')

    def get_path(self):
        # current_directory, input_path, output_path = self.get_path_from_local_machine()
        current_directory, input_path, output_path = self.get_path_from_js()
        # print 'current_directory-',current_directory
        # print 'input_path-',input_path
        # print 'output_path-',output_path
        return current_directory, input_path, output_path

    def get_path_from_local_machine(self):
        #--/home/ub/Documents/node_js/socketio/python/floor_plan_code
        current_directory = os.path.abspath(os.path.join(os.getcwd(), '..'))
        #--/home/ub/Documents/node_js/socketio/python/floor_plan_code/input_fps/anu/
        input_path = current_directory+ '/input_fps/anu/'
        # input_path = current_directory + '/input_fps/current_working_fps/'
        #--/home/ub/Documents/node_js/socketio/python/floor_plan_code/SVG_results/final_floorplan.svg
        output_path = current_directory+'/Component_Data/'
        return current_directory, input_path, output_path,''

    def get_path_from_js(self):
        js_message = sys.stdin.readlines()
        input_path, current_directory,output_path,image_name = '','','',''
        for row_num,each in enumerate(js_message):
            if row_num==0:
                current_directory = each.rstrip()
            elif row_num==1:
                input_path = each.rstrip()+'1_images/input/'
            elif row_num==2:
                output_path = each.rstrip()+'/3_detection_files/'
            # else:
            #     image_name = each.rstrip()+'.svg'
        return current_directory, input_path, output_path


    def write_list_rect(self,type, component_list,orginal_img_path,only_image_name,
                        text_file_name,text):
        # --write to text file for gravvitas
        file = open(self.path_gravvitas + only_image_name + text_file_name, 'a')
        for row in component_list:
            if type == 'rect':
                x1, y1 = row[0]
                x2, y2 = row[1]
                file.write(text+' : ' + str(x1) + ',' + str(y1) + ',' + str(x2) + ',' + str(y2) + '\n')
            elif type == 'contour':
                contour = ''
                for c, cord in enumerate(row):
                    if c == 0:
                        contour = contour + '[' + str(cord[0]) + ',' + str(cord[1]) + ']'
                    else:
                        contour = contour + ',[' + str(cord[0]) + ',' + str(cord[1]) + ']'
                file.write(text + ' : ' + contour + '\n')
        file.close()


    def create_path_state_files(self):
        state_path = self.path_component_data + 'state/'
        if not (os.path.exists(state_path)):
            os.mkdir(state_path)

    def create_path_extract_text(self):
        extract_text_path = self.path_output_d+'Extract_text/'
        if not(os.path.exists(extract_text_path)):
            os.mkdir(extract_text_path)
            os.mkdir(extract_text_path+'area_dim/')
            os.mkdir(extract_text_path+'area_dim_2/')
            os.mkdir(extract_text_path+'asp_ratio/')
            os.mkdir(extract_text_path+'density/')
            os.mkdir(extract_text_path+'final_images/')
            os.mkdir(extract_text_path+'text_identification/')

         #in the path "path_clean_image", I save the output of the function clean_image (image.py)
        path_clean_image=extract_text_path+'clean_image/'
        if os.path.exists(path_clean_image):
            shutil.rmtree(path_clean_image)
        else:
            os.mkdir(path_clean_image)
            os.mkdir(path_clean_image+'Canny/')
            os.mkdir(path_clean_image+'Hough/')
            os.mkdir(path_clean_image+'Clean/')
            os.mkdir(path_clean_image+'Open/')# in this path there are the final image

    def create_path_preprocessing(self):
        path_preprocessing=self.path_output_d+'preprocessing/'
        if not(os.path.exists(path_preprocessing)):
            os.mkdir(path_preprocessing)



if __name__ == '__main__':
    start = time.time()
    hwl1 = MainFormLogic()
    end = time.time()
    print end - start
