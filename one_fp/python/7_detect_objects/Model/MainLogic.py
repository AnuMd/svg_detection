__author__ = 'stefano'


import shutil,os,cv2,time, sys, subprocess,getopt,traceback,copy,pyttsx
import numpy as np
from PIL import Image
from natsort import natsorted


from image import Image_class
from graphCC import GraphCC
import imageOperations as imop
from connectedcomponent import CC
from room_functions import room_functions_class
from object_recognition_functions import object_recog_functions
from iterative_functions import iterative_functions_class


class MainFormLogic():
    #-------initialization method
    def __init__(self):
        self.thresholds = [0,0,0,0,0]
        self.path_floorplan = ''
        self.converted_floorplan_path = ''
        self.path_output = ''
        self.path_output_d = ''
        self.path_gravvitas = ''
        self.object_path = ''
        self.object_output_path = ''
        self.current_directory =''
        self.path_component_data = ''
        #--make 'debug_mode' True to see intermediate results-------
        debug_mode = False

        #----get path
        self.current_directory, self.input_path, self.output_path = self.get_path()


        #--create folders
        self.create_output_folders(self.current_directory,debug_mode)


        #--get list of input floorplans
        list_images_floorplan = self.pre_process_images(self.current_directory)

        # --copy back from js text files
        text_filePath = self.output_path + '/3_detection_files/'
        text_files = os.listdir(text_filePath)
        for text_file in text_files:
            if os.path.isfile(text_filePath + str(text_file)):
                shutil.copy(text_filePath + str(text_file), self.path_component_data)

        ##---take each floorplan name from list of images
        for file_name in list_images_floorplan:
            try:
                # # start_image_process_time = time.time()
                print 'Image is : ',file_name
                img = cv2.imread(self.path_floorplan + file_name, cv2.IMREAD_GRAYSCALE)
                orginal_img_height, orginal_img_width = img.shape
                self.floorplan_component_ID(file_name,img,self.path_floorplan,
                                            orginal_img_height, orginal_img_width,debug_mode)




            except(KeyboardInterrupt,SystemExit):
                print "User interrupted!"
                try:
                    sys.exit(0)
                except SystemExit:
                    os._exit(0)

            except:
                print "---------------------Error occured with-------------------------", str(file_name)
                traceback.print_exc()

        # ---copy text files
        svg_files = os.listdir(self.path_gravvitas)
        for svg_file in svg_files:
            if os.path.isfile(self.path_gravvitas + str(svg_file)):
                if '_2_object_details.txt' in str(svg_file):
                    shutil.copy(self.path_gravvitas + str(svg_file),
                                self.output_path + '3_detection_files/2_object_details.txt')
                elif '_2_svg_objects_details.txt' in str(svg_file):
                    shutil.copy(self.path_gravvitas + str(svg_file),
                                self.output_path + '3_detection_files/2_svg_objects_details.txt')



    def create_output_folders(self,current_directory,debug_mode):
        #create folders to store output - OUTPUT/OUTPUT_DEBUG/RESULTS_TEST/EDGES folders
        self.path_output=current_directory+'/OUTPUT/'
        if not(os.path.exists(self.path_output)):
                os.mkdir(self.path_output)
        self.path_output_d=current_directory+'/OUTPUT_DEBUG/'
        if not(os.path.exists(self.path_output_d)):
                os.mkdir(self.path_output_d)
        self.path_component_data = current_directory + '/Component_Data/'
        if not (os.path.exists(self.path_component_data)):
            os.mkdir(self.path_component_data)
        #--create path for OBJECTS results
        self.object_path = current_directory+'/OBJECTS/'
        if not os.path.exists(self.object_path):
            os.mkdir(self.object_path)
        #--create place to write text for GraVVITTAS
        self.path_gravvitas=current_directory+'/GraVVITAS/'
        if not(os.path.exists(self.path_gravvitas)):
            os.mkdir(self.path_gravvitas)


        #--incremental folder creation--------------------------------------
        #OUTPUT folder incremental values for folder name
        list_dir_output = os.listdir(self.path_output)
        if len(list_dir_output)>0:
            t = [int(x) for x in list_dir_output]
            t.sort()
            name = str(t[len(t)-1]+1)
            self.path_output = self.path_output+name+'/'
            os.mkdir(self.path_output)
        else:
            self.path_output = self.path_output+str(1)+'/'
            name="1"
            os.mkdir(self.path_output)

        #OUTPUT_DEBUG folder incremental values for folder name
        if not(os.path.exists(self.path_output_d+ name +"/")):
            os.mkdir(self.path_output_d+ name +"/")
            self.path_output_d = self.path_output_d+ name +"/"
        else:
            shutil.rmtree(self.path_output_d+ name +"/")
            os.mkdir(self.path_output_d+ name +"/")
            self.path_output_d = self.path_output_d+ name +"/"

        #GRAVVITAS folder incremental values for folder name
        if not(os.path.exists(self.path_gravvitas+ name +"/")):
            os.mkdir(self.path_gravvitas+ name +"/")
            self.path_gravvitas = self.path_gravvitas+ name +"/"
        else:
            shutil.rmtree(self.path_gravvitas+ name +"/")
            os.mkdir(self.path_gravvitas+ name +"/")
            self.path_gravvitas = self.path_gravvitas+ name +"/"
        self.path_gravvitas=self.path_gravvitas+'/TEXT/'
        if not(os.path.exists(self.path_gravvitas)):
            os.mkdir(self.path_gravvitas)

        #OBJECTS folder incremental values for folder name
        list_dir_output = os.listdir(self.object_path)
        if len(list_dir_output)>0:
            t = [int(x) for x in list_dir_output]
            t.sort()
            self.name = str(t[len(t)-1]+1)+'/'
        else:
            self.name = '1/'
        self.object_output_path = self.object_path
        self.object_path = self.object_path+self.name
        if not os.path.exists(self.object_path):
            os.mkdir(self.object_path)



        self.create_path_preprocessing()
        self.create_path_state_files()



    def pre_process_images(self,current_directory):
        input_path_floorplan = self.input_path

        #--check if input folder exists, carry out rest of the program if it does
        if not(os.path.exists(input_path_floorplan)):
            print 'Directory Not Found.'
            sys.exit()
        else:
            # self.path_floorplan = input_path_floorplan
            # print 'input_path_floorplan',input_path_floorplan
            original_fp_path_unsorted = os.listdir(input_path_floorplan)
            original_fp_path = natsorted(original_fp_path_unsorted)
            # for each_image in original_fp_path:


            self.converted_floorplan_path = self.current_directory+'/input_fps/converted_fps/'
            if (os.path.exists(self.converted_floorplan_path)):
                shutil.rmtree(self.converted_floorplan_path)
            os.makedirs(self.converted_floorplan_path)


            for floor in original_fp_path:
                # print 'str(floor)',str(floor)
                if str(floor)=='.DS_Store':
                    os.remove(input_path_floorplan+str(floor))
                else:
                    if floor[-4:]=='.png':
                        shutil.copy(input_path_floorplan+str(floor),self.converted_floorplan_path+str(floor))
                    elif floor[-4:]=='.svg' or floor[-4:]=='.SVG':
                        im_name = str(floor)
                        only_im_name = im_name[:-4]
                        # os.system('convert -density 40 '+input_path_floorplan+str(floor)+' -set density 40 '+self.converted_floorplan_path+only_im_name+'_40.png')
                        # os.system('convert -density 50 '+input_path_floorplan+str(floor)+' -set density 50 '+self.converted_floorplan_path+only_im_name+'_50.png')
                        # os.system('convert -density 60 '+input_path_floorplan+str(floor)+' -set density 60 '+self.converted_floorplan_path+only_im_name+'_60.png')
                        # os.system('convert -density 70 '+input_path_floorplan+str(floor)+' -set density 70 '+self.converted_floorplan_path+only_im_name+'_70.png')
                        # os.system('convert -density 100 '+input_path_floorplan+str(floor)+' -set density 100 '+self.converted_floorplan_path+only_im_name+'_100.png')
                        # os.system('convert -density 150 '+input_path_floorplan+str(floor)+' -set density 150 '+self.converted_floorplan_path+only_im_name+'_150.png')
                        # os.system('convert -density 200 '+input_path_floorplan+str(floor)+' -set density 200 '+self.converted_floorplan_path+only_im_name+'_200.png')
                        os.system('convert -density 250 '+input_path_floorplan+str(floor)+' -set density 250 '+self.converted_floorplan_path+only_im_name+'_250.png')

                    else:
                        im_conv = Image.open(input_path_floorplan+str(floor))
                        im_name = str(floor)
                        only_im_name = im_name[:-4]
                        im_conv.save(self.converted_floorplan_path+only_im_name+'.png')

            self.path_floorplan = self.converted_floorplan_path
            list_images_floorplan_unsorted = os.listdir(self.converted_floorplan_path)
            list_images_floorplan = natsorted(list_images_floorplan_unsorted)

            return list_images_floorplan

    def floorplan_component_ID(self,file_name,img,orginal_img_path,orginal_img_height,
                               orginal_img_width,debug_mode):

        # ---extract only image name without extensions
        only_image_name = file_name[0:-4]
        path = self.path_output_d + only_image_name + '/'
        if not (os.path.exists(path)):
            os.mkdir(path)


        print 're-create detection images'
        room_obj = room_functions_class()
        stefano_output_image, walls_and_doors_image, red_wall_image = room_obj.recreate_detections(self.path_component_data, self.current_directory,
                                     self.path_floorplan,only_image_name,
                                     orginal_img_height, orginal_img_width)


        print 'read avg door width'
        avg_door_width = self.read_avg_door_width(self.path_component_data)


        print 'Creating Room Graphs '
        image_color = cv2.imread(orginal_img_path + file_name, cv2.IMREAD_COLOR)
        ret2, otsu = cv2.threshold(img, 0, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)
        image = Image_class(self.current_directory, file_name, self.path_output,
                            self.path_output_d, self.path_component_data, otsu, image_color)
        all_image_results, text_coordinates = image.find_connected_componet_no_wall(self.path_gravvitas,
                                                                                    only_image_name,
                                                                                    stefano_output_image,
                                                                                    walls_and_doors_image,
                                                                                    red_wall_image,
                                                                                    otsu, avg_door_width, debug_mode)

        print 'Finding objects'
        object_recog_functions_object = object_recog_functions()
        object_details = object_recog_functions_object.find_objects(self.current_directory, self.path_output,
                                                   self.object_path, self.path_gravvitas,
                                                   only_image_name, self.thresholds,
                                                   avg_door_width, all_image_results,
                                                   text_coordinates, debug_mode)

        # #--testing
        # cv2.imwrite(self.path_component_data+'no_text.png',all_image_results[3])

        print 'Find Objects for SVG'
        selected_object_list = object_recog_functions_object.find_objects_for_svg(self.object_path,object_details,all_image_results,
                                                           self.path_component_data)
        self.write_detection_for_svg('point', selected_object_list,
                                     only_image_name, '_2_svg_objects_details.txt')

    def read_avg_door_width(self,path):
        avg_door_width = 0
        avg_door_width_details_file = open(path + '10_door_width.txt', 'r')
        for text_line in avg_door_width_details_file:
            avg_door_width = float(text_line)
            break
        return avg_door_width

    def get_path(self):
        # current_directory, input_path, output_path = self.get_path_from_local_machine()
        current_directory, input_path, output_path = self.get_path_from_js()
        # print 'current_directory',current_directory
        # print 'input_path',input_path
        # print 'output_path',output_path
        return current_directory, input_path, output_path

    def get_path_from_local_machine(self):
        #--/home/ub/Documents/node_js/socketio/python/floor_plan_code
        current_directory = os.path.abspath(os.path.join(os.getcwd(), '..'))
        #--/home/ub/Documents/node_js/socketio/python/floor_plan_code/input_fps/anu/
        input_path = current_directory+ '/input_fps/anu/'
        # input_path = current_directory + '/input_fps/current_working_fps/'
        #--/home/ub/Documents/node_js/socketio/python/floor_plan_code/SVG_results/final_floorplan.svg
        output_path = current_directory+'/Component_Data/'
        return current_directory, input_path, output_path

    def get_path_from_js(self):
        js_message = sys.stdin.readlines()
        input_path, current_directory,output_path = '','',''
        for row_num,each in enumerate(js_message):
            if row_num==0:
                current_directory = each.rstrip()
            elif row_num==1:
                input_path = each.rstrip()+ '1_images/input/'
            else:
                output_path = each.rstrip()
        return current_directory, input_path, output_path


    def write_detection_for_svg(self,type, component_list,only_image_name,
                        text_file_name):
        # test_image = self.image_color.copy()
        iterative_obj = iterative_functions_class()
        file = open(self.path_component_data+only_image_name+text_file_name,'a')
        for row in component_list:
            if type == 'rect':
                x1, y1 = row[0]
                x2, y2 = row[1]
                cx,cy = iterative_obj.find_centre_of_rectangle(x1, y1,x2, y2)
                file.write(str(cx) + ',' + str(cy) + '\n')
                # cv2.rectangle(test_image,(x1, y1),(x2,y2),(255,0,0),-1)
            if type == 'point':
                x,y = row
                file.write(str(x) + ',' + str(y) + '\n')
                # cv2.circle(test_image, (x, y), 5, (0, 0, 255), -1)
        file.close()



    def create_path_state_files(self):
        state_path = self.path_component_data + 'state/'
        if not (os.path.exists(state_path)):
            os.mkdir(state_path)


    def create_path_preprocessing(self):
        path_preprocessing=self.path_output_d+'preprocessing/'
        if not(os.path.exists(path_preprocessing)):
            os.mkdir(path_preprocessing)







if __name__ == '__main__':
    start = time.time()
    hwl1 = MainFormLogic()
    end = time.time()
    print end - start
