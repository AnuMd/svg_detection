__author__ = 'stefano'


import numpy as np
import cv2
import os
import sys
from decimal import Decimal
import sys, getopt

class PrecisionAndRecall():

    def __init__(self):

        args = sys.argv[1:]
        #
        # folder_gt = args[0]
        # folder_log = args[1]
        folder_gt = "test"
        folder_log = "1"

        a=os.path.dirname(os.getcwd())
        path_log = a + "/OUTPUT/" + folder_log + "/"
        if not(os.path.exists(path_log)):
            print "The folder with file .txt it's not exists. Give valid path"
            sys.exit()

        path_gt = a+ "/" + folder_gt + "/"

        if not(os.path.exists(path_gt)):
            print "The folder with file .grt it's not exists. Give valid path"
            sys.exit()

        list_file_gt = os.listdir(path_gt)
        for file in os.listdir(path_gt):
            if not(file.endswith(('.grt'))):
                list_file_gt.remove(file)
        if len(list_file_gt)==0:
            print "This folder doesn't contain file of ground truth. Give valid path"
            sys.exit()

       # list_log = os.listdir(a+"/groundTruth/")


        if not(os.path.exists(a+'/RESULTS_TEST/')):
            os.mkdir(a+'/RESULTS_TEST/')
        file = open(a+"/RESULTS_TEST/resultTest_exp_"+folder_log+".txt","w")
        t_precision_walls = []
        t_precision_doors = 0
        t_precision_windows = 0
        t_recall_walls = []
        t_recall_doors = 0
        t_recall_windows = 0
        t_precision_strain= 0
        t_recall_strain = 0

        t_n_walls_gt = 0
        t_n_doors_gt = 0
        t_n_windows_gt = 0
        t_n_strain_gt = 0
        t_n_walls = 0
        t_n_doors = 0
        t_n_windows = 0
        t_n_strain = 0
        check_file_log = False

        for l in list_file_gt:
            h1,wi1,list_walls_gt,list_doors_gt,list_windows_gt,list_strain_gt = self.readFile(path_gt+l)

            if os.path.exists(path_log+l[0:len(l)-4]+".txt"):
                check_file_log = True
                h2,wi2,list_walls_log,list_doors_log,list_windows_log,list_strain_log =  self.readFile(path_log+l[0:len(l)-4]+".txt")

                i1 = np.zeros((h1,wi1),dtype=np.uint8)
                for w1 in list_walls_gt:
                    p = np.array([[[w1[0], w1[1]],[w1[2], w1[3]],[w1[4], w1[5]],[w1[6], w1[7]]]], dtype=np.int32)
                    cv2.drawContours(i1, [p], -1, 1, -1)
                    #i1[w1[1]:w1[7]+1,w1[0]:w1[4]+1]=1

                i2 = np.zeros((h1,wi1),dtype=np.uint8)
                for w2 in list_walls_log:
                    p = np.array([[[w2[0], w2[1]],[w2[2], w2[3]],[w2[4], w2[5]],[w2[6], w2[7]]]], dtype=np.int32)
                    cv2.drawContours(i2, [p], -1, 1, -1)

                logic_and = i1*i2
                sum_i1 = np.sum(i1)
                sum_i2 = np.sum(i2)
                sum_logic = np.sum(logic_and)
                precision_walls = float(sum_logic)/float(sum_i2)
                recall_walls=float(sum_logic)/float(sum_i1)

                # #count_walls=0
                # for w1 in list_walls_gt:
                #     check_wall=False
                #     i1 = np.zeros((h1,wi1),dtype=np.uint8)
                #     i1[w1[1]:w1[7]+1,w1[0]:w1[4]+1]=1
                #     s = np.sum(i1)
                #
                #     for w2 in list_walls_log:
                #         i2 = np.zeros((h1,wi1),dtype=np.uint8)
                #         i2[w2[1]:w2[7]+1,w2[0]:w2[4]+1] = 1
                #         logic_and = i1*i2
                #         if float(s/2) < np.sum(logic_and):
                #             check_wall = True
                #     if check_wall:
                #         count_walls+=1
                #
                count_doors=0
                for w1 in list_doors_gt:
                    check_door=False
                    i1 = np.zeros((h1,wi1),dtype=np.uint8)
                    i1[w1[1]:w1[7]+1,w1[0]:w1[4]+1]=1
                    s = np.sum(i1)
                    for w2 in list_doors_log:
                        i2 = np.zeros((h1,wi1),dtype=np.uint8)
                        i2[w2[1]:w2[7]+1,w2[0]:w2[4]+1] = 1
                        logic_and = i1*i2
                        if float(s/2) < np.sum(logic_and):
                            check_door = True
                    if check_door:
                        count_doors+=1

                count_windows=0
                for w1 in list_windows_gt:
                    check_window=False
                    i1 = np.zeros((h1,wi1),dtype=np.uint8)
                    i1[w1[1]:w1[7]+1,w1[0]:w1[4]+1]=1
                    s = np.sum(i1)

                    for w2 in list_windows_log:

                        i2 = np.zeros((h1,wi1),dtype=np.uint8)
                        i2[w2[1]:w2[7]+1,w2[0]:w2[4]+1] = 1
                        logic_and = i1*i2
                        ss = np.sum(logic_and)
                        ss2 = np.sum(i1)
                        ss3 = np.sum(i2)
                        if float(s/2) < np.sum(logic_and):
                            check_window = True


                    if check_window:
                        count_windows+=1

                count_strain=0
                # for w1 in list_strain_gt:
                check_strain=False

                i1 = np.zeros((h1,wi1),dtype=np.uint8)
                if len(list_strain_gt)>0:
                    aaa = np.array(list_strain_gt, dtype=np.int32)
                    cv2.drawContours(i1,[aaa], -1, 255, -1)
                    s = np.sum(i1)
                    i2 = np.zeros((h1,wi1),dtype=np.uint8)
                    if len(list_strain_log)>0:
                        cv2.drawContours(i2, [np.array(list_strain_log, dtype=np.int32)] , -1, 255, -1)

                    logic_and = i1*i2
                    if float(s/2) < np.sum(logic_and):
                        check_strain = True
                else:
                    check_strain = True

                if check_strain:
                    count_strain+=1

                #precision_walls = float(count_walls)/len(list_walls_log)
                precision_doors = float(count_doors)/len(list_doors_log)
                precision_windows = float(count_windows)/len(list_windows_log)

                #recall_walls = float(count_walls)/len(list_walls_gt)
                recall_doors = float(count_doors)/len(list_doors_gt)
                recall_windows = float(count_windows)/len(list_windows_gt)



                t_precision_walls.append(precision_walls)
                t_precision_doors =  t_precision_doors + float(count_doors)
                t_precision_windows = t_precision_windows + float(count_windows)

                t_recall_walls.append(recall_walls)
                t_recall_doors = t_recall_doors + float(count_doors)
                t_recall_windows = t_recall_windows + float(count_windows)

                t_n_walls = t_n_walls +len(list_walls_log)
                t_n_doors = t_n_doors +len(list_doors_log)
                t_n_windows = t_n_windows +len(list_windows_log)


                t_n_walls_gt = t_n_walls_gt +len(list_walls_gt)
                t_n_doors_gt = t_n_doors_gt +len(list_doors_gt)
                t_n_windows_gt= t_n_windows_gt +len(list_windows_gt)



                x =  Decimal(precision_walls)
                s1 = round(x,4)
                x =  Decimal(recall_walls)
                s2 = round(x,4)
                file.write(l+":\n")
                file.write("Walls Precision: " +  str(s1) + "  Walls Recall: " + str(s2))
                x =  Decimal(precision_doors)
                s1 = round(x,4)
                x =  Decimal(recall_doors)
                s2 = round(x,4)
                file.write("  Doors Precision: " + str(s1) + "  Doors Recall: " + str(s2))
                x =  Decimal(precision_windows)
                s1 = round(x,4)
                x =  Decimal(recall_windows)
                s2 = round(x,4)
                file.write("  Windows Precision: " + str(s1) + "  Windows Precision: " + str(s2) +"\n")
                file.write("\n" )
                if len(list_strain_log)>0:
                    precision_strain = float(count_strain)/len(list_strain_log)
                    t_precision_strain = t_precision_strain + float(count_strain)
                    t_n_strain = t_n_strain +len(list_windows_log)
                    x =  Decimal(precision_strain)
                    s1 = round(x,4)
                else:
                    s1="Nan"

                if len(list_strain_gt)>0:
                    recall_strain = float(count_strain)/len(list_strain_gt)
                    t_recall_strain = t_recall_strain + float(count_strain)
                    t_n_strain_gt = t_n_strain_gt +len(list_strain_gt)
                    x =  Decimal(recall_strain)
                    s2 = round(x,4)
                else:
                    s2="Nan"

                file.write("  Strain Precision: " + str(s1) + "  Strain Precision: " + str(s2) +"\n")
                file.write("\n" )
        if check_file_log:
            #t_precision_walls = t_precision_walls / t_n_walls
            t_precision_doors =  t_precision_doors / t_n_doors
            t_precision_windows = t_precision_windows /t_n_windows
            #t_recall_walls = t_recall_walls / t_n_walls_gt
            t_recall_doors = t_recall_doors / t_n_doors_gt
            t_recall_windows = t_recall_windows / t_n_windows_gt


            file.write("Total:\n" )
            x =  Decimal(sum(t_precision_walls)/len(t_precision_walls))
            s1 = round(x,4)
            x =  Decimal(sum(t_recall_walls)/len(t_recall_walls))
            s2 = round(x,4)
            file.write(l+":\n")
            file.write("Walls Precision: " +  str(s1) + "  Walls Recall: " + str(s2))
            x =  Decimal(t_precision_doors)
            s1 = round(x,4)
            x =  Decimal(t_recall_doors)
            s2 = round(x,4)
            file.write("  Doors Precision: " + str(s1) + "  Doors Recall: " + str(s2))
            x =  Decimal(t_precision_windows)
            s1 = round(x,4)
            x =  Decimal(t_recall_windows)
            s2 = round(x,4)
            file.write("  Windows Precision: " + str(s1) + "  Windows Precision: " + str(s2))
            if t_n_strain>0:
                t_precision_strain = t_precision_strain /t_n_strain
                x =  Decimal(t_precision_strain)
                s1 = round(x,4)
            else:
                s1="Nan"
            if t_n_strain_gt>0:
                t_recall_strain = t_recall_strain / t_n_strain_gt
                x =  Decimal(t_recall_strain)
                s2 = round(x,4)
            else:
                s2="Nan"
            file.write("  Strain Precision: " + str(s1) + "  Strain Precision: " + str(s2) +"\n")
            file.write("\n" )
        else:
            file.write("no .txt files found")
        file.close()

    def readFile(self,path_log):

        list_walls_log = []
        list_doors_log = []
        list_windows_log = []
        list_strain_log = []
        file = open(path_log,"r")
        line=file.readline()
        split = line.split(";")
        split = map(float,split[0:2])
        split = map(int,split)
        h = split[0]
        w = split[1]
        line=file.readline()
        while(line!=""):
            if line=='WALLS\n':
                line=file.readline()
                while(line!="DOORS\n" and line!="WINDOWS\n" and line!="STRAIN\n" and line!=""):

                    s = line.split("\n")
                    split = s[0].split(";")
                    split = map(float,split)
                    split = map(int,split)
                    list_walls_log.append(split)
                    line=file.readline()
            if line=='DOORS\n':
                line=file.readline()
                while(line!="WINDOWS\n" and line!="STRAIN\n" and line!=""):
                    s = line.split("\n")
                    split = s[0].split(";")
                    split = map(float,split)
                    split = map(int,split)
                    list_doors_log.append(split)
                    line=file.readline()
            if line=='WINDOWS\n':
                line=file.readline()
                while(line!="" and line!="STRAIN\n"):

                    s = line.split("\n")
                    split = s[0].split(";")
                    split = map(float,split)
                    split = map(int,split)
                    list_windows_log.append(split)
                    line=file.readline()
            if line=='STRAIN\n':
                line=file.readline()
                while(line!="" ):
                    s1 = line.split("\n")
                    s2 =s1[0].split(" ")
                    s3 = s2[0].split("[")
                    s4 = s2[1].split("]")
                    list = [s3[2],s4[0]]
                    list = map(int, list)
                    list_strain_log.append([list])
                    line=file.readline()
                    # split = s[0].split(";")
                    # split = map(float,split)
                    # split = map(int,split)
                    # list_windows_log.append(split)
                    # line=file.readline()
        file.close()
        return h,w,list_walls_log,list_doors_log,list_windows_log,list_strain_log
if __name__ == '__main__':

    hwl1 = PrecisionAndRecall()