import os


from text_detection import text_detection

class main:
    def __init__(self):
        self.current_directory = os.path.abspath(os.path.join(os.getcwd(), '..'))
        self.text_detection_obj = text_detection(self.current_directory)
        self.text_detection_obj.compare_text_detection()


if __name__ == '__main__':
    main()